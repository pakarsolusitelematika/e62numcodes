#include "draw.hpp"
#include "domain_extended.hpp"
#include "util.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(Draw, DISABLED_Distribute1D) {
    RectangleDomain<Vec1d> domain(0, 1);
    domain.fillUniformBoundary(2);
    domain.fillUniformInterior(20);
    CircleDomain<Vec1d> c(0.5, 0.25);
    c.fillUniformBoundary(2);
    domain.subtract(c);
    CircleDomain<Vec1d> cc(1., 0.25);
    cc.fillUniform(10, 2);
    domain.add(cc);
    std::thread th([&] { draw2D(domain); });
    domain.relax(10, 1e-7, 0.3, 3, 5000);
    th.join();
}

TEST(Draw, DISABLED_Distribute2DRandom) {
    double rad = 1000;
    CircleDomain<Vec2d> domain(rad/2, rad/2);
    domain.fillUniform(4000, 400);
    std::mt19937 generator(get_seed());
    std::uniform_real_distribution<double> uniform(0, 1);
    for (int i = 0; i < 300; ++i) {
        double x = uniform(generator) * rad;
        double y = uniform(generator) * rad;
        double r = uniform(generator) * 10 + 10;
        CircleDomain<Vec2d> t({x, y}, r);
        t.fillUniform(12*r*r, 30 * r);
        domain.subtract(t, BOUNDARY_TYPE::SINGLE);
    }
    std::thread th([&] { draw2D(domain); });
    domain.relax(15, 1000.0, 1.0, 3, 5000);
    th.join();
}

TEST(Draw, DISABLED_Distribute3D) {
    RectangleDomain<Vec3d> domain(0, 1);
    domain.fillUniformBoundary({50, 50, 50});
    domain.fillUniformInterior(20);
    CircleDomain<Vec3d> c(0.5, 0.25);
    c.fillUniformBoundary(70);
    domain.subtract(c);
    CircleDomain<Vec3d> cc(1., 0.25);
    cc.fillUniform(300, 70);
    domain.add(cc);
    std::thread th([&] { draw2D(domain); });
    domain.relax(10, 1e-7, 0.3, 3, 5000);
    th.join();
}

// This is a good relax example.
TEST(Draw, DISABLED_ExampleDrawDomain) {
    /// [Domain relaxation and drawing]
    RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
    domain.fillUniformBoundary({50, 50});
    domain.fillUniformInterior({20, 20});
    CircleDomain<Vec2d> c({0.5, 0.5}, 0.25);
    c.fillUniformBoundary(70);
    domain.subtract(c);
    CircleDomain<Vec2d> cc({1, 1}, 0.25);
    cc.fillUniform(600, 70);
    domain.add(cc);
    // Draw in a separate thread to see results immediately
    std::thread th([&] { draw2D(domain); });
    domain.relax(10, 1e-2, 1.0, 3, 50000);
    th.join();
    /// [Domain relaxation and drawing]
}

TEST(Draw, DISABLED_ExampleInterference) {
    /// [Interference]
    RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
    domain.fillUniformBoundary({52, 52});
    domain.fillUniformInterior({50, 50});
    // Draw in a separate thread to see results immediately
    std::thread th([&] { draw2D(domain); });
    domain.relax(35, 3e-3, [] (const Vec2d& v) {
            double omega  = 62;
            return 2 + std::sin(omega*(v-Vec2d{0, 0.4}).norm()) +
                       std::sin(omega*(v-Vec2d{0, 0.6}).norm());
        }, 3, 50000);
    th.join();
    /// [Interference]
}

TEST(Draw, DISABLED_XYUneven) {
    RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
    domain.fillUniformBoundary({100, 100});
    domain.fillRandomInterior(4000);
    domain.fillUniformInterior({20, 20});
    CircleDomain<Vec2d> c({0.5, 0.5}, 0.25);
    c.fillUniformBoundary(100);
    domain.subtract(c);
    CircleDomain<Vec2d> cc({1, 1}, 0.25);
    cc.fillUniform(600, 100);
    domain.add(cc);
    // Draw in a separate thread to see results immediately
    std::thread th([&] { draw2D(domain); });
    domain.relax(15, 1e-2, [] (const Vec2d&) {
            return Vec2d({2, 1});
        }, 1, 50000);
    th.join();
}

TEST(Draw, DISABLED_IJS) {
    double r = 0.45;
    std::vector<Vec2d> coor = {{1, 0}, {0, 1}, {2, 1}, {0, 2}, {1, 2}, {1, 3}, {2, 3}};
    CircleDomain<Vec2d> ijs(coor[0], r);
    int noter = 300, zunaj = 70;
    ijs.fillUniform(noter, zunaj);
    for (size_t i = 1; i < coor.size(); ++i) {
        CircleDomain<Vec2d> cd(coor[i], r);
        cd.fillUniform(noter, zunaj);
        ijs.add(cd);
    }
    std::thread th([&] { draw2D(ijs); });
    ijs.relax(15, 0.5, 1.0, 3, 5000);
    th.join();
}

TEST(Draw, DISABLED_Example) {
    Range<Vec3d> pts;
    Range<double> cols;
    for (double x : linspace(-6, 6, 200)) {
        pts.push_back({x, std::sin(x), 1});
        cols.push_back(std::sin(x));
    }
    draw2D(pts, cols);
}

}  // namespace mm
