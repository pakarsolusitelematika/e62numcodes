#include "domain_extended.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(Domain, FindSupportAll) {
    RectangleDomain<Vec2d> d({0, 0}, {1, 1});
    d.fillUniform({10, 10}, {10, 10});
    d.findSupport(2, d.positions.size());  // should get everything
    int num = d.positions.size();
    Range<int> expected(num);
    for (int i = 0; i < num; ++i) expected[i] = i;
    for (auto& x : d.support) {
        sort(x.begin(), x.end());
        ASSERT_EQ(expected.size(), x.size());
        EXPECT_EQ(expected, x);
    }
    d.findSupport(d.positions.size());  // should get everything
    for (auto& x : d.support) {
        sort(x.begin(), x.end());
        ASSERT_EQ(expected.size(), x.size());
        EXPECT_EQ(expected, x);
    }
}

// see img/test_case_domain_support.png
TEST(Domain, FindSupportNoLimit) {
    RectangleDomain<Vec2d> d({0, 0}, {3, 3});
    d.fillUniform({2, 2}, {4, 4});
    d.findSupport(2, 10);  // should not be limiting
    int num = d.positions.size();
    std::map<Vec2d, Range<Vec2d>> expected({
        {{0, 0}, {{0, 0}, {0, 1}, {1, 0}, {1, 1}}},
        {{1, 0}, {{0, 0}, {0, 1}, {1, 0}, {1, 1}, {2, 0}, {2, 1}}},
        {{2, 0}, {{1, 0}, {1, 1}, {2, 0}, {2, 1}, {3, 0}, {3, 1}}},
        {{3, 0}, {{2, 0}, {2, 1}, {3, 0}, {3, 1}}},
        {{0, 1}, {{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}}},
        {{1, 1}, {{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}}},
        {{2, 1}, {{1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}, {3, 0}, {3, 1}, {3, 2}}},
        {{3, 1}, {{2, 0}, {2, 1}, {2, 2}, {3, 0}, {3, 1}, {3, 2}}},
        {{0, 2}, {{0, 1}, {0, 2}, {0, 3}, {1, 1}, {1, 2}, {1, 3}}},
        {{1, 2}, {{0, 1}, {0, 2}, {0, 3}, {1, 1}, {1, 2}, {1, 3}, {2, 1}, {2, 2}, {2, 3}}},
        {{2, 2}, {{1, 1}, {1, 2}, {1, 3}, {2, 1}, {2, 2}, {2, 3}, {3, 1}, {3, 2}, {3, 3}}},
        {{3, 2}, {{2, 1}, {2, 2}, {2, 3}, {3, 1}, {3, 2}, {3, 3}}},
        {{0, 3}, {{0, 2}, {0, 3}, {1, 2}, {1, 3}}},
        {{1, 3}, {{0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 2}, {2, 3}}},
        {{2, 3}, {{1, 2}, {1, 3}, {2, 2}, {2, 3}, {3, 2}, {3, 3}}},
        {{3, 3}, {{2, 2}, {2, 3}, {3, 2}, {3, 3}}},
    });
    for (int i = 0; i < num; ++i) {
        Vec2d cur = d.positions[i];
        Range<Vec2d> a = d.positions[d.support[i]];
        sort(a.begin(), a.end());
        EXPECT_EQ(expected[d.positions[i]], a);
        Range<double> dist;
        for (const auto& v : expected[cur]) dist.push_back((cur - v).squaredNorm());
        sort(dist.begin(), dist.end());
        EXPECT_EQ(dist, d.distances[i]);
    }
}

// see img/test_case_domain_support.png
TEST(Domain, FindSupportLimit) {
    RectangleDomain<Vec2d> d({0, 0}, {3, 3});
    d.fillUniform({2, 2}, {4, 4});
    int limit = 5;
    d.findSupport(2, 5);  // should be limiting
    std::map<Vec2d, Range<Vec2d>> expected({
        {{1, 1}, {{0, 1}, {1, 0}, {1, 1}, {1, 2}, {2, 1}}},
        {{2, 1}, {{1, 1}, {2, 0}, {2, 1}, {2, 2}, {3, 1}}},
        {{1, 2}, {{0, 2}, {1, 1}, {1, 2}, {1, 3}, {2, 2}}},
        {{2, 2}, {{1, 2}, {2, 1}, {2, 2}, {2, 3}, {3, 2}}},
    });
    for (const auto& x : d.support) {
        EXPECT_LE(x.size(), limit);
    }
    for (int i : d.types > 0) {  // internals are well defined
        Vec2d cur = d.positions[i];
        Range<Vec2d> a = d.positions[d.support[i]];
        sort(a.begin(), a.end());
        EXPECT_EQ(expected[cur], a);
        Range<double> dist;
        for (const auto& v : expected[cur]) dist.push_back((cur - v).squaredNorm());
        sort(dist.begin(), dist.end());
        EXPECT_EQ(dist, d.distances[i]);
    }
}

TEST(Domain, FindSupportDistancesDouble) {
    RectangleDomain<Vec1d> d(Vec1d(0.2), Vec1d(0.3));
    d.fillUniformWithStep(0.1, 0.1);
    d.findSupport(2);
    Range<Range<double>> expected = {{0, 0.1*0.1}, {0, 0.1*0.1}};
    ASSERT_EQ(expected.size(), d.distances.size());
    for (int i = 0; i < d.distances.size(); ++i) {
        ASSERT_EQ(expected[i].size(), d.distances[i].size());
    }
    for (int i = 0; i < d.distances.size(); ++i) {
        for (int j = 0; j < d.distances[i].size(); ++j) {
            EXPECT_DOUBLE_EQ(expected[i][j], d.distances[i][j]);
        }
    }
}

TEST(Domain, FindSupportForWhich) {
    RectangleDomain<Vec2d> d({0, 0}, {1, 1});
    d.positions = {1., 2., 3., 4., 5., 6., 7.};
    d.types = {-1, 2, -1, 2, 2, -1, -1};
    d.findSupport(1, d.types == -1);
    Range<Range<int>> expected = {{0}, {}, {2}, {}, {}, {5}, {6}};
    EXPECT_EQ(expected, d.support);
}

TEST(Domain, FindSupportSearchAmong) {
    RectangleDomain<Vec2d> d({0, 0}, {1, 1});
    d.positions = {1., 2., 3., 4., 5., 6., 7.};
    d.types = {-1, 2, -1, 2, 2, -1, -1};
    d.findSupport(1, d.types != 0, d.types > 0);
    Range<Range<int>> expected = {{1}, {1}, {1}, {3}, {4}, {4}, {4}};
    EXPECT_EQ(expected, d.support);
}

TEST(Domain, FindSupportForceSelf) {
    RectangleDomain<Vec2d> d({0, 0}, {1, 1});
    d.positions = {1., 2., 3., 4., 5., 6., 7.};
    d.types = {-1, 2, -1, 2, 2, -1, -1};
    d.findSupport(1, d.types != 0, d.types > 0);
    Range<Range<int>> expected = {{1}, {1}, {1}, {3}, {4}, {4}, {4}};
    EXPECT_EQ(expected, d.support);
    d.findSupport(1, d.types != 0, d.types > 0, true);
    expected = {{0}, {1}, {2}, {3}, {4}, {5}, {6}};
    EXPECT_EQ(expected, d.support);
    d.findSupport(2, d.types != 0, d.types > 0, true);
    expected = {{0, 1}, {1, 3}, {2, 1}, {3, 4}, {4, 3}, {5, 4}, {6, 4}};
    EXPECT_EQ(expected, d.support);
}

TEST(Domain, FindSupportDeath) {
    RectangleDomain<Vec2d> d({0, 0}, {1, 1});
    d.fillUniform(19, 2);
    EXPECT_DEATH(d.findSupport(0, 12), "Support radius must be greater than 0");
    EXPECT_DEATH(d.findSupport(-5, 12), "Support radius must be greater than 0");
    EXPECT_DEATH(d.findSupport(1, -30), "Support size must be greater than 0");
    EXPECT_DEATH(d.findSupport(1, 0), "Support size must be greater than 0");
    EXPECT_DEATH(d.findSupport(366), "Support size cannot exceed number of points.");
    EXPECT_DEATH(d.findSupport(65., 4, {1000}),
                 "One of the indexes for which to find the support is out of bounds.");
    EXPECT_DEATH(d.findSupport(65., 4, Range<int>{1}, Range<int>{5000}),
                 "One of the indexes for positions to search among is out of bounds.");
}

TEST(Domain, GetForce) {
    RectangleDomain<Vec1d> d(Vec1d(-1.), Vec1d(1.));
    d.fillUniform(19, 2);
    d.findSupport(0.15 * 0.15, 10);
    auto oneF = [](Vec1d) { return 1.0; };
    for (int i = 0; i < 19; ++i) ASSERT_NEAR(0, (d.getForce(1, oneF, 3))[0], 1e-11);
    EXPECT_GT(1, (d.getForce(19, oneF, 3))[0]);
    EXPECT_LT(-1, (d.getForce(20, oneF, 3))[0]);
}

TEST(Domain, GetForceDeath) {
    RectangleDomain<Vec1d> d(Vec1d(-1.), Vec1d(1.));
    d.fillUniform(19, 2);
    auto oneF = [](Vec1d) { return 1.0; };
    EXPECT_DEATH(d.getForce(-1, oneF, 3), "Support is not calculated for all points");
    d.findSupport(0.15 * 0.15, 10);
    EXPECT_DEATH(d.getForce(-1, oneF, 3),
                 "Cannot get force for point with invalid index");
    EXPECT_DEATH(d.getForce(21, oneF, 3),
                 "Cannot get force for point with invalid index");
    EXPECT_DEATH(d.getForce(1, oneF, 0),
                 "Exponent in force potential should be greater than 0.");
    EXPECT_DEATH(d.getForce(1, oneF, -10),
                 "Exponent in force potential should be greater than 0.");
}

TEST(Domain, DISABLED_Example) {
    /// [Domain creation and manipulation]
    // create domain
    RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
    domain.fillUniformBoundary({50, 50});
    domain.fillUniformInterior({20, 20});
    CircleDomain<Vec2d> c({0.5, 0.5}, 0.25);
    c.fillUniformBoundary(70);
    domain.subtract(c);
    CircleDomain<Vec2d> cc({1, 1}, 0.25);
    cc.fillUniform(300, 70);
    domain.add(cc);
    std::cout << domain << std::endl;

    // do stuff with domain
    domain.clearDuplicateNodes(1e-6);  // clear nodes that are too close
    domain.volume();  // calculates basic domain volume
    domain.surface_area();  // calculates basic domain surface
    Range<Vec2d> pos = domain.getBoundaryNodes();  // get all boundary nodes
    domain.clearInternalNodes();  // remove internal nodes
    Vec2d lower, upper;
    std::tie(lower, upper) = domain.getBBox();  // get bounding box
    auto domain_copy = Domain<Vec2d>::makeClone(domain);  // deep copy
    /// [Domain creation and manipulation]
}

TEST(Domain, Relax) {
    RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
    domain.fillUniformBoundary({50, 50});
    domain.fillUniformInterior({20, 20});
    CircleDomain<Vec2d> c({0.5, 0.5}, 0.25);
    c.fillUniformBoundary(70);
    domain.subtract(c);
    CircleDomain<Vec2d> cc({1, 1}, 0.25);
    cc.fillUniform(600, 70);
    domain.add(cc);

    Range<Vec2d> old_positions = domain.positions;

    /// allow only this nodes to move
    Range<int> interior = domain.types > 0;
    Range<int> move_only = interior[{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}];
    domain.relax(10, 1e-2, [] (const Vec2d& ) { return 1.0; }, 3, 50, move_only);

    Range<Vec2d> new_positions = domain.positions;

    std::vector<bool> should_be_moved(old_positions.size(), false);
    ASSERT_EQ(old_positions.size(), new_positions.size());
    for (int i : move_only) {  // relaxed nodes must move
        should_be_moved[i] = true;
        ASSERT_NE(old_positions[i], new_positions[i]);
    }
    for (int i = 0; i < new_positions.size(); ++i) {
        if (!should_be_moved[i]) {
            ASSERT_EQ(old_positions[i], new_positions[i]);
        }
    }
}

TEST(Domain, AddPointToBoundary1d) {
    CircleDomain<Vec1d> circ(2.2, 0.7);
    circ.fillUniform(3, 40);
    Range<Vec1d> positions = circ.positions;

    bool success;
    Vec1d added_point;
    Vec1d hint(1.5);
    std::tie(success, added_point) = circ.addPointToBoundary(hint);
    EXPECT_FALSE(success);
    EXPECT_EQ(positions, circ.positions);
}

TEST(Domain, AddPointToBoundary2d) {
    const double precision = 1e-5;

    /// [Add to boundary]
    CircleDomain<Vec2d> circ({2.2, 1.3}, 0.7);
    circ.fillUniform(3, 40);
    int size = circ.size();

    bool success;
    Vec2d added_point;
    Vec2d hint = {1.5, 0.8};
    std::tie(success, added_point) = circ.addPointToBoundary(hint);
    /// [Add to boundary]
    EXPECT_TRUE(success);
    EXPECT_NEAR(1.63704, added_point[0], precision);
    EXPECT_NEAR(0.88398, added_point[1], precision);
    EXPECT_EQ(circ.positions.back(), added_point);
    EXPECT_EQ(size+1, circ.size());

    hint = {2.8, 1.6};
    std::tie(success, added_point) = circ.addPointToBoundary(hint);
    EXPECT_TRUE(success);
    EXPECT_NEAR(2.82702, added_point[0], precision);
    EXPECT_NEAR(1.61119, added_point[1], precision);
    EXPECT_EQ(circ.positions.back(), added_point);
    EXPECT_EQ(size+2, circ.size());

    hint = {-2.8, 1.6};
    std::tie(success, added_point) = circ.addPointToBoundary(hint);
    EXPECT_FALSE(success);
    EXPECT_EQ(size+2, circ.size());
}

TEST(Domain, AddPointToBoundary3d) {
    CircleDomain<Vec3d> circ({0, 0, 0}, 1);
    circ.fillUniform(3, 150);
    int size = circ.size();
    double precision = 1e-3;

    bool success;
    Vec3d added_point;

    Vec3d hint = {0.8, 0.01, -0.01};
    std::tie(success, added_point) = circ.addPointToBoundary(hint, NODE_TYPE::BOUNDARY, precision);
    EXPECT_TRUE(success);
    EXPECT_NEAR(0.99947, added_point[0], precision);
    EXPECT_NEAR(0.0117135, added_point[1], precision);
    EXPECT_NEAR(-0.0105922, added_point[2], precision);
    EXPECT_NEAR(added_point.squaredNorm(), 1, precision);
    EXPECT_EQ(circ.positions.back(), added_point);
    EXPECT_EQ(size+1, circ.size());
}


}  // namespace mm
