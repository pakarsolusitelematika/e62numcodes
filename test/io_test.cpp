#include "io.hpp"
#include "common.hpp"
#include "types.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(IO, XMLRead) {
    XMLloader xml("test/data/test_xml.xml");
    EXPECT_EQ("low level test", xml.textRead({"mls", "weight"}));
    EXPECT_EQ("level 3 test", xml.textRead({"test", "level1", "level2", "level3"}));
    EXPECT_EQ(2.3, static_cast<double>(xml.getAttribute(
    {"test", "level1", "level2", "level3"}, "att")));
    EXPECT_EQ(2, static_cast<int>(xml.getAttribute(
    {"test", "level1", "level2", "level3"}, "att")));
    EXPECT_TRUE(static_cast<double>(xml.getAttribute(
    {"test", "level1", "level2", "level31"}, "bool")));
}

#ifndef __MIC__  // Disable HDF5 on MIC
TEST(IO, HDF5Read) {
    // open file
    std::string file_name = "test/data/test_hdf5.h5";
    HDF5IO reader;
    reader.openFile(file_name);
    // read parameters in /what
    reader.openFolder("/what");
    std::string read_str = reader.getStringAttribute("object");
    EXPECT_EQ("PVOL", read_str);
    std::string date_str = reader.getStringAttribute("date");
    EXPECT_EQ("20150501", date_str);
    std::string time_str = reader.getStringAttribute("time");
    EXPECT_EQ("000000", time_str);
    std::string source_str = reader.getStringAttribute("source");
    EXPECT_EQ("WMO:14024,RAD:SI41,PLC:Lisca,NOD:silis", source_str);

    reader.openFolder("/where");
    double height = reader.getDoubleAttribute("height");
    EXPECT_DOUBLE_EQ(950.0, height);
    double lat = reader.getDoubleAttribute("lat");
    EXPECT_DOUBLE_EQ(46.06776997447014, lat);
    double lon = reader.getDoubleAttribute("lon");
    EXPECT_DOUBLE_EQ(15.28489999473095, lon);

    reader.openFolder("/dataset1/how");
    double avgpwr = reader.getDoubleAttribute("avgpwr");
    EXPECT_DOUBLE_EQ(135.0, avgpwr);

    reader.openFolder("/dataset2/how");
    double avgpwr2 = reader.getDoubleAttribute("avgpwr");
    EXPECT_DOUBLE_EQ(135.0, avgpwr2);

    reader.openFolder("/dataset1/data1/what");
    std::string quantity = reader.getStringAttribute("quantity");
    EXPECT_EQ("DBZH", quantity);
    reader.openFolder("/dataset1/data1");
    auto tmp = reader.getBScope("data");
    reader.openFolder("/dataset1/data2/what");
    quantity = reader.getStringAttribute("quantity");
    EXPECT_EQ("VRAD", quantity);
}

TEST(IO, HDF5Write) {
    std::string file_name = "test/data/test_hdf5_write.h5";
    HDF5IO reader(file_name);
    reader.openFolder("test");

    // INT
    reader.setIntAttribute("int_attribute", 5);
    EXPECT_EQ(5, reader.getIntAttribute("int_attribute"));
    reader.setIntAttribute("int_attribute", 10);
    EXPECT_EQ(10, reader.getIntAttribute("int_attribute"));
    // DOUBLE
    reader.setDoubleAttribute("double_attribute", 5.1);
    EXPECT_DOUBLE_EQ(5.1, reader.getDoubleAttribute("double_attribute"));
    reader.setDoubleAttribute("double_attribute", 10.1245);
    EXPECT_DOUBLE_EQ(10.1245, reader.getDoubleAttribute("double_attribute"));
    // FLOAT
    reader.setFloatAttribute("float_attribute", 5.1);
    EXPECT_FLOAT_EQ(5.1, reader.getFloatAttribute("float_attribute"));
    reader.setFloatAttribute("float_attribute", 10.1245);
    EXPECT_FLOAT_EQ(10.1245, reader.getFloatAttribute("float_attribute"));
    // STRING
    reader.setStringAttribute("string_attribute", "Test string");
    EXPECT_EQ("Test string", reader.getStringAttribute("string_attribute"));
    reader.setStringAttribute("string_attribute", "Second test string 1234.");
    EXPECT_EQ("Second test string 1234.", reader.getStringAttribute("string_attribute"));
    std::remove(file_name.c_str());
}

TEST(IO, HDF5WriteArray) {
    std::string file_name = "test/data/test_hdf5_write.h5";
    HDF5IO reader(file_name, HDF5IO::DESTROY);
    reader.openFolder("test");

    std::vector<double> ret, data = {1.0, 10.234, 2.4};
    reader.setDoubleArray("vector_doubles", data);
    ret = reader.getDoubleArray("vector_doubles");
    ASSERT_EQ(data.size(), ret.size());
    for (size_t i = 0; i < ret.size(); i++) {
        EXPECT_DOUBLE_EQ(data[i], ret[i]);
    }

    data = {5.0, 1234.234, 556};
    reader.setDoubleArray("vector_doubles", data);
    ret = reader.getDoubleArray("vector_doubles");
    ASSERT_EQ(data.size(), ret.size());
    for (size_t i = 0; i < ret.size(); i++) {
        EXPECT_DOUBLE_EQ(data[i], ret[i]);
    }

    std::vector<std::vector<double>> data2d = {
        {0.12, 3.14157, 12.7}, {2.12, 2.6, 18.65}, {3.12, 4.6, 99.65}, {5, 5, 5}};
    reader.setDouble2DArray("vec_vec_double", data2d);
    std::vector<std::vector<double>> ret2d = reader.getDouble2DArray("vec_vec_double");
    ASSERT_EQ(data2d.size(), ret2d.size());
    for (size_t i = 0; i < data2d.size(); i++) {
        ASSERT_EQ(data2d[i].size(), ret2d[i].size());
        for (size_t j = 0; j < data2d[i].size(); j++) {
            EXPECT_EQ(data2d[i][j], ret2d[i][j]);
        }
    }

    std::remove(file_name.c_str());
}

TEST(IO, HDF5WriteArrayFloat) {
    std::string file_name = "test/data/test_hdf5_write.h5";
    HDF5IO reader(file_name, HDF5IO::DESTROY);
    reader.openFolder("test");

    std::vector<float> ret;
    std::vector<float> data = {1.0, 10.234, 2.4};
    reader.setFloatArray("vector_floats", data);
    ret = reader.getFloatArray("vector_floats");
    ASSERT_EQ(data.size(), ret.size());
    for (size_t i = 0; i < ret.size(); i++) {
        EXPECT_EQ(data[i], ret[i]);
    }

    data = {5.0, 1234.234, 556};
    reader.setFloatArray("vector_floats", data);
    ret = reader.getFloatArray("vector_floats");
    ASSERT_EQ(data.size(), ret.size());
    for (size_t i = 0; i < ret.size(); i++) {
        EXPECT_EQ(data[i], ret[i]);
    }

    std::vector<std::vector<float>> data2d = {
        {0.12, 3.14157, 12.7}, {2.12, 2.6, 18.65}, {3.12, 4.6, 99.65}, {5, 5, 5}};
    reader.setFloat2DArray("vec_vec_float", data2d);
    std::vector<std::vector<float>> ret2d = reader.getFloat2DArray("vec_vec_float");
    ASSERT_EQ(data2d.size(), ret2d.size());
    for (size_t i = 0; i < data2d.size(); i++) {
        ASSERT_EQ(data2d[i].size(), ret2d[i].size());
        for (size_t j = 0; j < data2d[i].size(); j++) {
            EXPECT_EQ(data2d[i][j], ret2d[i][j]);
        }
    }

    std::remove(file_name.c_str());
}

TEST(IO, HDF5WriteArrayInt) {
    std::string file_name = "test/data/test_hdf5_write.h5";
    HDF5IO reader(file_name);
    reader.openFolder("test");

    std::vector<int> ret;
    std::vector<int> data = {1, 10, 2};
    reader.setIntArray("vector_ints", data);
    ret = reader.getIntArray("vector_ints");
    ASSERT_EQ(data.size(), ret.size());
    for (size_t i = 0; i < ret.size(); i++) {
        EXPECT_EQ(data[i], ret[i]);
    }

    std::vector<std::vector<int>> data2d = {
        {12, 3, 12}, {2, 2, 18}, {3, 4, 99}, {5, 5, 5}};
    reader.setInt2DArray("vec_vec_int", data2d);
    std::vector<std::vector<int>> ret2d = reader.getInt2DArray("vec_vec_int");
    ASSERT_EQ(data2d.size(), ret2d.size());
    for (size_t i = 0; i < data2d.size(); i++) {
        ASSERT_EQ(data2d[i].size(), ret2d[i].size());
        for (size_t j = 0; j < data2d[i].size(); j++) {
            EXPECT_EQ(data2d[i][j], ret2d[i][j]);
        }
    }
    std::remove(file_name.c_str());
}


TEST(IO, HDF5ArrayWithVec) {
    std::string file_name = "test/data/test_hdf5_write.h5";
    HDF5IO reader(file_name, HDF5IO::DESTROY);
    reader.openFolder("test");

    Range<Vec<double, 3>> vector_data;
    for (auto i = 1; i < 100; ++i) vector_data.push_back(Vec<double, 3>{0.1, 0.2, 0.3});
    reader.setDouble2DArray("vector_data", vector_data, {10, 3});
    // note Chunk size {10, 3}--check documentation for more

    std::vector<std::vector<double>> tt = reader.getDouble2DArray("vector_data");
    for (auto i = 0; i < vector_data.size(); ++i) {
        for (auto j = 0; j < vector_data[0].size(); ++j)
            EXPECT_DOUBLE_EQ(tt[i][j], vector_data[i][j]);
    }

    std::remove(file_name.c_str());
}
#endif  // ifndef __MIC__

}  // namespace mm
