#include "mlsm_operators.hpp"

#include "domain_extended.hpp"
#include "Eigen/Sparse"
// #include "draw.hpp"  // uncomment if you need drawing

#include "gtest/gtest.h"

namespace mm {

/**
 * @file mlsm_operators_test.cpp
 * @brief MLSM operator Tests
 */

// [prep test field] -- test function sin(sum(a_i * x_i))
double argfun(const Vec3d& p, const Range<double> & a) {
    double ret = 0;
    for (int i = 0; i < Vec3d::size(); ++i) ret += p[i] * a[i];
    return ret;
}
// [support function :: scalar test function]
template <typename domain_engine>
Range<double> f_scalar(const domain_engine& domain, const Range<double> & a) {
    Range<double> field(domain.size(), 0);
    std::vector<int> all(domain.positions.size());
    std::iota(std::begin(all), std::end(all), 0);

    for (auto& c : all) field[c] = std::sin(argfun(domain.positions[c], a));
    return field;
}
// [support function :: vector test function]
template <typename domain_engine>
Range<Vec3d> f_vec(const domain_engine& domain, const Range<double> & a) {
    Range<Vec3d> field2(domain.size(), 0.);
    std::vector<int> all(domain.positions.size());
    std::iota(std::begin(all), std::end(all), 0);
    for (auto& c : all)
        for (int i = 0; i < Vec3d::size(); ++i) {
            Vec3d tmp;
            tmp[i] = (i + 1) * std::sin(argfun(domain.positions[c], a));
            field2[c] = tmp;
        }
    return field2;
}
// [support function :: find point of interest]
int point_of_interest(const Range<Vec3d>& positions) {
    KDTree<Vec3d> kd3(positions);  // test point (0.45, 0.45...)
    Range<double> distances; Range<int> closest;
    std::tie(closest, distances) = kd3.query(0.45, 1);
    return closest[0];
}

TEST(MLSM_Operators, explicit3DLaplace) {
    int supp_size = 7;
    int basis_size = 7;

    Range<double> a({0.5, 0.6, 1.2});

    RectangleDomain<Vec3d> domain(Vec3d(0.), Vec3d(1.));  // domain definition
    domain.fillUniformInteriorWithStep(0.1);
    domain.findSupport(supp_size);

    // calling support functions defined in mlsm_operators_test.cpp
    int point = point_of_interest(domain.positions);
    Range<double> field_scalar = f_scalar(domain, a);  // calls support function to generate values
    Range<Vec3d>  field_vector = f_vec(domain, a);     // calls support function to generate values

    EngineMLS<Vec3d, Gaussians, Monomials> mls({30 * domain.characteristicDistance(), basis_size},
                                               domain.positions[domain.support[0]], 1);
    auto op = make_mlsm(domain, mls, {point});

    double lap_mlsm = op.lap(field_scalar, point);
    double lap = 0;
    for (int i = 0; i < Vec3d::size(); ++i) lap += a[i] * a[i];
    lap = -lap * std::sin(argfun(domain.positions[point], a));

    EXPECT_NEAR(lap_mlsm, lap, 1e-3);
}

TEST(MLSM_Operators, explicit3DGradient) {
    int supp_size = 7;
    int basis_size = 7;

    Range<double> a({0.5, 0.6, 1.2});

    RectangleDomain<Vec3d> domain(Vec3d(0.), Vec3d(1.));  // domain definition
    domain.fillUniformInteriorWithStep(0.1);
    domain.findSupport(supp_size);

    // calling support functions defined in mlsm_operators_test.cpp
    Range<double> field = f_scalar(domain, a);  // scalar test field for testing laplace and grad
    Range<Vec3d>  field2 = f_vec(domain, a);  // vector test field for testing divergence
    int point = point_of_interest(domain.positions);

    EngineMLS<Vec3d, Gaussians, Monomials> mls({30 * domain.characteristicDistance(), basis_size},
                                               domain.positions[domain.support[0]], 1);
    auto op = make_mlsm(domain, mls, {point});

    // [Test gradient operator]
    Vec3d grad_mlsm = op.grad(field, point);
    Vec3d grad;
    for (int i = 0; i < Vec3d::size(); ++i)
        grad[i] = a[i] * std::cos(argfun(domain.positions[point], a));

    EXPECT_NEAR(grad_mlsm.norm(), grad.norm(), 1e-3);
}
TEST(MLSM_Operators, explicit3DDivergence) {
    int supp_size = 7;
    int basis_size = 7;
    Range<double> a({0.5, 0.6, 1.2});

    RectangleDomain<Vec3d> domain(Vec3d(0.), Vec3d(1.));  // domain definition
    domain.fillUniformInteriorWithStep(0.1);
    domain.findSupport(supp_size);

    // calling support functions defined in mlsm_operators_test.cpp
    Range<double> field = f_scalar(domain, a);  // scalar test field for testing laplace and grad
    Range<Vec3d>  field2 = f_vec(domain, a);    // vector test field for testing divergence
    int point = point_of_interest(domain.positions);

    EngineMLS<Vec3d, Gaussians, Monomials> mls({30 * domain.characteristicDistance(), basis_size},
                                               domain.positions[domain.support[0]], 1);
    auto op = make_mlsm(domain, mls, {point});

    // [Test divergence operator]
    double div_mlsm = op.div(field2, point);
    double div = 0;
    for (int i = 0; i < Vec3d::size(); ++i)
        div += (i + 1) * a[i] * std::cos(argfun(domain.positions[point], a));

    EXPECT_NEAR(div_mlsm, div, 1e-3);
}
TEST(MLSM_Operators, explicit3DLaplaceOfVector) {
    int supp_size = 7;
    int basis_size = 7;

    Range<double> a({0.5, 0.6, 1.2});

    RectangleDomain<Vec3d> domain(Vec3d(0.), Vec3d(1.));  // domain definition
    domain.fillUniformInteriorWithStep(0.1);
    domain.findSupport(supp_size);
    int point = point_of_interest(domain.positions);

    EngineMLS<Vec3d, Gaussians, Monomials> mls({30 * domain.characteristicDistance(), basis_size},
                                               domain.positions[domain.support[0]], 1);
    auto op = make_mlsm(domain, mls, {point});

    // calling support functions defined in mlsm_operators_test.cpp
    Range<double> field = f_scalar(domain, a);  // scalar test field for testing laplace and grad
    Range<Vec3d>  field2 = f_vec(domain, a);  // vector test field for testing divergence

    Vec3d lap_mlsm = op.lap(field2, point);

    Vec3d lap = 0.0;

    for (int j = 0; j < Vec3d::size(); ++j) {
        for (int i = 0; i < Vec3d::size(); ++i) lap[j] += a[i] * a[i];
        lap[j] = -lap[j] * (j + 1) * std::sin(argfun(domain.positions[point], a));
    }
    Vec3d error = lap_mlsm - lap;
    EXPECT_NEAR(error.maxCoeff(), -0.000438112, 1e-5);
}

TEST(MLSM_Operators, DISABLED_MLSMoperators_example) {
    /// [MLSM_example]
    int supp_size = 7;
    int basis_size = 7;

    Range<double> a({0.5, 0.6, 1.2});
    typedef Eigen::Matrix<double, 3, 3> matrix;
    RectangleDomain<Vec3d> domain(Vec3d(0.), Vec3d(1.));  // domain definition
    domain.fillUniformInteriorWithStep(0.1);
    domain.findSupport(supp_size);

    // returns index to point p=(0.45, 0.45) use any index to try how this works
    int point = point_of_interest(domain.positions);


    EngineMLS<Vec3d, Gaussians, Monomials> mls({30 * domain.characteristicDistance(), basis_size},
                                               domain.positions[domain.support[0]], 1);
    auto op = make_mlsm(domain, mls, {point});

    // scalar test field for testing laplace and grad
    Range<double> field_scalar = f_scalar(domain, a);
    // vector test field for testing divergence
    Range<Vec3d>  field_vector = f_vec(domain, a);

    double lap_mlsm = op.lap(field_scalar, point);
    Vec3d grad_mlsm = op.grad(field_scalar, point);
    double div_mlsm = op.div(field_vector, point);
    Vec3d lap_vec_mlsm = op.lap(field_vector, point);
    matrix grad_vec_mlsm = op.grad(field_vector, point);
    std::cout << op << std::endl;
    /// [MLSM_example]
    // do something to avoid annoying warnings
    lap_mlsm = 0 * lap_mlsm;
    grad_mlsm = 0 * grad_mlsm;
    div_mlsm = 0 * div_mlsm;
    lap_vec_mlsm = 0 * lap_vec_mlsm;
    div_mlsm = grad_vec_mlsm.norm();
}

TEST(MLSM_Operators, explicit3DGradientOfVector) {
    int supp_size = 7;
    int basis_size = 7;
    Range<double> a({0.5, 0.6, 1.2});

    RectangleDomain<Vec3d> domain(Vec3d(0.), Vec3d(1.));  // domain definition
    domain.fillUniformInteriorWithStep(0.1);
    domain.findSupport(supp_size);

    int point = point_of_interest(domain.positions);

    EngineMLS<Vec3d, Gaussians, Monomials> mls({30 * domain.characteristicDistance(), basis_size},
                                               domain.positions[domain.support[0]], 1);
    auto op = make_mlsm(domain, mls, {point});

    Range<Vec3d>  field2 = f_vec(domain, a);  // vector test field for testing divergence

    // Test gradient operator
    Eigen::Matrix<double, Vec3d::dimension, Vec3d::dimension> grad_mlsm = op.grad(field2, point);
    Eigen::Matrix<double, Vec3d::dimension, Vec3d::dimension> grad;
    for (int j = 0; j < Vec3d::size(); ++j) {
        for (int i = 0; i < Vec3d::size(); ++i)
            grad(j, i) = a[i] * std::cos(argfun(domain.positions[point], a)) * (j + 1);
    }
    Eigen::Matrix<double, Vec3d::dimension, Vec3d::dimension> error = grad - grad_mlsm;
    EXPECT_NEAR(error.maxCoeff(), 0.00167218, 1e-5);
}

TEST(MLSM_Operators, implicit1DLaplaceDense) {
    /// [Implicit laplace 1d example]
    // Solve 1-D boundary value problem
    // u''(x) = x; u(0) = 1, u(1) = -2
    // with solution u(x) = 1/6 (6 - 19 x + x^3)
    RectangleDomain<Vec1d> domain(Vec1d{0}, Vec1d{1});
    domain.fillUniformWithStep(0.1, Vec1d{1});
    EngineMLS<Vec1d, Monomials, Gaussians> mls(5, domain.positions, 1);
    int N = domain.positions.size();
    domain.findSupport(5);
    Eigen::MatrixXd M = Eigen::MatrixXd::Zero(N, N);
    auto op = make_mlsm<mlsm::lap>(domain, mls, domain.types == INTERNAL);
    Eigen::VectorXd rhs(N);
    for (int i : (domain.types == INTERNAL)) {
        op.lap(M, i, 1.0);  // laplace in interior
        rhs(i) = domain.positions[i][0];
    }
    for (int i : (domain.types == BOUNDARY)) {
        M(i, i) = 1;  // boundary conditions on the boundary
        if (domain.positions[i][0] == 0) rhs(i) = 1;
        else rhs[i] = -2;
    }
    Eigen::VectorXd sol = M.lu().solve(rhs);
    std::function<double(double)> analytic = [](double x) { return 1/6.0 * (6 - 19 * x + x*x*x); };
    for (int i = 0; i < domain.size(); ++i) {
        EXPECT_NEAR(sol[i], analytic(domain.positions[i][0]), 1e-14);
    }
    /// [Implicit laplace 1d example]
}

TEST(MLSM_Operators, implicit2DLaplaceSparse) {
    // Solve boundary value problem
    // laplace u = 0, u on edge = 1
    // with solution u == 1
    RectangleDomain<Vec2d> domain(Vec2d(0.), Vec2d(1.));
    domain.fillUniformWithStep(Vec2d(0.2), Vec2d(0.2));
    EngineMLS<Vec2d, Monomials, Monomials> mls(3, domain.positions, 1);
    int N = domain.positions.size();
    const int support_size = 9;
    domain.findSupport(support_size);
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    M.reserve(Range<int>(N, support_size));
    auto op = make_mlsm(domain, mls, domain.types == INTERNAL);
    Eigen::VectorXd rhs(N);
    for (int i : (domain.types == INTERNAL)) {
        op.lap(M, i, 1);  // laplace in interior
        rhs(i) = 0;
    }
    for (int i : (domain.types == BOUNDARY)) {
        M.coeffRef(i, i) = 1;  // boundary conditions on the boundary
        rhs[i] = 1;
    }
    Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor>> solver;
    solver.compute(M);
    Eigen::VectorXd sol = solver.solve(rhs);
    for (int i = 0; i < domain.size(); ++i) {
        EXPECT_NEAR(sol[i], 1.0, 1e-14);
    }
}

TEST(MLSM_Operators, implicit2dLaplaceOfvector) {
    /// [Implicit vector laplace 2d example]
    // Solve problem for F unknown vector field:
    // (Laplace F)(x, y) = [4 (1 - 2 pi^2 y^2) sin(2 pi x); 4 (1 - x - x y) e^(2 y)]
    // on [0, 1] x [0, 1] with BC:
    // F(x, 0) = [0, 1]
    // F(x, 1) = [2 sin(2 pi x), (1 - x) e^2]
    // F(0, y) = [0, e^(2 y)]
    // F(1, y) = [0, (1 - y) e^(2 y)]
    // with solution:
    // F(x, y) = [2 sin(2 pi x) y^2,  (1 - x y) e^(2 y)]
    std::function<Vec2d(Vec2d)> analytical = [] (const Vec2d& p) {
        return Vec2d({2*std::sin(2*M_PI*p[0])*p[1]*p[1],  (1 - p[0]*p[1]) * std::exp(2*p[1])});
    };
    // Prepare domain
    RectangleDomain<Vec2d> domain(Vec2d(0.), Vec2d(1.));
    domain.fillUniformWithStep(0.025, 0.025);
    int N = domain.size();
    int support_size = 9;
    domain.findSupport(support_size);
    // Prepare operators and matrix
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(3, domain.positions, std::sqrt(15.0 / N));
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(2*N, 2*N);
    M.reserve(Range<int>(2*N, 2*support_size));
    auto op = make_mlsm(domain, mls, domain.types == INTERNAL);
    Eigen::VectorXd rhs(2*N);
    // Set equation on interior
    for (int i : (domain.types == INTERNAL)) {
        op.lapvec(M, i, 1.0);  // laplace in interior
        double x = domain.positions[i][0];
        double y = domain.positions[i][1];
        rhs(i) = 4 * (1 - 2 * M_PI*M_PI * y*y) * std::sin(2 * M_PI * x);
        rhs(i+N) = 4 * (1 - x - x * y) * std::exp(2 * y);
    }
    // Set boundary conditions
    for (int i : (domain.types == BOUNDARY)) {
        M.coeffRef(i, i) = 1;  // boundary conditions on the boundary
        M.coeffRef(i+N, i+N) = 1;  // boundary conditions on the boundary
        double x = domain.positions[i][0];
        double y = domain.positions[i][1];
        if (x == 0) {
            rhs(i) = 0;
            rhs(i+N) = std::exp(2*y);
        } else if (x == 1) {
            rhs(i) = 0;
            rhs(i+N) = (1-y) * std::exp(2*y);
        } else if (y == 0) {
            rhs(i) = 0;
            rhs(i+N) = 1;
        } else if (y == 1) {
            rhs(i) = 2 * std::sin(2 * M_PI * x);
            rhs(i+N) = (1 - x) * std::exp(2);
        } else {
            assert(!"Should not be here.");
        }
    }
    // Sparse solve
    Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor>> solver;
    solver.compute(M);
    Eigen::VectorXd sol = solver.solve(rhs);
    for (int i = 0; i < N; ++i) {
        Vec2d correct = analytical(domain.positions[i]);
        EXPECT_NEAR(sol[i], correct[0], 1e-3);
        EXPECT_NEAR(sol[N+i], correct[1], 1e-3);
    }
    /// [Implicit vector laplace 2d example]
}



TEST(MLSM_Operators, implicit2dGradOfScalar) {
    /// [Implicit Grad example]
    // Solve problem for S unknown scalar field:
    // grad S . v + laplace S = phi
    // grad S . (y, x) + laplace S = 4 + 4xy + e^x (2x cos(2y) + (y-3) sin(2y))
    // on [0, 1] x [0, 1] with BC:
    // S(x, 0) = x^2
    // S(x, 1) = 1 + x^2 + sin(2) e^x
    // S(0, y) = y^2 + sin(2y)
    // S(1, y) = 1 + y^2 + e * sin(2y)
    // with solution:
    // S(x, y) = e^x sin(2y) + x^2 + y^2
    std::function<Vec2d(Vec2d)> analytical = [] (const Vec2d& p) {
        return std::exp(p[0]) * std::sin(2*p[1]) + p[0]*p[0] + p[1]*p[1];
    };
    // Prepare domain
    RectangleDomain<Vec2d> domain(0., 1.);
    domain.fillUniformWithStep(0.05, 0.05);
    int N = domain.size();
    int support_size = 9;
    domain.findSupport(support_size);
    // Prepare operators and matrix
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(3, domain.positions, 15.0 / N);
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    M.reserve(Range<int>(N, support_size));
    auto op = make_mlsm(domain, mls, domain.types == INTERNAL);
    Range<Vec2d> v(N);
    for (int i = 0; i < N; ++i) {
        v[i] = Vec2d({domain.positions[i][1], domain.positions[i][0]});
    }
    Eigen::VectorXd rhs(N);
    // Set equation on interior
    for (int i : (domain.types == INTERNAL)) {
        op.grad(M, i, v);  // grad in interior
        op.lap(M, i, 1.0);  // + laplace in interior
        double x = domain.positions[i][0];
        double y = domain.positions[i][1];
        rhs(i) = 4+4*x*y + std::exp(x) * (2*x * std::cos(2*y) + (y-3)*std::sin(2*y));
    }
    // Set boundary conditions
    for (int i : (domain.types == BOUNDARY)) {
        M.coeffRef(i, i) = 1;  // boundary conditions on the boundary
        double x = domain.positions[i][0];
        double y = domain.positions[i][1];
        if (x == 0) {
            rhs(i) = y*y + std::sin(2*y);
        } else if (x == 1) {
            rhs(i) = 1 + y*y + std::exp(1) * std::sin(2*y);
        } else if (y == 0) {
            rhs(i) = x*x;
        } else if (y == 1) {
            rhs(i) = 1 + x*x + std::sin(2) * std::exp(x);
        } else {
            assert(!"Should not be here.");
        }
    }

    // Sparse solve
    Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor>> solver;
    solver.compute(M);
    Eigen::VectorXd sol = solver.solve(rhs);
    for (int i = 0; i < N; ++i) {
        Vec2d correct = analytical(domain.positions[i]);
        ASSERT_NEAR(sol[i], correct[0], 0.5e-3);
    }
    /// [Implicit Grad example]
}

TEST(MLSM_Operators, implicit2dGradOfVector) {
    /// [Implicit Gradvec example]
    // Solve problem for u unknown scalar field:
    // grad u . v + 1/2 laplace u = f
    // grad u . (y, x) + 1/2 laplace u = (x^3+y+2 x y^2, x^2+(y-1) y)
    // on [0, 1] x [0, 1] with BC:
    // u(x, 0) = (0, -x)
    // u(x, 1) = (x*x, 0)
    // u(0, y) = (0, 0)
    // u(1, y) = (y, y-1)
    // with solution:
    // u(x, y) = (x^2 y, yx - x)
    std::function<Vec2d(Vec2d)> analytical = [] (const Vec2d& p) {
        return Vec2d({p[0]*p[0]*p[1], (p[1]-1)*p[0]});
    };
    // Prepare domain
    RectangleDomain<Vec2d> domain(0., 1.);
    domain.fillUniformWithStep(0.05, 0.05);
    int N = domain.size();
    int support_size = 9;
    domain.findSupport(support_size);
    // Prepare operators and matrix
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(3, domain.positions, 15.0 / N);
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(2*N, 2*N);
    M.reserve(Range<int>(2*N, support_size));
    auto op = make_mlsm(domain, mls, domain.types == INTERNAL);
    Range<Vec2d> v(N);
    for (int i = 0; i < N; ++i) {
        v[i] = Vec2d({domain.positions[i][1], domain.positions[i][0]});
    }
    Eigen::VectorXd rhs(2*N);
    // Set equation on interior
    for (int i : (domain.types == INTERNAL)) {
        op.gradvec(M, i, v);  // grad in interior
        op.lapvec(M, i, 0.5);  // + laplace in interior
        double x = domain.positions[i][0];
        double y = domain.positions[i][1];
        rhs(i) = x*x*x + y + 2*x*y*y;
        rhs(i+N) = x*x + (y-1)*y;
    }
    // Set boundary conditions
    for (int i : (domain.types == BOUNDARY)) {
        M.coeffRef(i, i) = 1;  // boundary conditions on the boundary
        M.coeffRef(i+N, i+N) = 1;  // boundary conditions on the boundary
        double x = domain.positions[i][0];
        double y = domain.positions[i][1];
        if (x == 0) {
            rhs(i) = 0;
            rhs(i+N) = 0;
        } else if (x == 1) {
            rhs(i) = y;
            rhs(i+N) = y-1;
        } else if (y == 0) {
            rhs(i) = 0;
            rhs(i+N) = -x;
        } else if (y == 1) {
            rhs(i) = x*x;
            rhs(i+N) = 0;
        } else {
            assert(!"Should not be here.");
        }
    }

    // Sparse solve
    Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor>> solver;
    solver.compute(M);
    Eigen::VectorXd sol = solver.solve(rhs);
    for (int i = 0; i < N; ++i) {
        Vec2d correct = analytical(domain.positions[i]);
        ASSERT_NEAR(sol[i], correct[0], 1e-3);
        ASSERT_NEAR(sol[i+N], correct[1], 1e-3);
    }
    /// [Implicit Gradvec example]
}


TEST(MLSM_Operators, ScalarNeumannLinear) {
    // Create rectangle domain for testing fill it uniformly
    RectangleDomain<Vec3d> domain(Vec3d(0.), Vec3d(1.));
    domain.fillUniformInteriorWithStep(0.1);
    domain.fillUniformBoundaryWithStep(0.1);
    // All right boundary points
    Range<int> right = domain.positions.filter([](Vec3d p) { return p[0] == 1.0; });

    // Set up MLSM instance support = 6, basis = 4
    int supp_size = 6;
    domain.findSupport(supp_size, right, domain.types == INTERNAL, true);
    EngineMLS<Vec3d, Monomials, Gaussians> mls(
            2, domain.positions[domain.support[right[0]]],
            30 * domain.characteristicDistance());

    auto mlsm = make_mlsm(domain, mls, right);

    // Scalar field
    Range<double> field(domain.size(), 1);
    // Linear gradient
    for (int i : domain.types == INTERNAL) {
        field[i] = domain.positions[i][1];
    }
    field[right] = 0;
    Vec3d direction1{1, 0, 0}, direction2{1, 2.4, 0.8};
    direction2 /= std::sqrt(direction2.dot(direction2));
    for (int i : right) {
        // Check zero derivation
        field[i] = mlsm.neumann(field, i, direction1, 0.0);
        EXPECT_NEAR(domain.positions[i][1], field[i], 1e-14);
        EXPECT_NEAR(0.0, direction1.dot(mlsm.grad(field, i)), 1e-14);

        // Non-zero derivation
        field[i] = mlsm.neumann(field, i, direction1, 5.0);
        EXPECT_NEAR(5, direction1.dot(mlsm.grad(field, i)), 1e-14);

        // Mixed derivation
        field[i] = mlsm.neumann(field, i, direction2, 0.0);
        EXPECT_NEAR(0.0, direction2.dot(mlsm.grad(field, i)), 1e-14);
    }
}

/**
 * This test Neumann method on all points in 1D scalar sin field.
 * By by calling the method such that derivation be cos(x), it compares the
 * returned value with sin(x).
 */
TEST(MLSM_Operators, ScalarNeumannSin) {
    // Create rectangle domain for testing fill it uniformly
    RectangleDomain<Vec1d> domain(Vec1d(0.), Vec1d(1.));
    domain.fillUniformInteriorWithStep(0.01);
    auto internal = domain.types == 1;
    // You need asymmetry in selecting the support so that the derivation is
    // is dependant on the central point
    int supp_size = 4;
    domain.findSupport(supp_size);
    EngineMLS<Vec1d, Monomials, Gaussians> mls(
            2, domain.positions[domain.support[0]],
            30 * domain.characteristicDistance());
    auto op = make_mlsm<mlsm::d1>(domain, mls, internal);
    // Create scalar field of sin(x)
    Range<double> field(domain.size(), 1);
    for (int i : domain.types != 0) {
        field[i] = std::sin(domain.positions[i][0]);
    }
    for (int i : internal) {
        double tmp = std::cos(domain.positions[i][0]);
        tmp = op.neumann(field, i, {1}, tmp);
        EXPECT_NEAR(tmp, field[i], field[i] * 1e-3);
    }
}

TEST(MLSM_Operators, ScalarBoundaryDeathTest) {
    // Create rectangle domain for testing fill it uniformly
    RectangleDomain<Vec1d> domain(Vec1d(0.), Vec1d(1.));
    domain.fillUniformInteriorWithStep(0.1);
    domain.addPoint(0.0, -1);
    domain.addPoint(1.0, -1);

    auto internal = domain.types == 1;
    int supp_size = 3;
    domain.findSupport(supp_size);
    EngineMLS<Vec1d, Monomials, Gaussians> mls(
            2, domain.positions[domain.support[0]],
            30 * domain.characteristicDistance());
    auto mlsm = make_mlsm(domain, mls, internal);
    // Create scalar field
    Range<double> field(domain.size(), 1);
    for (int i : domain.types != 0) {
        field[i] = std::sin(domain.positions[i][0]);
    }
    for (int i : internal) {
        EXPECT_DEATH(mlsm.neumann(field, i, {1}, 0.0), "negligible effect");
    }
}

/**
 * This test Neumann method on all points in 1D scalar sin field.
 * By by calling the method such that derivation be cos(x), it compares the
 * returned value with sin(x).
 */
TEST(MLSM_Operators, VectorNeumannSin) {
    // Create rectangle domain for testing fill it uniformly
    RectangleDomain<Vec1d> domain(Vec1d(0.), Vec1d(1.));
    domain.fillUniformInteriorWithStep(0.01);
    auto internal = domain.types == 1;
    // You need asymmetry in selecting the support so that the derivation is
    // is dependant on the central point
    int supp_size = 4;
    domain.findSupport(supp_size);
    EngineMLS<Vec1d, Monomials, Gaussians> mls(
            2, domain.positions[domain.support[0]],
            30 * domain.characteristicDistance());
    auto mlsm = make_mlsm(domain, mls, internal);
    // Create scalar field of sin(x)
    Range<Vec3d> field(domain.size(), 1.0);
    for (int i : domain.types != 0) {
        field[i][0] = std::sin(domain.positions[i][0]);
        field[i][1] = std::cos(domain.positions[i][0]);
        field[i][2] = std::exp(domain.positions[i][0]);
    }
    for (int i : internal) {
        Vec3d tmp = {
            std::cos(domain.positions[i][0]),
            -std::sin(domain.positions[i][0]),
            std::exp(domain.positions[i][0])};
        tmp = mlsm.neumann(field, i, {1}, tmp);
        EXPECT_NEAR(tmp[0], field[i][0], field[i][0] * 1e-3);
        EXPECT_NEAR(tmp[1], field[i][1], field[i][1] * 1e-3);
        EXPECT_NEAR(tmp[2], field[i][2], field[i][2] * 1e-3);
    }
}

TEST(MLSM_Operators, VectorBoundaryDeathTest) {
    // Create rectangle domain for testing fill it uniformly
    RectangleDomain<Vec1d> domain(Vec1d(0.), Vec1d(1.));
    domain.fillUniformInteriorWithStep(0.1);
    domain.addPoint(0., -1);
    domain.addPoint(1., -1);

    auto internal = domain.types == 1;
    int supp_size = 3;
    domain.findSupport(supp_size);
    EngineMLS<Vec1d, Monomials, Gaussians> mls(
            2, domain.positions[domain.support[0]],
            30 * domain.characteristicDistance());
    auto mlsm = make_mlsm(domain, mls, internal);
    // Create scalar field
    Range<Vec3d> field(domain.size(), 1.);
    for (int i : domain.types != 0) {
        field[i][0] = std::sin(domain.positions[i][0]);
        field[i][1] = std::cos(domain.positions[i][0]);
        field[i][2] = std::exp(domain.positions[i][0]);
    }
    for (int i : internal) {
        EXPECT_DEATH(mlsm.neumann(field, i, {1}, Vec3d{0, 0, 0}), "negligible effect");
    }
}

TEST(MLSM_Operators, Div3DSimple) {
    RectangleDomain<Vec3d> domain(Vec3d(0.), Vec3d(1.));
    domain.fillUniformInteriorWithStep(0.05);
    domain.fillUniformBoundaryWithStep(0.05);
    Range<int> internal = domain.types == 1;
    internal = internal.filter([&](const int i){return i % 13 == 0; });
    int n = domain.size();
    Range<Vec3d> field(n);
    Range<double> divfield(n);
    for (int i : domain.types != 0) {
        Vec3d p = domain.positions[i];
        field[i][0] = p[0]*p[1];
        field[i][1] = p[0];
        field[i][2] = p[0];
    }
    int supp_size = 7;
    // Find normal support for internal nodes
    domain.findSupport(supp_size, internal);

    EngineMLS<Vec3d, Monomials, Gaussians> mls(
        2,
        domain.positions[domain.support[internal[0]]],
        30 * domain.characteristicDistance());

    auto mlsm = make_mlsm(domain, mls, internal);

    for (int i : internal) {
        if (i % 1000) continue;
        divfield[i] = mlsm.div(field, i);
        EXPECT_NEAR(divfield[i], domain.positions[i][1], 1e-10);
    }
}

TEST(MLSM_Operators, 3dGradDivSimple) {
    RectangleDomain<Vec3d> domain(Vec3d(0.), Vec3d(1.));
    domain.fillUniformInteriorWithStep(0.05);
    domain.fillUniformBoundaryWithStep(0.05);
    Range<int> internal = domain.types == 1;
    internal = internal.filter([&](const int i){return i % 13 == 0; });
    int n = domain.size();
    Range<Vec3d> field(n), result(n);
    for (int i : domain.types != 0) {
        Vec3d p = domain.positions[i];
        field[i][0] = p[1]*p[0];
        field[i][1] = p[1]*p[0];
        field[i][2] = p[2] * p[2] * p[2];
        result[i][0] = 1;
        result[i][1] = 1;
        result[i][2] = 6 * p[2];
    }

    int supp_size = 27;
    // Find normal support for internal nodes
    domain.findSupport(supp_size, internal);

    EngineMLS<Vec3d, Monomials, Gaussians> mls(
        3,
        domain.positions[domain.support[internal[0]]],
        30 * domain.characteristicDistance());

    auto mlsm = make_mlsm<mlsm::graddiv>(domain, mls, internal);

    for (int i : internal) {
        Vec3d tmp = mlsm.graddiv(field, i);
        EXPECT_NEAR(tmp[0], result[i][0], 1e-10);
        EXPECT_NEAR(tmp[1], result[i][1], 1e-10);
        EXPECT_NEAR(tmp[2], result[i][2], 1e-10);
    }
}

TEST(MLSM_Operators, 3dGradDiv) {
    RectangleDomain<Vec3d> domain(Vec3d(0.), Vec3d(0.5));
    domain.fillUniformInteriorWithStep(0.01);
    domain.fillUniformBoundaryWithStep(0.01);

    Range<int> internal = domain.types == 1;
    internal = internal.filter([&](const int i){return i % 507 == 0; });

    int supp_size = 27;
    // Find normal support for internal nodes
    domain.findSupport(supp_size, internal);

    int n = domain.size();
    Range<Vec3d> field(n), result(n);

    for (int j : internal) {
        for (int i : domain.support[j]) {
            Vec3d p = domain.positions[i];
            double x = p[0], y = p[1], z = p[2];
            double exyz = std::exp(x - y*z);
            field[i][0] = std::sin(x * y);
            field[i][1] = std::cos(x + y - z);
            field[i][2] = z * exyz;
            result[i][0] = - y*y*std::sin(x*y) + (1-y*z) * exyz - std::cos(x + y - z);
            result[i][1] = y*z*z*exyz - 2*z * exyz - std::cos(x+y-z) -
                           x * y * std::sin(x*y) + std::cos(x*y);
            result[i][2] = exyz * y * (y*z-2) + std::cos(x+y-z);
        }
    }


    EngineMLS<Vec3d, Monomials, Gaussians> mls(
        3,
        domain.positions[domain.support[internal[0]]],
        30 * domain.characteristicDistance());

    auto mlsm = make_mlsm(domain, mls, internal);
    for (int i : internal) {
        Vec3d tmp = mlsm.graddiv(field, i);
        EXPECT_NEAR(tmp[0], result[i][0], 1e-3);
        EXPECT_NEAR(tmp[1], result[i][1], 1e-3);
        EXPECT_NEAR(tmp[2], result[i][2], 1e-3);
    }
}

TEST(MLSM_Operators, implicit2dGradDiv) {
    /// [Implicit graddiv example]
    // Solve problem for u unknown scalar field:
    // grad div u + laplace u = (2 + 4xy - 8pi^2sin(2*pi*x), 2(2x^2+y^2))
    // on [0, 1] x [0, 1] with BC:
    // u(0, y) = (y^2, 0)
    // u(1, y) = (y^2, y^2)
    // u(x, 0) = (sin(2*pi*x), 0)
    // u(x, 1) = (1+sin(2*pi*x, x^2)
    // with solution:
    // u(x, y) = (y^2 + sin(2*pi*x), x^2y^2)
    std::function<Vec2d(Vec2d)> analytical = [] (const Vec2d& p) {
        return Vec2d({p[1]*p[1]+std::sin(2*M_PI*p[0]), p[0]*p[0]*p[1]*p[1]});
    };
    // Prepare domain
    RectangleDomain<Vec2d> domain(0., 1.);
    domain.fillUniformWithStep(0.025, 0.025);  // error is below 1e-3 if 0.1 step is taken
    int N = domain.size();
    int support_size = 6;
    domain.findSupport(support_size);
    // Prepare operators and matrix
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(3, domain.positions, std::sqrt(15.0 / N));
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(2*N, 2*N);
    M.reserve(Range<int>(2*N, support_size));
    auto op = make_mlsm<mlsm::graddiv | mlsm::lap>(domain, mls, domain.types == INTERNAL);
    Eigen::VectorXd rhs(2*N);
    // Set equation on interior
    for (int i : (domain.types == INTERNAL)) {
        op.lapvec(M, i, 1.0);
        op.graddiv(M, i, 1.0);  // graddiv + laplace in interior
        double x = domain.positions[i][0];
        double y = domain.positions[i][1];
        rhs(i) = 2+4*x*y-8*M_PI*M_PI*std::sin(2*M_PI*x);
        rhs(i+N) = 2*(2*x*x + y*y);
    }
    // Set boundary conditions
    for (int i : (domain.types == BOUNDARY)) {
        M.coeffRef(i, i) = 1;  // boundary conditions on the boundary
        M.coeffRef(i+N, i+N) = 1;  // boundary conditions on the boundary
        double x = domain.positions[i][0];
        double y = domain.positions[i][1];
        if (x == 0) {
            rhs(i) = y*y;
            rhs(i+N) = 0;
        } else if (x == 1) {
            rhs(i) = y*y;
            rhs(i+N) = y*y;
        } else if (y == 0) {
            rhs(i) = std::sin(2*M_PI*x);
            rhs(i+N) = 0;
        } else if (y == 1) {
            rhs(i) = 1+std::sin(2*M_PI*x);
            rhs(i+N) = x*x;
        } else {
            assert(!"Should not be here.");
        }
    }

    // Sparse solve
    Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor>> solver;
    solver.compute(M);
    Eigen::VectorXd sol = solver.solve(rhs);
    for (int i = 0; i < N; ++i) {
        Vec2d correct = analytical(domain.positions[i]);
        EXPECT_NEAR(sol[i], correct[0], 0.8e-2);
        EXPECT_NEAR(sol[i+N], correct[1], 0.8e-2);
    }
    /// [Implicit graddiv example]
}

/**
 * @brief Closed form solution of diffusion equation
 * @param pos spatial coordinate
 * @param t time
 * @param a size of domain
 * @param D diffusion constant
 * @param N no. of expansion
 * @return value of temperature
 */
double diff_closedform(const Vec2d& pos, double t, double a, double D, size_t N) {
    double T = 0;
    double f = M_PI / a;
    for (size_t n = 1; n < N; n = n + 2) {
        for (size_t m = 1; m < N; m = m + 2) {
            T += 4.0 / f / f / (n * m) * std::sin(n * f * pos[0]) * std::sin(m * f * pos[1]) *
                 std::exp(-D * t * ((n * n + m * m) * f * f));
        }
    }
    return T;
}

// A test for Neumann boundary conditions
TEST(MLSM_Operators, QuarterDiffusionTest) {
    // Create rectangle domain for testing fill it uniformly
    RectangleDomain<Vec2d> domain(Vec2d(0.), Vec2d(1.));
    domain.fillUniformInteriorWithStep(0.05);
    domain.fillUniformBoundaryWithStep(0.05);

    // Upper and right boundaries have zero value
    auto up_right = domain.positions.filter([](Vec2d p) {
        return (p[0] == 1.0 || p[1] == 1.0);});
    // Down and left boundaries  have zero normal derivation
    auto down = domain.positions.filter([](Vec2d p) {
        return (p[1] == 0.0 && p[0] != 1.0);});
    auto left = domain.positions.filter([](Vec2d p) {
        return (p[0] == 0.0 && p[1] != 1.0);});
    // Internal values are determined with diffusion equation
    auto internal = domain.types == 1;
    // We will test on a subset of internal nodes
    auto testing = domain.positions.filter([](Vec2d p) {
        return (p[0] != 0.0 && p[1] != 1.0 && p[0] < 0.5 && p[1] < 0.5);});

    int supp_size = 5;
    // Find normal support for internal nodes
    domain.findSupport(supp_size, internal);
    // Special support for Neumann boundary conditions
    domain.findSupport(supp_size, down, internal, true);
    domain.findSupport(supp_size, left, internal, true);
    // For the sake of completeness find support for zero bounds (will not be used)
    domain.findSupport(supp_size, up_right);
    // Make MLS with Gaussian basis and weight
    EngineMLS<Vec2d, Gaussians, Gaussians> mls(
        {domain.characteristicDistance() * 10, 5},
        domain.positions[domain.support[internal[0]]],
        30 * domain.characteristicDistance());
    auto mlsm = make_mlsm(domain, mls, domain.types != 0);
    // Create scalar field
    Range<double> field(domain.size(), 1), tmp(domain.size(), 1);
    // Set zero boundary condition
    field[up_right] = 0;

    double t = 0.1, dt = 1e-5;
    // Uncomment bellow line to enable drawing
    // std::thread th([&] { draw2D(domain.positions, field); });
    for (int nt = 0; nt*dt < t; nt++) {
        // Test the correctness of the solution
        if (nt % static_cast<int>(t/dt/100) == 0 && nt != 0) {
            double tmp_val;
            for (int i : testing) {
                tmp_val = diff_closedform(
                    domain.positions[i]+Vec2d{1, 1}, nt * dt, 2, 1, 50);
                EXPECT_NEAR(tmp_val, field[i], 3e-3);
            }
        }
        // Calculate internal nodes with Laplace operator
        for (int i : internal) {
            tmp[i] += dt * mlsm.lap(field, i);
        }
        field[internal] = tmp[internal];
        // Calculate Neumann boundary condition
        for (int i : down) {
            field[i] = mlsm.neumann(field, i, Vec2d{0, 1}, 0.0);
        }
        for (int i : left) {
            field[i] = mlsm.neumann(field, i, Vec2d{1, 0}, 0.0);
        }
    }
    // th.join();
}

// Simple example for Neumann boundary conditions
TEST(MLSM_Operators, QuarterDiffusionExample) {
    /// [Quarter diffusion example]
    // Create rectangle domain for testing fill it uniformly
    RectangleDomain<Vec2d> domain(Vec2d(0.), Vec2d(1.));
    domain.fillUniformInteriorWithStep(0.01);
    domain.fillUniformBoundaryWithStep(0.01);

    // Upper and right boundaries have zero value
    auto up_right = domain.positions.filter([](Vec2d p) {
        return (p[0] == 1.0 || p[1] == 1.0);});
    // Down and left boundaries  have zero normal derivation
    auto down = domain.positions.filter([](Vec2d p) {
        return (p[1] == 0.0 && p[0] != 1.0);});
    auto left = domain.positions.filter([](Vec2d p) {
        return (p[0] == 0.0 && p[1] != 1.0);});
    // Internal values are determined with diffusion equation
    auto internal = domain.types == 1;

    // Support size 5
    int supp_size = 5;
    // Find normal support for internal nodes
    domain.findSupport(supp_size, internal);
    // Special support for Neumann boundary conditions
    // This finds support only in internal nodes but also forces each node into
    // its support node
    domain.findSupport(supp_size, down, internal, true);
    domain.findSupport(supp_size, left, internal, true);
    // For the sake of completeness find support for zero bounds (will not be used)
    domain.findSupport(supp_size, up_right);
    // Make MLS with Gaussian basis and Gaussian weight
    EngineMLS<Vec2d, Gaussians, Gaussians> mls(
        {domain.characteristicDistance() * 10, 5},
        domain.positions[domain.support[internal[0]]],
        30 * domain.characteristicDistance());
    auto mlsm = make_mlsm(domain, mls, domain.types != 0);
    // Create a scalar temperature field
    Range<double> T(domain.size(), 1), tmp(domain.size(), 1);
    // Set zero boundary condition
    T[up_right] = 0;
    // We will simulate 0.1s at 10^-5s precision
    double t = 0.1, dt = 1e-5;
    // Uncomment bellow line to enable drawing
    // std::thread th([&] { draw2D(domain.positions, T); });

    // Main loop
    for (int nt = 0; nt*dt < t; nt++) {
        // Calculate internal nodes with Laplace operator
        for (int i : internal) {
            tmp[i] += dt * mlsm.lap(T, i);
        }
        T[internal] = tmp[internal];
        // Calculate Neumann boundary condition for the left bound
        for (int i : left) {
            // This will set point T[i] so that d/dx T[i] = 0
            T[i] = mlsm.neumann(T, i, Vec2d{1, 0}, 0.0);
        }
        // Calculate Neumann boundary condition for the lower bound
        for (int i : down) {
            // This will set point T[i] so that d/dy T[i] = 0
            T[i] = mlsm.neumann(T, i, Vec2d{0, 1}, 0.0);
        }
    }

    // Uncomment bellow line to enable drawing
    // th.join();
    /// [Quarter diffusion example]
}

TEST(MLSM_Operators, ImplicitDer1) {
    /// [Implicit derivative example]
    // Solve problem for F unknown vector field:
    // f''(x) = 6x, f'(1) = 1, f(0) = 2
    // with solution:
    // f(x) = 2 - 2x + x^3
    std::function<Vec1d(Vec1d)> analytical = [] (const Vec1d& p) {
        return Vec1d(2 - 2*p[0] + p[0]*p[0]*p[0]);
    };
    // Prepare domain
    RectangleDomain<Vec1d> domain(Vec1d(0.), Vec1d(1.));
    domain.fillUniformWithStep(0.02, 0.02);
    int N = domain.size();
    int support_size = 3;
    domain.findSupport(support_size);
    // Prepare operators and matrix
    EngineMLS<Vec1d, Monomials, NNGaussians> mls(3, domain.positions, 15.0 / N);
    Eigen::MatrixXd M = Eigen::MatrixXd::Zero(N, N);
    auto op = make_mlsm(domain, mls, domain.types != 0);  // All nodes, including boundary
    Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);
    // Set equation on interior
    for (int i : (domain.types == INTERNAL)) {
        op.lapvec(M, i, 1.0);  // laplace in interior
        double x = domain.positions[i][0];
        rhs(i) = 6*x;
    }
    // Set boundary conditions
    for (int i : (domain.types == BOUNDARY)) {
        double x = domain.positions[i][0];
        if (x == 0) {
            M.coeffRef(i, i) = 1;  // boundary conditions on the boundary
            rhs(i) = 2;
        } else if (x == 1) {
            op.der1(M, 0, 0, i, 1.0);  // df / dx
            rhs(i) = 1;
        } else {
            assert(!"Should not be here.");
        }
    }

    VecXd sol = M.lu().solve(rhs);
    for (int i = 0; i < N; ++i) {
        Vec1d correct = analytical(domain.positions[i]);
        ASSERT_NEAR(sol[i], correct[0], 1e-3);
    }
    /// [Implicit derivative example]
}

TEST(MLSM_Operators, ImplicitDer1Sum) {
    /// [Sum of derivatives example]
    // The vector field is given by:
    // u = u_0*(sin(pi*(x^2+y^2))+1)
    // v = v_0*(cos(pi*(x^2+y^2))+1)
    // The derivatives are given by:
    // u_x = 2*pi*u_0*x*cos(pi*(x^2+y^2))
    // u_y = 2*pi*u_0*y*cos(pi*(x^2+y^2))
    // v_x = -2*pi*v_0*x*sin(pi*(x^2+y^2))
    // v_y = -2*pi*v_0*y*sin(pi*(x^2+y^2))

    double u0 = 1, v0 = 1;
    double alpha = 1, beta = 1, gamma = 1, delta = 1;

    // Vector field function that returns (u,v)
    std::function<Vec2d(Vec2d)> analytical = [&u0, &v0] (const Vec2d& p) {
        return Vec2d({u0*(std::sin(p.squaredNorm())+1),
                      v0*(std::cos(p.squaredNorm())+1)});
    };
    // Analytical function for \alpha*u_x + \beta*v_y
    std::function<double(Vec2d)> tx = [&alpha, &beta, &u0, &v0] (const Vec2d& p) {
        return alpha*(2*u0*p[0]*std::cos(p.squaredNorm())) +
               beta*(-2*v0*p[1]*std::sin(p.squaredNorm()));
    };
    // Analytical function for \gamma*u_y + \delta*v_x
    std::function<double(Vec2d)> ty = [&gamma, &delta, &u0, &v0] (const Vec2d& p) {
        return gamma*(2*u0*p[1]*std::cos(p.squaredNorm())) +
               delta*(-2*v0*p[0]*std::sin(p.squaredNorm()));
    };
    // Create and fill domain
    RectangleDomain<Vec2d> domain(Vec2d({-0.2, -0.5}), Vec2d({0.8, 0.5}));
    double step = 0.01;
    domain.fillUniformBoundaryWithStep(step);
    domain.fillUniformInteriorWithStep(step);
    int support_size = 12;
    domain.findSupport(support_size);
    int N = domain.size();
    // Prepare MLS engine
    double cd = domain.characteristicDistance();
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(3, domain.positions[domain.support[0]], cd);
    // Prepare operators
    auto op = make_mlsm<mlsm::d1>(domain, mls, domain.types != 0);
    // Fill Matrix with operators for
    // \alpha*u_x + \beta*v_y in x-direction and
    // \gamma*u_y + \delta*v_x in y-direction
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(2 * N, 2 * N);
    M.reserve(Eigen::VectorXd::Constant(2 * N, 2 * support_size));
    Eigen::VectorXd field(2 * N), rhs(2 * N);
    for (int i = 0; i < N; ++i) {
        // apply in x-direction
        op.der1(M, 0, 0, i, alpha, 0);
        op.der1(M, 1, 1, i, beta, 0);
        // apply in y-direction
        op.der1(M, 0, 1, i, gamma, 1);
        op.der1(M, 1, 0, i, delta, 1);
        // field to apply operators on
        auto f = analytical(domain.positions[i]);
        field(i) = f[0];
        field(i+N) = f[1];
        // expected analytical values
        rhs(i) = tx(domain.positions[i]);
        rhs(i+N) = ty(domain.positions[i]);
    }
    // perform multiplication
    Eigen::VectorXd prod = M*field;

    // check both components are close enough
    for (int i = 0; i < N; ++i) {
        ASSERT_NEAR(prod[i], rhs[i], 1e-3);
        ASSERT_NEAR(prod[i+N], rhs[i+N], 1e-3);
    }
    /// [Sum of derivatives example]
}

TEST(MLSM_Operators, DISABLED_Example) {
    /// [MLSM construction example]
    // Prepare domain
    RectangleDomain<Vec2d> domain(0., 1.);
    domain.fillUniformWithStep(0.025, 0.025);
    int N = domain.size();
    int support_size = 6;
    domain.findSupport(support_size);
    // Prepare engine
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(3, domain.positions, 15.0 / N);
    // Prepare operators. Specify only the ones you need, to avoid creating unnecessary shape
    // functions. List of flags is in the mlsm namespace.
    auto op = make_mlsm<mlsm::graddiv | mlsm::lap>(domain, mls, domain.types == INTERNAL);
    std::cout << op << std::endl;
    /// [MLSM construction example]
}

TEST(MLSM_Operators, DeathNoFindSupport) {
    omp_set_num_threads(1);
    RectangleDomain<Vec1d> domain(Vec1d(0.), Vec1d(1.));
    domain.fillUniformInteriorWithStep(0.1);
    EngineMLS<Vec1d, Monomials, Gaussians> mls(2, domain.positions, 1);
    EXPECT_DEATH(make_mlsm(domain, mls, {1}),
                 ".*Did you call findSupport before creating MLSM operators.*");
}

TEST(MLSM_Operators, DeathInvalidIndex) {
    RectangleDomain<Vec1d> domain(Vec1d(0.), Vec1d(1.));  // domain definition
    domain.fillUniformInteriorWithStep(0.1);  // 9 points
    domain.findSupport(5);
    EngineMLS<Vec1d, Monomials, Gaussians> mls(2, domain.positions, 1);
    EXPECT_DEATH(make_mlsm(domain, mls, {1, 3, 0, 9}),  // index 9 is invalid
                 ".*One of the specified indexes was not a valid index of a point in the domain.*");
}

}  // namespace mm
