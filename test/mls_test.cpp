#include "mls.hpp"

#include "gtest/gtest.h"

#include "domain.hpp"
#include "domain_extended.hpp"
#include "common.hpp"
#include "types.hpp"
#include "includes.hpp"

/**
 * @file test_mls.cpp
 * @brief MLS Tests
 */

namespace mm {

// What will be the size of the support domain
const int N = 200;

// 3 different functions for 3 different dimensions
double fun3d(Vec3d p) {
    return std::cos(p[0] * M_PI) * std::cos(p[1] * M_PI) + std::cos(p[2] * M_PI);
}

double fun2d(Vec2d p) {
    return std::cos(p[0] * M_PI) * std::cos(p[1] * M_PI);
}

double fun1d(Vec1d p) {
    return std::sin(p[0] * M_PI * 4);
}

TEST(MLS, DISABLED_Animation) {
    CircleDomain<Vec1d> domain({0}, M_PI), domain_full({0}, M_PI);
    domain.fillUniformInterior(10);
    Range<Vec1d> support_domain = domain.getInternalNodes();
    Range<double> values;
    for (auto &p : support_domain) values.push_back(std::sin(p[0]));

    double covariance = 1;
    EngineMLS<Vec1d, Monomials, Gaussians> WLS(1, support_domain, covariance);

    domain_full.fillUniformInterior(100);
    domain_full.types = 2;

    // domain.add(domain_full);
    // domain.findSupport(3, domain.types == 2, domain.types == INTERNAL);

    std::cerr << domain_full.size() << "\n";
    std::cerr << covariance << "\n";
    std::cerr << support_domain << "\n" << values << "\n";
    for (auto &p : domain_full.getInternalNodes()) {
        WLS.setupFunctionAt(p, values);
        std::cerr << p[0] << "\n" << WLS.getCoefficients().transpose()
                  << std::endl;
        auto shape = WLS.getShapeAt(p, {{1}});
        double res = 0;
        for (int i = 0; i < shape.size(); i++) {
            res += shape(i) * values[i];
        }
        std::cerr << res << "\n";
    }
}

TEST(MLS, DISABLED_WLSExample) {
    /// [WLS Example]
    CircleDomain<Vec1d> domain({0}, M_PI);
    domain.fillUniformInterior(10);
    Range<Vec1d> support_domain = domain.getInternalNodes();
    Range<double> values;
    for (auto &p : support_domain) values.push_back(std::sin(p[0]));

    Monomials<Vec1d> basis(3);
    Gaussians<Vec1d> weight(0.2);
    // You can use implicit constructors and just write 'WLS(3, support_domain, 0.2)'
    EngineMLS<Vec1d, Monomials, Gaussians> WLS(basis, support_domain, weight);
    std::cout << WLS << std::endl;
    // Get the best fit around a given point with given values
    WLS.setupFunctionAt({0.12}, values);
    // Evaluate it anywhere you want
    domain.fillUniformInterior(100);
    Range<double> vals, derivs;
    for (auto &p : domain.getInternalNodes()) {
        // Get approximation values and derivatives
        vals.push_back(WLS.evaluateAt(p));
        derivs.push_back(WLS.evaluateAt(p, {{1}}));
    }
    /// [WLS Example]

    // Test default and copy constructor
    EngineMLS<Vec1d, Monomials, Gaussians> WLS_ =
                EngineMLS<Vec1d, Monomials, Gaussians>(basis, support_domain, weight);
    WLS_.setupFunctionAt({0.12}, values);
    for (auto &p : domain.getInternalNodes()) {
        EXPECT_NEAR(WLS.evaluateAt(p, {{1}}), WLS_.evaluateAt(p, {{1}}), 2e-15);
    }
}

TEST(MLS, MLSExample) {
    /// [MLS Example]
    CircleDomain<Vec1d> domain({0}, M_PI);
    domain.fillUniformInterior(10);
    Range<Vec1d> support_domain = domain.getInternalNodes();
    Range<double> values;
    for (auto &p : support_domain) values.push_back(std::sin(p[0]));

    Monomials<Vec1d> basis(3);
    Gaussians<Vec1d> weight(0.2);
    // You can use implicit constructors and just write 'MLS(3, support_domain, 0.2)'
    EngineMLS<Vec1d, Monomials, Gaussians> MLS(basis, support_domain, weight);

    // Evaluate it anywhere you want
    domain.fillUniformInterior(1000);
    Range<double> vals, derivs;
    for (auto &p : domain.getInternalNodes()) {
        // Fit and get approximation values and derivatives
        MLS.setupFunctionAt(p, values);
        vals.push_back(MLS.evaluateAt(p));
        derivs.push_back(MLS.evaluateAt(p, {{1}}));
    }
    /// [MLS Example]
}


/**
* Finds node closest to pos among Range<vec_t> positions.
* This is slow debug only function, it generates a KDTree at each call
*/
template<class vec_t>
int find_closest(const Range<vec_t> &positions, vec_t pos) {
    KDTree<vec_t> kd3(positions);  // test point (0.45, 0.45...)
    Range<double> distances;
    Range<int> closest;
    std::tie(closest, distances) = kd3.query(pos, 1);
    return closest[0];
}

TEST(MLS, getShape) {
    /// [mlsShapeExample]
    typedef Vec2d vec_t;

    RectangleDomain<vec_t> domain(vec_t(0.), vec_t(1.));
    domain.fillUniformInteriorWithStep(0.02);
    domain.findSupport(6);

    int pos_i = find_closest(domain.positions, {0.2, 0.2});
    Range<vec_t> supp_domain = domain.positions[domain.support[pos_i]];

    double sigmaW = 1;
    vec_t pos = supp_domain[0];

    EngineMLS<vec_t, Monomials, Gaussians>
            mls(3, supp_domain, sigmaW * domain.characteristicDistance());

    mls.setSupport(supp_domain);
    Range<double> shape = mls.getShapeAt(pos);
    Range<double> shape_x = mls.getShapeAt(pos, {{1, 0}});
    Range<double> shape_y = mls.getShapeAt(pos, {{0, 1}});
    Range<double> shape_xx = mls.getShapeAt(pos, {{2, 0}});
    Range<double> shape_yy = mls.getShapeAt(pos, {{0, 2}});

    Range<double> weights = mls.getWeightValues();
    /// [mlsShapeExample]
    ASSERT_EQ(6, weights.size());
    ASSERT_EQ(6, shape.size());
    EXPECT_NEAR(1.0985, weights[0], 1e-5);
    EXPECT_NEAR(0.89417, weights[1], 1e-5);
    EXPECT_NEAR(0.89417, weights[2], 1e-5);
    EXPECT_NEAR(1.32332, weights[3], 1e-5);
    EXPECT_NEAR(1.32332, weights[4], 1e-5);
    EXPECT_NEAR(0.72785, weights[5], 1e-5);

    EXPECT_NEAR(1, shape[0], 1e-5);
    EXPECT_NEAR(0, shape[1], 1e-5);
    EXPECT_NEAR(0, shape[2], 1e-5);
    EXPECT_NEAR(0, shape[3], 1e-5);
    EXPECT_NEAR(0, shape[4], 1e-5);
    EXPECT_NEAR(0, shape[5], 1e-5);

    EXPECT_NEAR(0, shape_x[0], 1e-5);
    EXPECT_NEAR(25, shape_x[1], 1e-5);
    EXPECT_NEAR(0, shape_x[2], 1e-5);
    EXPECT_NEAR(-25, shape_x[3], 1e-5);
    EXPECT_NEAR(0, shape_x[4], 1e-5);
    EXPECT_NEAR(0, shape_x[5], 1e-5);

    EXPECT_NEAR(0, shape_y[0], 1e-5);
    EXPECT_NEAR(0, shape_y[1], 1e-5);
    EXPECT_NEAR(25, shape_y[2], 1e-5);
    EXPECT_NEAR(0, shape_y[3], 1e-5);
    EXPECT_NEAR(-25, shape_y[4], 1e-5);
    EXPECT_NEAR(0, shape_y[5], 1e-5);

    EXPECT_NEAR(-5000, shape_yy[0], 1e-5);
    EXPECT_NEAR(0, shape_yy[1], 1e-5);
    EXPECT_NEAR(2500, shape_yy[2], 1e-5);
    EXPECT_NEAR(0, shape_yy[3], 1e-5);
    EXPECT_NEAR(2500, shape_yy[4], 1e-5);
    EXPECT_NEAR(0, shape_yy[5], 1e-5);

    EXPECT_NEAR(-5000, shape_xx[0], 1e-5);
    EXPECT_NEAR(2500, shape_xx[1], 1e-5);
    EXPECT_NEAR(0, shape_xx[2], 1e-5);
    EXPECT_NEAR(2500, shape_xx[3], 1e-5);
    EXPECT_NEAR(0, shape_xx[4], 1e-5);
    EXPECT_NEAR(0, shape_xx[5], 1e-5);
}

TEST(MLS, WLSSetupFunction) {
    CircleDomain<Vec1d> domain({0}, M_PI);
    domain.fillUniformInterior(10);
    Range<Vec1d> support_domain = domain.getInternalNodes();
    Range<double> values;
    for (auto &p : support_domain) values.push_back(std::sin(p[0]));

    Monomials<Vec1d> basis(3);
    Gaussians<Vec1d> weight(0.2);
    // You can use implicit constructors and just write 'WLS(3, support_domain, 0.2)'
    EngineMLS<Vec1d, Monomials, Gaussians> WLS(basis, support_domain, weight);

    // Get the best fit around a given point with given values
    WLS.setupFunction(values);
    // Evaluate it anywhere you want
    domain.fillUniformInterior(1000);
    Range<double> vals, derivs;
    for (auto &p : domain.getInternalNodes()) {
        // Get approximation values and derivatives
        vals.push_back(WLS.evaluateAt(p));
        derivs.push_back(WLS.evaluateAt(p, {{1}}));
    }
}

TEST(MLS, getMatrix) {
    Vec3d pnt = {0.5, 0.5, 0.4};
    Range<Vec3d> supp_domain = {{0.5, 0.5, 0.4},
                                {0.5, 0.5, 0.5},
                                {0.4, 0.5, 0.4},
                                {0.5, 0.4, 0.4},
                                {0.5, 0.5, 0.3},
                                {0.5, 0.6, 0.4},
                                {0.6, 0.5, 0.4},
                                {0.4, 0.5, 0.5},
                                {0.4, 0.5, 0.3},
                                {0.4, 0.4, 0.4}
    };

    EngineMLS<Vec3d, Monomials, Monomials> MLS(3, supp_domain, 1);
    Eigen::MatrixXd M = MLS.getMatrix(pnt);
    Eigen::MatrixXd R(10, 10);
    R << 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            1, 0.1, 0, 0, 0.01, 0, 0, 0, 0, 0,
            1, 0, 0, -0.1, 0, 0, 0, -0, -0, 0.01,
            1, 0, -0.1, 0, 0, -0, 0.01, 0, -0, 0,
            1, -0.1, 0, 0, 0.01, -0, 0, -0, 0, 0,
            1, 0, 0.1, 0, 0, 0, 0.01, 0, 0, 0,
            1, 0, 0, 0.1, 0, 0, 0, 0, 0, 0.01,
            1, 0.1, 0, -0.1, 0.01, 0, 0, -0.01, -0, 0.01,
            1, -0.1, 0, -0.1, 0.01, -0, 0, 0.01, -0, 0.01,
            1, 0, -0.1, -0.1, 0, -0, 0.01, -0, 0.01, 0.01;
    ASSERT_EQ(10, M.rows());
    ASSERT_EQ(10, M.cols());
    for (int i = 0; i < M.rows(); ++i)
        for (int j = 0; j < M.cols(); ++j)
            EXPECT_DOUBLE_EQ(M(i, j), R(i, j));
    MLS.getShapeAt(pnt);  // so that matrix gets calculated and inverted
    EXPECT_NEAR(570.2445581810681, MLS.getConditionNumber(), 1e-10);
}

TEST(MLS, Monomials1D) {
    CircleDomain<Vec1d> domain({0}, 1);
    domain.fillUniformInterior(N);

    Range<Vec1d> support_domain = domain.getInternalNodes();
    Range<double> values;
    values.reserve(N);
    for (auto &e : support_domain) values.push_back(fun1d(e));

    // Make MLS engines with different basis sizes
    Range<int> basis_sizes = {1, 3, 5, 10};

    // Create multiple test functions
    for (auto &s : basis_sizes) {
        EngineMLS<Vec1d, Monomials, Gaussians> engine(s, support_domain, 0.05);
        double max_err = 0;

        for (int i = 0; i < N; i += 2) {
            engine.setupFunctionAt(support_domain[i], values);
            auto tmp = engine.evaluateAt(support_domain[i]);
            max_err = std::max(max_err, std::abs(tmp - values[i]));
            if (i % (N / 10) == 0) {
                auto shape = engine.getShapeAt(support_domain[i]);
                double val = 0;
                for (int j = 0; j < shape.size(); j++)
                    val += shape[j] * values[j];
                EXPECT_NEAR(val, tmp, 1e-10 * std::abs(tmp));
            }
        }
        auto basis_size = engine.getCoefficients().size();
        if (basis_size > 9) {
            EXPECT_GT(0.05, max_err);
        } else if (basis_size > 4) {
            EXPECT_GT(0.4, max_err);
        } else if (basis_size > 2) {
            EXPECT_GT(0.6, max_err);
        } else {
            EXPECT_GT(0.9, max_err);
        }
    }
}

TEST(MLS, Monomials2D) {
    CircleDomain<Vec2d> domain({0, 0}, 1);
    domain.fillUniformInterior(N);

    Range<Vec2d> support_domain = domain.getInternalNodes();
    Range<double> values;
    values.reserve(N);
    // Evaluate the function.
    for (auto &e : support_domain) {
        values.push_back(fun2d(e));
    }

    // Make MLS engines with differente basis sizes
    Range<int> basis_sizes = {1, 2, 3, 4, 5};

    // Create multiple test functions
    for (auto &s : basis_sizes) {
        EngineMLS<Vec2d, Monomials, Gaussians> engine(s, support_domain, 0.1);
        double max_err = 0;
        for (int i = 0; i < N; i += 2) {
            engine.setupFunctionAt(support_domain[i], values);
            auto tmp = engine.evaluateAt(support_domain[i]);
            max_err = std::max(max_err, std::abs(tmp - values[i]));
            if (i % (N / 10) == 0) {
                auto shape = engine.getShapeAt(support_domain[i]);
                double val = 0;
                for (int j = 0; j < shape.size(); j++)
                    val += shape[j] * values[j];
                EXPECT_NEAR(val, tmp, 1e-10 * std::abs(tmp));
            }
        }
        auto basis_size = engine.getCoefficients().size();
        if (basis_size > 14) {
            EXPECT_LT(max_err, 0.02);
        } else if (basis_size > 9) {
            EXPECT_LT(max_err, 0.09);
        } else if (basis_size > 5) {
            EXPECT_LT(max_err, 0.15);
        } else {
            EXPECT_LT(max_err, 0.4);
        }
    }
}

TEST(MLS, Monomials3D) {
    CircleDomain<Vec3d> domain({0, 0, 0}, 1);
    // Create support domain and get function values
    domain.fillUniformInterior(N);
    Range<Vec3d> support_domain = domain.getInternalNodes();

    Range<double> values;
    values.reserve(N);
    for (auto &e : support_domain) {
        values.push_back(fun3d(e));
    }

    // Make MLS engines with different basis
    Range<int> basis_sizes = {1, 2, 3};

    // Create multiple test functions
    for (auto &s : basis_sizes) {
        EngineMLS<Vec3d, Monomials, Gaussians> engine(s, support_domain, 0.1);
        double max_err = 0;
        for (int i = 0; i < N; i += 2) {
            engine.setupFunctionAt(support_domain[i], values);
            auto tmp = engine.evaluateAt(support_domain[i]);
            max_err = std::max(max_err, std::abs(tmp - values[i]));
            if (i % (N / 10) == 0) {
                auto shape = engine.getShapeAt(support_domain[i]);
                double val = 0;
                for (int j = 0; j < shape.size(); j++)
                    val += shape[j] * values[j];
                EXPECT_NEAR(val, tmp, 1e-10 * std::abs(tmp));
            }
        }
        auto basis_size = engine.getCoefficients().size();
        if (basis_size > 9) {
            EXPECT_LT(max_err, 0.1);
        } else if (basis_size > 3) {
            EXPECT_LT(max_err, 0.6);
        } else {
            EXPECT_LT(max_err, 0.7);
        }
    }
}

TEST(MLS, Gaussians1D) {
    CircleDomain<Vec1d> domain({0}, 1);
    domain.fillUniformInterior(N);
    Range<Vec1d> support_domain = domain.getInternalNodes();
    Range<double> values;
    values.reserve(N);
    for (auto &e : support_domain) {
        values.push_back(fun1d(e));
    }

    // Make MLS engines with different basis sizes
    Range<int> basis_sizes = {1, 3, 5};

    for (auto &s : basis_sizes) {
        domain.findSupport(s);
        EngineMLS<Vec1d, Gaussians, Gaussians> engine({0.01, s}, support_domain,
                                                      0.01);
        double max_err = 0;
        for (int i = 0; i < N; i += 2) {
            engine.setupFunctionAt(support_domain[i], values,
                                   domain.support[i]);
            auto tmp = engine.evaluateAt(support_domain[i]);
            max_err = std::max(max_err, std::abs(tmp - values[i]));

            auto shape = engine.getShapeAt(support_domain[i]);
            double val = 0;
            for (int j = 0; j < shape.size(); j++) val += shape[j] * values[j];
            EXPECT_NEAR(val, tmp, 1e-7 * std::abs(tmp));
        }
        auto basis_size = engine.getCoefficients().size();
        if (basis_size > 4) {
            EXPECT_LT(max_err, 0.04);
        } else if (basis_size > 2) {
            EXPECT_LT(max_err, 0.25);
        } else {
            EXPECT_LT(max_err, 0.7);
        }
    }
}

TEST(MLS, Gaussians2D) {
    CircleDomain<Vec2d> domain({0, 0}, 1);
    domain.fillUniformInterior(N);
    Range<Vec2d> support_domain = domain.getInternalNodes();
    Range<double> values;
    values.reserve(N);
    for (auto &e : support_domain) {
        values.push_back(fun2d(e));
    }

    // Make MLS engines with different basis sizes
    Range<int> basis_sizes = {1, 3, 5, 10};

    for (auto &s : basis_sizes) {
        domain.findSupport(s);
        EngineMLS<Vec2d, Gaussians, Gaussians> engine({0.05, s}, support_domain,
                                                      0.05);
        double max_err = 0;
        for (int i = 0; i < N; i += 2) {
            engine.setupFunctionAt(support_domain[i], values,
                                   domain.support[i]);
            auto tmp = engine.evaluateAt(support_domain[i]);
            max_err = std::max(max_err, std::abs(tmp - values[i]));
            if ((i * 10) % N == 0) {
                auto shape = engine.getShapeAt(support_domain[i]);
                double val = 0;
                for (int j = 0; j < shape.size(); j++)
                    val += shape[j] * values[j];
                EXPECT_NEAR(val, tmp, 1e-10 * std::abs(tmp));
            }
        }
        auto basis_size = engine.getCoefficients().size();
        if (basis_size > 9) {
            EXPECT_LT(max_err, 0.04);
        } else if (basis_size > 4) {
            EXPECT_LT(max_err, 0.08);
        } else {
            EXPECT_LT(max_err, 0.2);
        }
    }
}

TEST(MLS, ManualGaussCenters2D) {
    CircleDomain<Vec2d> domain({0, 0}, 1);
    domain.fillUniformInterior(N);
    Range<Vec2d> support_domain = domain.getInternalNodes();
    KDTree<Vec2d> kdtree(support_domain);
    Range<double> values;
    values.reserve(N);
    for (auto &e : support_domain) {
        values.push_back(fun2d(e));
    }

    Range<int> basis_sizes = {1, 3, 5, 10};

    for (auto &s : basis_sizes) {
        EngineMLS<Vec2d, Gaussians, Gaussians> engine({0.05, s}, support_domain,
                                                      0.05);
        double max_err = 0;
        for (int i = 0; i < N; i += 2) {
            engine.setupFunctionAt(
                    support_domain[i], values,
                    kdtree.query(support_domain[i], s).first);
            auto tmp = engine.evaluateAt(support_domain[i]);
            max_err = std::max(max_err, std::abs(tmp - values[i]));
            if ((i * 10) % N == 0) {
                auto shape = engine.getShapeAt(support_domain[i]);
                double val = 0;
                for (int j = 0; j < shape.size(); j++)
                    val += shape[j] * values[j];
                EXPECT_NEAR(val, tmp, 1e-10 * std::abs(tmp));
            }
        }
        auto basis_size = engine.getCoefficients().size();
        if (basis_size > 9) {
            EXPECT_LT(max_err, 0.04);
        } else if (basis_size > 4) {
            EXPECT_LT(max_err, 0.08);
        } else {
            EXPECT_LT(max_err, 0.2);
        }
    }
}

TEST(MLS, Interpolation1D) {
    RectangleDomain<Vec1d> domain(Vec1d(-2.), Vec1d(2.));
    domain.positions = {-2., -1., 0., 1., 2.};
    domain.findSupport(5);
    int monomials = 5;
    Range<double> values = {0., 1., 0., -1., 0.};
    EngineMLS<Vec1d, Monomials, Monomials> engine(monomials, domain.positions,
                                                  1);
    engine.setupFunctionAt(0., values);
    VecXd expected = {0, -4 / 3., 0, 1 / 3., 0};  //  we get x^3/3 - 4x/3
    // matrix should be:
    //  1 -2  4 -8 16
    //  1 -1  1 -1  1
    //  1  0  0  0  0
    //  1  1  1  1  1
    //  1  2  4  8 16
    VecXd coeff = engine.getCoefficients();
    for (int i = 0; i < monomials; ++i) {
        EXPECT_NEAR(expected[i], coeff[i], 1e-15);
    }
    EXPECT_NEAR(42.47191288906710630331, engine.getConditionNumber(), 1e-13);
}

TEST(MLS, EvaluateAtVectorized) {
    RectangleDomain<Vec1d> domain(Vec1d(-2.), Vec1d(2.));
    domain.positions = {-2., -1., 0., 1., 2.};
    domain.findSupport(5);
    int monomials = 5;
    Range<double> values = {0, 1, 0, -1, 0};
    EngineMLS<Vec1d, Monomials, Monomials> engine(monomials, domain.positions,
                                                  1);
    engine.setupFunctionAt(0., values);
    // interpolacijski polinom je x^3/3 - 4x/3
    Range<Vec1d> test_points = {-1., -0.5, 0., 0.5, 1.};
    Range<double> expected = {1, 0.625, 0, -0.625, -1};
    Range<double> actual = engine.evaluateAt(test_points);
    ASSERT_EQ(expected.size(), actual.size());
    for (int i = 0; i < expected.size(); ++i) {
        EXPECT_NEAR(actual[i], expected[i], 1e-15);
    }
}
// TEST(MLS, 3DTest) {
//     int order = 2;
//     double weight = 1e5;
//     Vec3d point = {25168.4, 5404.54, 14900};
//     // Range<double> values = {11.8119, 11.8116, 11.8116, 11.8113};
//     Range<double> values = {11.8, 11.8, 11.8, 11.8};
//     Range<Vec3d> support = {{25200, 5401, 15000},  {25200, 5400, 14800},
//                             {24900, 5402, 15000},  {24900, 5399, 14800}};
//     support = {{25209.2, 5415.8, 14810.2}, {25209.3, 5415.9, 15010.2},
//                {24909.2, 5415.7, 14810.1}, {24909.3, 5415.8, 15010.1}};
//     support = {{25291, 5258, 14901}, {25292, 5558, 14902},
//                {24991, 5257, 14900}, {24992, 5557, 14901}};
//
//
//     EngineMLS<Vec3d, Monomials, Monomials> mls(order, support, 1);
//     mls.setupFunctionAt(point, values);
//     prn(mls.evaluateAt(point));
// }

}  // namespace mm
