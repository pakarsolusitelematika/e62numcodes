#include "types.hpp"

#include "gtest/gtest.h"

namespace mm {

TEST(Vec, Construct) {
    Vec2d b({2.0, 3.0});
    EXPECT_EQ(2.0, b[0]);
    EXPECT_EQ(3.0, b[1]);

    Vec3d c(5.0);
    EXPECT_EQ(5.0, c[0]);
    EXPECT_EQ(5.0, c[1]);
    EXPECT_EQ(5.0, c[2]);

    Vec3d d;
    d << 1, 5, 7;
    EXPECT_EQ(1, d[0]);
    EXPECT_EQ(5, d[1]);
    EXPECT_EQ(7, d[2]);
}

TEST(Vec, Assign) {
    Vec2d a;
    a = {1, 2};
    EXPECT_DOUBLE_EQ(1.0, a[0]);
    EXPECT_DOUBLE_EQ(2.0, a[1]);

    a = 5.3;
    EXPECT_EQ(5.3, a[0]);
    EXPECT_EQ(5.3, a[1]);

    Vec2d b, c;
    b = a;
    EXPECT_EQ(a[0], b[0]);
    EXPECT_EQ(a[1], b[1]);

    c = std::move(a);
    EXPECT_EQ(b[0], c[0]);
    EXPECT_EQ(b[1], c[1]);

    a[1] = 4;
    EXPECT_EQ(a[1], 4);
}

TEST(Vec, Compare) {
    Vec3d a(0.0);
    Vec3d b(0.0);
    EXPECT_TRUE(a == b);
    EXPECT_TRUE(a >= b);
    EXPECT_TRUE(a <= b);
    EXPECT_FALSE(a != b);
    EXPECT_FALSE(a < b);
    EXPECT_FALSE(a > b);
    b[0] = 1;
    EXPECT_FALSE(a == b);
    EXPECT_FALSE(a >= b);
    EXPECT_TRUE(a <= b);
    EXPECT_TRUE(a != b);
    EXPECT_TRUE(a < b);
    EXPECT_FALSE(a > b);
    a = {1, 1, 0};
    b = {1, 1, 1};
    EXPECT_FALSE(a == b);
    EXPECT_FALSE(a >= b);
    EXPECT_TRUE(a <= b);
    EXPECT_TRUE(a != b);
    EXPECT_TRUE(a < b);
    EXPECT_FALSE(a > b);

    EXPECT_LT(Vec2d({0, 0.5}), Vec2d({0.5, 0}));
    EXPECT_FALSE(Vec2d({0.5, 0}) < Vec2d({0, 0.5}));
    EXPECT_GT(Vec2d({0.5, 0}), Vec2d({0, 0.5}));
    EXPECT_FALSE(Vec2d({0, 0.5}) > Vec2d({0.5, 0}));

    std::vector<Vec2d> expected = {{0, 0},   {0, 0.5}, {0, 1},   {0.5, 0},
                                   {0.5, 1}, {1, 0},   {1, 0.5}, {1, 1}};

    for (size_t i = 0; i < expected.size(); ++i) {
        for (size_t j = i + 1; j < expected.size(); ++j) {
            EXPECT_LT(expected[i], expected[j]) << expected[i].transpose() << " < "
                                                << expected[j].transpose();
            EXPECT_GT(expected[j], expected[i]) << expected[i].transpose() << " > "
                                                << expected[j].transpose();
        }
    }

    std::vector<Vec2d> tosort = expected;
    std::sort(tosort.begin(), tosort.end());
    ASSERT_EQ(expected.size(), tosort.size());
    for (size_t i = 0; i < expected.size(); ++i)
        EXPECT_EQ(expected[i], tosort[i]) << expected[i].transpose()
                                          << " == " << tosort[i].transpose();
}

TEST(Vec, BasicMath) {
    Vec3d a({1, 3, -1.2}), b({1, 0, 2.5});

    EXPECT_EQ(Vec3d({-1, -3, 1.2}), -a);
    EXPECT_EQ(Vec3d({2, 3, 1.3}), a + b);
    EXPECT_EQ(Vec3d({0, 3, -3.7}), a - b);
    EXPECT_EQ(Vec3d({0, -3, 3.7}), b - a);
    Vec3d r = 2.1 * a;
    EXPECT_DOUBLE_EQ(2.1, r[0]);
    EXPECT_DOUBLE_EQ(6.3, r[1]);
    EXPECT_DOUBLE_EQ(-1.2 * 2.1, r[2]);
    r = a * 2.1;
    EXPECT_DOUBLE_EQ(2.1, r[0]);
    EXPECT_DOUBLE_EQ(6.3, r[1]);
    EXPECT_DOUBLE_EQ(-1.2 * 2.1, r[2]);
    r = r / 2.1;
    EXPECT_DOUBLE_EQ(1, r[0]);
    EXPECT_DOUBLE_EQ(3, r[1]);
    EXPECT_DOUBLE_EQ(-1.2, r[2]);
    EXPECT_DOUBLE_EQ(7.25, b.squaredNorm());
    EXPECT_DOUBLE_EQ(2.6925824035672519, b.norm());
    a += b;
    ASSERT_EQ((Vec3d{2, 3, 1.3}), a);
    a *= 2.1;
    EXPECT_DOUBLE_EQ(4.2, a[0]);
    EXPECT_DOUBLE_EQ(6.3, a[1]);
    EXPECT_DOUBLE_EQ(1.3 * 2.1, a[2]);
    a /= 0.5;
    EXPECT_DOUBLE_EQ(8.4, a[0]);
    EXPECT_DOUBLE_EQ(12.6, a[1]);
    EXPECT_DOUBLE_EQ(2 * 1.3 * 2.1, a[2]);
}

TEST(Vec, Perp) {
    Vec2d v({1, 0});
    Vec2d perp = v.getPerpendicular();
    EXPECT_EQ(Vec2d({0, -1}), perp);

    Vec3d v3({4, -7.456, 6.3});
    Vec3d perp3 = v3.getPerpendicular();
    EXPECT_EQ(0, v3.dot(perp3));
}

TEST(Vec, Iterate) {
    Vec3d a(0.0);
    for (auto& x : a) x += 4;
    EXPECT_EQ((Vec3d{4, 4, 4}), a);
}

TEST(VecXd, ConstructAssign) {
    VecXd v(6);
    v << 1, 2, 3, 4, 0, 0;
    EXPECT_EQ(1, v[0]);
    EXPECT_EQ(2, v[1]);
    EXPECT_EQ(3, v[2]);
    EXPECT_EQ(4, v[3]);
    EXPECT_EQ(0, v[4]);
    EXPECT_EQ(0, v[5]);
    v = {1, 2, 3, 4, 5, 6};
    EXPECT_EQ(1, v[0]);
    EXPECT_EQ(2, v[1]);
    EXPECT_EQ(3, v[2]);
    EXPECT_EQ(4, v[3]);
    EXPECT_EQ(5, v[4]);
    EXPECT_EQ(6, v[5]);
    VecXd b({1, 2, -3, 4});
    ASSERT_EQ(4u, b.size());
    EXPECT_EQ(1, b[0]);
    EXPECT_EQ(2, b[1]);
    EXPECT_EQ(-3, b[2]);
    EXPECT_EQ(4, b[3]);

    VecXd c(3ull, 4.0);
    ASSERT_EQ(3u, c.size());
    EXPECT_EQ(4, c[0]);
    EXPECT_EQ(4, c[1]);
    EXPECT_EQ(4, c[2]);

    c = b;
    ASSERT_EQ(4u, c.size());
    EXPECT_EQ(1, c[0]);
    EXPECT_EQ(2, c[1]);
    EXPECT_EQ(-3, c[2]);
    EXPECT_EQ(4, c[3]);

    c = b[{1, 2}];
    ASSERT_EQ(2u, c.size());
    EXPECT_EQ(2, c[0]);
    EXPECT_EQ(-3, c[1]);
}

TEST(VecXd, Access) {
    VecXd v(6);
    v << 1, 2, 3, 4, 0, 0;
    v[v < 3] = 6;
    EXPECT_EQ(6, v[0]);
    EXPECT_EQ(6, v[1]);
    EXPECT_EQ(3, v[2]);
    EXPECT_EQ(4, v[3]);
    EXPECT_EQ(6, v[4]);
    EXPECT_EQ(6, v[5]);
}

TEST(VecXd, Compare) {
    VecX<int> c = {1, 2, 3, 1, 2, 3};
    VecX<int> d = {1, 2, 3};
    EXPECT_EQ((c[{0, 1, 2}]), d);
    EXPECT_NE((c[{1, 2, 0}]), d);
    EXPECT_EQ((c[{0, 1, 2}]), (c[{3, 4, 5}]));
    EXPECT_EQ(d, (c[{0, 1, 2}]));
}

TEST(Vec, DeathTest) {
    Vec3d a;
    EXPECT_DEATH(Vec2d({1, 2, 3}),
                 "Initializer list length must match vector dimension in Vec constructor.");
    EXPECT_DEATH(Vec2d({1}),
                 "Initializer list length must match vector dimension in Vec constructor.");
    EXPECT_DEATH((a = {1, 2}),
                 "Initializer list length must match vector dimension in Vec assignment.");
    EXPECT_DEATH((a = {1, 2, 3, 4}),
                 "Initializer list length must match vector dimension in Vec assignment.");
}

TEST(Vec, Sort) {
    Vec3d a({3, -2, 4});
    std::sort(a.begin(), a.end());
    ASSERT_EQ(3, a.size());
    EXPECT_EQ(-2, a[0]);  // must be exactly equal
    EXPECT_EQ(3, a[1]);
    EXPECT_EQ(4, a[2]);
}

TEST(VecXd, Sort) {
    VecX<double> a({3.2, -2.0, 4.2, 5.6, -9e-1});
    std::sort(a.begin(), a.end());
    ASSERT_EQ(5, a.size());
    EXPECT_EQ(-2.0, a[0]);  // must be exactly equal
    EXPECT_EQ(-9e-1, a[1]);
    EXPECT_EQ(3.2, a[2]);
    EXPECT_EQ(4.2, a[3]);
    EXPECT_EQ(5.6, a[4]);
}

TEST(Eigen, Sort) {
    Eigen::VectorXd x(3);
    x << 2.3, 4.6, -6.7;
    std::sort(begin(x), end(x));
    ASSERT_EQ(3, x.size());
    EXPECT_EQ(-6.7, x[0]);  // must be exactly equal
    EXPECT_EQ(2.3, x[1]);
    EXPECT_EQ(4.6, x[2]);
}

TEST(Range, ConstructAssign) {
    Range<double> a({1, 2, 3.6});
    ASSERT_EQ(3u, a.size());
    EXPECT_EQ(1, a[0]);
    EXPECT_EQ(2, a[1]);
    EXPECT_EQ(3.6, a[2]);
    a = {2.3, -4.1};
    ASSERT_EQ(2u, a.size());
    EXPECT_EQ(2.3, a[0]);
    EXPECT_EQ(-4.1, a[1]);
    a = {3, 4, 5, 6, 7.2};
    Range<double> b(1, 3);
    b = a[{1, 2, 4}];
    ASSERT_EQ(3u, b.size());
    EXPECT_EQ(4, b[0]);
    EXPECT_EQ(5, b[1]);
    EXPECT_EQ(7.2, b[2]);
    Range<int> c(Vec<int, 3>({2, -5, 3}));
    ASSERT_EQ(3u, c.size());
    EXPECT_EQ(2, c[0]);
    EXPECT_EQ(-5, c[1]);
    EXPECT_EQ(3, c[2]);
    Range<double> d(VecXd({-1.5, 3.4, 0.0, 1e6, -12.3}));
    ASSERT_EQ(5u, d.size());
    EXPECT_EQ(-1.5, d[0]);
    EXPECT_EQ(3.4, d[1]);
    EXPECT_EQ(0.0, d[2]);
    EXPECT_EQ(1e6, d[3]);
    EXPECT_EQ(-12.3, d[4]);
}

TEST(Range, Compare) {
    Range<double> a = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0};
    Range<int> result = a > 3.2;
    EXPECT_EQ(Range<int>({3, 4, 5, 6, 7}), result);
    result = a < 3.2;
    EXPECT_EQ(Range<int>({0, 1, 2}), result);
    result = a == 3.2;
    EXPECT_EQ(Range<int>(), result);
    EXPECT_EQ(a, a);

    Range<double> b(std::vector<double>({1, 2, 3}));
    ASSERT_EQ(3u, b.size());
    EXPECT_EQ(1, b[0]);
    EXPECT_EQ(2, b[1]);
    EXPECT_EQ(3, b[2]);

    b = std::vector<double>({1.2, 2.3});
    ASSERT_EQ(2u, b.size());
    EXPECT_DOUBLE_EQ(1.2, b[0]);
    EXPECT_DOUBLE_EQ(2.3, b[1]);

    Range<int> c = {1, 2, 3, 1, 2, 3};
    Range<int> d = {1, 2, 3};
    EXPECT_EQ((c[{0, 1, 2}]), d);
    EXPECT_NE((c[{1, 2, 0}]), d);
    EXPECT_EQ((c[{0, 1, 2}]), (c[{3, 4, 5}]));
    EXPECT_EQ(d, (c[{0, 1, 2}]));
}

TEST(Range, Access) {
    Range<Vec3d> nodes;
    for (double i = 1; i < 10; ++i) nodes.push_back({i, i, i});

    Range<int> rng = {0, 1, 2};
    Range<Vec3d> expected = {{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};
    EXPECT_EQ(expected, nodes[rng]);

    Range<double> test = {2.0, 5.5, -1.2, 4.2};
    EXPECT_EQ(2u, (test[{1, 2}].size()));
    EXPECT_EQ(5.5, (test[{1, 2}][0]));
    EXPECT_EQ(-1.2, (test[{1, 2}][1]));

    nodes[1] = 1;
    EXPECT_EQ(Vec3d(1.0), nodes[1]);
    nodes[1] = {1, 3, 4};
    EXPECT_EQ(Vec3d({1, 3, 4}), nodes[1]);

    nodes[{0, 1, 2}] = Range<Vec3d>{1.0, 2.0, 3.0};
    EXPECT_EQ(Vec3d({1, 1, 1}), nodes[0]);
    EXPECT_EQ(Vec3d({2, 2, 2}), nodes[1]);
    EXPECT_EQ(Vec3d({3, 3, 3}), nodes[2]);

    nodes[{0, 1, 2}] = Vec3d{1.0, 2.0, 3.0};
    EXPECT_EQ(Vec3d({1, 2, 3}), nodes[0]);
    EXPECT_EQ(Vec3d({1, 2, 3}), nodes[1]);
    EXPECT_EQ(Vec3d({1, 2, 3}), nodes[2]);

    nodes[{0, 1, 2}] = Vec3d(10.);
    for (const auto& x : Range<Vec3d>(nodes[{0, 1, 2}])) EXPECT_EQ(Vec3d(10.0), x);

    Range<double> d_range(5, 1);
    d_range = static_cast<Range<double>>(d_range[{0, 2}]);
    ASSERT_EQ(2u, d_range.size());
    EXPECT_EQ(1, d_range[0]);
    EXPECT_EQ(1, d_range[1]);
}

TEST(Range, Math) {
//      Range<double> a = {1.0, -2.2, 3.0}, b = {2.0, 3.0, 4.0};
//      Range<double> result = a + b;
//      EXPECT_DOUBLE_EQ(3, result[0]);
//      EXPECT_DOUBLE_EQ(0.8, result[1]);
//      EXPECT_DOUBLE_EQ(7, result[2]);
//      result = a - b;
//      EXPECT_EQ(-1, result[0]);
//      EXPECT_EQ(-5.2, result[1]);
//      EXPECT_EQ(-1, result[2]);
//      double s = dot(a, b);
//      EXPECT_EQ(1.0 * 2.0 + -2.2 * 3.0 + 3.0 * 4.0, s);
//      s = dot(a, b[{0, 1, 2}]);
//      EXPECT_EQ(1.0 * 2.0 + -2.2 * 3.0 + 3.0 * 4.0, s);
}

TEST(Range, AppendJoin) {
    Range<double> a = {1.0, -2.2, 3.0}, b = {2.0, 3.0, 4.0};
    Range<double> c(b);
    Range<double> d(a);
    a.append(b);
    EXPECT_EQ(Range<double>({1.0, -2.2, 3.0, 2.0, 3.0, 4.0}), a);
    EXPECT_EQ(c, b);
    EXPECT_EQ(Range<double>({1.0, -2.2, 3.0, 2.0, 3.0, 4.0}), join(d, c));
    EXPECT_EQ(Range<double>({1.0, -2.2, 3.0, 2.0, 3.0, 4.0}), d.join(c));
}

TEST(Range, Remove) {
    Range<double> a = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    a[{4, 3, 2, 3}].remove();
    EXPECT_EQ(Range<double>({0, 1, 5, 6, 7, 8}), a);
    a[Range<int>()].remove();
    EXPECT_EQ(Range<double>({0, 1, 5, 6, 7, 8}), a);
    a[{0, 1, 2, 3, 4, 5}].remove();
    EXPECT_EQ(Range<double>(), a);
}

TEST(Range, Filter) {
    Range<double> aa = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0};
    Range<int> ind = aa.filter([](double v) { return v > 2.3 && v < 6.4; });
    EXPECT_EQ(Range<int>({2, 3, 4, 5}), ind);
}

TEST(Range, DeathTest) {
    Range<int> a = {1, 2, 3};
    Range<int> b = {3, 1};
    EXPECT_DEATH((a[{4, 1}]), "One of indexes out of range when using multi-indexed write access.");
    EXPECT_DEATH((a[{4, 1}]) = 5,
                 "One of indexes out of range when using multi-indexed write access.");
    EXPECT_DEATH((static_cast<const Range<int>&>(a)[{4, 1}]),
                  "One of indexes out of range when using multi-indexed read access.");
    EXPECT_DEATH((a[{1, 2}] = {1}),
                 "Initializer list size must match container size in multi-indexed assignment.");
    EXPECT_DEATH((a[{1, 2}] = {1, 2, 3}),
                 "Initializer list size must match container size in multi-indexed assignment.");
    EXPECT_DEATH((a[{1, 2}] = Range<int>({1})),
                 "Container sizes must match in multi-indexed assignment.");
    EXPECT_DEATH((a[{1, 2}] = Range<int>({1, 2, 3})),
                 "Container sizes must match in multi-indexed assignment.");
    EXPECT_DEATH(static_cast<const Range<int>&>(a)[4],
                 "Index out of range when trying to access Range for read.");
    EXPECT_DEATH(a[4], "Index out of range when trying to access Range for write.");
//      EXPECT_DEATH(a + b, "Both operands of range addition must be of the same size.");
//      EXPECT_DEATH(b + a, "Both operands of range addition must be of the same size.");
//      EXPECT_DEATH(a - b, "Both operands of range subtraction must be of the same size.");
//      EXPECT_DEATH(b - a, "Both operands of range subtraction must be of the same size.");
}

TEST(Range, DISABLED_Examples) {
    /// [Vector demo]
    Vec3d a = {1, 2, 3.4}, b = {6, -3, 6};
    // math operators
    a += -2.0 * a + (a.dot(b)) * b + b / b.norm();
    b /= 7.0 * a.maxCoeff();
    b = b.cwiseAbs();  // elementwise abs
    // lexicographical compare
    if (a < b) {
        std::cout << a << std::endl;
    }
    // access
    a[1] = 3.4;
    std::cout << b[0] << std::endl;
    // various constructors and assignments
    b = 4;
    a = std::move(b);
    /// [Vector demo]
    /// [Range demo]
    Range<int> v;
    v.reserve(100);
    for (int i = 0; i < 100; ++i) {
        v.push_back(i);
    }
    v[v.filter([](int x) { return x % 2 == 0; })] = 0;  // set even to 0
    v[v > 80].remove();  // remove greater than 80
    std::cout << v << std::endl;  // cout in Matlab fomat

    Range<int> u(100, 4);
//      auto x = u + v;  // add two ranges of equal length
    v.append(u);  // append one range to another
    std::cout << join(v, u) << std::endl;  // join two ranges and return result
    /// [Range demo]
}

}  // namespace mm
