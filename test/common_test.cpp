#include "common.hpp"  // should not use any other user-defined libraries

#include "gtest/gtest.h"

namespace mm {

TEST(Common, SignumZero) {
    assert(signum(0.0f) == 0);
    assert(signum(0l) == 0);
    assert(signum(0ull) == 0);
    assert(signum(0.0l) == 0);
    assert(signum(0) == 0);
    assert(signum(0.0) == 0);
}

TEST(Common, SignumInfNan) {
    // just to define behaviour
    double nan = std::numeric_limits<double>::quiet_NaN();
    double inf = std::numeric_limits<double>::infinity();
    assert(signum(nan) == 0);
    assert(signum(inf) == 1);
    assert(signum(-inf) == -1);
}

TEST(Common, SignumPositive) {
    assert(signum('a') == 1);
    assert(signum(5.4) == 1);
    assert(signum(1e-7) == 1);
    assert(signum(23452345ull) == 1);
    assert(signum(1e20f) == 1);
    assert(signum(-23452345ull) == 1);
}

TEST(Common, SignumNegative) {
    assert(signum(-0b01001) == -1);
    assert(signum(-001001) == -1);
    assert(signum(-5.4) == -1);
    assert(signum(-1e-7) == -1);
    assert(signum(-1e20f) == -1);
    assert(signum(-23452345ll) == -1);
}

TEST(Random, GetSeed) {
    assert(get_seed() != get_seed());
}

TEST(Print, Format) {
    std::stringstream ss;
    std::vector<int> a = {1, 2, 3};
    print_formatted(a, "{", ", ", "}", ";", ss);
    EXPECT_EQ(std::string("{1, 2, 3};"), ss.str());

    std::stringstream sss;
    std::vector<std::vector<int>> b = {{1, 1}, {-1, 2}, {4, 3}};
    print_formatted(b, "{", ", ", "}", ";", sss);
    EXPECT_EQ(std::string("{{1, 1}, {-1, 2}, {4, 3}};"), sss.str());
}

TEST(Print, Mem2Str) {
    EXPECT_EQ("16 B", mem2str(16));
    EXPECT_EQ("1.6 kB", mem2str(1600));
    EXPECT_EQ("1.7 kB", mem2str(1682));
    EXPECT_EQ("1 MB", mem2str(1024*1024));
    EXPECT_EQ("5.9 GB", mem2str(5.5*1024*1024*1024));
    EXPECT_EQ("5.6 GB", mem2str(5.6*1000*1000*1000));
}

TEST(Text, Format) {
    EXPECT_EQ("test7.txt", format("test%d.txt", 7));
    EXPECT_EQ("test07.txt", format("test%02d.txt", 7));
    EXPECT_EQ("3.141596", format("%.6f", 3.14159625671837));
}

TEST(Text, Split) {
    EXPECT_EQ(std::vector<std::string>({"a", "b", "c"}), split("a,b,c", ','));
    EXPECT_EQ(std::vector<std::string>({"a", "b", "c"}), split("a, b, c", ", "));
    EXPECT_EQ(std::vector<std::string>({"", "", "", " ", ""}), split("aaa a", "a"));
    EXPECT_EQ(std::vector<std::string>({"a", "b", ""}), split("a,b,", ","));
    EXPECT_EQ(std::vector<std::string>({"a", "b", ""}), split("a,,b,,", ",,"));
}

TEST(Text, Join) {
    EXPECT_EQ("ababa", join({"a", "a", "a"}, 'b'));
    EXPECT_EQ("ababa", join({"a", "a", "a"}, "b"));
    EXPECT_EQ("abxabxa", join({"a", "a", "a"}, "bx"));
    EXPECT_EQ("aa", join({"a", "", "a"}, ""));
    EXPECT_EQ("a123123123123a123", join({"a", "", "", "", "a", ""}, "123"));
}

}  // namespace mm
