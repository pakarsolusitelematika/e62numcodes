#!/bin/bash

# this script checks system' configuration

BW='\x1b[37;1m'  # bold white
BR='\x1b[31;1m'  # bold red
BG='\x1b[32;1m'  # bold red
NC='\x1b[37;0m'  # no color

# packages and commands -- default = ubuntu
GRAPHVIZ="graphviz"
BOOST="libboost-dev"
SFML="libsfml-dev"
HDF5="libhdf5-serial-dev"  # libhdf5-cpp-8

IHDF5="-I/usr/include/hdf5/serial"
LBLAS="-lblas"

CHECK_PACKAGE="dpkg -s"
INSTALL_PACKAGE="sudo apt-get install"

if [[ `uname -a` == *ARCH* || `uname -a` == *MANJARO* ]]; then
    echo "Detected Arch-like disto."
    CHECK_PACKAGE="pacman -Q"
    INSTALL_PACKAGE="sudo pacman -S"
    BOOST="boost"
    SFML="sfml"
    HDF5="hdf5-cpp-fortran"
    IHDF5=""
    LBLAS="-lcblas"
fi

TO_INSTALL=""
FAILED=0

function error { >&2 echo -e "${BR}$1${NC}"; }
function showinfo { echo -e "${BW}$1${NC}"; }
function ok { echo -e "${BG}$1${NC}"; }

function check_installed {
    echo "    Checking $1 ..."
    $1 --version > /dev/null
    if [ $? -ne 0 ]; then
        error "You dont have $1 installed."
        TO_INSTALL+=" $1"
        FAILED=1
    fi
}

function check_package {
    for pac in "$@"; do
        echo "    Checking package $pac ..."
        $CHECK_PACKAGE $pac > /dev/null
        if [ $? -ne 0 ]; then
            error "You dont have $pac installed."
            echo -e "Please run\n    $INSTALL_PACKAGE $pac\nto install."
            TO_INSTALL+=" $pac"
            FAILED=1
        fi
    done
}

function has_gxx_version {
    echo "    Checking g++ version is >= 4.8 ..."
    VERSION=$(g++ -dumpversion | sed -e 's/\.\([0-9][0-9]\)/\1/g' -e 's/\.\([0-9]\)/0\1/g' -e 's/^[0-9]\{3,4\}$/&00/')
    if [ 40800 -gt "$VERSION" ]; then
        error "g++ version too old, please install newer version, at least 4.8"
        TO_INSTALL+=' g++-4.8'
        FAILED=1
    fi
}

function does_sfml_compile {
    echo "    Checking graphics support..."
    echo -e "#include <SFML/Graphics.hpp>\nint main() {}" > $TMPFILE
    g++ $TMPFILE -o /dev/null
    if [ $? -ne 0 ]; then
        error "SFML headers not found. Check your include path?
(see 'pkgfile $SFML' or
    pkg-config $SFML --cflags
for dir to include)."
        FAILED=1
        return 0
    fi
    echo -e "int main() {}" > $TMPFILE
    g++ $TMPFILE -o /dev/null -lsfml-window -lsfml-graphics -lsfml-system
    if [ $? -ne 0 ]; then
        error "SFML libs not found. Check your lib path? (see 'pkgfile $SFML' for dir to include)."
        FAILED=1
        return 0
    fi
    echo -e "
        #include <SFML/Graphics.hpp>
        int main() {
            sf::RenderWindow window(sf::VideoMode(200, 200), \"\");
            window.setFramerateLimit(60);
        }" > $TMPFILE
    g++ $TMPFILE -o /dev/null -lsfml-window -lsfml-graphics -lsfml-system
    if [ $? -ne 0 ]; then
        error "Basic SFML example does not compile, looks like you don't have the correct SFML
version installed.Please install version >= 2.0."
        FAILED=1
        return 0
    fi
}

function does_hdf5_compile {
    echo "    Checking HDF5 support..."
    echo -e "#include <hdf5.h>\n#include <H5Cpp.h>\nint main() {}" > $TMPFILE
    g++ $TMPFILE -o /dev/null $IHDF5
    if [ $? -ne 0 ]; then
        error "You don't seem to have hdf5 headers.
(see 'pkgfile $HDF5' or
    pkg-config $HDF5 --cflags
for dir to include)."
        FAILED=1
        return 0
    fi

    echo -e "int main() {}" > $TMPFILE
    g++ $TMPFILE -o /dev/null -L/usr/lib/x86_64-linux-gnu/hdf5/serial/ -lhdf5 -lhdf5_cpp
    if [ $? -ne 0 ]; then
        error "HDF5 libs not found. Check your lib path? (see 'pkgfile $HDF5' for dir to include)."
        echo "Running:
    cd /usr/lib; sudo ln -s x86_64-linux-gnu/hdf5/serial/libhdf5.so;
    cd /usr/lib; sudo ln -s x86_64-linux-gnu/hdf5/serial/libhdf5.a;
may help."
        FAILED=1
        return 0
    fi

    echo -e "#include <hdf5.h>\n#include <H5Cpp.h>\nint main() { hid_t file_id; }" > $TMPFILE
    g++ $TMPFILE -o /dev/null -L/usr/lib/x86_64-linux-gnu/hdf5/serial/ -lhdf5 -lhdf5_cpp $IHDF5
    if [ $? -ne 0 ]; then
        error "Basic HDF5 example does not compile, something went wrong. See compile erros for details."
        FAILED=1
        return 0
    fi
}

cwd=${PWD##*/}
if [ "$cwd" != "e62numcodes" ]; then
    echo -e "${BR}Run from e62numcodes/ directory in this project!${NC}"
    exit 1
fi

showinfo "Checking build tools ..."
check_installed g++
has_gxx_version
check_installed cmake
check_installed make
check_installed doxygen

showinfo "Checking package dependencies ..."
check_package $GRAPHVIZ
check_package $BOOST
check_package $SFML
check_package $HDF5
if [ $FAILED -eq 1 ]; then
    error "Missing packages!"
    echo -e "Run"
    echo "    $INSTALL_PACKAGE $TO_INSTALL"
    exit 1
fi

showinfo "Checking includes and links ..."
TMPFILE=`mktemp --suffix=.cpp`
does_sfml_compile
does_hdf5_compile
rm $TMPFILE

showinfo "Check libs ..."

if [ $FAILED -eq 0 ]; then
    ok "Configuring done."
else
    exit 1
fi
