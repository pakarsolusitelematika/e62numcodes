#!/bin/bash

# This script creates static wiki copy by scraping wiki itself.
echo "This will backup wiki as static files. The static backup will be unavailable during execution."
read -n 1 -p "This script takes ~1 minute. Press any key to continue or CTRL-C to abort."
ssh e6.ijs.si << CODE
cd /var/www/html/ParallelAndDistributedSystems/MeshlessMachine/
mkdir -p wiki_static
cd wiki_static
mv www-e6.ijs.si www-e6.ijs.si.old
wget --recursive --page-requisites --html-extension       --convert-links --no-parent  \
     -R "*Special*" -R "*action=*"       -R "*printable=*"  -R "*oldid=*" -R "*title=Talk:*" \
     -R "*limit=*" "http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/index.php"
rm -r www-e6.ijs.si.old
echo
echo
echo -e "\e[1mGo to http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki_static/ to see results!\e[0m"
echo
echo
CODE
