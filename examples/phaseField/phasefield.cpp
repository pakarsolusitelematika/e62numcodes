#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"
#include "mlsm_operators.hpp"
#include <omp.h>
#include "overseer2.hpp"

using namespace mm;

typedef Vec2d vec_t;
typedef double scal_t;

Overseer<scal_t, vec_t> O;

int main(int arg_num, char *arg[]) {
    O.timings.addCheckPoint("big bang");
    O(arg_num,arg);

    /// [DOMAIN INIT]
    O.timings.addCheckPoint("domain");
    RectangleDomain<vec_t> domain(O.domain_lo, O.domain_hi); //domain definition

    domain.fillUniformInteriorWithStep(O.d_space);
    domain.fillUniformBoundaryWithStep(O.d_space);

    domain.findSupport(O.n);

    Range<int> all = domain.types.filter([](vec_t p){return true;});
    Range<int> interior = domain.types == INTERNAL;
    Range<int> boundary = domain.types == BOUNDARY;

    Range<int> left  = domain.positions.filter([](vec_t p)->bool{return p[0]==0.0;});
    Range<int> right = domain.positions.filter([](vec_t p)->bool{return p[0]==O.domain_hi[0];});
    Range<int> mid_circ = domain.positions.filter([](vec_t p)->bool{return p.norm()<0.4;});

    Range<int> mid_plane = domain.positions.filter([](vec_t p)->bool{return p[1]==0.5;});
    prn(mid_plane.size());

    /// [MODEL INITIALIZATION]
    //init state
    Range<scal_t> Phi1(domain.size(),-1);
    Range<scal_t> Phi2(domain.size(),-1);
    Range<vec_t>  n(domain.size(),0);


    Phi1[mid_circ]=1;
    ///boundary conditions
    Phi1[boundary]=-1;
    Phi2=Phi1;
    /// [PROBLEM SOLUTION::explicit]
    O.out_file << "Phi=[];\n";
    O.out_file << "time=[];\n";
    O.out_file << "n=[];\n";


    ///[MLS INIT]
    O.timings.addCheckPoint("mls");

    //Gaussian Basis
    //EngineMLS<vec_t, Gaussians, Gaussians>
    //    mls({O.sigmaB,O.m}, domain.positions[domain.support[0]], O.sigmaW);
    //Monomial Basis

    // consider local normalization of weight functions -- mlsm.hpp << mls = new(approx_engine(new_values))
    EngineMLS<vec_t, Monomials, Gaussians> mls(
            O.m, domain.positions[domain.support[0]], O.sigmaW * domain.characteristicDistance());

    prn(mls);

    auto op = make_mlsm(domain, mls,all);


    O.timings.addCheckPoint("time loop");
    for (int tt = 0; tt <=O.t_steps; ++tt) {
        int i;

        #pragma omp parallel for private(i) schedule(static)
        for (i=0;i<interior.size();++i) {
            int c=interior[i];
            if(std::abs(Phi1[c])>0.95) {n[c]=0;continue;}
            n[c] =  op.grad(Phi1,c).transpose();
            n[c] =  n[c]/n[c].norm();
        }

        #pragma omp parallel for private(i) schedule(static)
        for (i=0;i<interior.size();++i) {
            int c=interior[i];
            Phi2[c] = Phi1[c] + O.dt * ( O.c1*op.lap(Phi1, c));
         }
        Phi1.swap(Phi2);


        /// [intermediate IO]
        if(tt % (1+O.t_steps/O.out_no) == 0 || tt == O.t_steps ){
            std::cout.setf( std::ios::fixed, std:: ios::floatfield);

            Range<scal_t> v_i;
           // for(auto c:interior) v_i.push_back(u3[c].norm());

            std::cout   << "step no:" << tt << "\t"
                        << std::setprecision(1)<<(double)tt/O.t_steps*100 << "% "
            //            << std::setprecision(4)<< "max. displacement:" <<
                        << "\n";


            O.out_file << "time=[time;" << tt*O.dt << "];\n";
            O.out_file << "Phi(:,"<<O.out_recordI+1<<")=" << Phi2 <<";\n";
            O.out_file << "n(:,:,"<<O.out_recordI+1<<")=" << n <<";\n";
            O.out_recordI++;
        }
    }

    O.timings.addCheckPoint("final IO");
    /// [FINAL OUTPUTS]
    O.out_file << "domain.d_space=" << O.d_space << ";\n";
    O.out_file << "domain.int=" << interior << ";\n";
    O.out_file << "domain.lo=" << std::get<0>(domain.getBBox()) << ";\n";
    O.out_file << "domain.hi=" << std::get<1>(domain.getBBox()) << ";\n";
    O.out_file << "pos=" << domain.positions << ";\n";
    O.out_file << "types=" << domain.types << ";\n";

    O.timings.addCheckPoint("the end");
    O.timings.showTimings();
}
