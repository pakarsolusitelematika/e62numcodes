/* 
[Implicit solution of a diffusion convection equation with a use of MLSM operators 
with a mixed boundary conditions -- check wiki for more details 
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Analysis_of_MLSM_performance
]

The equation we would like to solve is
\[
    \alpha \b{\nabla}^2 \phi - \b{u}\cdot \b{\nabla}\phi = 0
\]
*/

#include <Eigen/Sparse>
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"

using namespace Eigen;
using namespace mm;

int main() {

    // open data file
    std::ofstream out_file("domain_data0.01.m");

    Vec<int, 2> domain_size({100, 100}); 
    // set support size and basis functions
    int n = 12;  // support size
    Monomials<Vec2d> basis({{0,0},{1,0},{0,1},{2,0},{0,2}});

    double alpha = 0.01;

    // prepare domain
    RectangleDomain<Vec2d> domain({0,0},{1,1});
    domain.fillUniformBoundary(domain_size);
    domain.fillUniformInterior(domain_size - domain_size.Constant(2));
    domain.findSupport(n);

    // choose sigma for weighting function
    double sigma = std::pow(domain.characteristicDistance(),2);

    // Upper side has value zero
    auto up = domain.positions.filter([](Vec2d p)->bool{
        return (p[1] == 1.0 && p[0] != 1.0);});
    // Right side has zero-normal derivative (outflow)
    auto right = domain.positions.filter([](Vec2d p)->bool{
        return (p[0] == 1.0 && p[1] != 0.0);});
    // Down side has zero normal derivative (symmetry)
    auto down = domain.positions.filter([](Vec2d p)->bool{
        return (p[1] == 0.0 && p[0] != 0.0);});
    // Left side has linear profile from 0 to 1
    auto left = domain.positions.filter([](Vec2d p)->bool{
        return (p[0] == 0.0 && p[1] != 1.0);});

    // Internal values are determined with diffusion equation
    auto internal = domain.types == INTERNAL;

    Range<Vec2d> uv(domain.size());
    for (int c=0; c < domain.size(); c++){
        Vec2d point = domain.positions[c];
        uv[c] = Vec2d({point[0],-point[1]});
    }

    // initialize unknown concentration field and right-hand side
    VecXd phi(domain.size(),1);
    VecXd RHS(domain.size(),1);

    // zero source term
    RHS[internal] = 0.0;

    // dirichlet boundaries
    RHS[up] = 0.0;
    for (auto& c : left){
        Vec2d point = domain.positions[c];
        RHS[c] = (1.0 - point[1]);
    }

    // neumann boundaries
    RHS[down] = 0.0;
    RHS[right] = 0.0;


    // prep shape funcs
    std::vector<Triplet<double>> laplace;
    std::vector<Triplet<double>> grad;

    for (auto& c : internal) {
        Range<Vec2d> supp_domain = domain.positions[domain.support[c]];
        EngineMLS<Vec2d, Monomials, Gaussians> MLS(basis, supp_domain, sigma);
        VecXd shape_laplace = MLS.getShapeAt(supp_domain[0], {{2, 0}}) +
                      MLS.getShapeAt(supp_domain[0], {{0, 2}});
        VecXd shape_gradx = MLS.getShapeAt(supp_domain[0],{1,0});
        VecXd shape_grady = MLS.getShapeAt(supp_domain[0],{0,1});
        for (int i = 0; i < supp_domain.size(); ++i) {
            laplace.emplace_back(c, domain.support[c][i], shape_laplace[i]);
            grad.emplace_back(c, domain.support[c][i], uv[c][0]*shape_gradx[i] + uv[c][1]*shape_grady[i]);
        }
    }

    std::vector<Triplet<double>> right_gradx;
    for (auto& c : right){
        Range<Vec2d> supp_domain = domain.positions[domain.support[c]];
        EngineMLS<Vec2d, Monomials, Gaussians> MLS(basis, supp_domain, sigma);
        VecXd shape_gradx = MLS.getShapeAt(supp_domain[0],{1,0});
        for (int i = 0; i < supp_domain.size(); ++i) {
            right_gradx.emplace_back(c, domain.support[c][i], shape_gradx[i]);
        }        
    }

    std::vector<Triplet<double>> down_grady;
    for (auto& c : down){
        Range<Vec2d> supp_domain = domain.positions[domain.support[c]];
        EngineMLS<Vec2d, Monomials, Gaussians> MLS(basis, supp_domain, sigma);
        VecXd shape_grady = MLS.getShapeAt(supp_domain[0],{0,1});
        for (int i = 0; i < supp_domain.size(); ++i) {
            down_grady.emplace_back(c, domain.support[c][i], shape_grady[i]);
        }        
    }

    // prepare dirichlet boundaries
    std::vector<Triplet<double>> dirichlet;
    for (auto& c : left) {
        dirichlet.emplace_back(c, c, 1.0);
    }
    for (auto& c : up) {
        dirichlet.emplace_back(c, c, 1.0);
    }

    // prepare matrices
    SparseMatrix<double> laplace_m(domain.size(), domain.size());
    laplace_m.setFromTriplets(laplace.begin(), laplace.end());
    SparseMatrix<double> grad_m(domain.size(), domain.size());
    grad_m.setFromTriplets(grad.begin(), grad.end());
    SparseMatrix<double> dirichlet_m(domain.size(), domain.size());
    dirichlet_m.setFromTriplets(dirichlet.begin(), dirichlet.end());
    SparseMatrix<double> right_m(domain.size(), domain.size());
    right_m.setFromTriplets(right_gradx.begin(), right_gradx.end());
    SparseMatrix<double> down_m(domain.size(), domain.size());
    down_m.setFromTriplets(down_grady.begin(), down_grady.end());

    // draw
    std::thread th([&] { draw2D(domain.positions, phi); });

    Eigen::BiCGSTAB<SparseMatrix<double>> solver;
    SparseMatrix<double> tmp = dirichlet_m + alpha*laplace_m - grad_m + right_m + down_m;
    solver.compute(tmp);

    phi = solver.solve(RHS);
    std::cout << "#iterations:     " << solver.iterations() << std::endl;
    std::cout << "estimated error: " << solver.error()      << std::endl;


    th.join();  // end drawing

    /// [Diffusion explicit]
    out_file << "domain.size=" << domain_size << ";\n";
    out_file << "domain.int=" << internal << ";\n";
    out_file << "domain.lo=" << std::get<0>(domain.getBBox()) << ";\n";
    out_file << "domain.hi=" << std::get<1>(domain.getBBox()) << ";\n";
    out_file << "pos=" << domain.positions << ";\n";
    out_file << "types=" << domain.types << ";\n";
    out_file << "phi=" << phi << ";\n";
    // out_file << "supp=" << domain.support[240] << ";\n";
    out_file.close();

    return 0;
}
