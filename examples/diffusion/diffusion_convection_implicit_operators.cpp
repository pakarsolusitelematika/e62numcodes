/*
[Implicit solution of a diffusion convection equation with a use of MLSM operators
with a mixed boundary conditions -- check wiki for more details
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Analysis_of_MLSM_performance
]

The equation we would like to solve is
\[
    \alpha \b{\nabla}^2 \phi - \b{u}\cdot \b{\nabla}\phi = 0
\]
*/

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"
#include "mlsm_operators.hpp"

using namespace mm;

int main() {

    // [Inverse Peclet number]
    double alpha = 0.001;

    // [DOMAIN SIZE]
    Vec<int, 2> domain_size({300, 300});

    // [SUPPORT SIZE]
    int n = 10;

    // [Build domain]
    RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
    domain.fillUniformBoundary(domain_size);
    domain.fillUniformInterior(domain_size - domain_size.Constant(2));
    domain.findSupport(n);
    int N = domain.size();

    // [Filter the nodes into subgroups]
    // Upper side has value zero
    auto top = domain.positions.filter([](Vec2d p)->bool{
        return (p[1] == 1.0 && p[0] != 1.0);});
    // Right side has zero-normal derivative (outflow)
    auto right = domain.positions.filter([](Vec2d p)->bool{
        return (p[0] == 1.0 && p[1] != 0.0);});
    // Down side has zero normal derivative (symmetry)
    auto bottom = domain.positions.filter([](Vec2d p)->bool{
        return (p[1] == 0.0 && p[0] != 0.0);});
    // Left side has linear profile from 0 to 1
    auto left = domain.positions.filter([](Vec2d p)->bool{
        return (p[0] == 0.0 && p[1] != 1.0);});

    auto internal = domain.types == INTERNAL;

    /// [MLSM DEFINITION]
    // choose sigma for weighting function
    double sigma = domain.characteristicDistance();
    Monomials<Vec2d> basis({{0,0},{1,0},{0,1},{2,0},{0,2}});
    EngineMLS<Vec2d, Monomials, NNGaussians> mls(
            basis,  // basis functions
            domain.positions[domain.support[0]],
            sigma);

    /// [CONSTRUCT OPERATORS]
    auto op = make_mlsm(domain, mls, domain.types != 0);
    prn(mls.getConditionNumber());

    /// [FLOW FIELD]
    Range<Vec2d> u(N);
    for (int i = 0; i < N; i++){
        Vec2d point = domain.positions[i];
        u[i] = -Vec2d({point[0],-point[1]});
    }

    /// [Field declarations]
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    M.reserve(Range<int>(N, n));
    Eigen::VectorXd rhs(N);
    Eigen::VectorXd phi(N);

    /// [Fill matrix]
    for (int i : internal) {
        op.lap(M, i, alpha); // laplace phi
        op.grad(M, i, u); // u grad(phi)
        rhs(i) = 0;
    }

    // phi = 0
    for (int i : top) {
        M.coeffRef(i, i) = 1;
        rhs(i) = 0;
    }

    // phi = 1 - y
    for (int i : left) {
        M.coeffRef(i, i) = 1;
        rhs(i) = 1 - domain.positions[i][1];
    }

    // symmetry \partial_y u = 0
    for(int i : bottom) {
        op.der1(M, 0, 1, i, 1);
        rhs(i) = 0;
    }

    // outflow \partial_x u = 0
    for (int i : right) {
        op.der1(M, 0, 0, i, 1);
        rhs(i) = 0;

    }

    // Sparse solver
    Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor>> solver;
    solver.compute(M);

    /// [Solve System]
    phi = solver.solve(rhs);

    std::cout << "#iterations:     " << solver.iterations() << std::endl;
    std::cout << "estimated error: " << solver.error()      << std::endl;

    // draw
    std::thread th([&] { draw2D(domain.positions, phi); });
    th.join();  // end drawing

    // open data file
    std::ofstream out_file("diff_conv.m");

    /// [Diffusion convection implicit]
    out_file << "domain.size=" << domain_size << ";\n";
    out_file << "domain.int=" << internal << ";\n";
    out_file << "domain.lo=" << std::get<0>(domain.getBBox()) << ";\n";
    out_file << "domain.hi=" << std::get<1>(domain.getBBox()) << ";\n";
    out_file << "pos=" << domain.positions << ";\n";
    out_file << "types=" << domain.types << ";\n";
    out_file << "phi=" << phi << ";\n";
    // out_file << "supp=" << domain.support[240] << ";\n";
    out_file.close();

    return 0;
}
