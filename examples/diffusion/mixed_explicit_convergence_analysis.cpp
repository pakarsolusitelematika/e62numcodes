/* 
[Convergence analysis of explicit solution of a diffusion equation 
with a Dirichlet boundary conditions -- check wiki for more details
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Analysis_of_MLSM_performance
]

// This file contains only 1/4 of the whole solution. Other 3/4 can be
// obtained by translation and rotation of the system.

// To satisfy Dirichlet boundary conditions on the whole domain (i.e.)
// we employ Neumann boundary conditions at (x=0, y) and (x, y=0.5)
// and normal dirichlet bc on other two boundary axes.

The equation we would like to solve is
\[
    \nabla^2 T  = \frac{\partial T}{\partial t}
\]
*/

#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"
#include "mlsm_operators.hpp"

using namespace mm;

const double PI = 3.141592653589793238463;

/**
 * @brief Closed form solution of diffusion equation
 * @param pos spatial coordinate
 * @param t time
 * @param a size of domain
 * @param D diffusion constant
 * @param N no. of expansion
 * @return value of temperature
 */

double diff_closedform(const Vec2d& pos, double t, double a, double D, size_t N) {
    double T = 0;
    double f = M_PI / a;
    for (size_t n = 1; n < N; n = n + 2) {
        for (size_t m = 1; m < N; m = m + 2) {
            T += 16.0 / f / f / (n * m) * std::sin(n * f * pos[0]) * std::sin(m * f * pos[1]) *
                 std::exp(-D * t * ((n * n + m * m) * f * f));
        }
    }
    return T;
}

int main() {
    
    std::ofstream out_file("../post_process/explicit_mixed_convergence_dt_1e-6.dat");
    std::ofstream out_file2("../post_process/explicit_mixed_plot_data.dat");   
    /// [Diffusion explicit convergence analysis]
    double err;
    for (int size = 20; size<=150; size+=10) {
        Vec<int, 2> domain_size({size, size});  // domain size
        size_t n = 12;  // support size
        size_t m = 3;  // monomial basis of second order, i.e. 4 monomials
        double time = 0.01;  // time
        double dt = 1e-6;  // time step
        size_t t_steps = std::floor(time / dt);
        // prep domain
        RectangleDomain<Vec2d> domain({0, 0}, {0.5, 0.5});
        domain.fillUniformBoundary(domain_size);
        domain.fillUniformInterior(domain_size - domain_size.Constant(2));
        domain.findSupport(n);
        std::cout<<"characteristic distance: "
                 << domain.characteristicDistance()<<std::endl;
        double sigma = 0.5*domain.characteristicDistance();
        Range<int> interior = (domain.types == INTERNAL);
        // init state
        VecXd T1(domain.size(), 1);
        VecXd T2(domain.size(), 1);

        Range<int> boundary = (domain.types==BOUNDARY);
        Range<int> neumann_boundary = domain.positions.filter([](Vec2d p){
                return (p[0] < 1e-10 || p[1] > 0.5-1e-10) &&
                !((p[0]<1e-10 && p[1]<1e-10) ||
                  (p[0]>0.5-1e-10 && p[1]>0.5-1e-10)); });
        
        T2[domain.types == BOUNDARY] = 0;  //  Dirichlet bc
        T2[neumann_boundary] = 1.0;  //  Neumann bc
        T1 = T2;

        EngineMLS<Vec2d, Monomials, NNGaussians>
            MLS(m,  domain.positions[ domain.support[0] ]  , sigma);
        MLSM< RectangleDomain<Vec2d>, EngineMLS<Vec2d, Monomials, NNGaussians>,
              mlsm::d1 | mlsm::lap > op(domain, MLS, domain.types!=0 , true );

        //setting correct normals for derivation
        std::map<int, Vec2d> normals;
        for (auto& c: neumann_boundary){
            double x=  domain.positions[c][0] ;
            double y= domain.positions[c][1];
            double sqrt2=std::sqrt(2)/2.0;
            std::cout<<std::setprecision(16);
            if (x<1e-10) {
                if (y>0.5-1e-10) normals[c] = Vec2d({sqrt2,-sqrt2});
                else normals[c] = Vec2d({1,0});
            }
            else if (y > 0.5-1e-10) {
                normals[c] = Vec2d({0, -1.0});
            }
        }  
    
        //  time stepping
        for (size_t tt = 0; tt <  t_steps; ++tt) {
            //  new temp.
            for (auto& c : interior) {
                T2[c] = T1[c] + dt* op.lap(T1, c);
            }
            
            for (int& c: neumann_boundary) {
                T2[c] = op.neumann( T2  , c , normals[c]    , 0.0);
            }            
            //  compute error
            if (tt % 100 == 0) {
                Range<double> E2(domain.size(), 0);
                for (auto& c : interior) {
                    // we add {0.5,0} in the line below because we have to
                    // move the solution to be able to compare it to
                    // analytical solution. Analytical solution is made
                    // for [0,1]^2 with Dirichlet bc.
                    E2[c] = std::abs
                        (T2[c] - diff_closedform
                         (domain.positions[c]+ Vec2d({0.5,0}), tt*dt, 1, 1, 100
                          )
                         );
                }
                std::cout << "Max error:" <<
                    *std::max_element(E2.begin(), E2.end()) << "\n";
                err= *std::max_element(E2.begin(), E2.end());
            }
            // time advance
            T1 = T2;
        }
        if (size==60) {
            out_file2 <<domain.positions<<"\n"<<std::endl;
            out_file2 <<T2<<std::endl;
        }
        out_file << size <<" "<< err << std::endl;
    }
    out_file2.close();
    out_file.close();
    return 0;
}
