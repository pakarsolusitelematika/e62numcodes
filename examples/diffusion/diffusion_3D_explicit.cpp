/* 
[Explicit solution of a 3D diffusion equation with a use of MLSM operators 
with a mixed boundary conditions -- check wiki for more details 
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Analysis_of_MLSM_performance
]

The equation we would like to solve is
\[
    \nabla^2 T  = \frac{\partial T}{\partial t}
\]
*/

#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"

using namespace mm;


int main() {
    Vec<int, 3> domain_size(20);  // domain size
    int n = 10;  // support size
//      size_t m = 3;  // monomials 1, x, y, x^2, xy, y^2
    int m = 10;  // monomials 1, x, y, x^2, xy, y^2
    double time = 0.01;  // time
    double dt = 1e-5;  // time step

    int tsteps = std::floor(time / dt);
    double sigma = 0.01 * 0.05;

    // prep domain
    RectangleDomain<Vec3d> domain({0, 0, 0}, {1, 1, 1});
    domain.fillUniformBoundary(domain_size);
    domain.fillUniformInterior(domain_size - domain_size.Constant(2));
//      domain.relax(10, 1e-2, 1.0, 3, 500);
    domain.findSupport(n);

    Range<int> interior = domain.types == INTERNAL;
    Range<VecXd> shape_laplace(domain.size());

    // init state
    VecXd T1(domain.size(), 1);
    VecXd T2(domain.size(), 1);
    T2[domain.types == BOUNDARY] = 0;
    T1 = T2;

    // prep shape funcs
    for (auto& c : interior) {
        Range<Vec3d> supp_domain = domain.positions[domain.support[c]];
        EngineMLS<Vec3d, Gaussians, Gaussians> MLS({60*0.05, m}, supp_domain, sigma);
        shape_laplace[c] = MLS.getShapeAt(supp_domain[0], {2, 0, 0}) +
                           MLS.getShapeAt(supp_domain[0], {0, 2, 0}) +
                           MLS.getShapeAt(supp_domain[0], {0, 0, 2});
    }

    // draw
    //std::thread th([&] { draw2D(domain.positions, T1); });

    for (int tt = 0; tt < tsteps; ++tt) {
        // new temp.
        for (auto& c : interior) {
            double Lap = 0;
            for (int i = 0; i < n; ++i)
                Lap += shape_laplace[c][i] * T1[domain.support[c]][i];
            T2[c] = T1[c] + dt * Lap;
        }

        T1 = T2;

    }

    std::cout << "pos = "; print_formatted(domain.positions) << std::endl;
    std::cout << "T = "; print_formatted(T1) << std::endl;

    //th.join();  // end drawing

    return 0;
}
