/* 
[Implicit solution of a diffusion equation 
with a Dirichlet boundary conditions. This version is solved using build-in operators to set matrix elements.
There is another version which uses direct matrix element setting. -- check wiki for more details
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Analysis_of_MLSM_performance
]

This file contains only 1/4 of the whole solution. Other 3/4 can be
obtained by translation and rotation of the system.

To satisfy Dirichlet boundary conditions on the whole domain (i.e. [0,1]x[0,1])
we employ Neumann boundary conditions at (x=0, y) and (x, y=0.5)
and normal dirichlet bc on other two boundary axes.

The equation we would like to solve is
\[
    \nabla^2 T  = \frac{\partial T}{\partial t}
\]
*/

#include <Eigen/Sparse>
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"
#include <mlsm_operators.hpp>

using namespace Eigen;
using namespace mm;

const double PI  =3.141592653589793238463;

/**
 * @brief Closed form solution of diffusion equation
 * @param pos spatial coordinate
 * @param t time
 * @param a size of domain
 * @param D diffusion constant
 * @param N no. of expansion
 * @return value of temperature
 */

double diff_closedform(const Vec2d& pos, double t, double a, double D, size_t N) {
    double T = 0;
    double f = M_PI / a;
    for (size_t n = 1; n < N; n = n + 2) {
        for (size_t m = 1; m < N; m = m + 2) {
            T += 16.0 / f / f / (n * m) * std::sin(n * f * pos[0]) * std::sin(m * f * pos[1]) *
                std::exp(-D * t * ((n * n + m * m) * f * f));
        }
    }
    return T;
}


int main() {

   ///  [Diffusion mixed implicit operator version]
    Vec<int, 2> domain_size({50, 50}); 
    //  set support size and basis functions
    int n = 12;  // support size
    //  monomial base
    Monomials<Vec2d> basis({{0,0},{1,0},{0,1},{1,1},{2,0},{0,2}});
    //  time step
    double dt=1e-5;
    //  maximum time
    double time = 0.01;
    //  number of time steps
    size_t t_steps = std::floor(time / dt);
    //  prepare domain
    RectangleDomain<Vec2d> domain({0,0},{0.5,0.5});
    domain.fillUniformBoundary(domain_size);
    domain.fillUniformInterior(domain_size - domain_size.Constant(2));
    domain.findSupport(n);
    std::cout<<domain.size()<<std::endl;

    //  choose sigma for weighting function
    double sigma = std::pow(domain.characteristicDistance(), 2)/2.0;

    //  Upper side has value zero
    Range<int> up = domain.positions.filter([](Vec2d p)->bool{
            return (p[1] > 0.5-(1e-10) && p[0] > 1e-10 && p[0] < (0.5-1e-10));});
    //  Right side has zero-normal derivative (outflow)
    Range<int> right = domain.positions.filter([](Vec2d p)->bool{
            return (p[0] > 0.5-(1e-10) && p[1] > 1e-10);});
    //  Down side has zero normal derivative (symmetry)
    Range<int> down = domain.positions.filter([](Vec2d p)->bool{
            return (p[1] < 1e-10);});
    //  Left side has linear profile from 0 to 1
    Range<int> left = domain.positions.filter([](Vec2d p)->bool{
            return (p[0] < 1e-10 && p[1] > 1e-10   && p[1]< 0.5-(1e-10) );});
    Range<int> corner_point = domain.positions.filter( [](Vec2d p){return (p[0] < (1e-10) && p[1] > 0.5-1e-10);});
    
    //  Internal values are determined with diffusion equation
    Range<int> internal = domain.types == INTERNAL;
    Range<int> boundary = domain.types == BOUNDARY;

    //  initialize time stepping vectors
    VecXd RHS(domain.size(), 1);
    VecXd T2(domain.size(), 1);
    //  RHS has all zero on the boundary because of dirichlet boundary condition
    //  and because we set derivative to
    //  zero (derivative=neumann boundary condition)
    RHS[boundary] = 0.0;
    //  dirichlet boundary conditions
    T2[down] = 0.0;
    T2[right] = 0.0;
    //  neumann boundary conditions
    T2[up] = 1.0;
    T2[left] = 1.0;
    T2[corner_point]=1.0;

    Eigen::SparseMatrix<double, Eigen::RowMajor> M(domain.size(), domain.size());
    M.reserve(Range<int>(domain.size(), n ));
    
    EngineMLS< Vec2d , Monomials, Gaussians>
        MLS(basis, domain.positions[domain.support[0]], sigma);
    MLSM< RectangleDomain<Vec2d> , EngineMLS<Vec2d, Monomials, Gaussians>,
          mlsm::d1 | mlsm::lap > op(domain, MLS, domain.types!=0, true);
    
    //  interior shape functions -> laplace;
    //  save them as a triplet (i,j, value)
    //  i and j represent row and column of an element in the global matrix 
    for (auto& c : internal) {
        op.lap(M, c, -dt );
        M.coeffRef(c,c)+=1.0;
    }
    
    //  nastavljanje gradientov -> neumannovih robnih pogojev
    std::vector<Triplet<double>> left_gradx;
    for (auto& c : left){
        op.der1(M, 0, 0, c , 1.0);     
    }

    for (auto& c : up){
        op.der1(M, 0, 1, c, 1.0);     
    }
        
    //  setting corner point (i.e. [0,0.5])
    {
        int c = corner_point[0];
        op.der1(M, 0, 1, c, 1.0);  
        op.der1(M, 0, 0, c, 1.0);  
    }
    //  setting dirichlet boundary condition
    Range<Triplet<double>> dirichlet;
    for (auto& c : right) {
        M.coeffRef(c, c)+=1.0;
    }
    for (auto& c : down) {
        M.coeffRef(c, c)+=1.0;
    }
    
    //  sum all the matrices and prepare solver
    Eigen::BiCGSTAB<SparseMatrix<double>> solver;
    solver.compute(M);

    std::thread th([&] {draw2D(domain.positions, T2);});
    double max_error=0;
    for(size_t i=0;i<1000 ;i++){
        T2 = solver.solve(RHS);
        RHS=T2;
        //  setting rhs boundary again because we
        //  have to maintain boundary conditions
        RHS[boundary] = 0.0;
        if (i % 100 == 0) {
            Range<double> E2(domain.size(), 0);
            for (auto& c : internal) {
                //  we add {0.5,0} in the line below because we have to
                //  move the solution to be able to compare it to
                //  analytical solution. Analytical solution is made
                //  for [0,1]^2 with Dirichlet bc.
                E2[c] = std::abs
                    (T2[c] - diff_closedform
                     (domain.positions[c]+ Vec2d({0.5,0})  , i * dt, 1, 1, 100));
            }
            max_error= *std::max_element(E2.begin(), E2.end());
            std::cout << "Max error:" << max_error << "\n";
        }
    }
    th.join();  //  end drawing
    return 0;
}
