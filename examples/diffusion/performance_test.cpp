/*
 * This cpp provides execution tests for several different modes. The main focus in on the
 * analysis of data container impact on the performance. From the results it can be clearly
 * seen that using inappropriate types might result in serious slowdown.
*/
#include "common.hpp"
#include "draw.hpp"
#include "util.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"
#include <omp.h>
#include "mlsm_operators.hpp"

using namespace mm;
using namespace std::literals;


int LABEL_ID;
const std::vector<std::pair<std::string, std::string>> labels = {
    {"Start our types"      , "End our types"}, 	//Range class
    {"Start operators"      , "End operators"},		//MLSM class
    {"Start Eigen"          , "End Eigen"},
    {"Start C array"        , "End C array"},
    {"Start std::vector"    , "End std::vector"},
    {"Start std::vector 1D" , "End std::vector 1D"},
    {"Start our types 1D"   , "End our types 1D"}};

/* used globals */
int WIDTH;
Vec<int, 2> domain_size;  // domain size
int N;
int n;  // support size
int m;
double endtime = 0.2;  // time
double dt = 1e-5;  // time step
int t_steps; // number of steps
double sigma; // naive normalization

// prep domain
RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
Range<VecXd> shape_laplace(N);
Range<double> TEMP(N);
Range<int> interior;

Monitor monitor;


// RANGES //
void test_our_types() {
    std::string startlabel, endlabel;
    std::tie(startlabel, endlabel) = labels[LABEL_ID++];
    std::cerr << startlabel << std::endl;

    // init state
    VecXd T1(domain.size(), 1);
    VecXd T2(domain.size(), 1);

    // dirichlet BCs
    T2[domain.types == BOUNDARY] = 0;
    T1 = T2;

    Range<VecXd> Tshape_laplace(N, VecXd(n));
    Range<Range<int>> SD(N, Range<int>(n));
    for (int c : interior) {
        for (int j = 0; j < n; ++j) {
            SD[c][j] = domain.support[c][j];
            Tshape_laplace[c][j] = shape_laplace[c][j];
        }
    }
    // time stepping
    monitor.addCheckPoint(startlabel);
    for (int tt = 0; tt < t_steps; ++tt) {
        int i;
        for (i = 0; i < static_cast<int>(interior.size()); ++i) {
            int c = interior[i];
            double Lap = 0;
            for (int j = 0; j < n; ++j)
                Lap += Tshape_laplace[c][j] * T1[SD[c][j]];
            T2[c] = T1[c] + dt * Lap;
        }
        T1.swap(T2);   // time advance
    }
    monitor.addCheckPoint(endlabel);
    for (int i = 0; i < N; ++i) {
        TEMP[i] = T1[i];
    }
    std::cerr << endlabel << std::endl;
}

// OPERATORS //
void test_operators() {
    std::string startlabel, endlabel;
    std::tie(startlabel, endlabel) = labels[LABEL_ID++];
    std::cerr << startlabel << std::endl;
    // init state
    VecXd T1(domain.size(), 1);
    VecXd T2(domain.size(), 1);
    T2[domain.types == BOUNDARY] = 0;
    T1 = T2;

    EngineMLS<Vec2d, Gaussians, Gaussians> mls_op(
        {0.6, m}, domain.positions[domain.support[0]], sigma);
    auto op = make_mlsm(domain, mls_op, interior);

	monitor.addCheckPoint(startlabel);
    for (int tt = 0; tt < t_steps; ++tt) {
        // new temp.
        int i;
        //          #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < static_cast<int>(interior.size()); ++i) {
            int c = interior[i];
            T2[c] = T1[c] + dt * op.lap(T1, c);
        }
        T1.swap(T2);   // time advance
    }
    monitor.addCheckPoint(endlabel);
    for (int i = 0; i < N; ++i) {
        TEMP[i] = T1[i];
    }
    std::cerr << endlabel << std::endl;
}

// EIGEN //
void test_eigen() {
    std::string startlabel, endlabel;
    std::tie(startlabel, endlabel) = labels[LABEL_ID++];
    std::cerr << startlabel << std::endl;

    // init state
    Eigen::VectorXd T1(domain.size(), 1);
    Eigen::VectorXd T2(domain.size(), 1);
    for (int i = 0; i < N; ++i) T1[i] = T2[i] = 0;
    for (int i : interior) T1[i] = T2[i] = 1;
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Eshape_laplace(N, n);
    for (int i : interior) {
        for (int j = 0; j < n; ++j) {
            Eshape_laplace(i, j) = shape_laplace[i][j];
        }
    }
    Eigen::Array<int, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> SD(N, n);
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < n; ++j) {
            SD(i, j) = domain.support[i][j];
        }
    }
    monitor.addCheckPoint(startlabel);
    for (int tt = 0; tt < t_steps; ++tt) {
        // new temp.
        int i;
        for (i = 0; i < static_cast<int>(interior.size()); ++i) {
            int c = interior[i];
            double Lap = 0;
            for (int j = 0; j < n; ++j) Lap += Eshape_laplace(c, j) * T1[SD(c, j)];
            T2[c] = T1[c] + dt * Lap;
        }
        T1.swap(T2);   // time advance
    }
    monitor.addCheckPoint(endlabel);
    for (int i = 0; i < N; ++i) {
        TEMP[i] = T1[i];
    }
    std::cerr << endlabel << std::endl;
}
void test_carray() {
    std::string startlabel, endlabel;
    std::tie(startlabel, endlabel) = labels[LABEL_ID++];
    std::cerr << startlabel << std::endl;
    double* T1 = new double[N];
    double* T2 = new double[N];
    for (int i = 0; i < N; ++i) T1[i] = T2[i] = 0;
    for (int i : interior) T1[i] = T2[i] = 1;

    int* Csupport = new int[N * n];
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < n; ++j) {
            Csupport[n * i + j] = domain.support[i][j];
        }
    }
    double* Cshape_laplace = new double[N * n];
    for (int i : interior) {
        for (int j = 0; j < n; ++j) {
            Cshape_laplace[n * i + j] = shape_laplace[i][j];
        }
    }
    monitor.addCheckPoint(startlabel);
    for (int tt = 0; tt < t_steps; ++tt) {
        // new temp.
        int i;
        //          #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < static_cast<int>(interior.size()); ++i) {
            int c = interior[i];
            double Lap = 0;
            for (int j = 0; j < n; ++j)
                Lap += Cshape_laplace[c * n + j] * T1[Csupport[c*n + j]];
            T2[c] = T1[c] + dt * Lap;
        }
        std::swap(T1, T2);
    }
    monitor.addCheckPoint(endlabel);
    for (int i = 0; i < N; ++i) {
        TEMP[i] = T1[i];
    }
    delete[] Csupport;
    delete[] Cshape_laplace;
    delete[] T1;
    delete[] T2;
    std::cerr << endlabel << std::endl;
}
void test_vector() {
    std::string startlabel, endlabel;
    std::tie(startlabel, endlabel) = labels[LABEL_ID++];
    std::cerr << startlabel << std::endl;

    // init state
    std::vector<double> T1(domain.size(), 1);
    std::vector<double> T2(domain.size(), 1);
    for (int i = 0; i < N; ++i) T1[i] = T2[i] = 0;
    for (int i : interior) T1[i] = T2[i] = 1;

    std::vector<std::vector<double>> Vshape_laplace(N, std::vector<double>(n));
    for (int i : interior) {
        for (int j = 0; j < n; ++j) {
            Vshape_laplace[i][j] = shape_laplace[i][j];
        }
    }

    std::vector<std::vector<int>> SD(N, std::vector<int>(n));
    for (int i : interior) {
        for (int j = 0; j < n; ++j) {
            SD[i][j] = domain.support[i][j];
        }
    }
    monitor.addCheckPoint(startlabel);
    for (int tt = 0; tt < t_steps; ++tt) {
        // new temp.
        int i;
        //          #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < static_cast<int>(interior.size()); ++i) {
            int c = interior[i];
            double Lap = 0;
            for (int j = 0; j < n; ++j)
                Lap += Vshape_laplace[c][j] * T1[SD[c][j]];
            T2[c] = T1[c] + dt * Lap;
        }
        T1.swap(T2);   // time advance
    }
    monitor.addCheckPoint(endlabel);
    for (int i = 0; i < N; ++i) {
        TEMP[i] = T1[i];
    }
    std::cerr << endlabel << std::endl;
}
void test_vector_1d() {
    std::string startlabel, endlabel;
    std::tie(startlabel, endlabel) = labels[LABEL_ID++];
    std::cerr << startlabel << std::endl;

    // init state
    std::vector<double> T1(domain.size(), 1);
    std::vector<double> T2(domain.size(), 1);
    for (int i = 0; i < N; ++i) T1[i] = T2[i] = 0;
    for (int i : interior) T1[i] = T2[i] = 1;

    std::vector<double> Vshape_laplace(N*n);
    for (int i : interior) {
        for (int j = 0; j < n; ++j) {
            Vshape_laplace[n * i + j] = shape_laplace[i][j];
        }
    }
    std::vector<int> SD(N*n);
    for (int i : interior) {
        for (int j = 0; j < n; ++j) {
            SD[n * i + j] = domain.support[i][j];
        }
    }
    monitor.addCheckPoint(startlabel);
    for (int tt = 0; tt < t_steps; ++tt) {
        // new temp.
        int i;
        //          #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < static_cast<int>(interior.size()); ++i) {
            int c = interior[i];
            double Lap = 0;
            for (int j = 0; j < n; ++j)
                Lap += Vshape_laplace[c*n + j] * T1[SD[c*n+j]];
            T2[c] = T1[c] + dt * Lap;
        }
        T1.swap(T2);   // time advance
    }
    monitor.addCheckPoint(endlabel);
    for (int i = 0; i < N; ++i) {
        TEMP[i] = T1[i];
    }
    std::cerr << endlabel << std::endl;
}

void test_our_types_1d() {
    std::string startlabel, endlabel;
    std::tie(startlabel, endlabel) = labels[LABEL_ID++];
    std::cerr << startlabel << std::endl;

    VecXd T1(domain.size(), 1);
    VecXd T2(domain.size(), 1);

    // dirichlet BCs
    T2[domain.types == BOUNDARY] = 0;
    T1 = T2;

    VecXd Vshape_laplace(N*n);
    for (int i : interior) {
        for (int j = 0; j < n; ++j) {
            Vshape_laplace[n * i + j] = shape_laplace[i][j];
        }
    }
    Range<int> SD(N*n);
    for (int i : interior) {
        for (int j = 0; j < n; ++j) {
            SD[n * i + j] = domain.support[i][j];
        }
    }
    monitor.addCheckPoint(startlabel);
    for (int tt = 0; tt < t_steps; ++tt) {
        // new temp.
        int i;
        //          #pragma omp parallel for private(i) schedule(static)
        for (i = 0; i < static_cast<int>(interior.size()); ++i) {
            int c = interior[i];
            double Lap = 0;
            for (int j = 0; j < n; ++j)
                Lap += Vshape_laplace[c*n+j] * T1[SD[c*n+j]];
            T2[c] = T1[c] + dt * Lap;
        }
        T1.swap(T2);   // time advance
    }
    monitor.addCheckPoint(endlabel);
    for (int i = 0; i < N; ++i) {
        TEMP[i] = T1[i];
    }
    std::cerr << endlabel << std::endl;
}
void init(int w) {
    LABEL_ID = 0;
    WIDTH = w;
    domain_size = {WIDTH, WIDTH};  // domain size
    N = WIDTH*WIDTH;
    n = 5;  // support size
    m = 5;  // gaussian basis of size 5
    t_steps = std::floor(endtime / dt);
    sigma = 0.01 * 1 / domain_size[1];  // naive normalization

    std::cerr << "--------- INIT STARTED -----------\n";
//      prn(t_steps);

    // prep domain
    shape_laplace.resize(N);
    TEMP.resize(N);

    domain.fillUniformBoundary(domain_size);
    domain.fillUniformInterior(domain_size - domain_size.Constant(2));
    //      domain.relax(10, 1e-2, 1.0, 3, 500);
    domain.findSupport(n);
    interior = domain.types == INTERNAL;

    for (auto& c : interior) {
        Range<Vec2d> supp_domain = domain.positions[domain.support[c]];
        EngineMLS<Vec2d, Gaussians, Gaussians> MLS({0.6, m}, supp_domain, sigma);
        shape_laplace[c] = MLS.getShapeAt(supp_domain[0], {{2, 0}}) +
                           MLS.getShapeAt(supp_domain[0], {{0, 2}});
    }
    std::cerr << "---------- INIT DONE ----------\n";
}

int main(int argc, char* argv[]) {
    monitor.addCheckPoint("INIT");

    //      omp_set_num_threads(1);
    int mode = 0;
    if (argc >= 2) {
        std::stringstream ss(argv[1]);
        ss >> mode;
    }

    init(10);  // dummy
    prn(n);
    prn(m);
    prn(endtime);
    prn(sigma);

    std::cout << "deltat=" << dt << std::endl;
    std::vector<int> widths = {5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100};
    std::cout << "widths="; print_formatted(widths) << std::endl;
    std::vector<std::vector<double>> times(labels.size(), std::vector<double>(widths.size()));
    std::vector<std::vector<double>> L3hit(labels.size(), std::vector<double>(widths.size()));
    std::vector<std::vector<double>> L3miss(labels.size(), std::vector<double>(widths.size()));
    std::vector<std::vector<double>> L2hit(labels.size(), std::vector<double>(widths.size()));
    std::vector<std::vector<double>> L2miss(labels.size(), std::vector<double>(widths.size()));
    int cnt = 0;
    for (int w : widths) {
//          prn(cnt);
        init(w);
        std::cerr << cnt << " / " << widths.size() << std::endl;
        monitor.clear();
        monitor.addCheckPoint("START");
        switch(mode) {
            case 1: test_our_types(); break;
            case 2: test_operators(); break;
            case 3: test_eigen(); break;
            case 4: test_carray(); break;
            case 5: test_vector(); break;
            case 6: test_vector_1d(); break;
            case 7: test_our_types_1d(); break;
            default:
                test_our_types(); // order must remain the same
                test_operators();
                test_eigen();
                test_carray();
                test_vector();
                test_vector_1d();
                test_our_types_1d();
        }
        monitor.addCheckPoint("END");

        std::cerr << std::setprecision(16);
        int i = 0;
        for (const auto& t : labels) {
            times[i][cnt] = monitor.getTime(t.first, t.second);
            L3hit[i][cnt] = getL3CacheHits(
              monitor.getState(t.first), monitor.getState(t.second));
            L2hit[i][cnt] = getL2CacheHits(
              monitor.getState(t.first), monitor.getState(t.second));
            L3miss[i][cnt] = getL3CacheMisses(
              monitor.getState(t.first), monitor.getState(t.second));
            L2miss[i][cnt] = getL2CacheMisses(
              monitor.getState(t.first), monitor.getState(t.second));
            ++i;
        }

        ++cnt;
    }

    auto names = {"our", "op", "eig", "carr", "vec", "vec1d", "our1d"};
    int i = 0;
    for (auto name : names) {
        std::cout << name << "Time="; print_formatted(times[i++]) << std::endl;
    }
    i = 0;
    for (auto name : names) {
        std::cout << name << "L3hit="; print_formatted(L3hit[i++]) << std::endl;
    }
    i = 0;
    for (auto name : names) {
        std::cout << name << "L2hit="; print_formatted(L2hit[i++]) << std::endl;
    }
    i = 0;
    for (auto name : names) {
        std::cout << name << "L3miss="; print_formatted(L3miss[i++]) << std::endl;
    }
    i = 0;
    for (auto name : names) {
        std::cout << name << "L2miss="; print_formatted(L2miss[i++]) << std::endl;
    }

    return 0;
}
