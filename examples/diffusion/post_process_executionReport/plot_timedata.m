(* ::Package:: *)

Import["out_5_v5.m"]

labels = {"our types", "our operators", "Eigen", "C array", "std::vector", "std::vector 1d", "our types 1d"};

imgsize = 800;
SetOptions[ListPlot,
  Frame -> True,
  PlotTheme -> {"OpenMarkersThick"},
  LabelStyle -> {FontFamily -> "Verdana",  GrayLevel[0]},
  GridLines -> Automatic,
  ImageSize -> imgsize
];
legend = Placed[
    PointLegend[labels,
      LegendFunction -> (Grid[{{#}}, Frame -> True, FrameStyle -> Thickness[10^-4], Background->White]&),
      LegendLabel -> StringForm["n = `1`\n\[Sigma] = `2`\n\[CapitalDelta]t = `3`", n, sigma, deltat],
      LegendMarkerSize -> 10],
    {Right, Bottom}
  ];

NN = widths^2;
dataOur = Transpose[{NN, ourTime}];
dataOp = Transpose[{NN, opTime}];
dataEig = Transpose[{NN, eigTime}];
dataCarr = Transpose[{NN, carrTime}];
dataVec = Transpose[{NN, vecTime}];
dataVec1d = Transpose[{NN, vec1dTime}];
dataOur1d = Transpose[{NN, our1dTime}];
data = {dataOur, dataOp, dataEig, dataCarr, dataVec, dataVec1d, dataOur1d};
pltime = ListPlot[
  data,
  FrameLabel -> {"N", "t[s]"},
  PlotLabel -> "Running time for different types.",
  PlotLegends -> legend
]

dataOurL2 = Transpose[{NN, ourL2hit / (ourL2miss + ourL2hit)}];
dataOpL2 = Transpose[{NN, opL2hit / (opL2miss + opL2hit)}];
dataEigL2 = Transpose[{NN, eigL2hit / (eigL2miss + eigL2hit)}];
dataCarrL2 = Transpose[{NN, carrL2hit / (carrL2miss + carrL2hit)}];
dataVecL2 = Transpose[{NN, vecL2hit / (vecL2miss + vecL2hit)}];
dataVec1dL2 = Transpose[{NN, vec1dL2hit / (vec1dL2miss + vec1dL2hit)}];
dataOur1dL2 = Transpose[{NN, our1dL2hit / (our1dL2miss + our1dL2hit)}];
dataL2 = {dataOurL2, dataOpL2, dataEigL2, dataCarrL2, dataVecL2, dataVec1dL2, dataOur1dL2};
plL2 = ListPlot[
  dataL2,
  FrameLabel-> {"N", "L2 cache hit ratio"},
  PlotLabel -> "L2 cache hit ratio for different types.",
  PlotLegends -> legend
]

dataOurL3 = Transpose[{NN, ourL3hit / (ourL3miss + ourL3hit)}];
dataOpL3 = Transpose[{NN, opL3hit / (opL3miss + opL3hit)}];
dataEigL3 = Transpose[{NN, eigL3hit / (eigL3miss + eigL3hit)}];
dataCarrL3 = Transpose[{NN, carrL3hit / (carrL3miss + carrL3hit)}];
dataVecL3 = Transpose[{NN, vecL3hit / (vecL3miss + vecL3hit)}];
dataVec1dL3 = Transpose[{NN, vec1dL3hit / (vec1dL3miss + vec1dL3hit)}];
dataOur1dL3 = Transpose[{NN, our1dL3hit / (our1dL3miss + our1dL3hit)}];
dataL3 = {dataOurL3, dataOpL3, dataEigL3, dataCarrL3, dataVecL3, dataVec1dL3, dataOur1dL3};
plL3 = ListPlot[
  dataL3,
  FrameLabel ->  {"N", "L3 cache hit ratio"},
  PlotLabel -> "L3 cache hit ratio for different types.",
  PlotLegends -> legend
]

dataOurL2t = Transpose[{NN, (ourL2miss + ourL2hit)}];
dataOpL2t = Transpose[{NN, (opL2miss + opL2hit)}];
dataEigL2t = Transpose[{NN, (eigL2miss + eigL2hit)}];
dataCarrL2t = Transpose[{NN, (carrL2miss + carrL2hit)}];
dataVecL2t = Transpose[{NN, (vecL2miss + vecL2hit)}];
dataVec1dL2t = Transpose[{NN, (vec1dL2miss + vec1dL2hit)}];
dataOur1dL2t = Transpose[{NN, (our1dL2miss + our1dL2hit)}];
dataL2t = {dataOurL2t, dataOpL2t, dataEigL2t, dataCarrL2t, dataVecL2t, dataVec1dL2t, dataOur1dL2t};
plL2t = ListPlot[
  dataL2t,
  FrameLabel ->  {"N", "L2 cache tries"},
  PlotLabel -> "Total L2 cache tries",
  PlotLegends -> legend
]

dataOurL3t = Transpose[{NN, (ourL3miss + ourL3hit)}];
dataOpL3t = Transpose[{NN, (opL3miss + opL3hit)}];
dataEigL3t = Transpose[{NN, (eigL3miss + eigL3hit)}];
dataCarrL3t = Transpose[{NN, (carrL3miss + carrL3hit)}];
dataVecL3t = Transpose[{NN, (vecL3miss + vecL3hit)}];
dataVec1dL3t = Transpose[{NN, (vec1dL3miss + vec1dL3hit)}];
dataOur1dL3t = Transpose[{NN, (our1dL3miss + our1dL3hit)}];
dataL3t = {dataOurL3t, dataOpL3t, dataEigL3t, dataCarrL3t, dataVecL3t, dataVec1dL3t, dataOur1dL3t};
plL3t = ListPlot[
  dataL3t,
  FrameLabel ->  {"N", "L3 cache tries"},
  PlotLabel -> "Total L3 cache tries",
  PlotLegends -> legend
]

means = Map[Last[Mean[Drop[#, 20]]]&, N[dataL2]];
fits = Map[LinearModelFit[#, x, x] &, data];
fits = Table[Coefficient[m["BestFit"], x, 1], {m, fits}];

dataCorr = Transpose[{means, fits}];
dataCorr = Map[Labeled[#1[[1]], #1[[2]]]&, Transpose[{dataCorr, labels}]];
plcorr = ListPlot[
  dataCorr,
  FrameLabel->{"Means", "Coefficients"}  
]

prefix = ToString[StringForm["img_plot`1`_", n]];
Export[prefix<>"time.pdf" ,pltime];
Export[prefix<>"L2hitratio.pdf" ,plL2];
Export[prefix<>"L3hitratio.pdf" ,plL3];
Export[prefix<>"L2total.pdf" ,plL2t];
Export[prefix<>"L3total.pdf" ,plL3t];
Export[prefix<>"correlation.pdf", plcorr];







