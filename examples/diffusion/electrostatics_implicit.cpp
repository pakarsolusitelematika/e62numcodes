/* 
[Electrostatics, implicit]

The equation we would like to solve is
\[
    \nabla^2 \phi = 0
\]
where $\phi$ is the electrostatic potential.
*/

#include <Eigen/Sparse>
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"

using namespace mm;
using namespace Eigen;

int main(int argc, char const *argv[])
{


    // output file
    std::ofstream out_file("domain_data.m");

    // domain parameters, size, support and monomial order
    int domain_size = 3000;
    size_t n = 15; 
    size_t m = 3; 

    double radius = 5.0;
    double width = 0.3;
    double height = 3.0;

    // extra boundary labels
    int LEFT = -10;
    int RIGHT = -20;

    // build circular domain
    CircleDomain<Vec2d> domain({0,0},radius);
    domain.fillUniformInterior(domain_size);
    domain.fillUniformBoundaryWithStep(domain.characteristicDistance());

    // build left rectangle
    RectangleDomain<Vec2d> left({-2-width,-height},{-2+width,height});
    left.fillUniformBoundaryWithStep(domain.characteristicDistance());
    left.types[left.types == BOUNDARY] = LEFT;
    domain.subtract(left);

    // build right rectangle
    RectangleDomain<Vec2d> right({2-width,-height},{2+width,height});
    right.fillUniformBoundaryWithStep(domain.characteristicDistance());
    right.types[right.types == BOUNDARY] = RIGHT;
    domain.subtract(right);

    // relax domain and find supports
    domain.relax(50, 1e-2, 1.0, 3, 1000);
    domain.findSupport(n);

    // get interior and boundary ranges
    Range<int> interior = domain.types == INTERNAL;
    Range<int> boundary = domain.types == BOUNDARY;
    Range<int> leftrect = domain.types == LEFT;
    Range<int> rightrect = domain.types == RIGHT;

    // initialize unknown concentration field and right-hand side
    VecXd phi(domain.size(),1);
    VecXd RHS(domain.size(),1);

    // initialize interior values (this is an important step)
    RHS[interior] = 0.0;

    // set dirichlet boundary conditions
    RHS[boundary] = 0.0;
    RHS[leftrect] = -1.0;
    RHS[rightrect] = 1.0;

    // prepare shape functions and laplacian
    std::vector<Triplet<double>> l_coeff;
    for (auto& c : interior) {
        Range<Vec2d> supp_domain = domain.positions[domain.support[c]];
        EngineMLS<Vec2d, Monomials, Gaussians> MLS(m,supp_domain,std::pow(domain.characteristicDistance(),2));
        VecXd shape = MLS.getShapeAt(supp_domain[0], {{2, 0}}) +
                      MLS.getShapeAt(supp_domain[0], {{0, 2}});
        for (int i = 0; i < supp_domain.size(); ++i) {
            l_coeff.emplace_back(c, domain.support[c][i], shape[i]);
        }
    }

    // prepare dirichlet boundaries
    std::vector<Triplet<double>> b_coeff;
    for (auto& c : domain.types < 0) {
        b_coeff.emplace_back(c, c, 1.0);
    }

    // prepare matrices
    SparseMatrix<double> laplace_m(domain.size(), domain.size());
    laplace_m.setFromTriplets(l_coeff.begin(), l_coeff.end());
    SparseMatrix<double> boundary_m(domain.size(), domain.size());
    boundary_m.setFromTriplets(b_coeff.begin(), b_coeff.end());

    // draw
    //std::thread th([&] { draw2D(domain.positions, phi); });

    // initialize solver
    Eigen::BiCGSTAB<SparseMatrix<double>> solver;

    // join matrices together
    SparseMatrix<double> tmp = boundary_m + laplace_m;
    solver.compute(tmp);

    // solve system of equations
    phi = solver.solve(RHS);
    std::cout << "#iterations:     " << solver.iterations() << std::endl;
    std::cout << "estimated error: " << solver.error()      << std::endl;

    // end drawing
    //th.join();    

    // print output to file
    out_file << "domain.size=" << domain_size << ";\n";
    out_file << "domain.int=" << interior << ";\n";
    out_file << "domain.lo=" << std::get<0>(domain.getBBox()) << ";\n";
    out_file << "domain.hi=" << std::get<1>(domain.getBBox()) << ";\n";
    out_file << "pos=" << domain.positions << ";\n";
    out_file << "types=" << domain.types << ";\n";
    out_file << "phi=" << phi << ";\n";

    // out_file << "supp=" << domain.support[240] << ";\n";
    out_file.close();

    return 0;
}