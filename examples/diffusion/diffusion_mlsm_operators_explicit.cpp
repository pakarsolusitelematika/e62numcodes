/*
[Explicit solution of a diffusion equation with a use of MLSM operators
with a mixed boundary conditions -- check wiki for more details
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Analysis_of_MLSM_performance
]

The equation we would like to solve is
\[
    \nabla^2 T  = \frac{\partial T}{\partial t}
\]
*/

#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"
#include "mlsm_operators.hpp"


using namespace mm;

/**
 * @brief Closed form solution of diffusion equation
 * @param pos spatial coordinate
 * @param t time
 * @param a size of domain
 * @param D diffusion constant
 * @param N no. of expansion
 * @return value of temperature
 */
double diff_closedform(const Vec2d& pos, double t, double a, double D, size_t N) {
    double T = 0;
    double f = M_PI / a;
    for (size_t n = 1; n < N; n = n + 2) {
        for (size_t m = 1; m < N; m = m + 2) {
            T += 16.0 / f / f / (n * m) * std::sin(n * f * pos[0]) * std::sin(m * f * pos[1]) *
                 std::exp(-D * t * ((n * n + m * m) * f * f));
        }
    }
    return T;
}

int main() {
    std::ofstream out_file("domain_data.m");

    // [Diffusion explicit with MLSM operators]
    Timer t1;

    Vec<int, 2> domain_size({100, 100});  // domain size
    size_t n = 12; // support size
    size_t m = 3;  // monomial basis of second order, i.e. 6 monomials
    double time = 0.1;  // time
    double dt = 1e-5;  // time step

    int t_steps = std::floor(time / dt);

    // prep domain
    RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
    domain.fillUniformBoundary(domain_size);
    domain.fillUniformInterior(domain_size - domain_size.Constant(2));
    //      domain.relax(10, 1e-2, 1.0, 3, 500);
    domain.findSupport(n);

    double sigma = 0.01 * 1 / domain_size[1];  // naive normalization
    Range<int> interior = domain.types == INTERNAL;

    // init state
    VecXd T1(domain.size(), 1);
    VecXd T2(domain.size(), 1);

    // dirichlet BCs
    T2[domain.types == BOUNDARY] = 0;
    T1 = T2;

    t1.addCheckPoint("shapes start");
    // prep operators
    EngineMLS<Vec2d, Monomials, Gaussians> mls(m, domain.positions[domain.support[0]], sigma);
    auto op = make_mlsm(domain, mls, interior);
    t1.addCheckPoint("stepping start");

    // time stepping
    for (int tt = 0; tt < t_steps; ++tt) {
        // new temp.
        for (auto& c : interior) {
            T2[c] = T1[c] + dt * op.lap(T1,c);
        }
        // compute error
        if (tt % 100 == 0) {
            Range<double> E2(domain.size(), 0);
            for (auto& c : interior) {
                E2[c] = std::abs(T2[c] - diff_closedform(domain.positions[c], tt * dt, 1,
                                                         1, 50));
            }
            std::cout << "Max error:" << *std::max_element(E2.begin(), E2.end()) << "\n";
        }
        // time advance
        T1 = T2;
    }
    t1.addCheckPoint("end");

    t1.showTimings();
    // [Diffusion explicit with MLSM operators]

    out_file << "domain.size=" << domain_size << ";\n";
    out_file << "domain.int=" << interior << ";\n";
    out_file << "domain.lo=" << std::get<0>(domain.getBBox()) << ";\n";
    out_file << "domain.hi=" << std::get<1>(domain.getBBox()) << ";\n";
    out_file << "pos=" << domain.positions << ";\n";
    out_file << "types=" << domain.types << ";\n";
    out_file << "T=" << T2 << ";\n";
    // out_file << "supp=" << domain.support[240] << ";\n";
    out_file.close();

    return 0;
}
