/* 
[Reaction-diffusion in trilobe shaped catalyst, implicit]

The equation we would like to solve is
\[
    \nabla^2 c - Th^2 c = 0
\]
where $c$ is the concentration of the reactant and $Th$ is
known as Thiele's modulus, a dimensionless parameter given as
\[
    Th^2 = L^2 k/D  
\]
where $L$ is a length parameter defined as the ratio of volume
to surface, $k$ is the reaction rate, and $D$ is the effective
diffusion constant.
*/

#include <Eigen/Sparse>

#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"

using namespace mm;
using namespace Eigen;

int main(int argc, char const *argv[])
{
    // thiele modulus
    double thiele = 1.0; // thiele modulus

    // output file
    std::ofstream out_file("domain_data.m");

    // domain parameters, size, support and monomial order
    int domain_size = 600;
    int n = 12; 
    int m = 3; 

    // trilobe parameters
    double height = 1.0;
    double radius = 2.*height/3.;

    // build domain
    CircleDomain<Vec2d> domain({0,2.0/3.0*height},radius);
    domain.fillUniform(domain_size,domain_size/4);

    CircleDomain<Vec2d> lbottom({-height/std::sqrt(3.0),-1.0/3.0*height},radius);
    lbottom.fillUniform(domain_size,domain_size/4);
    domain.add(lbottom);

    CircleDomain<Vec2d> rbottom({height/std::sqrt(3.0),-1.0/3.0*height},radius);
    rbottom.fillUniform(domain_size,domain_size/4);
    domain.add(rbottom);

    domain.relax(10, 1e-2, 1.0, 3, 500);

    // find supports
    domain.findSupport(n);

    // set interior and boundary flags
    Range<int> interior = domain.types == INTERNAL;
    Range<int> boundary = domain.types == BOUNDARY;

    // unknown concentration field and right-hand side
    VecXd C(domain.size(), 1);
    VecXd RHS(domain.size(),1);

    // set dirichlet boundary conditions
    RHS[boundary] = 1.0;

    // prepare shape functions and laplacian
    std::vector<Triplet<double>> coeff;
    for (auto& c : interior) {
        Range<Vec2d> supp_domain = domain.positions[domain.support[c]];
        EngineMLS<Vec2d, Monomials, Gaussians> MLS(m,supp_domain,std::pow(0.75*domain.characteristicDistance(),2));
        // laplacian
        VecXd shape = MLS.getShapeAt(supp_domain[0], {{2, 0}}) +
                      MLS.getShapeAt(supp_domain[0], {{0, 2}});
        for (int i = 0; i < supp_domain.size(); ++i) {
            coeff.emplace_back(c, domain.support[c][i], shape[i]);
        }
    }

    // prepare reaction term
    std::vector<Triplet<double>> coeff2;
    for (auto& c : interior) {
        coeff2.emplace_back(c, c, -thiele*thiele);
    }

    // prepare dirichlet boundaries
    std::vector<Triplet<double>> coeff3;
    for (auto& c : boundary) {
        coeff3.emplace_back(c, c, 1.0);
    }

    // prepare matrices
    SparseMatrix<double> shape_laplace(domain.size(), domain.size());
    shape_laplace.setFromTriplets(coeff.begin(), coeff.end());
    
    SparseMatrix<double> reaction(domain.size(), domain.size());
    reaction.setFromTriplets(coeff2.begin(), coeff2.end());
    
    SparseMatrix<double> bound(domain.size(), domain.size());
    bound.setFromTriplets(coeff3.begin(), coeff3.end());

    // draw
    std::thread th([&] { draw2D(domain.positions, C); });

    // initialize solver
    Eigen::BiCGSTAB<SparseMatrix<double>> BCGST;

    // join matrices together
    SparseMatrix<double> tmp = bound + shape_laplace + reaction;
    BCGST.compute(tmp);

    // solve system of equations
    C = BCGST.solve(RHS);

    // end drawing
    th.join();    

    // print output to file
    out_file << "domain.size=" << domain_size << ";\n";
    out_file << "domain.int=" << interior << ";\n";
    out_file << "domain.lo=" << std::get<0>(domain.getBBox()) << ";\n";
    out_file << "domain.hi=" << std::get<1>(domain.getBBox()) << ";\n";
    out_file << "pos=" << domain.positions << ";\n";
    out_file << "types=" << domain.types << ";\n";
    out_file << "C =" << C << ";\n";

    // out_file << "supp=" << domain.support[240] << ";\n";
    out_file.close();

    return 0;
}