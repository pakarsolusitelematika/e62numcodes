/*
[Explicit solution of a diffusion equation
with a Dirichlet boundary conditions -- check wiki for more details
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Analysis_of_MLSM_performance
]

The equation we would like to solve is
\[
    \nabla^2 T  = \frac{\partial T}{\partial t}
\]
*/

#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"

using namespace mm;

/**
 * @brief Closed form solution of diffusion equation
 * @param pos spatial coordinate
 * @param t time
 * @param a size of domain
 * @param D diffusion constant
 * @param N no. of expansion
 * @return value of temperature
 */
double diff_closedform(const Vec2d& pos, double t, double a, double D, size_t N) {
    double T = 0;
    double f = M_PI / a;
    for (size_t n = 1; n < N; n = n + 2) {
        for (size_t m = 1; m < N; m = m + 2) {
            T += 16.0 / f / f / (n * m) * std::sin(n * f * pos[0]) * std::sin(m * f * pos[1]) *
                 std::exp(-D * t * ((n * n + m * m) * f * f));
        }
    }
    return T;
}

int main() {
    std::ofstream out_file("domain_data.m");
    /// [Diffusion explicit]
    Vec<int, 2> domain_size({50, 50});  // domain size
    size_t n = 12;  // support size
    size_t m = 3;  // monomial basis of second order, i.e. 6 monomials
    double time = 0.01;  // time
    double dt = 1e-5;  // time step

    size_t t_steps = std::floor(time / dt);
    double sigma = 0.01 * 1 / domain_size[1];  // naive normalization

    // prep domain
    RectangleDomain<Vec2d> domain({0, 0}, {1, 1});
    domain.fillUniformBoundary(domain_size);
    domain.fillUniformInterior(domain_size - domain_size.Constant(2));
//      domain.relax(10, 1e-2, 1.0, 3, 500);
    domain.findSupport(n);

    Range<int> interior = domain.types == INTERNAL;
    Range<VecXd> shape_laplace(domain.size());

    // init state
    VecXd T1(domain.size(), 1);
    VecXd T2(domain.size(), 1);

    // dirichlet BCs
    T2[domain.types == BOUNDARY] = 0;
    T1 = T2;

    // prep shape funcs
    for (auto& c : interior) {
        Range<Vec2d> supp_domain = domain.positions[domain.support[c]];
        EngineMLS<Vec2d, Monomials, Gaussians> MLS(m, supp_domain, sigma);
        shape_laplace[c] = MLS.getShapeAt(supp_domain[0], {2, 0}) +
                           MLS.getShapeAt(supp_domain[0], {0, 2});
    }

    // draw
    std::thread th([&] { draw2D(domain.positions, T1); });

        // time stepping
    for (size_t tt = 0; tt < t_steps; ++tt) {
        // new temp.
        for (auto& c : interior) {
            double Lap = 0;
            for (size_t i = 0; i < n; ++i)
                Lap += shape_laplace[c][i] * T1[domain.support[c]][i];
            T2[c] = T1[c] + dt * Lap;
        }
        // compute error
        if (tt % 100 == 0) {
            Range<double> E2(domain.size(), 0);
            for (auto& c : interior) {
                E2[c] = std::abs(T2[c] - diff_closedform(domain.positions[c], tt * dt, 1,
                                                         1, 50));
            }
            std::cout << "Max error:" << *std::max_element(E2.begin(), E2.end()) << "\n";
        }
        // time advance
        T1 = T2;
    }

    th.join();  // end drawing

    /// [Diffusion explicit]
    out_file << "domain.size=" << domain_size << ";\n";
    out_file << "domain.int=" << interior << ";\n";
    out_file << "domain.lo=" << std::get<0>(domain.getBBox()) << ";\n";
    out_file << "domain.hi=" << std::get<1>(domain.getBBox()) << ";\n";
    out_file << "pos=" << domain.positions << ";\n";
    out_file << "types=" << domain.types << ";\n";
    out_file << "T=" << T2 << ";\n";
    // out_file << "supp=" << domain.support[240] << ";\n";
    out_file.close();

    return 0;
}
