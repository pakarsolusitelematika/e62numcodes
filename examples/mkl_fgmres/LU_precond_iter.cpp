
/*---------------------------------------------------------------------------
/  Example program for solving non-degenerate system of equations.
/  Full functionality of RCI FGMRES solver is exploited. Example shows how
/  ILU0 preconditioner accelerates the solver by reducing the number of
/  iterations.
/
/  This is a simplified C++ wrapper for MKL routines
/---------------------------------------------------------------------------*/

#include <stdio.h>
#include <assert.h>
#include <iostream>
#include <vector>

#include "math.h"
#include "mkl_blas.h"
#include "mkl_spblas.h"
#include "mkl_rci.h"
// #define N 4
// #define size 128

typedef std::vector<double> vec;

template <class T>
std::ostream& operator<<(std::ostream& xx, const std::vector<T>& arr) {
    // do it like the matlab does it.
    xx << "[";
    for (size_t i = 0; i < arr.size(); ++i) {
        xx << arr[i];
        if (i < arr.size() - 1) xx << ";";
    }
    xx << "]";  //<< std::endl;
    return xx;
}

class CSRMatrix {
  public:
    // Size of the matrix
    MKL_INT n;
    // Row-major array of all values of non-zero elements
    vec values;
    // ia[i] corresponds to the column of the element values[i] in the matrix
    std::vector<MKL_INT> ja;
    // Index of the first element in each row:
    // If ia[i] = j then values[j] is the first element in i-th row
    std::vector<MKL_INT> ia;

    CSRMatrix(const vec& val, const std::vector<MKL_INT>& j,
              const std::vector<MKL_INT>& i):n(i.size()-1), values(val), ja(j), ia(i) {}
    CSRMatrix(const std::vector<vec>& dense) {
        n = dense.size();
        ia.reserve(n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (dense[i][j] != 0) {
                    values.push_back(dense[i][j]);
                    ja.push_back(j+1);
                    if (ia.size() == i) {
                        ia.push_back(ja.size());
                    }
                }
            }
        }
        ia.push_back(ja.size()+1);
    }
    vec operator*(const vec& x) const {
        assert(x.size() == n && "Invalid vector size for matrix-vector multipl.");
        vec result(n);
        char cvar='N';
        mkl_dcsrgemv(&cvar, &n, &values[0], &ia[0], &ja[0], &x[0], &result[0]);
        return result;
    }
};

/* Solves A.x = rhs */
vec solve(CSRMatrix& A, vec& rhs) {
    MKL_INT RCI_request=0, ierr=0;
    std::vector<MKL_INT> ipar(128, 0);
    double dvar;
    vec solution(A.n, 0), trvec(A.n, 0), b=rhs, residual(A.n, 0), dpar(128), bilu0(A.values.size());
    // vec tmp(A.n*(2*A.n+1)+(A.n*(A.n+9))/2+1);
    ipar[14] = 100;
    vec tmp(A.n*(2*ipar[14] + 1) + ipar[14] * (ipar[14] + 9)/2 + 1);

    // Initialize the solver
    dfgmres_init(&A.n, &solution[0], &b[0], &RCI_request, &ipar[0], &dpar[0], &tmp[0]);
    if (RCI_request != 0) {
        fprintf(stderr, "ERROR during FGMRES init. Error code: %d\n", RCI_request);
        assert(RCI_request == 0 && "Error during initialization.");
    }


    /*---------------------------------------------------------------------------
    / Calculate ILU0 preconditioner.
    /                      !ATTENTION!
    / DCSRILU0 routine uses some IPAR, DPAR set by DFGMRES_INIT routine.
    / Important for DCSRILU0 default entries set by DFGMRES_INIT are
    / ipar[1] = 6 - output of error messages to the screen,
    / ipar[5] = 1 - allow output of errors,
    / ipar[30]= 0 - abort DCSRILU0 calculations if routine meets zero diagonal element.
    /
    / If ILU0 is going to be used out of MKL FGMRES context, than the values
    / of ipar[1], ipar[5], ipar[30], dpar[30], and dpar[31] should be user
    / provided before the DCSRILU0 routine call.
    /
    / In this example, specific for DCSRILU0 entries are set in turn:
    / ipar[30]= 1 - change small diagonal value to that given by dpar[31],
    / dpar[30]= 1.E-20 instead of the default value set by DFGMRES_INIT.
    /                  It is a small value to compare a diagonal entry with it.
    / dpar[31]= 1.E-16 instead of the default value set by DFGMRES_INIT.
    /                  It is the target value of the diagonal value if it is
    /                  small as compared to dpar[30] and the routine should change
    /                  it rather than abort DCSRILU0 calculations.
    /---------------------------------------------------------------------------*/
    ipar[30]=1;
    dpar[30]=1.E-20;
    dpar[31]=1.E-16;
    dcsrilu0(&A.n, &A.values[0], &A.ia[0], &A.ja[0], &bilu0[0], &ipar[0], &dpar[0], &ierr);
    // nrm2 = dnrm2(&matsize, &bilu0[0], &incx );
    if (ierr != 0) {
        fprintf(stderr, "Preconditioner dcsrilu0 has returned the ERROR code %d\n", ierr);
        assert(ierr == 0);
    }


    /*---------------------------------------------------------------------------
    / Set the desired parameters:
    / do the restart after 2 iterations
    / LOGICAL parameters:
    / do not do the stopping test for the maximal number of iterations
    / do the Preconditioned iterations of FGMRES method
    / Set parameter ipar[10] for preconditioner call. For this example,
    / it reduces the number of iterations.
    / DOUBLE PRECISION parameters
    / set the relative tolerance to 1.0E-3 instead of default value 1.0E-6
    / NOTE. Preconditioner may increase the number of iterations for an
    / arbitrary case of the system and initial guess and even ruin the
    / convergence. It is user's responsibility to use a suitable preconditioner
    / and to apply it skillfully.
    /---------------------------------------------------------------------------*/
    ipar[14] = 100;
    ipar[7] = 0; // Do not perform stopping tests
    ipar[10] = 1; // Make sure to use preconditioner
    dpar[0]=1.0E-3;


    std::cout << "ipar: " << ipar << "\ndpar: " << dpar << "\n";
    printf("Some info about the current run of RCI FGMRES method:\n\n");
    if (ipar[7]) {
        printf("As ipar[7]=%d, the automatic test for the maximal number of iterations will be\n", ipar[7]);
        printf("performed\n");
    }
    else {
        printf("As ipar[7]=%d, the automatic test for the maximal number of iterations will be\n", ipar[7]);
        printf("skipped\n");
    }
    printf("+++\n");
    if (ipar[8]) {
        printf("As ipar[8]=%d, the automatic residual test will be performed\n", ipar[8]);
    }
    else {
        printf("As ipar[8]=%d, the automatic residual test will be skipped\n", ipar[8]);
    }
    printf("+++\n");
    if (ipar[9]) {
        printf("As ipar[9]=%d, the user-defined stopping test will be requested via\n", ipar[9]);
        printf("RCI_request=2\n");
    }
    else {
        printf("As ipar[9]=%d, the user-defined stopping test will not be requested, thus,\n", ipar[9]);
        printf("RCI_request will not take the value 2\n");
    }
    printf("+++\n");
    if (ipar[10]) {
        printf("As ipar[10]=%d, the Preconditioned FGMRES iterations will be performed, thus,\n", ipar[10]);
        printf("the preconditioner action will be requested via RCI_request=3\n");
    }
    else {
        printf("As ipar[10]=%d, the Preconditioned FGMRES iterations will not be performed,\n", ipar[10]);
        printf("thus, RCI_request will not take the value 3\n");
    }
    printf("+++\n");
    if (ipar[11]) {
        printf("As ipar[11]=%d, the automatic test for the norm of the next generated vector is\n", ipar[11]);
        printf("not equal to zero up to rounding and computational errors will be performed,\n");
        printf("thus, RCI_request will not take the value 4\n");
    }
    else {
        printf("As ipar[11]=%d, the automatic test for the norm of the next generated vector is\n", ipar[11]);
        printf("not equal to zero up to rounding and computational errors will be skipped,\n");
        printf("thus, the user-defined test will be requested via RCI_request=4\n");
    }
    printf("+++\n\n");

    /*---------------------------------------------------------------------------
    / Check the correctness and consistency of the newly set parameters
    /---------------------------------------------------------------------------*/
    dfgmres_check(&A.n, &solution[0], &b[0], &RCI_request, &ipar[0], &dpar[0], &tmp[0]);
    if (RCI_request != 0) {
        fprintf(stderr, "ERROR during check for FGMRES init. Error code: %d\n", RCI_request);
        assert(RCI_request == 0 && "Error during parameters check.");
    }
    MKL_INT itercount = 0, i = 1;
    // Main iteration loop
    while (true) {
        // itercount++;
        if (itercount % 100 == 0) printf("%d\n", itercount);
        dfgmres(&A.n, &solution[0], &b[0], &RCI_request, &ipar[0], &dpar[0], &tmp[0]);
        /*---------------------------------------------------------------------------
        / If RCI_request=0, then the solution was found with the required precision
        /---------------------------------------------------------------------------*/
        if (RCI_request==0) break;
        /*---------------------------------------------------------------------------
        / If RCI_request=1, then compute the vector A*tmp[ipar[21]-1]
        / and put the result in vector tmp[ipar[22]-1]
        /---------------------------------------------------------------------------
        / NOTE that ipar[21] and ipar[22] contain FORTRAN style addresses,
        / therefore, in C code it is required to subtract 1 from them to get C style
        / addresses
        /---------------------------------------------------------------------------*/
        else if (RCI_request == 1) {
            char cvar = 'N';
            mkl_dcsrgemv(&cvar, &A.n, &A.values[0], &A.ia[0], &A.ja[0], &tmp[ipar[21]-1], &tmp[ipar[22]-1]);
        }
        /*---------------------------------------------------------------------------
        / If RCI_request=2, then do the user-defined stopping test
        / The residual stopping test for the computed solution is performed here
        / ---------------------------------------------------------------------------
        / NOTE: from this point vector b[N] is no longer containing the right-hand
        / side of the problem! It contains the current FGMRES approximation to the
        / solution. If you need to keep the right-hand side, save it in some other
        / vector before the call to dfgmres routine. Here we saved it in vector
        / rhs[N]. The vector b is used instead of rhs to preserve the
        / original right-hand side of the problem and guarantee the proper
        / restart of FGMRES method. Vector b will be altered when computing the
        / residual stopping criterion!
        /---------------------------------------------------------------------------*/
        else if (RCI_request == 2) {
            /* Request to the dfgmres_get routine to put the solution into b[N] via ipar[12]
            / ---------------------------------------------------------------------------
            / WARNING: beware that the call to dfgmres_get routine with ipar[12]=0 at this stage may
            / destroy the convergence of the FGMRES method, therefore, only advanced users should
            / exploit this option with care */
            ipar[12] = 1;
            /* Get the current FGMRES solution in the vector b[N] */
            dfgmres_get(&A.n, &solution[0], &b[0], &RCI_request, &ipar[0], &dpar[0], &tmp[0], &itercount);
            /* Compute the current true residual via MKL (Sparse) BLAS routines */
            residual = A*b;
            dvar=-1.0E0;
            i=1;
            daxpy(&A.n, &dvar, &rhs[0], &i, &residual[0], &i);
            dvar = dnrm2(&A.n, &residual[0], &i);
            if (dvar<1.0E-3) break;
        }
        /*---------------------------------------------------------------------------
        / If RCI_request=3, then apply the preconditioner on the vector
        / tmp[ipar[21]-1] and put the result in vector tmp[ipar[22]-1]
        / ---------------------------------------------------------------------------
        / NOTE that ipar[21] and ipar[22] contain FORTRAN style addresses,
        / therefore, in C code it is required to subtract 1 from them to get
        / C style addresses
        / Here is the recommended usage of the result produced by ILU0 routine
        / via standard MKL Sparse Blas solver routine mkl_dcsrtrsv.
        /---------------------------------------------------------------------------*/
        else if (RCI_request == 3) {
            char cvar1 = 'L';
            char cvar = 'N';
            char cvar2 = 'U';
            mkl_dcsrtrsv(&cvar1, &cvar, &cvar2, &A.n, &bilu0[0], &A.ia[0],
                         &A.ja[0], &tmp[ipar[21]-1], &trvec[0]);
            cvar1 = 'U';
            cvar = 'N';
            cvar2 = 'N';
            mkl_dcsrtrsv(&cvar1, &cvar, &cvar2, &A.n, &bilu0[0], &A.ia[0],
                         &A.ja[0], &trvec[0], &tmp[ipar[22]-1]);
        }
        /*---------------------------------------------------------------------------
        / If RCI_request=4, then check if the norm of the next generated vector is
        / not zero up to rounding and computational errors. The norm is contained
        / in dpar[6] parameter
        /---------------------------------------------------------------------------*/
        else if (RCI_request == 4) {
            if (dpar[6]<1.0E-12) break;
        }
        /*---------------------------------------------------------------------------
        / If RCI_request=anything else, then dfgmres subroutine failed
        / to compute the solution vector: computed_solution[N]
        /---------------------------------------------------------------------------*/
        else {
            fprintf(stderr, "ERROR in FGMRES after %d iterations. Error code: %d\n", itercount, RCI_request);
            assert(RCI_request == 0 && "Error during iteration.");
        }
        /*---------------------------------------------------------------------------
        / Reverse Communication ends here
        / Get the current iteration number and the FGMRES solution (DO NOT FORGET to
        / call dfgmres_get routine as solution is still containing
        / the initial guess!). Request to dfgmres_get to put the solution
        / into vector computed_solution[N] via ipar[12]
        /---------------------------------------------------------------------------*/

    }
    ipar[12] = 0;
    dfgmres_get(&A.n, &solution[0], &rhs[0], &RCI_request, &ipar[0], &dpar[0], &tmp[0], &itercount);
    /*---------------------------------------------------------------------------
    / Print solution vector: solution[N] and the number of iterations: itercount
    /---------------------------------------------------------------------------*/

    printf("\nNumber of iterations: %d\n" ,itercount);
    printf("\n");

    // if(itercount == 2 && fabs(ref_norm2 - nrm2) < 1.e-6) {
    //     printf("--------------------------------------------------------\n");
    //     printf("C example of FGMRES with ILU0 preconditioner \n");
    //     printf("has successfully PASSED all stages of computations\n");
    //     printf("--------------------------------------------------------\n");
    //     return {0};
    // } else {
    //     printf("Probably, the preconditioner was computed incorrectly:\n");
    //     printf("Either the preconditioner norm %e differs from the expected norm %e\n",nrm2,ref_norm2);
    //     printf("and/or the number of iterations %d differs from the expected number %d\n",itercount,ref_nit);
    //     printf("-------------------------------------------------------------------\n");
    //     printf("Unfortunately, FGMRES+ILU0 C example has FAILED\n");
    //     printf("-------------------------------------------------------------------\n");
    //     return {0};
    // }

    return solution;
}

std::ostream& operator<<(std::ostream& xx, const CSRMatrix& mat) {
    xx << mat.n << "x" << mat.n << " matrix:\n";
    xx << "v: " << mat.values << "\nja: " << mat.ja << "\nia: " << mat.ia << "\n";
    int el = 0;
    for (int i = 0; i < mat.n; i++) {
        for (int j = 0; j < mat.n; j++) {
            if (mat.ia[i+1] > el && mat.ja[el] == j+1) {
                std::cout << "\t" << mat.values[el];
                el += 1;
            } else {
                std::cout << "\t0";
            }
        }
        std::cout << "\n";
    }
    return xx;
}

int main() {
    MKL_INT n = 151;
    vec values;
    std::vector<MKL_INT> ja, ia;

    values.reserve(4*n*n-4*n);
    ja.reserve(4*n*n-4*n);
    ia.reserve(n*n+1);
    vec expected_solution(n*n, 0);

    for (int i = 0; i < n*n; i++) {
        if (i/n == 0 || i/n == n-1 || i % n == 0 || i % n == n-1) {
            // std::cout << i << " is boundary\n";
            ia.push_back(ja.size()+1);
            values.push_back(1);
            ja.push_back(i+1);
            expected_solution[i] = i/n;
        } else {
            // std::cout << i << " is internal\n";
            ia.push_back(ja.size()+1);
            values.push_back(-1);
            values.push_back(-1);
            values.push_back(4);
            values.push_back(-1);
            values.push_back(-1);
            ja.push_back(i-n+1);
            ja.push_back(i);
            ja.push_back(i+1);
            ja.push_back(i+2);
            ja.push_back(i+1+n);
            expected_solution[i] = 0;
        }
    }
    ia.push_back(ja.size()+1);

    // std::cout << "expected_solution: " << expected_solution << "\n";
    CSRMatrix csr(values, ja, ia);
    if (n < 10) std::cout << csr;
    vec r = csr*expected_solution;
    // std::cout << r << "\n";
    vec computed = solve(csr, expected_solution);
    printf("The system has been solved \n");
    printf("The following solution has been obtained: \n");
    std::cout << computed[n] << "\n";
    printf("The expected solution is: \n");
    std::cout << expected_solution[n] << "\n";
    // for (int i = 0; i < n; ++i) {
    //     for (int j = 0; j < n; ++j) {
    //         printf("% 3.0f", computed[i*n+j]);
    //     }
    //     printf("\n");
    // }
}

int main_1d() {
    MKL_INT n = 10000000;
    vec values;
    std::vector<MKL_INT> ja, ia;

    values.reserve(3*n-4);
    ja.reserve(3*n-4);
    ia.reserve(n+1);
    vec expected_solution(n, 5.0);

    for (int i = 0; i < n; i++) {
        if (i == 0 || i == n-1) {
            ia.push_back(ja.size()+1);
            values.push_back(1);
            ja.push_back(i+1);
        } else {
            ia.push_back(ja.size()+1);
            values.push_back(-1);
            values.push_back(2);
            values.push_back(-1);
            ja.push_back(i);
            ja.push_back(i+1);
            ja.push_back(i+2);
        }
        expected_solution[i] = 2.*i - 3.5;
    }
    ia.push_back(ja.size()+1);

    // std::cout << expected_solution << "\n";
    if (n == 4){
        CSRMatrix csr(
            {4.,-1.,-1.,-1.,4.,-1.,-1.,4.,-1.,-1.,-1.,4.},
            {1,2,3,1,2,4,1,3,4,2,3,4},
            {1,4,7,10,13});
        std::cout << csr;
        vec r = csr*expected_solution;
        std::cout << r << "\n";
        vec computed = solve(csr, r);
        std::cout << computed << "\n";
    } else {
        CSRMatrix csr(values, ja, ia);
        if (n < 10) std::cout << csr;
        vec r = csr*expected_solution;
        // std::cout << r << "\n";
        vec computed = solve(csr, r);
        printf("The system has been solved \n");
        printf("The following solution has been obtained: \n");
        std::cout << computed[n/2] << "\n";
        printf("The expected solution is: \n");
        std::cout << expected_solution[n/2] << "\n";
    }
    return 0;
}
