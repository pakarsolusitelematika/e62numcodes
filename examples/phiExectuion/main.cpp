#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <omp.h>
#include <math.h>

int main(int argc, char *argv[]) {
    int numthreads;
    int n;

    assert(argc == 3 && "args: numthreads n");
    sscanf(argv[1], "%d", &numthreads);
    sscanf(argv[2], "%d", &n);

    printf("Init...\n");
    printf("Start (%d threads)...\n", numthreads);
    printf("%d test cases\n", n);

    int m = 1000000;
    double ttime = omp_get_wtime();

    int i;
    double d = 0;
#pragma offload target(mic:0)
    {
#pragma omp parallel for private (i) schedule(static) num_threads(numthreads)
        for(i = 0; i < n; ++i) {
            for(int j = 0; j < m; ++j) {
                d = sin(d) + 0.1 + j;
                d = pow(0.2, d)*j;
            }
        }
    }
    double time = omp_get_wtime() - ttime;
    fprintf(stderr, "%d %d %.6f\n", n, numthreads, time);
    printf("time: %.6f s\n", time);
    printf("Done d = %.6lf.\n", d);

    return 0;
}