#ifndef __GLB__
#define __GLB__

#include "io.hpp"
/**
 * @brief class with all global thingies like params, timers ...
 */

template <typename scal_t, typename vec_t>
class Overseer {
private:
    XMLloader xml;
public:
    //Timer
    Timer timings;
    //MLS params
    std::ofstream out_file;
    int out_recordI;
    
    int num_threads;
   
    int n;                // support size;
    int m;                // number of basis or order of basis
    scal_t sigmaW;        // squared sigma for weight
    scal_t sigmaB;        // squared sigma for Gaussian basis
    //num param
    scal_t dt;            // time step
    scal_t dl;            // square of speed of sound ACM
    scal_t dl2;           // CBS relax param
    vec_t d_space;    	  // discretization step
    int t_steps;
    //phy params
    scal_t rho,mu;		  // \rho \mu
    scal_t time;          // time
    vec_t domain_lo;	  // domain lo point
    vec_t domain_hi;      // domain hi point
    //io params
    int out_no;           // number of outputs

    Overseer() {};
    ~Overseer() {out_file.close();}

    void operator ()(int argn,  char* arg[]) {
        ///[hardcoded params]
        domain_lo = 0;
        domain_hi = 1;

        //[external params]
        out_file.open("domain_data.m", std::ofstream::out);
        //load other parameters from xml
        std::string fn;
        if (argn != 2) fn = "default"; else fn = std::string(arg[1]);
        xml(fn + ".xml");

        std::cout << "loading params, using: "; print_white(fn + ".xml\n");

        m =          xml.getAttribute({"mls"}, "m");

        n =          xml.getAttribute({"mls"}, "n");
        sigmaW =     xml.getAttribute({"mls"}, "sigmaW");
        sigmaB =     xml.getAttribute({"mls"}, "sigmaB");
       
        dt =         xml.getAttribute({"num"}, "dt");
        dl =         xml.getAttribute({"num"}, "dl");
        dl2 =         xml.getAttribute({"num"}, "dl2");

        d_space =    xml.getAttribute({"num"}, "d_space");


        time =       xml.getAttribute({"phy"}, "time");
        rho = 		   xml.getAttribute({"phy"}, "rho");
        mu = 		     xml.getAttribute({"phy"}, "mu");

        out_no =     xml.getAttribute({"io"}, "out_num");
        num_threads= xml.getAttribute({"sys"}, "num_threads");

        //[params LOGIC]
        if (out_no < 1) out_no = 1;
        t_steps = floor(time / dt);

        sigmaW = sigmaW * d_space.mean();
        sigmaB = sigmaB * d_space.mean();

        // outputs
        std::cout<<"-----------------------------------------------------------"<< std::endl;;
        xml.doc.Print();
        std::cout << std::endl;

        omp_set_num_threads(num_threads);

        std::cout<<"Re = " << rho/mu << std::endl;
        std::cout<<"-----------------------------------------------------------"<< std::endl;;
        print_green("Params loaded ... \n");
    }
};

#endif