#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"
#include "mlsm_operators.hpp"
#include <omp.h>
#include "overseer.hpp"

using namespace mm;

typedef Vec3d vec_t;
typedef double scal_t;

Overseer<scal_t, vec_t> O;

int main(int arg_num, char *arg[]) {
    O.timings.addCheckPoint("big bang");
    O(arg_num,arg);

    /// [DOMAIN INIT]
    O.timings.addCheckPoint("domain");
    RectangleDomain<vec_t> domain(O.domain_lo, O.domain_hi); //domain definition

    domain.fillUniformInteriorWithStep(O.d_space);
    domain.fillUniformBoundaryWithStep(O.d_space);

    double r=0.4;
    CircleDomain<vec_t> c({0.9, 0.5, 0.25}, r);

    c.fillUniformBoundary(8*floor((r/O.d_space[0])*(r/O.d_space[0])));
    domain.subtract(c);
    domain.relax(20, 0.1e-5, 1.0, 3, 1000,100);

    domain.findSupport(O.n);

    Range<int> all = domain.types.filter([](vec_t p){return true;});

    Range<int> interior = domain.types == INTERNAL;
    Range<int> boundary = domain.types == BOUNDARY;

    Range<int> top = domain.positions.filter([](vec_t p)->bool{return (p[2]==1.0)?true:false;});
    Range<int> bottom = domain.positions.filter([](vec_t p)->bool{return (p[2]==0.0)?true:false;});
    Range<int> mid_plane = domain.positions.filter([](vec_t p)->bool{return (p[2]==0.5)?true:false;});
    prn(mid_plane.size());

    /// [MODEL INITIALIZATION]
    //init state
    VecXd T1(domain.size(), 1);
    VecXd T2(domain.size(), 1);

    VecXd P1(domain.size(), 0);
    VecXd P2(domain.size(), 0);

    Range<vec_t> v1(domain.size(),0);
    Range<vec_t> v2(domain.size(),0);

    ///boundary conditions
    T2[top] = 1;
    T2[bottom] = -1;

    v2[top] = vec_t({1,0,0});

    T1 = T2;
    v1 = v2;
    /// [PROBLEM SOLUTION::explicit]
    O.out_file << "T=[];\n";
    O.out_file << "time=[];\n";
    O.out_file << "v=[];\n";

    ///[MLS INIT]
    O.timings.addCheckPoint("mls");
    EngineMLS<vec_t, Monomials, Gaussians>
        mls(O.m, domain.positions[domain.support[0]], O.sigmaW*domain.characteristic_distance());
    prn(mls);
    auto op = make_mlsm(domain, mls,interior);

    O.timings.addCheckPoint("time loop");
    for (int tt = 0; tt <=O.t_steps; ++tt) {
        int i;
        #pragma omp parallel for private(i) schedule(static)
        for (i=0;i<interior.size();++i) {
            int c=interior[i];

            ///Heat transport
            //T2[c] = T1[c] + O.dt * op.lap(T1, c) - O.dt*op.grad(T1,c)*v1[c];
            ///Navier-Stokes
            v2[c] = v1[c] + O.dt * ( - 1/O.rho    * op.grad(P1,c).transpose()
                                     + O.mu/O.rho * op.lap(v1, c)
                                     -              op.grad(v1,c)*v1[c]);
            ///Mass continuity
            P2[c] = P1[c] - O.dl * O.dt * O.rho * op.div(v1,c) + O.dl2 * O.dl * O.dt * O.dt * op.lap(P1,c);
        }
        T1.swap(T2);
        v1.swap(v2);
        P1.swap(P2);

        /// [intermediate IO]
        if(tt % (1+O.t_steps/O.out_no) == 0 || tt == O.t_steps ){
            std::cout.setf( std::ios::fixed, std:: ios::floatfield);

            Range<scal_t> v_i;
            for(auto c:mid_plane) v_i.push_back(v2[c][2]);

            std::cout   << "step no:" << tt << "\t"
                        << std::setprecision(1)<<(double)tt/O.t_steps*100 << "% "
                        << std::setprecision(4)<< "max. mid. v:" << *std::max_element(v_i.begin(), v_i.end())
                        << "\n";

            O.out_recordI++;
            //O.out_file << "T=[T;" << T2 << "];\n";
            O.out_file << "time=[time;" << tt*O.dt << "];\n";
            O.out_file << "v(:,:,"<<O.out_recordI<<")=" << v2 <<";\n";
        }
    }

    O.timings.addCheckPoint("final IO");
    /// [FINAL OUTPUTS]
    O.out_file << "domain.d_space=" << O.d_space << ";\n";
    O.out_file << "domain.int=" << interior << ";\n";
    O.out_file << "domain.lo=" << std::get<0>(domain.getBBox()) << ";\n";
    O.out_file << "domain.hi=" << std::get<1>(domain.getBBox()) << ";\n";
    O.out_file << "pos=" << domain.positions << ";\n";
    O.out_file << "types=" << domain.types << ";\n";

    O.timings.addCheckPoint("the end");
    O.timings.showTimings();

}
