#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"
#include "mlsm_operators.hpp"
#include <omp.h>
#include "draw.hpp"

using namespace mm;

typedef Vec2d vec_t;
typedef double scal_t;

int main() {
    std::ofstream out_file("domain_data.m");

    /// [SETUP]
    omp_set_num_threads(4);
    vec_t d_space(0.025);      // disretization size;

    size_t n = 5;                      // support size;
    size_t m = 5;                      // number of basis or order of basis
    scal_t time = 0.5;               // time
    scal_t dt = 1e-4;                  // time step
    size_t out_no=1;                  // nuber of outputs

    scal_t sigmaW = 0.01 * d_space.mean();  // naive normalization
    scal_t sigmaB = 60 * d_space.mean();  // naive normalization

    RectangleDomain<vec_t> domain(0, 1); //domain definition

    size_t t_steps = floor(time / dt);

    /// [DOMAIN INIT]
    domain.fillUniformInteriorWithStep(d_space);
    domain.fillUniformBoundaryWithStep(d_space);


    /*CircleDomain<vec_t> obstacle(0.5, 0.20);
    obstacle.fillUniformBoundary(50);
    domain.subtract(obstacle);
    domain.relax(6, 1e-4, 2.0, 3, 100,50);
    */
    domain.findSupport(n);

    Range<size_t> all = domain.types.filter([](vec_t p){return true;});
    Range<int> interior = domain.types == INTERNAL;
    Range<size_t> boundary = domain.types == BOUNDARY;

    Range<size_t> top = domain.positions.filter([](vec_t p)->bool{return (p[1]==1.0)?true:false;});
    Range<size_t> bottom = domain.positions.filter([](vec_t p)->bool{return (p[1]==0.0)?true:false;});

    ///[MLS INIT]
    EngineMLS<vec_t, Gaussians, Gaussians> mls({sigmaB,m}, domain.positions[domain.support[0]], sigmaW);
    auto op = make_mlsm(domain, mls,interior);

    /// [MODEL INITIALIZATION]
    //init state
    VecXd T1(domain.size(), 1);
    VecXd T2(domain.size(), 1);

    VecXd P1(domain.size(), 0);
    VecXd P2(domain.size(), 0);


    Range<vec_t> v1(domain.size(),0);
    Range<vec_t> v2(domain.size(),0);

    //boundary conditions
    T2[top] = 1;
    T2[bottom] = -1;

    v2[top] = vec_t({100,0});


    T1 = T2;
    v1 = v2;
    /// [PROBLEM SOLUTION::explicit]
    out_file << "T=[];\n";
    out_file << "time=[];\n";
    out_file << "v=[];\n";

    size_t io_i=0;
    for (size_t tt = 0; tt < t_steps; ++tt) {
        size_t i;

        #pragma omp parallel for private(i) schedule(static)
        for (i=0;i<interior.size();++i) {
            size_t c=interior[i];

            T2[c] = T1[c] + dt * op.lap(T1, c) - dt*op.grad(T1,c)*v1[c];
            v2[c] = v1[c] + dt * ( -op.grad(P1,c).transpose()
                + 0.1*op.lap(v1, c) - dt*op.grad(v1,c)*v1[c]);
            P2[c] = -op.div(v1,c);
        }

        T1.swap(T2);
        v1.swap(v2);
        P1.swap(P2);

        /// intermediate IO
        if(tt % (t_steps/out_no) == 0 || tt == t_steps ){
            io_i++;
            std::cout << "time:" << tt*dt << "\n";
            out_file << "T=[T;" << T2 << "];\n";
            out_file << "time=[time;" << tt*dt << "];\n";
            out_file << "v(:,:,"<<io_i<<")=" << v2 <<";\n";
            prn(tt);
        }
    }


    /// [FINAL OUTPUTS]
    out_file << "domain.d_space=" << d_space << ";\n";
    out_file << "domain.int=" << interior << ";\n";
    out_file << "domain.lo=" << std::get<0>(domain.getBBox()) << ";\n";
    out_file << "domain.hi=" << std::get<1>(domain.getBBox()) << ";\n";
    out_file << "pos=" << domain.positions << ";\n";
    out_file << "types=" << domain.types << ";\n";

    out_file.close();
}
