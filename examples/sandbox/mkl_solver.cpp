#define EIGEN_USE_MKL_ALL

#include <iostream>
#include <vector>
#include "Eigen/Sparse"
#include "Eigen/IterativeLinearSolvers"

using namespace std;
using namespace Eigen;

int main(int argc, char* argv[]) {

    assert(argc == 2 && "Second argument is size of the system.");
    stringstream ss(argv[1]);
    int n;
    ss >> n;
    cout << "n = " << n << endl;

    // Fill the matrix
    VectorXd b = VectorXd::Ones(n) / n / n;
    b(0) = b(n-1) = 1;
    SparseMatrix<double, RowMajor> A(n, n);
    A.reserve(vector<int>(n, 3));  // 3 per row
    for (int i = 0; i < n-1; ++i) {
        A.insert(i, i) = -2;
        A.insert(i, i+1) = 1;
        A.insert(i+1, i) = 1;
    }
    A.coeffRef(0, 0) = 1;
    A.coeffRef(0, 1) = 0;
    A.coeffRef(n-1, n-2) = 0;
    A.coeffRef(n-1, n-1) = 1;

    // Solve the system
    BiCGSTAB<SparseMatrix<double, RowMajor>, IncompleteLUT<double>> solver;
    solver.setTolerance(1e-10);
    solver.setMaxIterations(1000);
    solver.compute(A);
    VectorXd x = solver.solve(b);
    cout << "#iterations:     " << solver.iterations() << endl;
    cout << "estimated error: " << solver.error()      << endl;
    cout << "sol: " << x.head(6).transpose() << endl;

    return 0;
}
