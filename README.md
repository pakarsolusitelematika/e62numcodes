# E6 2 @ IJS
Codebase for numerical solvers of department E6 at the Jozef Stefan Institute.
When using this project in your research, please quote it along with three main
contributors, Kosec Gregor, Kolman Maks and Slak Jure.

The documentation page: 
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/technical_docs/html/

The wiki page:
http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Main_Page

## Installation
To make this work from plain Ubuntu installation, run
```
sudo apt-get install cmake doxygen graphviz libboost-dev libsfml-dev libhdf5-serial-dev
git clone https://gitlab.com/e62Lab/e62numcodes.git
cd e62numcodes
./run_tests.sh
```
which installs dependancies, clones the repository, goes into the root folder
of the repository and runs tests.
This will check the configuration, notify you of potentially missing
dependencies, build and run all tests, check code style and docs. If this 
works, you are ready to go! Otherwise install any missing packages and if it 
still fails, raise an issue!

## Building and testing

Out of source builds are preferred. Run
```
mkdir -p build
cd build
cmake ..
make
```
to build all targets. Note that you only have to run `cmake` once, after that
only `make` is sufficient.

Binaries are placed into `bin/` folder an dlibraries are placed into `lib/`
folder.

Tests can be run all at once via 
```
make run_all_tests
``` 
or individually via eg. 
```
make basisfunc_run_tests
```
Each header file should be accompanied by a `<util_name>_test.cpp` file with 
unit tests tests for all provided methods and can also contain examples
to be included in doxygen.

For more details on building and dependancies see 
[How to build](http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/How_to_build) 
in the wiki.

For more details on writing and running tests see 
[Testing](http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Testing) 
in the wiki.

The `run_tests.sh` script provides a nice interface to all different checks we 
have.
```
Usage: ./run_tests.sh
Options:
  -c   run only configuration check
  -t   run only unit tests
  -s   run only stylechecks
  -d   run only docs check
  -h   print this help
Example:
 ./run_tests.sh -sd
```

## Docs

Every function, class method or namespace should have a docstring briefly
describing its functionality and intended usage. Documentation is generated 
automatically from these docstrings using `doxygen` and is put in `docs/`
folder.

To generate docs run command 
```
make docs
```
This will create folders `docs/html` and `docs/latex`. To view HTML 
documentation open `docs/html/index.html`. To view PDF documentation go to 
directory `docs/latex` and run `make` and open file`refman.pdf`. To deploy to 
server run `make deploy`.

Take a look af full [technical documentation](http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/technical_docs/html/).

## Examples and Wiki 

Basic demonstrations of core library features can be found at the 
[technical documentation front page](http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/technical_docs/html/).

More relaxed discussions can be found in [wiki documentation](http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Main_Page)
including [examples of solving the difusion equation](http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Analysis_of_MLSM_performance)
which show how different parts of the library work together in real world applications.

## Coding style

We adhere to our own [style guide](http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Coding_style),
roughly based on [Google's C++ style guide](https://google.github.io/styleguide/cppguide.html).