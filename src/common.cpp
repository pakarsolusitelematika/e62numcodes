/**
 * @file
 * @brief Implementation of non templated functions defined in the header common.hpp.
 */

#include "common.hpp"

/**
 * A namespace wrapping everything in this library. Name mm stands for Meshless Machine.
 */
namespace mm {

std::ostream& print_formatted(int x, const std::string&, const std::string&,
                              const std::string&, const std::string&, std::ostream& xx) {
    return xx << x;
}
std::ostream& print_formatted(double x, const std::string&, const std::string&,
                              const std::string&, const std::string&, std::ostream& xx) {
    return xx << std::fixed << std::setprecision(16) << x;
}

void print_red(const std::string& s) { std::cout << "\x1b[31;1m" << s << "\x1b[37;0m"; }

void print_white(const std::string& s) { std::cout << "\x1b[37;1m" << s << "\x1b[37;0m"; }

void print_green(const std::string& s) { std::cout << "\x1b[32;1m" << s << "\x1b[37;0m"; }

unsigned int get_seed() {
    try {
        std::random_device rd;
        return rd();
    } catch (std::exception& e) {
        return std::chrono::system_clock::now().time_since_epoch().count();
    }
}

std::string mem2str(size_t bytes) {
    double amount = bytes;
    std::vector<std::string> suffix = {"B", "kB", "MB", "GB"};
    for (int i = 0; i < 4; ++i) {
        if (amount < 100) {
            std::stringstream ss;
            ss << static_cast<int>(amount*10+0.5) / 10.0 << " " << suffix[i];
            return ss.str();
        }
        amount /= 1000;
    }
    return "More than your mom.";
}

std::vector<std::string> split(const std::string& str, const std::string& delim) {
    assert(delim != "" && "Delimiter must not be empty.");
    std::vector<std::string> tokens;
    size_t prev = 0, pos = 0;
    do {
        pos = str.find(delim, prev);
        if (pos == std::string::npos) pos = str.length();
        tokens.emplace_back(str.substr(prev, pos-prev));
        prev = pos + delim.length();
    } while (pos < str.length() && prev < str.length());
    if (prev == str.length()) tokens.emplace_back("");
    return tokens;
}
std::vector<std::string> split(const std::string& str, char delim) {
    return split(str, std::string(1, delim));
}

std::string join(const std::vector<std::string>& parts, const std::string& joiner) {
    if (parts.size() == 0) return "";
    std::string result = parts[0];
    int n = parts.size();
    for (int i = 1; i < n; ++i) {
        result += joiner + parts[i];
    }
    return result;
}
std::string join(const std::vector<std::string>& parts, char joiner) {
    return join(parts, std::string(1, joiner));
}

int Timer::addCheckPoint(const std::string& label) {
    labels.push_back(label);
    times.push_back(std::chrono::high_resolution_clock::now());
    return times.size() - 1;
}
void Timer::showTimings(std::ostream& os) const {
    if (labels.size() < 2) {
        print_red("TIMER::you need at least two checkpoints!\n");
        assert(0 && "You need at least two checkpoints");
    } else {
        size_t M = 0;
        for (const auto& label : labels) M = std::max(M, label.size());
        for (size_t c = 1; c < labels.size(); ++c) {
            os << std::left
               << std::setw(M) << labels[c - 1] << " -- " << std::setw(M) << labels[c]
               << ' ' << std::setw(10) << std::scientific
               << std::chrono::duration<double>(times[c] - times[c-1]).count() << " [s]"
               << std::endl;
        }
        os << std::left << std::setw(2*M+5)
           << "total time " << std::setw(10)  // << std::scientific
           << std::chrono::duration<double>(times[times.size() - 1] - times[0]).count()
           << " [s]" << std::endl;
    }
}

void Timer::showTimings(const std::string& from, const std::string& to,
                        std::ostream& os) const {
    showTimings(getID(from), getID(to), os);
}

void Timer::showTimings(int from, int to, std::ostream& os) const {
    assert(0 <= from && from < size() && "Invalid from ID.");
    assert(0 <= to && to < size() && "Invalid to ID.");
    os << std::left << std::setw(20) << labels[from] << " -- "
       << std::setw(20) << labels[to] << std::setw(18)
       << std::chrono::duration<double>(times[to] - times[from]).count()
       << "[s]" << std::endl;
}

Timer::time_type Timer::getTime(int id) const {
    assert(0 <= id && id < static_cast<int>(times.size()) && "Invalid id.");
    return times[id];
}
Timer::time_type Timer::getTime(const std::string& label) const {
    return times[getID(label)];
}
double Timer::getTime(const std::string& from, const std::string& to) const {
    return std::chrono::duration<double>(times[getID(to)] - times[getID(from)]).count();
}
int Timer::getID(const std::string& label) const {
    auto it = std::find(labels.begin(), labels.end(), label);
    assert(it != labels.end() && "Timer::'label' checkpoint not found\n");
    return it - labels.begin();
}
int Timer::size() const { return labels.size(); }
void Timer::clear() {
    times.clear();
    labels.clear();
}

}  // namespace mm
