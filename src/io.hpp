#ifndef SRC_IO_HPP_
#define SRC_IO_HPP_

#include "includes.hpp"
#include "common.hpp"
#include <tinyxml2/tinyxml2.hpp>
#ifndef __MIC__
#include <hdf5.h>
#include <H5Cpp.h>
#endif

namespace mm {

/**
 * @file io.hpp
 * @brief Basic IO functionality
 * @example io_test.cpp
 */

/**
 * @brief Primitives for loading XML files.
 */
class XMLloader {
  public:
    /**
     * Proxy to allow typecasts and calling simply
     * double a = getAttribute(path, att_name);
     **/
    class Proxy {
        const XMLloader* parent;  ///< pointer to parent
        std::vector<std::string> path;  ///< path to the att. tag
        std::string att;  ///< attribute name

      public:
        /// Constructor
        Proxy(const XMLloader* parent_, const std::vector<std::string>& path_,
              const std::string& att_) : parent(parent_), path(path_), att(att_) {}
        /// Cast operator to double
        operator double() const;
        /// Cast operator to float
        operator float() const;
        /// Cast operator to int
        operator int() const;
        /// Cast operator to int
        operator size_t() const;
        /// Cast operator to bool
        operator bool() const;
        friend std::ostream& operator<<(std::ostream& xx, const Proxy& arr);
    };

    tinyxml2::XMLDocument doc;  ///< document to parse
    /// Function used for document walks.
    const tinyxml2::XMLElement* walk(const std::vector<std::string>& path) const;

    XMLloader();  ///< allow default constructor
    /**
     * Loads xml from file.
     * @param file Filename of a file to load from.
     */
    XMLloader(const std::string& file);
    /**
     * Loads xml from file.
     * @param file Filename of a file to load from.
     */
    void operator()(const std::string& file);
    /**
     * @brief reads text from xml element -
     * @param path textRead([{"level1","level2",...}])
     */
    std::string textRead(const std::vector<std::string>& path) const;
    /**
     * @brief   Reads element's attribute based on return type
     * @param  path  Path to read from.
     * @param  att   Attribute to read.
     */
    Proxy getAttribute(const std::vector<std::string>& path, const std::string& att) const;

    /// Specialized getAttribute for double
    double getDoubleAttribute(const std::vector<std::string>& path, const std::string& att) const;
    /// Specialized getAttribute for float
    float getFloatAttribute(const std::vector<std::string>& path, const std::string& att) const;
    /// Specialized getAttribute for int
    int getIntAttribute(const std::vector<std::string>& path, const std::string& att) const;
    /// Specialized getAttribute for bool
    bool getBoolAttribute(const std::vector<std::string>& path, const std::string& att) const;
};

#ifndef __MIC__  // Disable HDF5 support on MIC
/// HDF5IO string error value.
#define HDF5IO_STRING_ERR ""
/// HDF5IO int error value.
#define HDF5IO_INT_ERR -999999
/// HDF5IO float error value.
#define HDF5IO_FLOAT_ERR -999999.9f
/// HDF5IO double error value.
#define HDF5IO_DOUBLE_ERR -999999.9

/**
 * @class HDF5IO
 * @brief Class that handles low level hdf5 IO.
 */
class HDF5IO {
    /// Name of the open file
    H5std_string file_name;
    /// Name of the open group
    H5std_string folder_name;
    /// Open file object
    H5::H5File file;
    /// Group (folder) object
    H5::Group group;
    /// Exception handling:
    ///   0: ignore,
    ///   1: print warning to stderr (default),
    ///   2: throw exception,
    ///   3: print warning and assert,
    int exception_handling;

    /**
     * @brief Print and handle HDF5 Exceptions and info.
     * @param error H5::Exception thrown by some HDF5Cpp function
     * @param function Name of the function that caught the exception
     */
    void printError(const H5::Exception& error, const std::string& function);

    /// Open file in a smart way.
    static H5::H5File openFileHelper(const std::string& file_name, int mode);
  public:
    /// Mode to open the file
    enum Mode {
        APPEND = 0xffffff,  ///< appends to the existing contents
        DESTROY = 0xefffff   ///< removes old contents
    };

    /**
     * @brief Constructor that opens the given file.
     * @details The constructor will fail if you give it a non-existent file
     * with new_file = false or an existing file with new_file = true.
     * @param _file_name Path and name of the file to open
     * @param mode How to open the file, either APPEND, DESTROY, H5F_ACC_EXCL,
     * H5F_ACC_TRUNC, H5F_ACC_RDONLY or H5F_ACC_RDWR
     * @param _exception_handling
     * Exception handling:
     *     0: ignore,
     *     1: print warning to stderr (default),
     *     2: throw exception,
     *     4: print warning and assert,
     */
    HDF5IO(const std::string& _file_name, int mode = APPEND, int _exception_handling = 1);
    /// Initialize empty object to be used later.
    HDF5IO(int _exception_handling = 1);

    /// Remove move assignment
    HDF5IO& operator=(HDF5IO&&) = delete;
    /// Remove copy assignment
    HDF5IO& operator=(const HDF5IO&) = delete;

    /**
     * @brief Open a new file.
     * @details It opens a new file if it exists and closes the previous one.
     * @param _file_name Name of the file to open.
     * @param mode How to open the file, either APPEND, DESTROY, H5F_ACC_EXCL,
     * H5F_ACC_TRUNC, H5F_ACC_RDONLY or H5F_ACC_RDWR
     */
    void openFile(const std::string& _file_name, int mode = APPEND);

    /// Closes current file.
    void closeFile();
    /**
     * @brief Enters a folder inside a HDF5 file
     *
     * @param folder_name Folder name to open. You have to use absolute path
     * inside the file every time.
     */
    void openFolder(const std::string& folder_name);

    /**
     * @brief Creates a new folder in the file
     *
     * @param folder_name Folder name to create. You have to use absolute path
     * inside the file every time.
     */
    void createFolder(const std::string& folder_name);

    /**
     * @brief Closes the currently open folder in hdf5 file
     */
    void closeFolder();

    /// Lists all folders in the currently open folder.
    std::vector<std::string> ls();

    /**
     * @brief Templated function to get all attributes
     *
     * @param name Name of the attribute.
     * @tparam T Type of the attribute.
     * @return Value of the attribute.
     */
    template <class T>
    T getAttribute(const std::string& name) {
        assert(group.getId() > 0 && "Open a folder before reading attributes.");
        T result;
        auto attribute = group.openAttribute(name);
        attribute.read(attribute.getDataType(), &result);
        attribute.close();
        return result;
    }

    /**
     * @brief Templated function to set all attributes
     *
     * @param name Name of the attribute.
     * @param value Value of the attribute.
     * @param type HDF5 style type of T.
     * @tparam T Type of the attribute.
     * @return Success.
     */
    template <class T>
    bool setAttribute(const std::string& name, const T& value, const H5::DataType& type) {
        assert(group.getId() > 0 && "Open a folder before writing attributes.");
        H5::Attribute attribute;
        H5E_auto2_t func;
        void* client_data;
        H5::Exception::getAutoPrint(func, &client_data);
        H5::Exception::dontPrint();
        try {
            attribute = group.openAttribute(name);
        } catch (H5::AttributeIException error) {
            attribute = group.createAttribute(name, type, H5::DataSpace());
        }
        H5::Exception::setAutoPrint(func, client_data);
        attribute.write(type, &value);
        attribute.close();
        return true;
    }

    /**
     * @brief Writes a double array to hdf5 folder (group). The function creates
     * the attribute if it does not exists.
     * The function works only with scalars, use setDouble2DArray to store array of
     * vectors. In any case you can use a more complete setDouble2DArray also for 1D arrays.
     * IMPORTANT: The IO performance is gravely affected by the chunk size
     * read more on https://support.hdfgroup.org/HDF5/doc/Advanced/Chunking/.
     * In general the chunk size should not exceed the size of data or 4GB, however
     * a common pitfall is the chunk size is too small.
     * @param name Attribute name.
     * @param value Value to write
     * @param chunk_dims_ chunk size
     * @return Success
     */
    template<class vec_t>
    bool setDoubleArray(const std::string& name, const vec_t& value, hsize_t chunk_dims_ = -1ull) {
        try {
            hsize_t dim[1] = {static_cast<hsize_t>(value.size())};
            H5::DataSet dataset;

            size_t default_chunk_size = 10000;
            if (chunk_dims_ == -1ull) {
                chunk_dims_ = std::min(default_chunk_size, static_cast<size_t>(value.size()));
            }

            H5E_auto2_t func;
            void* client_data;
            H5::Exception::getAutoPrint(func, &client_data);
            H5::Exception::dontPrint();
            try {
                dataset = group.openDataSet(name);
                dataset.extend(dim);
            } catch(H5::GroupIException not_found_error) {
                hsize_t chunk_dims[1];
                chunk_dims[0] = chunk_dims_;

                H5::DSetCreatPropList prop;
                prop.setChunk(1, chunk_dims);

                hsize_t maxdim[1] = {H5S_UNLIMITED};
                dataset = group.createDataSet(
                    name, H5::PredType::NATIVE_DOUBLE,
                    H5::DataSpace(1, dim, maxdim), prop);
            }
            H5::Exception::setAutoPrint(func, client_data);

            dataset.write(&value[0], H5::PredType::NATIVE_DOUBLE);
            dataset.close();
            return true;
        } catch (H5::Exception error) {
            printError(error, "setDoubleArrayAttribute");
        }
        return false;
    }
   /**
     * @brief Writes a int array to hdf5 folder (group). The function creates
     * the attribute if it does not exists.
     * The function works only with scalars, use setDouble2DArray to store array of
     * vectors. In any case you can use a more complete setDouble2DArray also for 1D arrays.
     * IMPORTANT: The IO performance is gravely affected by the chunk size
     * read more on https://support.hdfgroup.org/HDF5/doc/Advanced/Chunking/.
     * In general the chunk size should not exceed the size of data or 4GB, however
     * a common pitfall is the chunk size is to small.
     * @param name Attribute name.
     * @param value Value to write
     * @param chunk_dims_ chunk size
     * @return Success
     */
    template<class vec_t>
    bool setIntArray(const std::string& name, const vec_t& value, hsize_t chunk_dims_ = -1ull) {
        try {
            hsize_t dim[1] = {static_cast<hsize_t>(value.size())};
            H5::DataSet dataset;

            size_t default_chunk_size = 10000;
            if (chunk_dims_ == -1ull) {
                chunk_dims_ = std::min(default_chunk_size, static_cast<size_t>(value.size()));
            }

            H5E_auto2_t func;
            void* client_data;
            H5::Exception::getAutoPrint(func, &client_data);
            H5::Exception::dontPrint();
            try {
                dataset = group.openDataSet(name);
                dataset.extend(dim);
            } catch(H5::GroupIException not_found_error) {
                hsize_t chunk_dims[1];
                chunk_dims[0] = chunk_dims_;

                H5::DSetCreatPropList prop;
                prop.setChunk(1, chunk_dims);

                hsize_t maxdim[1] = {H5S_UNLIMITED};
                dataset = group.createDataSet(
                    name, H5::PredType::NATIVE_INT32,
                    H5::DataSpace(1, dim, maxdim), prop);
            }
            H5::Exception::setAutoPrint(func, client_data);

            dataset.write(&value[0], H5::PredType::NATIVE_INT32);
            dataset.close();
            return true;
        } catch (H5::Exception error) {
            printError(error, "setDoubleArrayAttribute");
        }
        return false;
    }

    /**
     * @brief Writes a float array to hdf5 folder (group). The function creates
     * the attribute if it does not exists.
     * The function works only with scalars, use setDouble2DArray to store array of
     * vectors. In any case you can use a more complete setDouble2DArray also for 1D arrays.
     * IMPORTANT: The IO performance is gravely affected by the chunk size
     * read more on https://support.hdfgroup.org/HDF5/doc/Advanced/Chunking/.
     * In general the chunk size should not exceed the size of data or 4GB, however
     * a common pitfall is the chunk size is to small.
     * @param name Attribute name.
     * @param value Value to write
     * @param chunk_dims_ chunk size
     * @return Success
     */
    template<class vec_t>
    bool setFloatArray(const std::string& name, const vec_t& value, hsize_t chunk_dims_ = -1ull) {
        try {
            size_t width = value.size();
            hsize_t dim[1] = {static_cast<hsize_t>(width)};
            H5::DataSet dataset;
            size_t default_chunk_size = 10000;
            if (chunk_dims_ == -1ull) {
                chunk_dims_ = std::min(default_chunk_size, width);
            }
            H5E_auto2_t func;
            void* client_data;
            H5::Exception::getAutoPrint(func, &client_data);
            H5::Exception::dontPrint();
            try {
                dataset = group.openDataSet(name);
                dataset.extend(dim);
            } catch(H5::GroupIException not_found_error) {
                hsize_t chunk_dims[1];
                chunk_dims[0] = chunk_dims_;

                H5::DSetCreatPropList prop;
                prop.setChunk(1, chunk_dims);

                hsize_t maxdim[1] = {H5S_UNLIMITED};
                dataset = group.createDataSet(
                    name, H5::PredType::NATIVE_FLOAT,
                    H5::DataSpace(1, dim, maxdim), prop);
            }
            H5::Exception::setAutoPrint(func, client_data);

            std::vector<float> cast_value(width);
            for (size_t j = 0; j < width; j++) {
                cast_value[j] = value[j];
            }
            dataset.write(&cast_value[0], H5::PredType::NATIVE_FLOAT);
            dataset.close();
            return true;
        } catch (H5::Exception error) {
            printError(error, "setFloatArrayAttribute");
        }
        return false;
    }

    /**
     * @brief Writes a 2D double array to hdf5 folder (group). The function
     * creates the attribute if it does not exists.
     * @param name Attribute name.
     * @param value Value to write
     * @param unlimited If the written dataset should be expandable e.g.
     * If you can later store a bigger array in the same spot without manually
     * deleting it.
     * IMPORTANT: The IO performance is gravely affected by the chunk size
     * read more on https://support.hdfgroup.org/HDF5/doc/Advanced/Chunking/.
     * In general the chunk size should not exceed the size of data or 4GB, however
     * a common pitfall is the chunk size is to small.
     * @param chunk_dims 2-array for chunking data. Describes the size of
     * the chunks inside the file.
     * @return Success
     */
    template<class vec_t>
    bool setDouble2DArray(const std::string& name, const vec_t& value,
            std::vector<hsize_t> chunk_dims = {-1ull, -1ull}, bool unlimited = false) {
        try {
            assert(value.size() > 0 && "Value should have size greater than zero.");
            size_t height = value.size(), width = value[0].size();
            hsize_t dim[2] = {height, width};
            H5::DataSet dataset;
            size_t default_chunk_size = 10000;
            if (chunk_dims[0] == -1ull) {
                chunk_dims = {std::min(default_chunk_size, height),
                              std::min(default_chunk_size, width)};
            }

            H5E_auto2_t func;
            void* client_data;
            H5::Exception::getAutoPrint(func, &client_data);
            H5::Exception::dontPrint();
            try {
                dataset = group.openDataSet(name);
                dataset.extend(dim);
            } catch(H5::GroupIException not_found_error) {
                H5::DSetCreatPropList prop;
                prop.setChunk(2, &chunk_dims[0]);
                hsize_t maxdim[2] = {H5S_UNLIMITED, H5S_UNLIMITED};
                if (!unlimited) {maxdim[0] = dim[0]; maxdim[1] = dim[1];}
                dataset = group.createDataSet(
                    name, H5::PredType::NATIVE_DOUBLE,
                    H5::DataSpace(2, dim, maxdim), prop);
            }
            H5::Exception::setAutoPrint(func, client_data);
            std::vector<double> linear_value(width * height);
            for (size_t i = 0; i < height; i++) {
                for (size_t j = 0; j < width; j++) {
                    linear_value[i * width + j] = value[i][j];
                }
            }
            dataset.write(&linear_value[0], H5::PredType::NATIVE_DOUBLE);
            dataset.close();
            return true;
        } catch (H5::Exception error) {
            printError(error, "setDouble2DArrayAttribute");
        }
        return false;
    }

    /**
     * @brief Writes a 2D float array to hdf5 folder (group). The function
     * creates the attribute if it does not exists.
     * @param name Attribute name.
     * @param value Value to write
     * @param unlimited If the written dataset should be expandable e.g.
     * If you can later store a bigger array in the same spot without manually
     * deleting it.
     * @param chunk_dims 2-array for chunking data. Describes the size of
     * the chunks inside the file.
     * @return Success
     */
    template<class vec_t>
    bool setFloat2DArray(const std::string& name, const vec_t& value,
        std::vector<hsize_t> chunk_dims = {-1ull, -1ull}, bool unlimited = false) {
        try {
            assert(value.size() > 0 && "Value should have size greater than zero.");
            size_t height = value.size(), width = value[0].size();
            hsize_t dim[2] = {height, width};
            size_t default_chunk_size = 10000;
            if (chunk_dims[0] == -1ull) {
                chunk_dims = {std::min(default_chunk_size, height),
                              std::min(default_chunk_size, width)};
            }
            H5E_auto2_t func;
            void* client_data;
            H5::Exception::getAutoPrint(func, &client_data);
            H5::Exception::dontPrint();
            H5::DataSet dataset;
            try {
                dataset = group.openDataSet(name);
                dataset.extend(dim);
            } catch(H5::GroupIException not_found_error) {
                // hsize_t chunk_dims[2] = {100, 5};
                H5::DSetCreatPropList prop;
                prop.setChunk(2, &chunk_dims[0]);
                hsize_t maxdim[2] = {H5S_UNLIMITED, H5S_UNLIMITED};
                if (!unlimited) {maxdim[0] = dim[0]; maxdim[1] = dim[1];}
                dataset = group.createDataSet(
                    name, H5::PredType::NATIVE_FLOAT,
                    H5::DataSpace(2, dim, maxdim), prop);
            }
            H5::Exception::setAutoPrint(func, client_data);
            std::vector<float> linear_value(width * height);
            for (size_t i = 0; i < height; i++) {
                for (size_t j = 0; j < width; j++) {
                    linear_value[i * width + j] = value[i][j];
                }
            }

            dataset.write(&linear_value[0], H5::PredType::NATIVE_FLOAT);
            dataset.close();
            return true;
        } catch (H5::Exception error) {
            printError(error, "setFloat2DArrayAttribute");
        }
        return false;
    }

    /**
     * @brief Writes a 2D int array to hdf5 folder (group). The function
     * creates the attribute if it does not exists.
     * @param name Attribute name.
     * @param value Value to write
     * @param unlimited If the written dataset should be expandable e.g.
     * If you can later store a bigger array in the same spot without manually
     * deleting it.
     * @param chunk_dims 2-array for chunking data. Describes the size of
     * the chunks inside the file.
     * @return Success
     */
    template<class vec_t>
    bool setInt2DArray(const std::string& name, const vec_t& value,
        std::vector<hsize_t> chunk_dims = {-1ull, -1ull}, bool unlimited = false) {
        try {
            assert(value.size() > 0 && "Value should have size greater than zero.");
            size_t height = value.size(), width = value[0].size();
            hsize_t dim[2] = {height, width};
            size_t default_chunk_size = 10000;
            if (chunk_dims[0] == -1ull) {
                chunk_dims = {std::min(default_chunk_size, height),
                              std::min(default_chunk_size, width)};
            }
            H5E_auto2_t func;
            void* client_data;
            H5::Exception::getAutoPrint(func, &client_data);
            H5::Exception::dontPrint();
            H5::DataSet dataset;
            try {
                dataset = group.openDataSet(name);
                dataset.extend(dim);
            } catch(H5::GroupIException not_found_error) {
                // hsize_t chunk_dims[2] = {100, 5};
                H5::DSetCreatPropList prop;
                prop.setChunk(2, &chunk_dims[0]);
                hsize_t maxdim[2] = {H5S_UNLIMITED, H5S_UNLIMITED};
                if (!unlimited) {maxdim[0] = dim[0]; maxdim[1] = dim[1];}
                dataset = group.createDataSet(
                    name, H5::PredType::NATIVE_INT32,
                    H5::DataSpace(2, dim, maxdim), prop);
            }
            H5::Exception::setAutoPrint(func, client_data);
            std::vector<int> linear_value(width * height);
            for (size_t i = 0; i < height; i++) {
                for (size_t j = 0; j < width; j++) {
                    linear_value[i * width + j] = value[i][j];
                }
            }

            dataset.write(&linear_value[0], H5::PredType::NATIVE_INT32);
            dataset.close();
            return true;
        } catch (H5::Exception error) {
            printError(error, "setFloat2DArrayAttribute");
        }
        return false;
    }

    /**
     * @brief Writes a string to hdf5 folder (group). The function creates the
     * attribute if it does not exists.
     * @param name Attribute name.
     * @param value Value to write
     * @return Success
     */
    bool setStringAttribute(const std::string& name, const std::string& value);

    /**
     * @brief Writes a double to hdf5 folder (group). The function creates the
     * attribute if it does not exists.
     * @param name Attribute name.
     * @param value Value to write
     * @return Success
     */
    bool setDoubleAttribute(const std::string& name, double value);

    /**
     * @brief Writes a float to hdf5 folder (group). The function creates the
     * attribute if it does not exists.
     * @param name Attribute name.
     * @param value Value to write
     * @return Success
     */
    bool setFloatAttribute(const std::string& name, float value);

    /**
     * @brief Writes an int to hdf5 folder (group). The function creates the
     * attribute if it does not exists.
     * @param name Attribute name.
     * @param value Value to write
     * @return Success
     */
    bool setIntAttribute(const std::string& name, int value);

    /**
     * @brief Reads a int vector attribute from a hdf5 file or folder.
     * @param name Attribute name
     * @return Vecgtor of values or empty vector if error occurred.
     */
    std::vector<int> getIntArray(const std::string& name);
    /**
     * @brief Reads a double vector attribute from a hdf5 file or folder.
     * @param name Attribute name
     * @return Vecgtor of values or empty vector if error occurred.
     */
    std::vector<double> getDoubleArray(const std::string& name);

    /**
     * @brief Reads a double vector attribute from a hdf5 file or folder.
     * @param name Attribute name
     * @return Vecgtor of values or empty vector if error occurred.
     */
    std::vector<float> getFloatArray(const std::string& name);

    /**
     * @brief Reads a double 2D array attribute from a hdf5 file or folder.
     * @param name Attribute name
     * @return 2D vector of values or empty vector if error occurred.
     */
    std::vector<std::vector<double>> getDouble2DArray(const std::string& name);

    /**
     * @brief Reads a float 2D array attribute from a hdf5 file or folder.
     * @param name Attribute name
     * @return 2D vector of values or empty vector if error occurred.
     */
    std::vector<std::vector<float>> getFloat2DArray(const std::string& name);

        /**
     * @brief Reads a int 2D array attribute from a hdf5 file or folder.
     * @param name Attribute name
     * @return 2D vector of values or empty vector if error occurred.
     */
    std::vector<std::vector<int>> getInt2DArray(const std::string& name);


    /**
     * @brief Reads a string attribute from a hdf5 file or folder.
     * @param name Attribute name
     * @return Read string attribute or HDF5IO_STRING_ERR if error occurred.
     */
    std::string getStringAttribute(const std::string& name);

    /**
     * @brief Reads a float attribute from a hdf5 file or folder.
     * @param name Attribute name
     * @return Read float attribute or HDF5IO_FLOAT_ERR if error occurred.
     */
    float getFloatAttribute(const std::string& name);

    /**
     * @brief Reads a double attribute from a hdf5 file or folder.
     * @param name Attribute name
     * @return Read double attribute or HDF5IO_DOUBLE_ERR if error occurred.
     */
    double getDoubleAttribute(const std::string& name);

    /**
     * @brief Reads an int attribute from a hdf5 file or folder.
     * @param name Attribute name
     * @return Read int attribute or HDF5IO_INT_ERR if error occurred.
     */
    int getIntAttribute(const std::string& name);

    /**
     * @brief Reads a b-scope (a 2D unsigned char array) from a hdf5 file or folder.
     * @param name b-scope name
     * @return Read b-scope or an empty array if error occurred.
     */
    std::vector<std::vector<unsigned char>> getBScope(const std::string& name);
};
#endif  // ifndef __MIC__

}  // namespace mm

#endif  // SRC_IO_HPP_
