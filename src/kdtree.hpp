#ifndef SRC_KDTREE_HPP_
#define SRC_KDTREE_HPP_

#include "ANN/ANN.h"
#include "includes.hpp"
#include "types.hpp"

namespace mm {


/**
 * @file kdtree.hpp
 * @brief KDTree wrapper for searching k-nearest neighbors
 * @example kdtree_test.cpp
 */

/**
 * @brief kd-tree wrapper over ANN classes
 * @details A wrapper that accepts and understands our other classes namely
 * class Vec
 */
template <class vec_t>
class KDTree {
    /**
     * @brief Custom deleter for unique pointer
     * @details There were many problems getting KDTree to work in other classes so
     * we made some changes and used unique_ptr to store local data. However
     * ANNpointArray requires a call to custom deleter function, so we write our own
     * wrapper to use it.
     */
    struct ANNpointArrayDeleter {
        /// Deleter for ANNpointArray
        void operator()(ANNpointArray* p) {
            annDeallocPts(*p);
            delete p;
        }
    };

    /// numeric data type, eg. double
    typedef typename vec_t::scalar_t scalar_t;
    static_assert(std::is_same<double, scalar_t>::value || std::is_same<float, scalar_t>::value,
                  "Not Implemented Error: We should template ANN.h to use scalar_t other "
                  "than double.");
    /// The dimensionality of our vector space
    static const int dim;
    /// Number of points
    int n_pts;
    /// Don't even ask. ANN understands this format, but its actually just
    /// double**. Oh, and we need a unique_ptr pointing at this so the move
    /// assignment and constructor work. High-five for double*** type.
    typedef typename std::unique_ptr<ANNpointArray, ANNpointArrayDeleter> data_pointer_t;
    /// Local storage for points
    data_pointer_t data_pts;
    /// Search structure
    std::unique_ptr<ANNkd_tree> kd_tree;

    /// @todo TODO(maks) Use some other library. ANN does not support adding
    /// or removing points after initial tree builds. Perhaps
    /// https://github.com/jtsiomb/kdtree , but it does not (yet) support more
    /// than one closest neighbor search. Maybe patch it and submit a pull
    /// request?
  public:
    /**
     * Constructor that builds the search tree for the given points
     * @param points A collection of points.
     */
    explicit KDTree(const Range<vec_t>& points) {
        assert(points.size() > 0 && "Put at least some points into KDTree.");
        // Save the size of the tree
        n_pts = points.size();
        // Copy files to the class
        data_pts = data_pointer_t(new ANNpointArray, ANNpointArrayDeleter());
        *data_pts = annAllocPts(n_pts, dim);
        for (int i = 0; i < n_pts; i++) {
            for (int j = 0; j < dim; j++) (*data_pts)[i][j] = points[i][j];
        }
        // Build the search structure
        kd_tree = make_unique<ANNkd_tree>(*data_pts, n_pts, dim);
    }

    /**
     * @brief Default constructor. Makes a KDTree with one point as a placeholder which is 0.
     * Do not depend on this value as it may change in the future.
     */
    KDTree() {
        // Initialize values
        n_pts = 1;
        data_pts = data_pointer_t(new ANNpointArray, ANNpointArrayDeleter());
        *data_pts = annAllocPts(n_pts, dim);
        // Set all coordinates to 0
        for (int i = 0; i < dim; i++) (*data_pts)[0][i] = 0;
        // Create a tree
        kd_tree = make_unique<ANNkd_tree>(*data_pts, n_pts, dim);
    }

    /// Allow move constructor
    KDTree(KDTree&&) = default;
    /// Allow move assignment
    KDTree& operator=(KDTree&&) = default;
    /// Remove copy constructor
    KDTree(const KDTree&) = delete;
    /// Remove copy assignment
    KDTree& operator=(const KDTree&) = delete;

    /**
     * @brief Grows a new tree with new points
     * @details The function deletes the old points and the old tree and builds
     * a new one with the given points
     *
     * @param points A vector containing vec_t points that we will try to
     * find in our queries
     */
    void resetTree(const Range<vec_t>& points) {
        assert(points.size() > 0 && "Put at least some points into KDTree.");
        // Save the size of the tree
        n_pts = points.size();
        // Copy files to the class
        data_pts = data_pointer_t(new ANNpointArray, ANNpointArrayDeleter());
        *data_pts = annAllocPts(n_pts, dim);
        for (int i = 0; i < n_pts; i++) {
            for (int j = 0; j < dim; j++) (*data_pts)[i][j] = points[i][j];
        }
        // Build the search structure
        kd_tree = make_unique<ANNkd_tree>(*data_pts, n_pts, dim);
    }

    /**
     * @brief Find k nearest neighbors
     * @details Uses ANN query so find k nearest points to `point`
     *
     * @param point Find closest points to this point
     * @param k How many nearest points to find
     *
     * @return A pair of two vectors of size k containing indices of nearest
     * neighbors and squared distances to the nearest neighbor
     */
    std::pair<Range<int>, Range<double>> query(const vec_t& point, int k = 1) const {
        assert(k <= n_pts && "Cannot find more points than all of them.");
        // Save it to ann model
        ANNpoint pt = annAllocPt(dim);
        for (int i = 0; i < dim; i++) {
            pt[i] = point[i];
        }

        ANNidxArray nn_idx = new ANNidx[k];  // near neighbor indices
        ANNdistArray dists = new ANNdist[k];  // near neighbor distances

        // pt: query point, k: number of nn, nn_idx: output, dist: output
        kd_tree->annkSearch(pt, k, nn_idx, dists);

        // Copy and the indices to return them
        Range<int> result_idx(k);
        Range<double> result_dist(k);
        for (int i = 0; i < k; i++) {
            result_idx[i] = nn_idx[i];
            result_dist[i] = dists[i];
        }

        // Clean things up
        annDeallocPt(pt);
        delete[] nn_idx;
        delete[] dists;

        return make_pair(result_idx, result_dist);
    }

    /**
     * @brief Find k nearest neighbors in radius
     * @details Uses ANN query so find k nearest points to `point` whithin the
     * given radius
     *
     * @param point Find closest points to this point
     * @param radius_squared Maximum distance (squared) to search
     * @param k Maximum number of nearest points to find
     *
     * @return A pair of two vectors of size k containing indices of nearest
     * neighbors and squared distances to the nearest neighbor
     */
    std::pair<Range<int>, Range<double>> query(
            const vec_t& point, const scalar_t& radius_squared, int k) const {
        // Save it to ann model
        ANNpoint pt = annAllocPt(dim);
        for (int i = 0; i < dim; i++) {
            pt[i] = point[i];
        }

        ANNidxArray nn_idx = new ANNidx[k];  // near neighbor indices
        ANNdistArray dists = new ANNdist[k];  // near neighbor distances

        // pt: query point, k: number of nn, nn_idx: output, dist: output,
        // 0: epsilon error 0 means exact search
        auto num = kd_tree->annkFRSearch(pt, radius_squared, k, nn_idx, dists);

        // Copy and the indices to return them
        Range<int> result_idx;
        Range<double> result_dist;
        result_idx.reserve(std::min(num, k));
        result_dist.reserve(std::min(num, k));
        for (int i = 0; i < k; i++) {
            if (nn_idx[i] == ANN_NULL_IDX) {
                break;
            }
            result_idx.push_back(nn_idx[i]);
            result_dist.push_back(dists[i]);
        }

        // Clean things up
        annDeallocPt(pt);
        delete[] nn_idx;
        delete[] dists;

        return make_pair(result_idx, result_dist);
    }
    /**
     * @brief Get the coordinates of a point in the tree
     * @details Given the index of a point as it appeared in the original list
     * this function returns a vec_t object with its coordinates. <br/>
     * NOTE: This is slow as it has to copy the values from it internal
     * containers. It should NOT be used as a substitute for storing your own
     * values.
     *
     * Example:
     * @snippet kdtree_test.cpp KDTree get
     *
     * @param index Index of the point as given in the constructor.
     * @return Vector object with the coordinates of the `index`-th point.
     */
    vec_t get(int index) const {
        assert(0 <= index && "Requested index in kdtree must ne nonnegative.");
        assert(index < n_pts && "Requested index in kdtree too large.");
        vec_t result;
        for (int i = 0; i < dim; i++) result[i] = (*data_pts)[index][i];
        return result;
    }

    /// Vectorized version of KDTree::get
    Range<vec_t> get(const Range<int>& indexes) const {
        Range<vec_t> result;
        result.reserve(indexes.size());
        for (int i : indexes) result.push_back(get(i));
        return result;
    }

    /// Returns number of points in kdtree.
    int size() const { return n_pts; }
};

/// @cond
template <class vec_t>
const int KDTree<vec_t>::dim = vec_t::dimension;
/// @endcond

/// Print basic info about the tree.
template <class vec_t>
std::ostream& operator<<(std::ostream& os, const KDTree<vec_t>& tree) {
    return os << "KDTree:\n"
              << "    dimension: " << vec_t::dimension << '\n'
              << "    num of points: " << tree.size() << "\n"
              << "    first point: " << tree.get(0) << "\n"
              << "    last point: " << tree.get(tree.size() - 1) << "\n";
}

}  // namespace mm

#endif  // SRC_KDTREE_HPP_
