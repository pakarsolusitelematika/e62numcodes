#ifndef SRC_DRAW_HPP_
#define SRC_DRAW_HPP_

#include "domain.hpp"
#include "types.hpp"

#include <SFML/Graphics.hpp>

namespace mm {

/**
 * @file draw.hpp
 * @brief File implementig drawing of domains. This is in a separate file,
 * because of its dependancies, which are large and their inclusion may
 * not always be wanted by default.
 * @example draw_test.cpp
 */

/**
 * Internal namespace for utilifty functions used by draw2d functions.
 */
namespace draw_internal {

/// Function needed by HSL2RGB, converts hue.
double Hue2RGB(double arg1, double arg2, double H);

/**
 * Converts HSL color model to RGB color model.
 * @param hue Hue in range [0, 360)
 * @param saturation Saturation in range [0, 100)
 * @param luminance Luminance in range [0, 100)
 */
sf::Color HSL2RGB(int hue, int saturation, int luminance);

/// returns bounding box for all points with 10% padding
template <class vec_t>
std::pair<Vec2d, Vec2d> getPaddedBBox(const Range<vec_t>& pos) {
    int n = pos.size();
    if (n == 0) return {{-1, -1}, {1, 1}};
    double minx = std::numeric_limits<double>::infinity();
    double miny = minx, maxy = -miny, maxx = -minx;
    for (int i = 0; i < pos.size(); ++i) {
        assert(pos[i].size() > 0 && "Cannot draw an empty vector");
        minx = std::min(minx, pos[i][0]);
        maxx = std::max(maxx, pos[i][0]);
        if (pos[i].size() > 1) {
            miny = std::min(miny, pos[i][1]);
            maxy = std::max(maxy, pos[i][1]);
        }
    }
    double dx = maxx - minx, dy = maxy - miny;
    double padding = 0.1;
    minx -= padding * dx;
    maxx += padding * dx;
    if (dy == 0) {
        miny -= padding * dx;
        maxy += padding * dx;
    } else {
        miny -= padding * dy;
        maxy += padding * dy;
    }
    return {{minx, miny}, {maxx, maxy}};
}

/// Get array of colors for data -- single color
Range<sf::Color> getColorData(const sf::Color& c, int size);

/// Get array of colors for data -- single color
Range<sf::Color> getColorData(const Range<sf::Color>& RGBA, int size);

/**
 * Get array of colors for data -- scalar values as hues
 * @param hues Array or doubles. Min and max are blue and red,
 * intermediate values are interpolated.
 * @param size Number of points to draw.
 */
template <class container_t>
Range<sf::Color> getColorData(const container_t& hues, int size) {
    assert(size == static_cast<int>(hues.size()) &&
           "Hue and positions array must be of same size");
    double min = *std::min_element(std::begin(hues), std::end(hues));
    double max = *std::max_element(std::begin(hues), std::end(hues));
    Range<sf::Color> rgba;
    rgba.reserve(hues.size());
    for (auto it = std::begin(hues); it != std::end(hues); ++it) {  // big = red to blue = small
        double h = (1 - (*it - min) / (max - min)) * 250;
        rgba.push_back(draw_internal::HSL2RGB(h, 100, 50));
    }
    return rgba;
}

}  // namespace draw_internal

/**
 * Draw points with colors and specified point size.
 * @param pos Positions to draw.
 * @param RGBA Color so use for each point.
 * @param point_size Size of points in px.
 **/
template <class vec_t, class colors_t>
void draw2D(const Range<vec_t>& pos, const colors_t& RGBA, int point_size = 3) {
    assert(point_size > 0 && "Point size must be a positive integer.");
    Vec2d low, up;
    std::tie(low, up) = draw_internal::getPaddedBBox(pos);
    double minx = low[0], miny = low[1];
    double maxx = up[0], maxy = up[1];
    double dx = maxx - minx;
    double dy = maxy - miny;

    unsigned int sizex = 600u;
    unsigned int sizey = static_cast<unsigned>(static_cast<double>(sizex) / dx * dy);

    assert(sizey < 2000 && "Window would be too large");

    sf::RenderWindow window(sf::VideoMode(sizex, sizey),
                            "Drawing... Press Esc or q key to close.");
    window.setFramerateLimit(60);

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) window.close();
            if (event.type == sf::Event::KeyPressed &&
                (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) ||
                 sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q))) window.close();
        }
        window.clear(sf::Color::White);

        // update colors every time for drawing in threads
        Range<sf::Color> colors = draw_internal::getColorData(RGBA, pos.size());

        for (int i = 0; i < pos.size(); ++i) {
            double x = (pos[i][0] - minx) / dx * sizex;
            double y;
            if (pos[i].size() > 1) {
                y = sizey - (pos[i][1] - miny) / dy * sizey;
            } else {
                y = 20;
            }
            sf::CircleShape shape(point_size);
            shape.setPosition(x, y);
            shape.setFillColor(colors[i]);
            window.draw(shape);
        }

        window.display();
    }
}

/**
 * Draws domain. Spawns a new window and draws domain border and interior in 2D.
 * You may want to call this function in a new thread, so other
 * operations on domain can run concurrently. Shape and size of the window are
 * decided dynamically in such a* way that axis rations are preserved.
 */
template <class vec_t>
void draw2D(const Domain<vec_t>& d, sf::Color internal_color = sf::Color::Red,
            sf::Color boundary_color = sf::Color::Blue, int point_size = 3) {
    Range<sf::Color> colors(d.positions.size());
    colors[d.types > 0] = internal_color;
    colors[d.types < 0] = boundary_color;
    draw2D(d.positions, colors, point_size);
}

}  // namespace mm

#endif  // SRC_DRAW_HPP_
