#ifndef SRC_KDTREE_MUTABLE_HPP_
#define SRC_KDTREE_MUTABLE_HPP_

#include "ssrc/spatial/kd_tree.h"
#include "includes.hpp"
#include "types.hpp"
#include <iostream>

namespace mm {
/**
 * @file kdtree_mutable.hpp
 * @brief KDTree wrapper for searching k-nearest neighbors, including insert/remove.
 * @example kdtree_mutable_test.cpp
 */

/**
 * @brief kd-tree wrapper over Libssrckdtree classes
 * @details A wrapper that accepts and understands our other classes namely
 * class Vec
 */
template <class vec_t>
class KDTreeMutable {
    /// The dimensionality of our vector space
    static const int dim = vec_t::dimension;
    /// number of points
    int n_pts = 0;
    /// numeric data type, eg. double
    typedef typename vec_t::scalar_t scalar_t;
    static_assert(std::is_same<double, scalar_t>::value,
                   "Not Implemented Error: only implemented for double type!");
    /// Type of the tree. Nodes consist of pairs (array, integer)
    typedef ssrc::spatial::kd_tree< std::array<double, dim>,  int > Tree_t;
    /// Kd_tree object containg real tree structre.
    Tree_t tree;
    /// List of points in the tree
    Range<vec_t> list_of_points;

  public:
    /**
    * @brief Constructor that builds the search tree for the given points
    * @param points A collection of points.
    */
    explicit KDTreeMutable(const mm::Range<vec_t>& points = Range<vec_t>()) {
        insert(points);
    }

    /**
    * @brief Member function which inserts a point into the tree.
    * @param point Point to be inserted into the tree.
    * @return Bool (true or false) depending on success of insertion.
    * If point already exists it returns false.
    */
    bool insert(const vec_t& point) {
        std::array<double, dim> vec;
        for (int j = 0; j < dim; j++) vec[j] = point[j];
        bool successful = !tree.insert(vec, n_pts);
        list_of_points.push_back(point);
        // for some reason tree.insert return 0 if it successfully inserted a point
        // and 1 if unsuccessfully.
        // This could be classified as a bug and might change in future versions of library.
        if (successful == 1) {
            n_pts += 1;
            return true;
        }
        return false;
    }

    /**
    *  @brief Member function which inserts a points into the tree.
    *  @param points Points to be inserted into the tree.
    *  @return Bool (true or false) depending on success of insertion.
    *  If point already exists it returns false.
    */
    bool insert(const Range<vec_t>& points) {
        // Copy files to the  class
        for (int i = 0; i < points.size(); i++) {
            bool successful = this->insert(points[i]);
            if (!successful) {
                bool already_exists = this->check(points[i]);
                //  inserting an already present point results in red warning message
                if (already_exists) std::cout <<
                                      "\033[1;31mWARNING: point already exists!\033[0m"<< std::endl;
                else return false;
            }
        }
        return true;
    }

    /**
    *  @brief Member function which removes a point from the tree.
    *  @param point Point to be removed from the tree.
    *  @return Bool (true or false) depending on success of insertion. If point does not exist, it returns false.
    */
    bool remove(const vec_t& point) {
        std::array<double, dim> vec;
        for (int j = 0; j < dim; j++) vec[j] = point[j];
        bool successful = tree.remove(vec);
        if (successful) {
            n_pts -= 1;
            return true;
        }
        return false;
    }

    /** Return number of nodes in the tree.
    *  @return Int: number of nodes in the tree.
    */
    int size() const {
        return n_pts;
    }


    /**
    * @brief Find k nearest neighbors
    * @details Uses Libssrckdtree find_nearest_neighbor for a single nearest neighbor
    * and find_nearest_neighbors for multiple nearest neighbors.
    * The latter needs boost (check Libssrc source file).
    * By default it omits query point.
    *
    * @param point Find closest points to this point.
    * @param k How many nearest points to find.
    * @param omit_query_point Bool value indicating wheter to include reference point into the result or not.
    *
    * @return A pair of two vectors of size k containing
    * points (which are of type vec_t) of nearest
    * neighbors and squared distances to the nearest neighbor
    */
    std::pair<mm::Range<int>, mm::Range<double>> query(
            const vec_t& point, int k = 1, bool omit_query_point = false) {
        assert(k <= n_pts && "Cannot find more points than all of them.");
        assert(k > 0 && "Number of points to find has to be larger than zero.");
        // conversion of point from vec_t to array for usage with internal tree class
        std::array<double, dim> vec;
        for (int j = 0; j < dim; j++) vec[j] = point[j];

        mm::Range<int> result_idx(k);  // vector which will hold nearest neighbors
        mm::Range<double> result_dist(k);
        // search for nearest neighbor
        if (k == 1) {
            typename Tree_t::const_iterator closest_point =
              tree.find_nearest_neighbor(vec, omit_query_point);

            result_dist[0] = distance(closest_point->first, vec);
            result_idx[0] = closest_point->second;

            return make_pair(result_idx, result_dist);
        }

        if (k > 1) {
            std::pair<typename Tree_t::knn_iterator, typename Tree_t::knn_iterator>
              closest_points = tree.find_nearest_neighbors(vec, k, omit_query_point);
            int i = 0;
            for (typename Tree_t::knn_iterator iter =
                   closest_points.first; iter != closest_points.second ; ++iter, ++i) {
                result_dist[i] = distance(iter->first, vec);
                result_idx[i] = iter->second;
            }
            return make_pair(result_idx, result_dist);
        }
        return make_pair(result_idx, result_dist);
    }




    /**
    * @brief Find k nearest neighbors in radius "radius" from reference point
    * @details The function is not yet implemented (it's missing from libssrc)
    */
    std::pair<mm::Range<int>, mm::Range<double>> query(
        const vec_t& /*point*/, double /*radius*/, int /*k*/ = 1,
        bool /*omit_query_point*/ = false) {
        assert(false && "Range restricted search is not yet implemented!");
    }
    /** Calculates distance between two points.
    *  @param a vec_t point
    *  @param b vec_t point
    *  @return double square of euclidean distance
    */
    double distance(const std::array<double, dim>& a, const std::array<double, dim>& b) {
        double dist = 0.0;
        for (int i = 0; i < dim; i++) dist += std::pow(a[i] - b[i], 2);
        return dist;
    }

    /**
     * @brief Checks if the point is in the tree.
    *  @param point
    *  @return bool, wheather the point is contained in the tree
    */
    bool check(vec_t point) const {
        std::array<double, dim> vec;
        for (int j = 0; j < dim; j++) vec[j] = point[j];
        return tree.get(vec);
    }

    /**
    * @brief Returns point at "index" place in the list of all points in the tree.
    *  @param index index to get the wanted point
    *  @return vec_t point
    */
    vec_t get(const int index) const {
        assert(index <= n_pts && "KDTreeMutable::get : you gave me too large index");
        return list_of_points[index];
    }

    /**
    * @brief Vectorized get.
    *  @param indices A collection of indices (integer numbers)
    *  @return Range<vec_t> A collection of points
    */
    Range<vec_t> get(const Range<int>& indices) const {
        Range<vec_t> vector_of_points;
        vector_of_points.reserve(indices.size());
        for (int i : indices) vector_of_points.push_back(get(i));
        return vector_of_points;
    }

    /**
    *  @brief Optimizes the tree for faster future searches.
    *  @details It is a very time consuming method and is not meant to be used frequently.
    */
    void optimize() {
        tree.optimize();
    }

    /**
    *  @brief Clears all previous points from the tree and inserts new points.
    *  @param points New points to be inserted into the tree.
    */
    void resetTree(const Range<vec_t> points) {
        tree.clear();
        n_pts = 0;
        this->insert(points);
    }
};

/// Print basic info about the tree.
template <class vec_t>
std::ostream& operator<< (std::ostream& os, const KDTreeMutable<vec_t>& tree) {
    return os << "KDTreeMutable:\n"
           << "    dimension: " << vec_t::dimension << '\n'
           << "    num of points: " << tree.size() << std::endl;
}

}  //  namespace mm
#endif  //  SRC_KDTREE_MUTABLE_HPP_
