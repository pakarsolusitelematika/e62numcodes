#include "draw.hpp"

namespace mm {

/**
 * @file draw.cpp
 * @brief Implementation of functions used for drawing data.
 **/

namespace draw_internal {

double Hue2RGB(double arg1, double arg2, double H) {
    if (H < 0) H += 1;
    if (H > 1) H -= 1;
    if ((6 * H) < 1) return (arg1 + (arg2 - arg1) * 6 * H);
    if ((2 * H) < 1) return arg2;
    if ((3 * H) < 2) return (arg1 + (arg2 - arg1) * ((2.0 / 3.0) - H) * 6);
    return arg1;
}

sf::Color HSL2RGB(int hue, int saturation, int luminance) {
    // reconvert to range [0,1]
    double H = hue / 360.0;
    double S = saturation / 100.0;
    double L = luminance / 100.0;

    float arg1, arg2;

    if (S <= 1e-6) {
        return sf::Color(L * 255, L * 255, L * 255);
    } else {
        if (L < 0.5) {
            arg2 = L * (1 + S);
        } else {
            arg2 = (L + S) - (S * L);
        }
        arg1 = 2 * L - arg2;

        sf::Uint8 r = (255 * Hue2RGB(arg1, arg2, (H + 1.0 / 3.0)));
        sf::Uint8 g = (255 * Hue2RGB(arg1, arg2, H));
        sf::Uint8 b = (255 * Hue2RGB(arg1, arg2, (H - 1.0 / 3.0)));
        return sf::Color(r, g, b);
    }
}

Range<sf::Color> getColorData(const sf::Color& c, int size) {
    return Range<sf::Color>(size, c);
}

Range<sf::Color> getColorData(const Range<sf::Color>& RGBA, int size) {
    assert(size == RGBA.size() && "Color and positions array must be of same size");
    return RGBA;
}

}  // namespace draw_internal

}  // namespace mm
