#ifndef SRC_MLSM_OPERATORS_HPP_
#define SRC_MLSM_OPERATORS_HPP_

#include "common.hpp"
#include "domain.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "types.hpp"

/// Shortcut for all asserts that need to be done for nodes in every operator.
#define NODE_ASSERTS(node) \
    assert(0 <= node && "Node index must be nonnegative."); \
    assert(node < domain_size && "Node index is larger than domain size."); \
    assert(support_domain[support_size * node] != -1 && "Shape functions were not initialized " \
            "for this node. Check if your node is among the indices given to MLSM.");

/**
 * @file mlsm_operators.hpp
 * @brief basic Meshless Local Strong Form operators
 * @example mlsm_operators_test.cpp
 */

namespace mm {

/**
 * Namespace holding MLSM related things, like masks for shapes.
 */
namespace mlsm {

/**
 * Type representing flags for shape functions. It has to be an integral type and combinable
 * with bitwise or operator `|` indicating flag union.
 */
typedef int shape_flags;
static const shape_flags d1 =  0b00000001;  ///< Indicates to calculate d1 shapes
static const shape_flags lap = 0b00000010;  ///< Indicates to calculate laplace shapes
static const shape_flags d2 =  0b00000100;  ///< Indicates to calculate d2 shapes
static const shape_flags div = d1;  ///< Indicates to prepare all shapes needed for div
static const shape_flags grad = d1;  ///< Indicates to prepare all shapes needed for div
static const shape_flags graddiv = d2;  ///< Indicates to prepare all shapes needed for graddiv
static const shape_flags all = d1 | d2 | lap;  ///< Indicates to prepare all shapes, this is default

}  // namespace mlsm

/**
 * @brief A class for evaluating typical operators needed in spatial discretization
 * @details MLSM class contains a reference to an approximation engine, typically MLS,
 * and to a domain engine. All types are degraded to std::vector
 * for optimal performance (<a href="tech_report.pdf"> check technical report
 * </a>). Note that: domain positions and support indexes are copied to local
 * std::vector for better cache utilization (in Domain class 2D containers are
 * used, i.e. Range<Range<int>>).
 * @tparam approx_engine A copy-constusctible EngineMLS like engine used to create the shape
 * functions.
 * @tparam domain_engine A domain class storing the nodes of the domain and their support.
 * @tparam mask A bitmask indicating which shapes to use. The masks for basic shapes are
 * located in mlsm namespace. These are mlsm::d1, mlsm::d2 and mlsm::lap.
 * If you want to solve a Laplace equation with Neumann BC, you would create a class with
 * the shape `mlsm::d1 | mlsm::lap`. The default is mlsm::all which prepares all shapes.
 *
 * If you try to call a function that needs a shape that was not specified for preparation
 * by your flags, you will get a *compile time* error like:
 * @code
 * Error: static assertion failed: D2 shapes must be initialized to use this function.
 * Check your MLSM shape bitmask.
 * @endcode
 *
 * @sa mlsm
 */
template <typename domain_engine, typename approx_engine, mlsm::shape_flags mask = mlsm::all>
class MLSM {
  private:
    static_assert(mask != 0, "Cannot create an operator with no shapes.");
    /// bitmask telling us which shapes to create
    static const mlsm::shape_flags shape_mask = mask;
    /// const reference to the domain
    const domain_engine& domain;
    /// copy of approximation engine
    approx_engine approx;

    /// @cond
    typedef typename decltype(domain_engine::positions)::value_type vec_t;
    typedef typename vec_t::scalar_t scalar_t;
    /// @endcond

    /// support size
    int support_size;
    /// domain size
    int domain_size;
    /// dimensionality of the problem
    static const int dim = vec_t::dimension;
    /// weight shape deduced from the weight function object, used in non-const weight shapes
    vec_t initial_weight_shape = static_cast<scalar_t>(0.);
    /// basis shape deduced from the weight function object, used in non-const basis shapes
    vec_t initial_basis_shape = static_cast<scalar_t>(0.);
    /// shape container for laplace operator
    std::vector<scalar_t> shape_laplace;
    /// local copy of support domains
    std::vector<int> support_domain;
    /// shape container for first derivatives
    std::vector<scalar_t> shape_d1;
    /** shape container for second derivatives. Access shape for
     *  @f[
     *    \left. \frac{\partial^2}{\partial x_i \partial x_j}\right|_n \ ,
     *    \ \ \ \ i \leq j
     *  @f]
     *  derivation by starting take the following elements
     *  @f[
     *    \mathrm{support\_size}\,\left(
     *      \frac{1}{2}\mathrm{dim}\, (\mathrm{dim}+1)\,\mathrm{node} +
     *      \frac{1}{2} j \, (j+1) + i
     *      \right) + k \ ,
     *    \ \ \ \ k=0,1, \dots, \mathrm{support\_size}-1
     *  @f]
     */
    std::vector<scalar_t> shape_d2;

  public:
    /**
     * @brief Constructor initializing specified shape functions.
     * @param domain_ domain of the calculation
     * @param approx_ approximation engine usually MLS
     * @param ind list of indices of nodes scheduled to prepare operators
     * @param use_const_shape if true MLS will use constant shape for basis and weight functions, if false
     * weight will be multiplied with a distance to the closest support node (dx), in another words
     * If you use use_const_shape supply a normalized weight, i.e. sigma*dx
     * if you use !use_const_shape supply only shape parameter and it will be automatically normalized with dx
     **/
    MLSM(const domain_engine& domain_, approx_engine& approx_, const Range<int>& ind,
         bool use_const_shape)
        : domain(domain_), approx(approx_) {
        domain_size = domain.size();

        assert(domain.support.size() == domain_size &&
               "Did you call findSupport before creating MLSM operators?");
        assert(ind.size() > 0 && "Collection of indexes must be nonempty.");
        for (auto& c : ind) {
            assert(0 <= c && c < domain_size &&
                   "One of the specified indexes was not a valid index of a point in the domain.");
        }
        support_size = domain.support[ind[0]].size();
        for (auto& c : ind) {
            assert(support_size == domain.support[c].size() &&
                   "Support sized of the given indexes are different, but should be the same.");
        }
        // Fills support with -1, to indicate which nodes have their shapes computed.
        support_domain.resize(domain_size * support_size, -1);

        // checks flags
        if (shape_mask & mlsm::d1)  {
            shape_d1.resize(domain_size * dim * support_size, 0);
        }
        if (shape_mask & mlsm::lap) {
            shape_laplace.resize(domain_size * support_size, 0);
        }
        if (shape_mask & mlsm::d2) {
            shape_d2.resize(domain_size * dim * (dim+1) * support_size / 2, 0);
        }

        if (!use_const_shape) {
            initial_weight_shape = approx.getWeightShape();
            initial_basis_shape = approx.getBasisShape();
        }

        // construct shape functions for every point specified
        int cc;
        // create local copies of mls for each thread
        // prn(omp_get_max_threads());
        std::vector<approx_engine> approx_copies(omp_get_max_threads(), approx);
        #pragma omp parallel for private(cc) schedule(static)
        for (cc = 0 ;  cc < ind.size() ; ++cc) {
            int c = ind[cc];
            //  store local copy of MLS engine -- for parallel computing
            approx_engine& local_approx = approx_copies[omp_get_thread_num()];
            // preps local 1D support domain vector (cache friendly storage)
            for (int j = 0; j < support_size; ++j) {
                support_domain[c * support_size + j] = domain.support[c][j];
            }
            // resets local mls with local parameters
            Range<vec_t> supp_domain = domain.positions[domain.support[c]];
            local_approx.setSupport(supp_domain);
            // note that all weight shapes are in squared form --
            // distances[1] is a squared distance to first supp. node
            if (!use_const_shape) {
                scalar_t dx = std::sqrt(domain.distances[c][1]);
                local_approx.resetWeightShape(initial_weight_shape * dx);
                local_approx.resetBasisShape(initial_basis_shape * dx);
            }

            // Laplace
            if (shape_mask & mlsm::lap) {
                for (int d = 0; d < dim; ++d) {
                    std::array<int, dim> derivative;
                    derivative.fill(0);
                    derivative[d] = 2;
                    auto sh = local_approx.getShapeAt(supp_domain[0], derivative);
                    for (int j = 0; j < support_size; ++j) {
                        shape_laplace[c * support_size + j] += sh[j];
                    }
                }
            }

            std::array<int, dim> derivative;
            // all first derivatives
            if (shape_mask & mlsm::d1) {
                for (int d = 0; d < dim; ++d) {
                    derivative.fill(0);
                    derivative[d] = 1;
                    auto sh = local_approx.getShapeAt(supp_domain[0], derivative);
                    /// TODO replace copy loop with std::copy
                    for (int j = 0; j < support_size; ++j) {
                        shape_d1[dim * support_size * c + support_size * d + j] = sh[j];
                    }
                }
            }
            // All second order derivatives
            if (shape_mask & mlsm::d2) {
                for (int d1 = 0; d1 < dim; ++d1) {
                    for (int d2 = 0; d2 <= d1; ++d2) {
                        derivative.fill(0);
                        derivative[d1] += 1;
                        derivative[d2] += 1;
                        auto sh = local_approx.getShapeAt(supp_domain[0], derivative);
                        int idx = dim * (dim + 1) * c / 2 + (d1 * (d1 + 1) / 2 + d2);
                        /// TODO replace copy loop with std::copy
                        for (int j = 0; j < support_size; ++j) {
                            shape_d2[idx * support_size + j] = sh[j];
                        }
                    }
                }
            }
        }
    }

    /**
     * @brief returns laplace of field u in node-th node.
     * @param u field to be considered
     * @param node index of a node where the operators is evaluated
     * @details @f$\sum(shape_{\nabla^2}[i]u_{x}[i])@f$
     * @return scalar
     */
    template<template<class> class container>
    scalar_t lap(const container<scalar_t>& u, int node) const {
        static_assert(shape_mask & mlsm::lap, "Laplace shape must be initialized to use this "
                "function. Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        scalar_t lap = 0;
        for (int i = 0; i < support_size; ++i)
            lap += shape_laplace[node * support_size + i] *
                    u[support_domain[node * support_size + i]];
        return lap;
    }

    /**
     * @brief Fill the matrix with the equation for the Laplacian of a scalar
     * field @f$\vec{u}@f$ at the point `node` being equal to some value.
     * This is the implicit version of scalar MLSM::lap(u, node) function above.
     * @param M matrix representing the equations for all nodes.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only the `node`-th row of the matrix is changed.
     * User must make sure that the matrix is large enough.
     * @param alpha scalar to multiply the operator with.
     * @details Fills the `node`-th row of matrix `M` so that `node`-th row of the equation
     * @f[ M u = v @f]
     * is a good approximation of the differential equation
     * @f[ \alpha \triangle u(node) = v(node) @f]
     * This is handy when solving equations implicitly.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit laplace 1d example
     */
    template<class matrix_t>
    void lap(matrix_t& M, int node, scalar_t alpha) const {
        static_assert(shape_mask & mlsm::lap, "Laplace shape must be initialized to use this "
                "function. Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        assert(M.rows() == domain_size && "Matrix has a wrong number of rows.");
        assert(M.cols() == domain_size && "Matrix has a wrong number of columns.");
        for (int i = 0; i < support_size; ++i) {
            M.coeffRef(node, support_domain[node * support_size + i]) +=
                alpha * shape_laplace[node * support_size + i];
        }
    }

    /**
     * @brief returns Laplace of vector field u in node-th node.
     * @param u field to be considered
     * @param node index of a node where the operator is evaluated
     * @details @f$\sum(shape_{\nabla^2}[i]u_{x}[i])@f$ for all dimensions
     * @return vector
     */
    template<template<class> class container>
    vec_t lap(const container<vec_t>& u, int node) const {
        static_assert(shape_mask & mlsm::lap, "Laplace shape must be initialized to use this "
                "function. Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        vec_t lap = static_cast<scalar_t>(0.);
        for (int i = 0; i < support_size; ++i)
            for (int d = 0; d < dim; ++d)
                lap[d] += shape_laplace[node * support_size + i] *
                          u[support_domain[node * support_size + i]][d];
        return lap;
    }

    /**
     * @brief Fill the matrix with the equation for Laplacian of a vector field @f$\vec{u}@f$
     * at the point `node` being equal to some other field.
     * This is the implicit version of `lap(u, node)` function above, but cannot be named the
     * same due to signature clash with the scalar version `lap(M, node, alpha)`.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `3N x 3N` matrix. Vector field `u` is considered to be represented as
     * @f$ u_j(node) = u[j*dim + node]@f$ or equivalently
     * @f$ \vec{u} = \begin{bmatrix} u_0 \\ u_1 \\ u_2 \end{bmatrix} @f$.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `node`-th, `N+node`-th and `2N+node`-th rows of the matrix are changed.
     * User must make sure that the matrix is large enough.
     * @param alpha scalar to multiply the operator with.
     * @details Fills the rows of matrix `M` so that `node`-th, `N+node`-th and `2N+node`-th rows
     * of the equation
     * @f[ M u = v @f]
     * are a good approximation of the differential equation
     * @f[ \alpha \triangle \vec{u}(node) = \vec{v}(node) @f]
     * This is handy when solving equations implicitly.
     *
     * @note While 3 was used as the dimension of the vector field in the above description,
     * vector fields of arbitrary dimensions are supported. The given matrix must have dimensions
     * `dim*N x dim*N` and `dim` rows at indexes `node`, `N+node`, ..., `N*(dim-1)+node` are
     * modified.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit vector laplace 2d example
     */
    template<class matrix_t>
    void lapvec(matrix_t& M, int node, scalar_t alpha) const {
        static_assert(shape_mask & mlsm::lap, "Laplace shape must be initialized to use this "
                "function. Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        assert(M.rows() == dim*domain_size && "Matrix has a wrong number of rows.");
        assert(M.cols() == dim*domain_size && "Matrix has a wrong number of columns.");
        for (int d = 0; d < dim; ++d) {
            for (int i = 0; i < support_size; ++i) {
                M.coeffRef(d*domain_size + node,
                           d*domain_size + support_domain[node * support_size + i]) +=
                        alpha * shape_laplace[node * support_size + i];
            }
        }
    }


    /**
     * @brief returns gradient of a scalar field u in node-th node.
     * @param u field to be considered
     * @param node index of a node where the operators is evaluated
     * @details return
     * @f$[\sum_i shape_{dx}[i]u[i],  \sum_i shape_{dy}[i]u[i], \sum_i shape_{dz}[i]u[i]] @f$
     * @return vector containing the components of the gradient, eg. @f$[u_x(node), u_y(node),
     * u_z(node)]@f$.
     */
    template<template<class> class container>
    vec_t grad(const container<scalar_t>& u, int node) const {
        static_assert(shape_mask & mlsm::grad, "D1 shapes must be initialized to use this function."
                " Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        vec_t ret = static_cast<scalar_t>(0.);
        for ( int d = 0; d < dim; ++d)  // loop over dimensions
            for (int i = 0; i < support_size; ++i)  // convolution loop
                ret[d] += shape_d1[node * dim * support_size + d * support_size + i] *
                            u[support_domain[node * support_size + i]];
        return ret.transpose();
    }

    /**
     * @brief Fill the matrix with the equation for gradient of a scalar field @f$S@f$
     * at the point `node` along another field @f$\vec{v}@f$ being equal to some other scalar field
     * @f$\psi@f$.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `N x N` matrix.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `node`-th row of the matrix is changed.
     * User must make sure that the matrix is large enough.
     * @param v vector field to multiply the operator with.
     * @details Fills the rows of matrix `M` so that `node`-th row of the equation
     * @f[ M S = \psi @f]
     * is a good approximation of the differential equation
     * @f[ \vec{v}(node) (\operatorname{grad}S)(node) = \psi(node) @f]
     * This is handy when solving equations implicitly.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit Grad example
    */
    template<class matrix_t>
    void grad(matrix_t& M, int node, const Range<vec_t>& v) const {
        static_assert(shape_mask & mlsm::grad, "D1 shapes must be initialized to use this function."
                " Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        assert(M.rows() == domain_size && "Matrix has a wrong number of rows.");
        assert(M.cols() == domain_size && "Matrix has a wrong number of columns.");
        for (int i = 0; i < support_size; ++i) {
            scalar_t shape_value = 0;
            for (int d = 0; d < dim; ++d) {
                shape_value += v[support_domain[node * support_size + i]][d] *
                        shape_d1[node * dim * support_size + d * support_size + i];
            }
            M.coeffRef(node, support_domain[node * support_size + i]) += shape_value;
        }
    }

    /**
     * @brief Returns gradient of a vector field @f$ \vec u @f$ in `node`-th node.
     * @param u Vector field to be considered.
     * @param node Index of a node where the operators is evaluated.
     * @return Matrix of a gradient of a vector field @f$ \nabla \vec u @f$.
     *   The returned structure is a matrix @f$ \mathrm{grad} @f$ such that
     *   @f$ \mathrm{grad}(i, j) = \frac{\partial u_i}{\partial x_j} @f$. e.g.
     *   @f$ \mathrm{grad}(0, 0) = \frac{\partial u_x}{\partial x} @f$,
     *   @f$ \mathrm{grad}(0, 1) = \frac{\partial u_x}{\partial y} @f$,
     *   @f$ \mathrm{grad}(1, 0) = \frac{\partial u_y}{\partial x} @f$.
     *
     * @details For 3d vector field it returns
     *   @f$ \left[\sum\limits_{i}\mathrm{shape}_{\mathrm dx}[i]\vec u[i],
     *        \sum\limits_{i}\mathrm{shape}_{\mathrm dy}[i]\vec u[i],
     *        \sum\limits_{i}\mathrm{shape}_{\mathrm dz}[i]\vec u[i] \right]
     *   @f$
     *   @f[
     *     \mathrm{grad} = \begin{bmatrix}
     *         \frac {\partial u_1}{\partial x_1} & \frac {\partial u_1}{\partial x_2} & \cdots \\
     *         \frac {\partial u_2}{\partial x_1} & \frac {\partial u_2}{\partial x_2} & \cdots \\
     *         \vdots & \vdots & \ddots
     *     \end{bmatrix}
     *   @f]
     */
    template<template<class> class container>
    Eigen::Matrix<scalar_t, dim, dim> grad(const container<vec_t>& u, int node) const {
        static_assert(shape_mask & mlsm::grad, "D1 shapes must be initialized to use this function."
                " Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        Eigen::Matrix<scalar_t, dim, dim> ret(dim, dim);
        ret.setZero();
        for (int d_v = 0; d_v < dim; ++d_v)
            for (int d = 0; d < dim; ++d)  // loop over dimensions
                for (int i = 0; i < support_size; ++i)  // convolution loop
                    ret(d, d_v) += shape_d1[node * dim * support_size + d * support_size + i] *
                                   u[support_domain[node * support_size + i]][d_v];
        return ret.transpose();
    }

    /**
     * @brief Fill the matrix with the equation for gradient of a vector field @f$u@f$
     * at the point `node` along another field @f$\vec{v}@f$ being equal to some other vector field
     * @f$f@f$. the name `gradvec` is due to a name clash with the scalar version above.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `3N x 3N` matrix. Vector field `u` is considered to be represented as
     * @f$ u_j(node) = u[j*dim + node]@f$ or equivalently
     * @f$ \vec{u} = \begin{bmatrix} u_0 \\ u_1 \\ u_2 \end{bmatrix} @f$.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `node`-th, `N+node`-th and `2N+node`-th rows of the matrix are changed.
     * User must make sure that the matrix is large enough.
     * @param v vector to multiply the operator with.
     * @details Fills the rows of matrix `M` so that `node`-th, `N+node`-th and `2N+node`-th rows
     * of the equation
     * @f[ M u = v @f]
     * are a good approximation of the differential equation
     * @f[ (\vec{v} \nabla) \vec{u}(node) = \vec{f}(node) @f]
     * This is handy when solving equations implicitly.
     *
     * @note While 3 was used as the dimension of the vector field in the above description,
     * vector fields of arbitrary dimensions are supported. The given matrix must have dimensions
     * `dim*N x dim*N` and `dim` rows at indexes `node`, `N+node`, ..., `N*(dim-1)+node` are
     * modified.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit Gradvec example
    */
    template<class matrix_t>
    void gradvec(matrix_t& M, int node, const Range<vec_t>& v) const {
        static_assert(shape_mask & mlsm::grad, "D1 shapes must be initialized to use this function."
                " Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        assert(M.rows() == dim*domain_size && "Matrix has a wrong number of rows.");
        assert(M.cols() == dim*domain_size && "Matrix has a wrong number of columns.");
        for (int i = 0; i < support_size; ++i) {
            scalar_t shape_value = 0;
            for (int d = 0; d < dim; ++d) {
                shape_value += v[support_domain[node * support_size + i]][d] *
                               shape_d1[node * dim * support_size + d * support_size + i];
            }
            for (int d = 0; d < dim; ++d) {
                M.coeffRef(d * domain_size + node,
                           d * domain_size + support_domain[node * support_size + i]) +=
                        shape_value;
            }
        }
    }

    /**
     * @brief Returns divergence of a vector field `u` in `node`-th node.
     * @details return @f$\sum(shape_{dx}[i]u_{x}[i]) + \sum(shape_{dy}[i]u_y[i]_{x_i}]) + ...@f$
     * @param u field to be considered
     * @param node index of a node where the operators is evaluated
     * @return scalar div value of the given field at the point `node`
     */
    template<template<class> class container>
    scalar_t div(const container<vec_t>& u, int node) const {
        static_assert(shape_mask & mlsm::div, "D1 shapes must be initialized to use this function. "
                "Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        scalar_t ret = 0;
        for (int d = 0; d < dim; ++d)
            for (int i = 0; i < support_size; ++i)  // convolution loop
                ret += shape_d1[node * dim * support_size + d * support_size + i] *
                        u[support_domain[node * support_size + i]][d];
        return ret;
    }

    /**
     * @brief Returns gradient of a div vector field @f$ \vec u @f$
     * in `node`-th node @f$ \nabla(\nabla\cdot\vec u)|_{\mathrm{node}}@f$.
     * @param u Vector field to be considered.
     * @param node Index of a node where the operators is evaluated.
     * @return Vector of gradient of a div of a vector field.
     *
     * @details For 3d vector field it returns
     *   @f$ \sum\limits_{i=0}^{\mathrm{dim}}\sum\limits_{j\in S_\mathrm{node}} \left[
     *        \mathrm{shape}_{\mathrm dx\mathrm di}[j] u_i[j],
     *        \mathrm{shape}_{\mathrm dy\mathrm di}[j] u_i[j],
     *        \mathrm{shape}_{\mathrm dz\mathrm di}[j] u_i[j]
     *       \right] ^ T
     *   @f$
     *
     *   Which mathematically equals
     *
     *   @f[
     *     \mathrm{graddiv} = \left[
     *         \frac {\partial}{\partial x_1} \sum\limits_{i=1}^{\mathrm{dim}}
     *           \frac{\partial u_i}{\partial x_i},
     *         \frac {\partial}{\partial x_2} \sum\limits_{i=1}^{\mathrm{dim}}
     *           \frac{\partial u_i}{\partial x_i},
     *         \cdots,
     *         \frac {\partial}{\partial x_{\mathrm{dim}}} \sum\limits_{i=1}^{\mathrm dim}
     *           \frac{\partial u_i}{\partial x_i}
     *     \right]^T
     *   @f]
     */
    template<template<class> class container>
    vec_t graddiv(const container<vec_t>& u, int node) const {
        static_assert(shape_mask & mlsm::graddiv, "D2 shapes must be initialized to use this "
                "function. Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        vec_t ret;
        ret.setZero();
        for (int d1 = 0; d1 < dim; ++d1) {
            for (int d2 = 0; d2 < dim; ++d2) {  // loop over dimensions
                int dmin = std::min(d1, d2);
                int dmax = std::max(d1, d2);
                for (int i = 0; i < support_size; ++i)  // convolution loop
                    ret[d1] += shape_d2[dim * (dim+1) * support_size * node / 2 +
                               support_size * (dmax*(dmax+1)/2 + dmin) + i] *
                               u[support_domain[node * support_size + i]][d2];
            }
        }
        return ret;
    }

    /**
     * @brief Fill the matrix with the equation for gradient of divergence of a vector field
     * @f$\vec{u}@f$ at the point `node` being equal to some other field.
     * This is the implicit version of `graddiv(u, node)` function above.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `3N x 3N` matrix. Vector field `u` is considered to be represented as
     * @f$ u_j(node) = u[j*dim + node]@f$ or equivalently
     * @f$ \vec{u} = \begin{bmatrix} u_0 \\ u_1 \\ u_2 \end{bmatrix} @f$.
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `node`-th, `N+node`-th and `2N+node`-th rows of the matrix are changed.
     * User must make sure that the matrix is large enough.
     * @param alpha scalar to multiply the operator with.
     * @details Fills the rows of matrix `M` so that `node`-th, `N+node`-th and `2N+node`-th rows
     * of the equation
     * @f[ M u = v @f]
     * are a good approximation of the differential equation
     * @f[ \alpha \nabla(\nabla \cdot \vec{u})(node) = \vec{v}(node) @f]
     * This is handy when solving equations implicitly.
     *
     * @note While 3 was used as the dimension of the vector field in the above description,
     * vector fields of arbitrary dimensions are supported. The given matrix must have dimensions
     * `dim*N x dim*N` and `dim` rows at indexes `node`, `N+node`, ..., `N*(dim-1)+node` are
     * modified.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit graddiv example
     */
    template<class matrix_t>
    void graddiv(matrix_t& M, int node, scalar_t alpha) const {
        static_assert(shape_mask & mlsm::graddiv, "D2 shapes must be initialized to use this "
                "function. Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        assert(M.rows() == dim*domain_size && "Matrix has a wrong number of rows.");
        assert(M.cols() == dim*domain_size && "Matrix has a wrong number of columns.");

        // Implicit equation is for all d: \sum_k \sum_j \chi_djk u_j(s_k) = v_d(s)
        for (int d = 0; d < dim; ++d) {
            for (int k = 0; k < support_size; ++k) {
                for (int j = 0; j < dim; ++j) {  // loop over dimensions
                    int dmin = std::min(d, j);
                    int dmax = std::max(d, j);
                    int idx = dim * (dim+1) * node / 2 +  (dmax*(dmax+1)/2 + dmin);
                    M.coeffRef(d*domain_size + node,
                               j*domain_size + support_domain[node * support_size + k]) +=
                            alpha * shape_d2[idx * support_size + k];
                }
            }
        }
    }

    /**
     * @brief Calculates a new value @f$ a @f$ of node @f$ node @f$ for a
     * specific boundary condition to be true.
     * @details By setting @f$ \vec u[node] = \vec a @f$ we satisfy the condition
     *     @f$ (\vec n \cdot \nabla) \vec u |_{node} = \vec v @f$. This is done
     *     using the following formula
     *   @f[ \vec a = \frac{ \vec v -
     *       \sum\limits_{i=1}^{\mathrm{support}}
     *           \left(\sum\limits_{d\in\{x, y, ...\}}
     *               n_d \ \ \mathrm{shape}_{\mathrm dd}[i]\right)
     *           \  \vec u[i]
     *    }{
     *        \sum\limits_{d\in \{x, y, ...\}} n_d \ \mathrm{shape}_{\mathrm dd}[0]
     *    } @f]
     *
     *     MLSM SUPPORT DOMAIN NOTICE:
     *     - Each node should be its first support node.
     *     - For symmetrical supports the central node may not affect the
     *       calculated derivation and therefore this function is unable to
     *       give a value and will crash.
     *
     * @tparam container A container type for the field. This will usually
     * be `Range` or `std::vector`.
     * @tparam field_t Type of field that is present in the domain. It's
     * type and dimensionality are generally independent of domain. This will be
     * `double` for scalar field or `Vec3d` for 3D vector field.
     *
     * @param u `field_t` field to be considered
     * @param node index of a node where the operators is evaluated
     * @param norm vector @f$ \vec n @f$ with the same dimensionality as the
     *             support domain giving the direction of the derivation
     * @param val `field_t` @f$ \vec v @f$ giving the value of the derivation
     *
     * @return A new value @f$ \vec a @f$ of type `field_t`
     *
     *  @throws assert Crashes if `node`'s first support node is not `node`
     *  @throws assert Crashes if `node`'s value has negligible effect on the
     *                 derivation, because no value could change the derivation
     */
    template<template<class> class container, class field_t>
    field_t neumann(const container<field_t>& u, int node, const vec_t& norm,
                   const field_t val) const {
        static_assert(shape_mask & mlsm::d1, "D1 shapes must be initialized to use this function. "
                "Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        assert(support_domain[node * support_size] == node
               && "First support node should be the node itself.");
        field_t result = val;
        scalar_t divisor = 0, nominator = 0;
        for (int d = 0; d < dim; ++d) {
            for (int i = 1; i < support_size; ++i) {
                result -= norm[d] * shape_d1[node * dim * support_size + d * support_size + i] *
                          u[support_domain[node * support_size + i]];
                nominator += std::abs(
                    norm[d] * shape_d1[node * dim * support_size + d * support_size + i]);
            }
            // i = 0
            divisor += norm[d] * shape_d1[node * dim * support_size + d * support_size];
        }
        assert(nominator < 1e10 * std::abs(divisor) &&
               "There was a problem getting the value because the given node"
               " has a negligible effect on the given derivation.");
        return result / divisor;
    }

    /**
     * @brief Fill the matrix with the equation of first derivative of some component of
     * @f$\vec{u}@f$  along some axis at the point `node` being equal to some value.
     * If used for boundary conditions then this is the implicit version of `neumann(u, node)`
     * function above.
     * @param M matrix representing the equations for all nodes. If `N` is the number of domain
     * nodes, then `M` is a `3N x 3N` matrix. Vector field `u` is considered to be represented as
     * @f$ u_j(node) = u[j*dim + node]@f$ or equivalently
     * @f$ \vec{u} = \begin{bmatrix} u_0 \\ u_1 \\ u_2 \end{bmatrix} @f$.
     * @param comp Which component to derive (0 based).
     * @param var With respect to which variable to derive (0 based).
     * @param node index of a node from 0 to `N` for which to write the equation for.
     * This means only `N*comp+node`-th row of the matrix is changed.
     * User must make sure that the matrix is large enough.
     * @param alpha scalar to multiply the operator with.
     * @param apply_to_comp the component to place the derivative at
     * This will place the derivative in 'N*apply_to_comp+node'-th row. Default value -1 will
     * set 'apply_to_comp = comp'.
     * @details Fills the rows of matrix `M` so that `N*comp+node` row
     * of the equation
     * @f[ M u = g @f]
     * is a good approximation of the differential equation
     * @f[ \alpha \frac{\partial u_{comp}}{\partial x_{var}}(node) = g(node) @f]
     * This is handy when solving equations implicitly.
     *
     * @note While 3 was used as the dimension of the vector field in the above description,
     * vector fields of arbitrary dimensions are supported. The given matrix must have dimensions
     * `dim*N x dim*N` and component and variable indices must be lower than `dim`.
     *
     * Example:
     * @snippet mlsm_operators_test.cpp Implicit derivative example
     */
    template<class matrix_t>
    void der1(matrix_t& M, int comp, int var, int node, scalar_t alpha,
              int apply_to_comp = -1) const {
        static_assert(shape_mask & mlsm::d1, "D1 shapes must be initialized to use this "
                "function. Check your MLSM shape bitmask.");
        NODE_ASSERTS(node);
        assert(0 <= comp && comp < dim && "Component index out of bounds.");
        assert(0 <= var && var < dim && "Variable index out of bounds.");
        assert(comp*domain_size + node < M.rows() && "Matrix does not have enough rows.");
        assert(comp*domain_size + node < M.cols() && "Matrix does not have enough columns.");
        assert(-1 <= apply_to_comp && apply_to_comp < dim &&
            "Component to apply to index out of bounds.");

        if (apply_to_comp == -1) {
            apply_to_comp = comp;
        }
        for (int k = 0; k < support_size; ++k) {
            M.coeffRef(apply_to_comp*domain_size + node,
                       comp*domain_size + support_domain[node * support_size + k]) +=
                    alpha * shape_d1[dim * support_size * node + support_size * var + k];
        }
    }

    template <typename D, typename A, int M>
    friend std::ostream& operator<<(std::ostream& os, const MLSM<D, A, M>& op);
};

/**
 * Prints data about this mlsm setup to stdout.
 * @param os Stream to print to.
 * @param op Operator collection to print.
 */
template <typename D, typename A, int M>
std::ostream& operator<<(std::ostream& os, const MLSM<D, A, M>& op) {
    std::string shapes = "";
    if (M & mlsm::d1) shapes += " d1";
    if (M & mlsm::lap) shapes += " d2";
    if (M & mlsm::d2) shapes += " lap";

    size_t mem = sizeof(op);
    size_t mem_d1 = mem_used(op.shape_d1);
    size_t mem_d2 = mem_used(op.shape_d2);
    size_t mem_lap = mem_used(op.shape_laplace);
    size_t mem_supp = mem_used(op.support_domain);

    os << "MLSM operator:\n"
       << "    type: " << typeid(op).name() << '\n'
       << "    dimension: " << op.dim << '\n'
       << "    domain size: " << op.domain_size << '\n'
       << "    support size: " << op.support_size << '\n'
       << "    shapes:" << shapes  << " (mask = " << std::bitset<8>(M) << ")\n"
       << "    mem used total: " << mem2str(mem + mem_d1 + mem_d2 + mem_lap + mem_supp) << '\n'
       << "        d1:   " << mem2str(mem_d1) << '\n'
       << "        d2:   " << mem2str(mem_d2) << '\n'
       << "        lap:  " << mem2str(mem_lap) << '\n'
       << "        supp: " << mem2str(mem_supp) << '\n'
       << '\n' << op.domain << '\n' << op.approx;
    return os;
}

/// @cond
template <typename domain_engine, typename approx_engine, mlsm::shape_flags mask>
const int MLSM<domain_engine, approx_engine, mask>::dim;
/// @endcond

/**
 * @brief make_mlsm returns mlsm class
 * @param domain reference to the domain engine
 * @param approx pointer to the approximation engine
 * @param ind range of nodes, in terms of indices, where the operators are prepared
 * @param use_const_shape defines either to use constant shapes or to use dynamic scaling
 * read more in MLSM constructor documentation
 * @tparam mask bitmask indicating which flags to create
 * @tparam approx_engine type of approximation engine
 * @tparam domain_engine type of domain engine
 * @warning template parameters have a different order than in class MLSM, to support
 * calls in form `make_mlsm<mlsm::lap>(engine, domain)`.
 * @return mlms object with operators
 */
template<mlsm::shape_flags mask = mlsm::all, typename domain_engine, typename approx_engine>
MLSM<domain_engine, approx_engine, mask> make_mlsm(const domain_engine& domain,
        approx_engine& approx, const Range<int>& ind, bool use_const_shape = true) {
    return MLSM<domain_engine, approx_engine, mask>(domain, approx, ind, use_const_shape);
}

}  // namespace mm

#endif  // SRC_MLSM_OPERATORS_HPP_
