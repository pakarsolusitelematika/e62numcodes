#ifndef SRC_DOMAIN_EXTENDED_HPP_
#define SRC_DOMAIN_EXTENDED_HPP_

#include "common.hpp"
#include "domain.hpp"
#include "kdtree.hpp"
#include "types.hpp"

namespace mm {

/**
 * @file
 * @brief File implementig extra functionality for domains.  This is in a separate file,
 * because of its dependancies, which are large and their inclusion may not always be
 * wanted by default.
 * @example domain_extended_test.cpp
 */

/**
 * @brief Finds and sets support of a domain. For each point at `support_size` points
 * closest to it are found and stored in support attribute. Points are stored in non
 * descending fashion according to the distances from the queried points, the first one
 * being the point itself.
 * The squares of distances from point `i` to its support are stored in `distances[i]`
 * array.
 * @param support_size Size of support for each node.
 */
template <class vec_t>
void Domain<vec_t>::findSupport(int support_size) {
    findSupport(std::numeric_limits<scalar_t>::infinity(), support_size);
}

/**
 * Find support for a subset of nodes.
 * @param support_size Size of support for each node. Must be smaller or equal to the
 * number of nodes.
 * @param for_which Indexes of nodes for which to find the support.
 **/
template <class vec_t>
void Domain<vec_t>::findSupport(int support_size, const Range<int>& for_which) {
    findSupport(std::numeric_limits<scalar_t>::infinity(), support_size,
                for_which);
}

/**
 * Find support for a subset of nodes, and also looking for support only among
 *`search_among`.
 * @param support_size Size of support for each node. Must be smaller or equal to the
 * number of nodes.
 * @param for_which Indexes of nodes for which to find the support.
 * @param search_among Indexes of nodes among which to search for support.
 * @param force_self If true each node's support will contain itself regardless,
 * even if it is not in `search_among`.
 *
 * Example: (find support for boundary nodes, but support is made only of internal nodes)
 * @code
 * d.findSupport(1, d.types < 0, d.types > 0);
 * @endcode
 **/
template <class vec_t>
void Domain<vec_t>::findSupport(int support_size, const Range<int>& for_which,
                                const Range<int>& search_among, bool force_self) {
    findSupport(std::numeric_limits<scalar_t>::infinity(), support_size, for_which,
                search_among, force_self);
}

/**
 * Same as without radius, but only points within the radius are included in support.
 * If radius is infinite, this is equal to the first version.
 */
template <class vec_t>
void Domain<vec_t>::findSupport(scalar_t radius_squared, int max_support_size) {
    Range<int> all_ind(positions.size());
    std::iota(all_ind.begin(), all_ind.end(), 0);
    findSupport(radius_squared, max_support_size, all_ind);
}

/**
 * Same as without radius, but only points within the radius are included in support.
 */
template <class vec_t>
void Domain<vec_t>::findSupport(scalar_t radius_squared, int max_support_size,
                                const Range<int>& for_which) {
    assert(positions.size() > 0 && "Cannot find support in an empty domain.");
    Range<int> all_ind(positions.size());
    std::iota(all_ind.begin(), all_ind.end(), 0);
    findSupport(KDTree<vec_t>(positions), radius_squared, max_support_size,
                for_which, all_ind);
}

/**
 * Same as without radius, but only points within the radius are included in support.
 */
template <class vec_t>
void Domain<vec_t>::findSupport(scalar_t radius_squared, int max_support_size,
                                const Range<int>& for_which,
                                const Range<int>& search_among,
                                bool force_self) {
    assert(search_among.size() > 0 &&
           "You probably did not mean to only look for support nodes among nodes in an emtpy set.");
    for (int x : search_among)
        assert(0 <= x && x < positions.size() &&
               "One of the indexes for positions to search among is out of bounds.");
    findSupport(KDTree<vec_t>(positions[search_among]), radius_squared, max_support_size,
                for_which, search_among, force_self);
}

/**
 * Overload with user provided kdtree. The tree must contain points with indexes listed in
 * `search_among`. The rest of parameters are the same as usual.
 */
template <class vec_t>
void Domain<vec_t>::findSupport(const KDTree<vec_t>& tree, scalar_t radius_squared,
                                int max_support_size, const Range<int>& for_which,
                                const Range<int>& search_among, bool force_self) {
    assert(max_support_size > 0 && "Support size must be greater than 0.");
    assert(max_support_size <= tree.size() &&
           "Support size cannot exceed number of points that we are searching among.");
    assert(tree.size() == search_among.size() &&
           "Size of indexes to search among must match the size of the tree.");
    assert(radius_squared > 0 && "Support radius must be greater than 0.");
    assert(for_which.size() > 0 &&
           "You probably did not mean to find support for an empty set of points.");
    assert(search_among.size() > 0 &&
           "You probably did not mean to only look for support nodes among nodes in an emtpy set.");
    for (int x : for_which)
        assert(0 <= x && x < positions.size() &&
               "One of the indexes for which to find the support is out of bounds.");
    for (int x : search_among)
        assert(0 <= x && x < positions.size() &&
               "One of the indexes for positions to search among is out of bounds.");

    int n = positions.size();
    support.resize(n);  // make sure push back does not reallocate
    distances.resize(n);
    // Only clear those supports that we will re-write
    support[for_which] = Range<int>();
    distances[for_which] = Range<scalar_t>();
    bool is_inf = std::isinf(radius_squared);
    for (int i : for_which) {
        auto res = (is_inf) ? tree.query(positions[i], max_support_size)
                            : tree.query(positions[i], radius_squared, max_support_size);
        support[i].reserve(res.first.size());
        distances[i].reserve(res.first.size());
        if (force_self && (search_among[res.first[0]] != i)) {
            // Add current point to its support
            support[i].push_back(i);
            distances[i].push_back(0);
            // Remove the furthest point from support
            res.first.pop_back();
            res.second.pop_back();
        }
        for (int j : res.first) support[i].push_back(search_among[j]);
        for (scalar_t j : res.second) distances[i].push_back(j);
    }
}

/**
 * Method runs an iterative process of relaxing domain and minimizing node
 * density, resulting in an interior distributed according to the repulsion force
 * field. The functions first finds support for all nodes. The size of support
 * is equal for all nodes and specified by `support_size` parameter. Then it
 * moves each point according to the "force" induced by its support. For
 * specific force related details see `getForce`.
 * This algorithm is then repeated for `num_of_iterations` iterations.
 * @param support_size Number of points in support.
 * @param force_factor Force if multiplied by this parameter. This can be used
 * to explicitly regulate move size and consequently stability. A suitable
 * starting value is from 0.1 to 0.01.
 * @param repulsion Repulsion force field
 * @param alpha Potential exponent parameter
 * @param num_of_iterations Number of iterations to run.
 * @param verbose If nonzero, it prints max move performed by relax
 * every `verbose` iterations.
 * @param move_only Move only nodes with indices in this list.
 * @sa getForce
 */
template <class vec_t>
template <class Func>
void Domain<vec_t>::relax(int support_size, scalar_t force_factor, Func repulsion,
                          scalar_t alpha, int num_of_iterations, const Range<int>& move_only,
                          int verbose) {
    int n = positions.size();
    assert(n >= support_size && "Cannot relax a domain with less that support_size nodes.");
    for (int i : move_only) {
        assert(0 <= i && i <= size() && "At least one index supplied in move_only was invalid.");
    }
    scalar_t ch_dist = characteristicDistance();
    std::mt19937 generator(get_seed());
    std::uniform_real_distribution<scalar_t> uniform(0, 1);
    vec_t diff = bbox.second - bbox.first;
    force_factor *= std::pow(ch_dist, alpha + 1) / std::pow(support_size, 1.0 / vec_t::dimension);
    for (int c = 0; c < num_of_iterations; ++c) {
        findSupport(support_size);
        double max_move = 0;
        for (int i : move_only) {
            vec_t dp = force_factor * getForce(i, repulsion, alpha);
            positions[i] += dp;
            max_move = std::max(max_move, dp.norm());
            // if point escaped put it randomly inside
            while (!contains(positions[i])) {
                for (int j = 0; j < vec_t::dimension; ++j) {
                    positions[i][j] = bbox.first[j] + uniform(generator) * diff[j];
                }
            }
        }
        if (verbose > 0 && c % verbose == 0) {
            std::cout << "[relax] max move in " << c << "-th iteration was "
                      << max_move << std::endl;
        }
    }
}

/// Overload where all of the interior can move
template <class vec_t>
template <class Func>
void Domain<vec_t>::relax(int support_size, scalar_t force_factor, Func repulsion,
                          scalar_t alpha, int num_of_iterations, int verbose) {
    relax(support_size, force_factor, repulsion, alpha, num_of_iterations, types > 0, verbose);
}

/// Overload for constant vector function
template <class vec_t>
void Domain<vec_t>::relax(int support_size, scalar_t force_factor, const vec_t& repulsion,
                          scalar_t alpha, int num_of_iterations, int verbose) {
    relax(support_size, force_factor, [=](const vec_t&) { return repulsion; }, alpha,
          num_of_iterations, verbose);
}

/// Overload for constant scalar function
template <class vec_t>
void Domain<vec_t>::relax(int support_size, scalar_t force_factor, scalar_t repulsion,
                          scalar_t alpha, int num_of_iterations, int verbose) {
    relax(support_size, force_factor, [=](const vec_t&) { return repulsion; }, alpha,
          num_of_iterations, verbose);
}

/**
 * Return force acting on point with index `index`.
 * Force between two points is calculated as
 * @f[ \displaystyle \vec{F}_{ij} = -\chi^{\alpha+1}_r \cdot \frac{\mathrm{force\_factor}}
 * {(\mathrm{support\_size})^{\frac{1}{\mathrm{dim}}}} \cdot \frac{\Delta \vec{p} \ast
 *\vec{r}}
 * {|\Delta \vec{p} \ast \vec{r}|^{\alpha + 1}} \cdot |\vec{r}|^{\alpha + 1} @f]
 * and force to a single node is equal to the sum of forces to it induced by nodes
 * in its support:
 * @f[ \vec{F}_i = \sum_{j\,\in\,\mathrm{support(i)}} \vec{F}_{ji}. @f]
 * Explanation of variables:  @f$\Delta\vec{p} = \vec{p}_i - \vec{p}_j@f$ is the
 *difference
 * vector betwwen two points, @f$\vec{p}_i@f$ is the position of `i`-th node,
 * @f$\vec{r} = \mathrm{repulsion}(\vec{p}_i) \ast \mathrm{repulsion}(\vec{p}_j)@f$ is the
 *repusion
 * force vector, @f$\chi_r@f$ is the Domain::characteristic_distance, and @f$\ast@f$ is
 * elementwise vector multiplication. If repulsion is constant, then nodes will be
 *distibuted
 * evenly, otherwise this specifies the "antidensity".
 *
 * Factor invoslving support size takes care that the force is independant of support size
 *changes,
 * multiplying difference vector with repulsion takes care of different repulsion in x and
 *y axis
 * (simple coordinate transform before applying the force
 *@f$\frac{\vec{x}}{|\vec{x}|^{\alpha+1}}@f$.
 * Multiplication with absolute repulsion value cancels out the additional repulsion
 *caused by
 * potential, so that you are left with a single @f$\vec{r}@f$. Think of  @f$\vec{r}@f$ as
 *a scalar
 * and cancel them out -- you are left only with @f$
 *r(\vec{p})\frac{\Delta\vec{p}}{|\Delta\vec{p}|}
 * @f$ -- just a simple application of repulsion force (this would be enough if we did not
 *want
 * different x and y densities). Normalizing factor @f$ \chi^{\alpha+1}_r @f$ makes the
 *force
 * independant to scaling the domain or increasing the number of nodes.
 *
 * Force is dependant on repulsion norm and alpha -- changing them and not the force may
 *cause the
 * nodes to go wild or not move at all.
 *
 * @param index Index of the point to calculate the force for.
 * @param repulsion Function returning the repulsion vector field.
 * Function signature must be either `VecXd repulsion(const VecXd&)` or
 * or `scalar_t repulsion(const VecXd&)`. Repulsion represents the
 * strength of repulsion force between nodes in each dimension at a certain
 * point in space.
 * The function should be positive in each dimension if you want the
 * force to actually be repulsive. In case of negative values the force will
 * attractive and nodes will turn into one blob :) A good starting point is
 * to set the repulsion to 1.
 * @param alpha Alpha in the equation above -- regulates how force is dependant
 * on distnace between points. Alpha should be positive. A suitable starting value is 3.
 *Greater
 * alpha means steeper potential and more forceful initial but later very delicate
 *movement,
 * while smaller alpha keeps the forces larger even when nodes are further away.
 * @return Force vector representing the resulting force of acting on this
 * point.
 **/
template <class vec_t>
template <class Func>
vec_t Domain<vec_t>::getForce(int index, Func repulsion, scalar_t alpha) const {
    assert(support.size() == positions.size() &&
           "Support is not calculated for all points.");
    assert(0 <= index && index < support.size() &&
           "Cannot get force for point with invalid index.");
    assert(alpha > 0 && "Exponent in force potential should be greater than 0.");
    vec_t resulting_force = vec_t::Zero();
    const vec_t& point = positions[index];
    vec_t ri = static_cast<vec_t>(repulsion(positions[index]));
    scalar_t ri_norm_pow = std::pow(ri.norm(), alpha + 1);
    for (int j : support[index]) {
        if (j != index) {
            vec_t rj = static_cast<vec_t>(repulsion(positions[j]));
            vec_t diff = ri.cwiseProduct(rj).cwiseProduct(positions[j] - point);
            resulting_force += ri_norm_pow * std::pow(rj.norm(), alpha + 1) * diff /
                               std::pow(diff.norm(), alpha + 1);
        }
    }
    return -resulting_force;
}

/**
 * Returns unit normal to an interval, eq. vector 1. Here only for compilation reasons.
 */
template <typename vec_t>
vec_t Domain<vec_t>::getUnitNormal(const Range<Vec1d>& /* p */) { return Vec1d({1}); }

/**
 * Returns right hand side unit normal given two points on boundary.
 * @param p List of two points on the boundary.
 */
template <typename vec_t>
vec_t Domain<vec_t>::getUnitNormal(const Range<Vec2d>& p) {
    assert(p.size() == 2 && "Two points are needed to compute the normal in 2d.");
    Vec2d a = p[0], b = p[1];
    vec_t boundary_direction = b - a;
    boundary_direction /= boundary_direction.norm();
    return boundary_direction.getPerpendicular();
}

/**
 * Returns unit normal to the surface given by three points.
 * @param p List of three points on the boundary.
 */
template <typename vec_t>
vec_t Domain<vec_t>::getUnitNormal(const Range<Vec3d>& p) {
    assert(p.size() == 3 && "Three points are needed to compute the normal in 3d.");
    vec_t a = p[0], b = p[1], c = p[2];
    vec_t t1 = b-a, t2 = b-c;
    vec_t normal = t1.cross(t2);
    return normal/normal.norm();
}

/**
 * Adds a new point to the boundary close to the point hint. First finds two closest points on the
 * boundary, constructs a unit normal, does an exponential search for the other side and then uses
 * bisection to find the precise location of the boundary. the returned point lies within
 * `precision` of the analytical boundary. Works in 2D and 3D.
 *
 * @param hint Point close to the boundary hinting where to add the boundary point.
 * @param type Type of the node to be added. NODE_TYPE::BOUNDARY is chosen by default.
 * @param precision The distance of the found point from the boundary will be at most `precision`.
 * @param tree KDtree containing the boundary points. This can be user supplied and is useful if
 * more subsequent calls will be performed to avoid rebuilding the tree every time.
 * @return Whether the addition was successful and the coordinates of the point added, if it was.
 *
 * Example:
 * @snippet domain_extended_test.cpp Add to boundary
 */
template <typename vec_t>
std::pair<bool, vec_t> Domain<vec_t>::addPointToBoundary(
        const vec_t& hint, int type, double precision, const KDTree<vec_t>& tree) {
    assert(tree.size() > 0 &&
           "Cannot refine boundary on a domain with no boundary nodes.");
    static_assert(vec_t::dimension == 1 || vec_t::dimension == 2 || vec_t::dimension == 3,
                  "Currently available only in 1, 2 or 3 dimensions.");
    if (vec_t::dimension == 1) {  // noop in 1d
        return {false, hint};
    }
    Range<int> idxs;
    Range<double> distances;
    std::tie(idxs, distances) = tree.query(hint, vec_t::dimension);
    assert(idxs.size() == vec_t::dimension && "Cannot enough closest points on boundary.");

    // Find unit normal
    vec_t unit_normal = getUnitNormal(tree.get(idxs));
    // Find point on the other side of the boundary with exponential search
    vec_t start = hint;
    bool is_inside = contains(start);
    scalar_t max_stretch = characteristicDistance() / 100;
    while (true)  {
        if (contains(start + max_stretch * unit_normal) != is_inside) break;
        if (contains(start - max_stretch * unit_normal) != is_inside) { max_stretch *= -1; break; }
        max_stretch *= 2;
        if (std::isinf(max_stretch)) {  // hint is bad
            return {false, vec_t()};
        }
    }

    // Find the point on the boundary using bisection
    scalar_t stretch = max_stretch;
    while (std::abs(stretch) > precision) {
        stretch /= 2;
        if (contains(start + stretch * unit_normal) == is_inside) {
            start += stretch * unit_normal;
        }
    }

    // Make unit normal point outside
    if (is_inside) {
        unit_normal *= signum(max_stretch);
    } else {
        unit_normal *= -signum(max_stretch);
    }

    // Make sure point is inside (it's maybe EPS away)
    while (!contains(start)) start -= precision * unit_normal;

    // Add the point
    addPoint(start, type);
    return {true, start};
}

/**
 * Adds a new point to the boundary close to the point `hint`.
 * @sa Domain::addPointToBoundary(hint, type, precision, tree)
 */
template <typename vec_t>
std::pair<bool, vec_t> Domain<vec_t>::addPointToBoundary(const vec_t& hint, int type,
                                                         double precision) {
    Range<vec_t> boundary = positions[types < 0];
    assert(boundary.size() > 0 &&
           "Cannot refine boundary on a domain with no boundary nodes.");
    static_assert(vec_t::dimension == 1 || vec_t::dimension == 2 || vec_t::dimension == 3,
                  "Currently available only in 1, 2 or 3 dimensions.");
    return addPointToBoundary(hint, type, precision, KDTree<vec_t>(boundary));
}

/**
* Refines a point with given index `node`.
* @sa Domain::refine(region, num_of_links, fraction, new_ids, rebuild_support)
*/
template <class vec_t>
Range<int> Domain<vec_t>::refine(int node, int num_of_links, scalar_t fraction,
                                 bool rebuild_support) {
    return refine({node}, num_of_links, fraction, rebuild_support);
}

/**
* @brief Refine a region of nodes `region` by splitting a given number of links `num_of_links`.
* @details The refine routine iterates through points in the given region and creates child nodes in
* the middle of links with the point support nodes. The new children are filtered to
* meet a minimum distance criterion to prevent points that would be too close.
*
* @param region Node indexes around which to refine.
* @param num_of_links The number of links to refine.
* @param fraction Fraction of the shortest link to use as minimum distance criterion.
* @param rebuild_support Whether to rebuild support or not.
* @return The indexes of the added nodes in `positions`.
*
* Example: (refine all nodes)
* @code
* d.refine(d.types != 0, 12, 0.4, true);
* @endcode
*
* @todo TODO(Ivan): write tests and "refine" algorithm (pun intended).
*/
template <class vec_t>
Range<int> Domain<vec_t>::refine(const Range<int>& region, int num_of_links,
                                 scalar_t fraction, bool rebuild_support) {
    int n = region.size();
    int size = positions.size();

    assert(n > 0 && "The region to refine is empty.");
    assert(num_of_links > 0 && "You should refine at least one link.");

    // refine relies on addPointToBoundary that has "noop" function
    // in case of 1D domain. Realistically though, this refine function is
    // unnecessarily complex for 1D refinement.
    static_assert(vec_t::dimension < 4, "Only available in 1, 2 or 3 dimensions");

    if (rebuild_support) {
        findSupport(num_of_links + 1, region);
    }

    // create empty ranges for storage
    Range<vec_t> new_points;
    Range<int> new_types;
    Range<Range<int>> children;
    new_points.reserve(num_of_links*n);
    new_types.reserve(num_of_links*n);
    children.resize(n);

    // iterate through points in region and generate new points
    int np = 0;
    for (int i = 0; i < n; i++) {
        int c = region[i];  // the global domain index
        Range<int> supp = support[c];
        scalar_t min_dist = fraction*std::sqrt(distances[c][1]);

        // create a list of points to compare too
        Range<vec_t> close_points;
        for (int j = 1; j <= num_of_links; j++) {
            close_points.push_back(positions[supp[j]]);
        }
        Range<int> idx;
        idx.reserve(num_of_links);
        for (int j = 1; j <= num_of_links; j++) {
            for (int i = 0; i < n; i++) {
                if (supp[j] == region[i]) {
                    idx.push_back(i);
                    break;
                }
            }
        }
        for (auto& i : idx) {
            close_points.append(new_points[children[i]]);
        }

        // generate new points
        for (int j = 1; j <= num_of_links; j++) {
            vec_t child = positions[c];
            child += positions[supp[j]];
            child *= 0.5;

            // check that child is not too close to other points
            bool too_close = false;
            for (vec_t point : close_points) {
                vec_t dist = (child - point).norm();
                if (dist < min_dist) {
                    too_close = true;
                    break;
                }
            }

            if (!too_close) {
                // add new point
                close_points.push_back(child);
                new_points.push_back(child);
                children[i].push_back(np++);  // increase new point counter

                // decide type of new node
                int child_type = NODE_TYPE::INTERNAL;
                if (types[c] < 0 && types[supp[j]] < 0) {
                    child_type = NODE_TYPE::BOUNDARY;
                }
                new_types.push_back(child_type);
            }
        }
    }

    // add points to domain
    Range<vec_t> boundary = positions[types < 0];
    KDTree<vec_t> tree(boundary);
    for (int i = 0; i < new_points.size(); i++) {
        // for boundary points move point to boundary
        if (new_types[i] < 0) {
            bool success;
            vec_t added_point;
            std::tie(success, added_point) =
                addPointToBoundary(new_points[i], new_types[i], EPS, tree);
        } else {
            addPoint(new_points[i], new_types[i]);
        }
    }

    // return back indices of new nodes
    Range<int> new_ids(new_points.size());
    for (int i = 0; i < new_points.size(); i++) {
        new_ids[i] = size + i;  // Fill in new indexes. They will begin from old size.
    }
    return new_ids;
}

}  // namespace mm

#endif  // SRC_DOMAIN_EXTENDED_HPP_
