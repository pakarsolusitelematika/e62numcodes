#ifndef SRC_DOMAIN_HPP_
#define SRC_DOMAIN_HPP_

/**
 * @file
 * @brief Header file implementing domains.
 * All domains are derived from an abstract base class Domain.
 * Specific basi geometric-shaped domains are supported:
 *  - rectangular
 *  - circular
 *
 * as well as their compositions:
 * - union
 * - difference
 * @example domain_test.cpp
 * @todo TODO(jureslak): add rotations and translations.
 */

#include "common.hpp"
#include "includes.hpp"
#include "types.hpp"
#include "util.hpp"

namespace mm {

template <class T>
class KDTree;

/**
 * Enum holding the defaults for node types;
 */
enum NODE_TYPE : int {
    BOUNDARY = -1,  ///< Boundary is -1
    INTERNAL = 1  ///< Internal is +1
};

/**
 * Enum for the normal direction vector, telling in which side of the shape are the nodes
 * contained.
 */
enum NORMAL : int {
    INSIDE = 1,  ///< Indicating normal vector is pointing inside
    OUTSIDE = -1  ///< Indicating normal vector is pointing outside
};

/// Enum for the thicknessness of domain.
enum THICKNESS : int {
    THICK = 1,  ///< Indicating domain is thick
    THIN = -1  ///< Indicating domain is thin
};

/**
 * Enum to indicate what type of border conflict resolution is wanted
 * when two borders overlap in add or subtract.
 * <b>This only affects overlaping borders.</b>
 */
enum class BOUNDARY_TYPE {
    NONE,    ///< No border
    SINGLE,  ///< Single border
    DOUBLE   ///< Duplicated border
};

/**
 * @brief An abstract base class for domains.
 */
template <class vec_t>
class Domain {
  public:  // any variable added here should also be copied in the clone function
    typedef typename vec_t::Scalar scalar_t;  ///< Numeric data type.
    Range<vec_t> positions;  ///< Positions of internal discretization points.
    /**
     * Variable used to store support points of each node. For each node i, support[i] is
     * list of indices of i's suport ordered by distance from i.
     * The first element in the list is always i (if support had been set), followed by one or more
     * others. The according squared distances are stored in `distances` array.
     * Support positions can for eaxmple be accessed using simply:
     * @code
     * positions[support[i]]
     * @endcode
     **/
    Range<Range<int>> support;
    /**
     * This variable stores squared distances to each point of point's support. distances[i] is a
     * sorted array of squared distances to support points, starting with zero and continuing in
     * nondescending fashion.
     **/
    Range<Range<scalar_t>> distances;
    /**
     * Storing types of nodes aligned with poistions. <b>Negative types are reserved for
     * boundary nodes, positive for interior. Zero type is not used</b>
     * Example:
     * @code
     * positions = {p1, p2, p3, p4, p5};
     * types =     { 2, -1,  2,  1, -1};
     * @endcode
     * Nodes p2 and p5 are of type -1, p1 and p3 of type 2 and so on.
     */
    Range<int> types;
    Range<std::unique_ptr<Domain<vec_t>>> child_domains;  ///< List of child domains

    /// Allow default constructor
    Domain<vec_t>() : normal_vector(INSIDE), thickness(THICK), contains_precision(EPS) {}
    /// Allow move constructor
    Domain<vec_t>(Domain<vec_t>&&) = default;
    /// Allow move assignment
    Domain<vec_t>& operator=(Domain<vec_t>&&) = default;
    /// Remove copy constructor
    Domain<vec_t>(const Domain<vec_t>&) = delete;
    /// Remove copy assignment
    Domain<vec_t>& operator=(const Domain<vec_t>&) = delete;
    /// Virtual destructor, so derived destuctors get invoked and we avoid memory leaks.
    virtual ~Domain() {}
    /// makes a clone of a domain
    template <typename T>
    static T&& makeClone(const T &t) {
        return std::move(*static_cast<T*>(static_cast<const Domain<vec_t>*>(&t)->clone()));
    }
    /**
     * @brief Returs a range of all boundary nodes.
     * @returns A range of all nodes with type < 0
     */
    Range<vec_t> getBoundaryNodes() const { return positions[types < 0]; }
    /**
     * @brief Returs a range of all internal nodes.
     * @returns A range of all nodes with type > 0
     */
    Range<vec_t> getInternalNodes() const { return positions[types > 0]; }
    /// returns number of nodes in a domain
    int size() const { return positions.size(); }
    /**
     * @brief Tests if a point is contained in a domain and not contained
     * in any obstacles.
     * @param point The point we are interested in,
     * @return True if point is conatined in the basic domain
     * and false otherwise.
     */
    bool contains(const vec_t& point) const {
        auto it = child_domains.rbegin();  // reverse iterator, because new add
        for (; it != child_domains.rend(); ++it) {  // overrides old one
            switch ((*it)->normal_vector) {
                case INSIDE:  // inside one of the union members
                    if ((*it)->contains(point)) return true;
                    break;
                case OUTSIDE:  // inside one of the obstacles
                    if (!(*it)->contains(point)) return false;
                    break;
            }
        }
        return inside(point);
    }

    /**
     * Adds a single point with specified type to the domain.
     * @param point Point to add.
     * @param type Type of the point to add.
     */
    void addPoint(const vec_t& point, int type) {
        assert(contains(point) &&
               "Point you are tyring to add is not contained in the domain!");
        positions.push_back(point);
        types.push_back(type);
    }

    /**
     * @brief Adds a domain as a member to the union. Domain given as a parameter is deep
     * copied.
     * Domain nodes are moved to the union and removed from given domain.
     * @param d Domain to be added as a union.
     * @param bt How to solve overlapping boundary conflicts.
     */
    void add(const Domain& d, BOUNDARY_TYPE bt = BOUNDARY_TYPE::SINGLE) {
        add(std::unique_ptr<Domain>(d.clone()), bt);
    }
    /**
     * @brief Adds a domain as a member to the union. The ownership of a pointer is
     * transferred to this instance and the pointer is freed when this instance
     * goes out of scope. Domain nodes are moved to the union and removed from given
     * domain.
     * @param d Pointer to a domain that is to be added as a union.
     * @param bt How to solve overlapping boundary conflicts.
     */
    void add(std::unique_ptr<Domain<vec_t>> d, BOUNDARY_TYPE bt = BOUNDARY_TYPE::SINGLE) {
        d->setUnionNormal();
        switch (bt) {
            case BOUNDARY_TYPE::DOUBLE:
                setThin();
                d->setThin();
                break;
            case BOUNDARY_TYPE::SINGLE:
                setThick();
                d->setThin();
                break;
            case BOUNDARY_TYPE::NONE:
                setThick();
                d->setThick();
                break;
        }
        Range<int> invalid_old;
        for (int i = 0; i < size(); ++i) {
            if (types[i] < 0 && d->contains(positions[i])) {
                invalid_old.push_back(i);
            }
        }
        positions[invalid_old].remove();
        types[invalid_old].remove();

        Range<int> invalid_new;
        for (int i = 0; i < d->size(); ++i) {
            if (d->types[i] < 0 && contains(d->positions[i])) {
                invalid_new.push_back(i);
            }
        }
        d->positions[invalid_new].remove();
        d->types[invalid_new].remove();

        positions.append(d->positions);
        types.append(d->types);

        // update bbox
        for (int i = 0; i < vec_t::dimension; ++i) {
            bbox.first[i] = std::min(bbox.first[i], d->bbox.first[i]);
            bbox.second[i] = std::max(bbox.second[i], d->bbox.second[i]);
        }

        d->clear();  // we dont need these any more
        child_domains.push_back(std::move(d));
        setThick();
    }

    /**
     * @brief Adds a domain as an obstacle. Given domain is deep copied.
     * Nodes are automatically updated and nodes that are now inside and
     * obstacle are automatically deleted.
     * @param d Domain to add as an obstacle.
     * @param bt How to solve overlapping boundary conflicts.
     */
    void subtract(const Domain& d, BOUNDARY_TYPE bt = BOUNDARY_TYPE::SINGLE) {
        subtract(std::unique_ptr<Domain>(d.clone()), bt);
    }
    /**
     * @brief Adds a domain as an obstacle. Ownership of the unique_ptr is
     * transffered to this instance. Nodes are automatically updated and nodes
     * that are now inside and obstacle are automatically deleted.
     * @param d unique_ptr to Domain that is to be added as an obstacle.
     * @param bt How to solve overlapping boundary conflicts.
     */
    void subtract(std::unique_ptr<Domain<vec_t>> d, BOUNDARY_TYPE bt = BOUNDARY_TYPE::SINGLE) {
        d->setObstacleNormal();
        // set boundary type
        switch (bt) {
            case BOUNDARY_TYPE::DOUBLE:
                setThick();
                d->setThick();
                break;
            case BOUNDARY_TYPE::SINGLE:
                setThin();
                d->setThick();
                break;
            case BOUNDARY_TYPE::NONE:
                setThin();
                d->setThin();
                break;
        }
        // clear invalid positions and their types
        Range<int> invalid = positions.filter(
            [&](const vec_t& point) { return !d->contains(point); });
        positions[invalid].remove();
        types[invalid].remove();

        // add new boundary nodes
        Range<int> new_boundary;
        int num_of_nodes = d->size();
        for (int i = 0; i < num_of_nodes; ++i) {
            if (d->types[i] < 0 && contains(d->positions[i])) {
                new_boundary.push_back(i);
            }
        }
        positions.append(d->positions[new_boundary]);
        types.append(d->types[new_boundary]);

        // leave bbox as is

        d->clear();  // we dont need these any more
        child_domains.push_back(std::move(d));  // add it to the obstacle list
        setThick();
    }
    /**
     * @brief Returns a sting of Matlab-like representation of internal and
     * boundary nodes.
     */
    std::string getMatlabData() const {
        std::stringstream ss;
        ss << "boundary=[";
        for (const vec_t& x : getBoundaryNodes()) {
            ss << x << ';';
        }
        ss << "];\n";
        ss << "internal=[";
        for (const vec_t& x : getInternalNodes()) ss << x << ';';
        ss << "];\n";
        return ss.str();
    }
    /// Checks if domain is in valid state.
    bool valid() const {
        if (size() != types.size()) return false;
        for (const vec_t& pos : getInternalNodes()) {
            if (!contains(pos)) return false;
        }
        for (const vec_t& pos : getBoundaryNodes()) {
            if (contains(pos) == (thickness == THIN)) return false;
        }
        return true;
    }
    /// Clears all data about positions and types
    void clear() {
        positions.clear();
        types.clear();
        support.clear();
        distances.clear();
    }
    /// Clears boundary nodes
    void clearBoundaryNodes() {
        auto indices = types < 0;
        int n = size();
        positions[indices].remove();
        types[indices].remove();
        if (support.size() == n) support[indices].remove();
        if (distances.size() == n) distances[indices].remove();
    }
    /// Clears internal nodes
    void clearInternalNodes() {
        auto indices = types > 0;
        int n = size();
        positions[indices].remove();
        types[indices].remove();
        if (support.size() == n) support[indices].remove();
        if (distances.size() == n) distances[indices].remove();
    }
    /**
     * Remove nodes that are to close.
     * If two nodes are too close they are removed by the following scheme:
     * - if they are of the same type, the first one is removed.
     * - if one is boundary and one is internal, the internal one is removed
     * @param precision If nodes are closer than precision, one of them is removed.
     */
    void clearDuplicateNodes(scalar_t precision) {
        Range<int> to_remove;
        int n = size();
        for (int i = 0; i < n; ++i) {
            for (int j = i + 1; j < n; ++j) {
                if ((positions[i] - positions[j]).stableNorm() < precision) {
                    if (signum(types[i]) == signum(types[j]) || types[i] > 0) {
                        to_remove.push_back(i);
                    } else {
                        to_remove.push_back(j);
                    }
                }
            }
        }
        types[to_remove].remove();
        positions[to_remove].remove();
    }
    /**
     * Guesses appropriate precision for contains tests.
     * Our guess is
     * @f$\left(\displaystyle\frac{V(T)}{\mathrm{num\_of\_points}}\right)^{\frac{1}{\mathrm{dim}}}*\varepsilon@f$
     * where @f$V(T)@f$ is the volume of the object of discretization, @f$\mathrm{dim}@f$
     * is its
     * dimension and @f$\varepsilon@f$ is standard error tolerance, eg. 1e-6.
     * For sphere the values are @f$V(T) = 4 \pi r^2@f$ and @f$\mathrm{dim} = 2@f$.
     * Above is calculated for boundary and interior of the domain and the minimum is
     * taken. <b>If domain contains any children, this guesses may be very bad!</b>
     */
    void guessSetContainsPrecision() {
        int internal_count = (types > 0).size();
        int boundary_count = (types < 0).size();
        if (internal_count == 0 && boundary_count == 0) contains_precision = EPS;
        scalar_t precision_interior = std::numeric_limits<scalar_t>::infinity();
        scalar_t precision_boundary = std::numeric_limits<scalar_t>::infinity();

        if (internal_count > 0)
            precision_interior = EPS * std::pow(volume() / internal_count,
                                                1.0 / vec_t::dimension);
        if (vec_t::dimension > 1 && boundary_count > 0)
            precision_boundary = EPS * std::pow(surface_area() / boundary_count,
                                                1.0 / (vec_t::dimension - 1));

        contains_precision = std::min(precision_boundary, precision_interior);
        if (std::isinf(contains_precision)) contains_precision = EPS;
    }
    /// Sets contains precision. @sa contains
    void setContainsPrecision(scalar_t precision) { contains_precision = precision; }
    /// Returns contains precision. @sa contains
    scalar_t getContainsPrecision() const { return contains_precision; }

    /// Returns thickness of the domain
    THICKNESS getThickness() const { return thickness; }

    /**
     * Calculates characteristic distance. This is the same order of magnitude as
     * distance between two neighbouring points if they were distributed evenly.
     * The formula is
     * @f$ \chi_r = \left(\frac{V(T)}{num\_of\_points}\right)^{\frac{1}{dim}} @f$
     */
    scalar_t characteristicDistance() const {
        return std::pow(volume() / size(), 1.0 / vec_t::dimension);
    }

    /// Returns boundary box of a domain.
    const std::pair<vec_t, vec_t>& getBBox() const { return bbox; }
    /// Calculates (and sets) bbox of a domain.
    std::pair<vec_t, vec_t> calculateBBox() {
        assert(size() > 0 && "Cannot find bbox of empty domain.");
        vec_t min = std::numeric_limits<scalar_t>::infinity();
        vec_t max = -min;
        for (int i = 0; i < size(); ++i) {
            for (int j = 0; j < vec_t::dimension; ++j) {
                min[j] = std::min(min[j], positions[i][j]);
                max[j] = std::max(max[j], positions[i][j]);
            }
        }
        return bbox = {min, max};
    }

    /// Outputs a simple report about out domain, like number of nodes, support size.
    std::ostream& output_report(std::ostream& os = std::cout) const {
        int internal_count = (types > 0).size();
        int boundary_count = (types < 0).size();
        size_t mem = sizeof(*this);
        size_t mem_pos = mem_used(positions);
        size_t mem_types = mem_used(types);
        size_t mem_supp = mem_used(support);
        size_t mem_dist = mem_used(distances);
        int max_support_size = -1;
        for (int i = 0; i < support.size(); ++i) {
            max_support_size = std::max(max_support_size, support[i].size());
            mem_supp += mem_used(support[i]);
        }
        size_t mem_total = mem + mem_pos + mem_types + mem_supp + mem_dist;
        os << "    dimension: " << vec_t::dimension << '\n'
           << "    number of points: " << size() << " of which " << internal_count
           << " internal and " << boundary_count << " boundary\n"
           << "    support size: ";
        if (max_support_size == -1) os << "support not found yet";
        else os << max_support_size;
        os << "\n    children: " << child_domains.size() << "\n"
           << "    characteristic distance: " << characteristicDistance() << '\n'
           << "    mem used total: " << mem2str(mem_total) << '\n'
           << "        pos & types:   " << mem2str(mem_pos + mem_types) << '\n'
           << "        supp & dist:   " << mem2str(mem_supp + mem_dist) << '\n';
        return os;
    }

    /// Returns surface area of base domain, paying no attention to children.
    virtual scalar_t surface_area() const = 0;
    /// Returns volume of base domain, paying no attention to children.
    virtual scalar_t volume() const = 0;

    // FUNCTIONS IMPLEMENTED IN DOMAIN EXTENSION
    void findSupport(int support_size);
    void findSupport(int support_size, const Range<int>& for_which);
    void findSupport(int support_size, const Range<int>& for_which,
                     const Range<int>& search_among, bool force_self = false);
    void findSupport(scalar_t radius_squared, int max_support_size);
    void findSupport(scalar_t radius_squared, int max_support_size,
                     const Range<int>& for_which);
    void findSupport(scalar_t radius_squared, int max_support_size,
                     const Range<int>& for_which, const Range<int>& search_among,
                     bool force_self = false);
    void findSupport(const KDTree<vec_t>& tree, scalar_t radius_squared,
                     int max_support_size, const Range<int>& for_which,
                     const Range<int>& search_among, bool force_self = false);
    template <class Func>
    void relax(int support_size, scalar_t force_factor, Func repulsion, scalar_t alpha,
               int num_of_iterations, const Range<int>& move_only, int verbose = 0);
    template <class Func>
    void relax(int support_size, scalar_t force_factor, Func repulsion,
               scalar_t alpha, int num_of_iterations, int verbose = 0);
    void relax(int support_size, scalar_t force_factor, const vec_t& repulsion,
               scalar_t alpha, int num_of_iterations, int verbose = 0);
    void relax(int support_size, scalar_t force_factor, scalar_t repulsion,
               scalar_t alpha, int num_of_iterations, int verbose = 0);
    template <class Func>
    vec_t getForce(int index, Func repulsion, scalar_t alpha) const;
    std::pair<bool, vec_t> addPointToBoundary(const vec_t& hint, int type = NODE_TYPE::BOUNDARY,
                                              double precision = EPS);
    std::pair<bool, vec_t> addPointToBoundary(
            const vec_t& hint, int type, double precision, const KDTree<vec_t>& tree);
    vec_t getUnitNormal(const Range<Vec1d>& p);
    vec_t getUnitNormal(const Range<Vec2d>& p);
    vec_t getUnitNormal(const Range<Vec3d>& p);
    Range<int> refine(const Range<int>& region, int num_of_links, scalar_t fraction,
                      bool rebuild_support = true);
    Range<int> refine(int node, int num_of_links, scalar_t fraction, bool rebuild_support = true);

  protected:  // any variable added here should also be copied in the clone function
    /**
     * Normal vector pointing towards nodes.
     * - true = nodes inside
     * - false = nodes outside
     */
    NORMAL normal_vector;
    THICKNESS thickness;  ///< Is the domain thick or thin?
    std::pair<vec_t, vec_t> bbox;  ///< Bounding box. Children should set it in their constructor.
    /**
     * Margin of error for contains tests.
     * @sa contains
     * @sa guessSetContainsPrecision
     * @sa setContainsPrecision
     * @sa getContainsPrecision
     */
    scalar_t contains_precision;

    /// Clones base class varaibles into domain d.
    void clone_base(Domain* d) const {
        d->positions = positions;
        d->types = types;
        d->support = support;
        d->distances = distances;
        d->normal_vector = normal_vector;
        d->contains_precision = contains_precision;
        d->thickness = thickness;
        d->bbox = bbox;
        d->child_domains.reserve(child_domains.size());
        for (const auto& p : child_domains) {
            d->child_domains.push_back(std::unique_ptr<Domain>(p->clone()));
        }
    }

  private:
    /// Sets normal vector to outside and flip child normals.
    void setObstacleNormal() {
        if (normal_vector == INSIDE) flipNormal();
    }
    /// Sets normal vector to inseide and preserve child normals.
    void setUnionNormal() {
        if (normal_vector == OUTSIDE) flipNormal();
    }
    /// Flip normal from inside to outside and vice versa, including children
    void flipNormal() {
        normal_vector = (normal_vector == INSIDE) ? OUTSIDE : INSIDE;
        for (const auto& d : child_domains) d->flipNormal();
    }
    /// Sets all domains thickness
    void setThick() {
        if (thickness == THIN) thickness = THICK;
        for (const auto& d : child_domains) d->setThick();
    }
    /// Sets all domains thin
    void setThin() {
        if (thickness == THICK) thickness = THIN;
        for (const auto& d : child_domains) d->setThin();
    }
    /**
     * A pure virutal function inherited classes need to implement to
     * test is a point lies in a domain, paying no respect to obstacles.
     * It is used by `contains` function to test if a point lies inside the
     * area of a given geometric shape, e.g. circle or rectangle. They should take
     * into
     * account the
     * normal vector - making domain include the boundary for EPS constant.
     */
    virtual bool inside(const vec_t& point) const = 0;

    /**
     * A clone function to be overriden by subclasses and copy their own data.
     */
    virtual Domain* clone() const = 0;
};

/// Print basic info about the domain.
template <class vec_t>
std::ostream& operator<<(std::ostream& os, const Domain<vec_t>& d) {
    os << "Domain:\n";
    d.output_report(os);
    return os;
}

/**
 * @brief Class to hold information about Rectangle domains.
 * Domain is a vec_t::dimension cuboid between beg and end points.
 */
template <class vec_t>
class RectangleDomain : public Domain<vec_t> {
  public:  // any variable added here should also be copied in the clone function
    vec_t beg;  ///< Start point of a domain.
    vec_t end;  ///< End point of a domain.
    using Domain<vec_t>::positions;
    using Domain<vec_t>::types;
    using Domain<vec_t>::clearBoundaryNodes;
    using Domain<vec_t>::clearInternalNodes;
    using Domain<vec_t>::guessSetContainsPrecision;
    using typename Domain<vec_t>::scalar_t;

    /// Allow default constructor
    RectangleDomain<vec_t>() {}
    /// Allow move constructor
    RectangleDomain<vec_t>(RectangleDomain<vec_t>&&) = default;
    /// Allow move assignment
    RectangleDomain<vec_t>& operator=(RectangleDomain<vec_t>&&) = default;
    /// Remove copy constructor
    RectangleDomain<vec_t>(const RectangleDomain<vec_t>&) = delete;
    /// Remove copy assignment
    RectangleDomain<vec_t>& operator=(const RectangleDomain<vec_t>&) = delete;

    /**
     * Creates a rectangular domain. The dimensions of both points must be the same.
     * @param beg_ A starting point for a domain.
     * @param end_ An ending point for a domain.
     */
    RectangleDomain(const vec_t& beg_, const vec_t& end_) : beg(beg_), end(end_) {
        vec_t lower = beg, upper = end;
        for (int i = 0; i < vec_t::dimension; ++i)
            if (lower[i] > upper[i]) std::swap(lower[i], upper[i]);
        bbox = {lower, upper};
    }
    /**
     * Overload for unambiguous RectangleDomain({1.2, 2.1}, {-2.3, 4.56});
     * @todo TODO(jureslak): Cehck for dimensions.
     */
    RectangleDomain(std::initializer_list<scalar_t> beg, std::initializer_list<scalar_t> end) :
        RectangleDomain<vec_t>(vec_t(beg), vec_t(end)) {}

    /**
     * @brief Creates a rectangular domain containing all points in positions_
     * @param positions_ A list of coordinates of elements
     * @param type Type of the elements -- all elements have the same type.
     */
    RectangleDomain(const Range<vec_t>& positions_, int type) :
        RectangleDomain<vec_t>(positions_, Range<int>(positions_.size(), type)) {}

    /**
     * @brief Creates a rectangular domain containing all points in positions_
     * @param positions_ A list of coordinates of elements
     * @param types_ Types of the elements. Elements can have different types.
     */
    RectangleDomain(const Range<vec_t>& positions_, const Range<int>& types_) {
        assert(positions_.size() > 0 && "Add at least some points to domain.");
        assert(positions_.size() == types_.size() &&
               "Lengths of positions and types must match in RectangleDomain constructor.");
        positions = positions_;
        types = types_;
        beg = end = positions[0];
        for (const auto& p : positions) {
            for (int i = 0; i < vec_t::dimension; ++i) {
                beg[i] = std::min(beg[i], p[i]);
                end[i] = std::max(end[i], p[i]);
            }
        }
        bbox = {beg, end};
        guessSetContainsPrecision();
    }

    /**
     * @brief Fills boundary and interior uniformly by supplying node counts.
     * @sa fillUniformInterior
     * @sa fillUniformBoundary
     **/
    void fillUniform(const Vec<int, vec_t::dimension>& internal_counts,
                     const Vec<int, vec_t::dimension>& boundary_counts) {
        fillUniformInterior(internal_counts);
        fillUniformBoundary(boundary_counts);
    }
    /**
     * @brief Fills boundary and interior uniformly by supplying spatial step.
     * @sa fillUniformInterior
     * @sa fillUniformBoundary
     **/
    void fillUniformWithStep(const vec_t& internal_steps, const vec_t& boundary_steps) {
        fillUniformInteriorWithStep(internal_steps);
        fillUniformBoundaryWithStep(boundary_steps);
    }

    /**
     * @brief Uniformly discretizes underlying domain interior with counts[i] points
     * in dimension i. It fills positions with appropriate coordinates and types with 1s.
     * See test_domain.hpp for examples. Similar to numpy's linspace and Matlab's
     * meshgrid.
     * @param counts how many discretization points to use in each dimension.
     */
    void fillUniformInterior(const Vec<int, vec_t::dimension>& counts) {
        clearInternalNodes();
        int size = positions.size();
        positions.append(linspace(beg, end, counts, false));
        int size_diff = positions.size() - size;
        for (int i = 0; i < size_diff; ++i) types.push_back(NODE_TYPE::INTERNAL);
        guessSetContainsPrecision();
    }

    /**
     * @brief Uniformly discretizes underlying domain interior by calling fillUniformInterior with
     * appropriate counts.
     * @param steps what discretization step to use in each dimension
     */
    void fillUniformInteriorWithStep(const vec_t& steps) {
        for (auto step : steps)
            assert(step > EPS &&
                   "Step in interior discretization of rectangle domain is too small!");
        Vec<int, vec_t::dimension> counts;
        for (int i = 0; i < vec_t::dimension; ++i)
            counts[i] = static_cast<int>(std::ceil((end[i] - beg[i]) / steps[i])) - 1;
        fillUniformInterior(counts);
    }
    /**
     * @brief Uniformly discretizes underlying domain boundary. It fills positions
     * with appropriate coordinates and types with -1s. Dimensions higher than 3 are
     * currently unsupported.
     * @param counts how many discretization points to use in each dimension.
     */
    void fillUniformBoundary(const Vec<int, vec_t::dimension>& counts) {
        static_assert(vec_t::dimension <= 3,
                      "Discretization of surfaces of cuboids of dimension >=4 is "
                      "currently not supported.");
        clearBoundaryNodes();
        for (auto x : counts) assert(x >= 2 && "All counts must be greater than 2.");
        if (vec_t::dimension == 1) {
            positions.push_back(beg);
            positions.push_back(end);
            types.push_back(NODE_TYPE::BOUNDARY);
            types.push_back(NODE_TYPE::BOUNDARY);
        } else if (vec_t::dimension == 2) {
            int size = positions.size();
            positions.append(linspace(beg, end, {counts[0], 2}));
            positions.append(linspace(beg, end, {2, counts[1] - 2}, {true, false}));
            int size_diff = positions.size() - size;
            for (int i = 0; i < size_diff; ++i) types.push_back(NODE_TYPE::BOUNDARY);
        } else if (vec_t::dimension == 3) {
            int size = positions.size();
            positions.append(
                linspace(beg, end, {counts[0], counts[1], 2}, {true, true, true}));
            positions.append(
                linspace(beg, end, {counts[0], 2, counts[2] - 2}, {true, true, false}));
            positions.append(linspace(beg, end, {2, counts[1] - 2, counts[2] - 2},
                                      {true, false, false}));
            int size_diff = positions.size() - size;
            for (int i = 0; i < size_diff; ++i) types.push_back(NODE_TYPE::BOUNDARY);
        }
        guessSetContainsPrecision();
    }
    /**
     * @brief Uniformly discretizes underlying domain boundary by calling
     * fillUniformBoundary with appropriate counts.
     * @param steps Which spatial step to use in each dimension.
     */
    void fillUniformBoundaryWithStep(const vec_t& steps) {
        for (auto step : steps)
            assert(step > EPS &&
                   "Step in boundary discretization of rectangle domain is too small!");
        Vec<int, vec_t::dimension> counts;
        for (int i = 0; i < vec_t::dimension; ++i)
            counts[i] = static_cast<int>(std::ceil((end[i] - beg[i]) / steps[i])) + 1;
        fillUniformBoundary(counts);
    }
    /**
     * @brief Fills boundary and interior randomly with uniform distribution.
     * @sa fillRandomInterior
     * @sa fillRandomBoundary
     **/
    void fillRandom(int internal_count, int boundary_count) {
        fillRandomInterior(internal_count);
        fillRandomBoundary(boundary_count);
    }
    /// Randomly fills ball interior with points.
    void fillRandomInterior(int num_of_points) {
        clearInternalNodes();  // http://mathworld.wolfram.com/BallPointPicking.html
        std::mt19937 generator(get_seed());
        std::uniform_real_distribution<scalar_t> uniform(0, 1);
        for (int i = 0; i < num_of_points; ++i) {
            vec_t x;
            for (int j = 0; j < vec_t::dimension; ++j)
                x[j] = beg[j] + uniform(generator) * (end[j] - beg[j]);
            positions.push_back(x);
            types.push_back(NODE_TYPE::INTERNAL);
        }
        guessSetContainsPrecision();
    }
    /// Randomly fills ball boundary with points.
    void fillRandomBoundary(int num_of_points) {
        static_assert(vec_t::dimension <= 3,
                      "Random point picking of surfaces of cuboids of dimension >=4 is "
                      "currently not supported.");
        clearBoundaryNodes();
        if (vec_t::dimension == 1) {
            for (int i = 0; i < num_of_points; ++i) {
                std::mt19937 generator(get_seed());
                    int choice = random_choice(Range<int>{0, 1}, Range<double>{0.5, 0.5},
                                               true, generator);
                if (choice) {
                    positions.push_back(beg);
                    types.push_back(NODE_TYPE::BOUNDARY);
                } else {
                    positions.push_back(end);
                    types.push_back(NODE_TYPE::BOUNDARY);
                }
            }
        } else if (vec_t::dimension == 2) {
            std::mt19937 generator(get_seed());
            std::uniform_real_distribution<scalar_t> uniform(0, 1);
            Range<bool> elem = {0, 1};
            vec_t diff = end - beg;
            Range<scalar_t> weights = {std::abs(diff[0]), std::abs(diff[1])};
            vec_t point;
            for (int i = 0; i < num_of_points; ++i) {
                int choice = random_choice(elem, weights, false, generator);
                bool first = uniform(generator) < 0.5;
                if (choice == 0) {  // x-axis random
                    if (first)  // lower side
                        point = {beg[0] + uniform(generator) * diff[0], beg[1]};
                    else
                        point = {beg[0] + uniform(generator) * diff[0], end[1]};
                } else {  // y-axis random
                    if (first)  // left side
                        point = {beg[0], beg[1] + uniform(generator) * diff[1]};
                    else
                        point = {end[0], beg[1] + uniform(generator) * diff[1]};
                }
                positions.push_back(point);
                types.push_back(NODE_TYPE::BOUNDARY);
            }
        } else if (vec_t::dimension == 3) {
            std::mt19937 generator(get_seed());
            std::uniform_real_distribution<scalar_t> uniform(0, 1);
            Range<int> elem = {0, 1, 2};
            vec_t diff = end - beg;
            Range<scalar_t> weights = {std::abs(diff[0] * diff[1]),
                                     std::abs(diff[0] * diff[2]),
                                     std::abs(diff[1] * diff[2])};
            for (int i = 0; i < num_of_points; ++i) {
                int choice = random_choice(elem, weights, false, generator);
                bool first = uniform(generator) < 0.5;
                vec_t point;
                scalar_t a = beg[0] + uniform(generator) * diff[0];  // random sides
                scalar_t b = beg[1] + uniform(generator) * diff[1];
                scalar_t c = beg[2] + uniform(generator) * diff[2];
                if (choice == 0) {  // xy
                    if (first) point = {a, b, beg[2]};
                    else point = {a, b, end[2]};
                } else if (choice == 1) {  // xz
                    if (first) point = {a, beg[1], c};
                    else point = {a, end[1], c};
                } else {  // yz
                    if (first) point = {beg[0], b, c};
                    else point = {end[0], b, c};
                }
                positions.push_back(point);
                types.push_back(NODE_TYPE::BOUNDARY);
            }
        }
        guessSetContainsPrecision();
    }

    scalar_t volume() const override {
        vec_t diff = end - beg;
        return std::abs(
            std::accumulate(std::begin(diff), std::end(diff), 1.0, std::multiplies<scalar_t>()));
    }
    scalar_t surface_area() const override {
        vec_t diff = (end - beg).cwiseAbs();
        scalar_t vol = volume();
        scalar_t pov = 0;
        for (scalar_t x : diff)
            if (x != 0) pov += vol / x;
        return 2 * pov;
    }

  protected:
    using Domain<vec_t>::bbox;
    using Domain<vec_t>::contains_precision;
    using Domain<vec_t>::normal_vector;
    using Domain<vec_t>::thickness;

  private:
    /**
     * @brief vec_test if a point is contained in a basic domain, paying
     * no notice to subdomains. A point is inside a cuboid if every of
     * its coordinates lies in the interval defined with corresponding
     * begin and end coordinates. An EPS margin pointing away from normal is used.
     * @param point The point we are interested in,
     * @return True if point is conatined in the basic domain
     * and false otherwise.
     */
    bool inside(const vec_t& point) const override {
        scalar_t margin = contains_precision * thickness * normal_vector;
        for (int i = 0; i < vec_t::dimension; ++i) {
            bool inside_case_1 = (beg[i] - margin < point[i] && point[i] < end[i] + margin);
            bool inside_case_2 = (end[i] - margin < point[i] && point[i] < beg[i] + margin);
            if (!(inside_case_1 || inside_case_2)) return normal_vector == OUTSIDE;
        }
        return normal_vector == INSIDE;
    }
    /**
     * Return a new RectangleDomain, which is a copy of this one.
     */
    RectangleDomain* clone() const override {
        auto p = new RectangleDomain(beg, end);
        this->clone_base(p);
        return p;
    }
};

/// Print basic info about the domain.
template <class vec_t>
std::ostream& operator<<(std::ostream& os, const RectangleDomain<vec_t>& d) {
    os << "RectangleDomain:\n"
       << "    beg: " << d.beg << "\n"
       << "    end: " << d.end << "\n";
    return d.output_report(os);
}

/**
 * @brief Class to hold information about circular domains of dimension
 * vec_t::dimension.
 * For dimension 1 this is effectively the same as RectangleDomain.
 */
template <class vec_t>
class CircleDomain : public Domain<vec_t> {
  public:  // any variable added here should also be copied in the clone function
    using Domain<vec_t>::positions;
    using Domain<vec_t>::types;
    using Domain<vec_t>::clearBoundaryNodes;
    using Domain<vec_t>::clearInternalNodes;
    using Domain<vec_t>::guessSetContainsPrecision;
    using typename Domain<vec_t>::scalar_t;  ///< Numeric data type.
    vec_t center;  ///< Center of a circle.
    scalar_t radius;  ///< Radius of a circle.

    /// Allow default constructor
    CircleDomain<vec_t>() {}
    /// Allow move constructor
    CircleDomain<vec_t>(CircleDomain<vec_t>&&) = default;
    /// Allow move assignment
    CircleDomain<vec_t>& operator=(CircleDomain<vec_t>&&) = default;
    /// Remove copy constructor
    CircleDomain<vec_t>(const CircleDomain<vec_t>&) = delete;
    /// Remove copy assignment
    CircleDomain<vec_t>& operator=(const CircleDomain<vec_t>&) = delete;

    /**
     * Create a circle domain around a center with given radius. It
     * automatically generates discretization points on the edge.
     * Parameter can be omitted as is suitable for one dimension.
     * @param center_ Center of a circle.
     * @param radius_ Radius of a circle.
    */
    CircleDomain(const vec_t& center_, scalar_t radius_)
            : center(center_), radius(radius_) {
        assert(radius > 0 && "Circle radius must be greater than 0.");
        bbox = {center - vec_t(radius), center + vec_t(radius)};
    }
    /**
     * @brief Fills boundary and interior approximately uniformly.
     * @sa fillUniformInterior
     * @sa fillUniformBoundary
     **/
    void fillUniform(int internal_count, int boundary_count) {
        fillUniformInterior(internal_count);
        fillUniformBoundary(boundary_count);
    }
    /**
     * @brief
     * Adds uniform discretization points to positions and type arrays. For one
     * dimension the constructor parameter is ignored, as the edge contains only
     * two points, for 2 dimansions it is generated uniformly according to the
     * angle. For 3 dimensions an approximation using fibonachi spheres is used.
     * Higher dimensions are currently unsupported.
     * @param num_of_points Number of points to fill the boundary with.
     */
    void fillUniformBoundary(int num_of_points) {
        static_assert(vec_t::dimension <= 3,
                      "Discretization of surfaces of spheres of dimension >=4 is "
                      "currently not supported.");
        clearBoundaryNodes();
        if (vec_t::dimension == 1) {  // always only two points
            positions.push_back({center[0] - radius});
            positions.push_back({center[0] + radius});
            types.push_back(NODE_TYPE::BOUNDARY);
            types.push_back(NODE_TYPE::BOUNDARY);
        } else if (vec_t::dimension == 2) {
            // normal uniform distribution by angle
            scalar_t dfi = 2 * M_PI / num_of_points;
            for (int i = 0; i < num_of_points; ++i) {
                positions.push_back(center + radius *
                                    vec_t({std::cos(i * dfi), std::sin(i * dfi)}));
                types.push_back(NODE_TYPE::BOUNDARY);
            }
        } else if (vec_t::dimension == 3) {  // Fibonacci spheres
            // code from http://stackoverflow.com/questions/9600801
            // explanation: http://blog.marmakoide.org/?p=1
            scalar_t offset = 2.0 / num_of_points;
            scalar_t increment = M_PI * (3.0 - std::sqrt(5));
            scalar_t x, y, z, r, phi;
            for (int i = 0; i < num_of_points; ++i) {
                y = ((i * offset) - 1) + (offset / 2);
                r = std::sqrt(1 - y * y);
                phi = i * increment;
                x = std::cos(phi) * r;
                z = std::sin(phi) * r;
                positions.push_back(center + radius * vec_t{x, y, z});
                types.push_back(NODE_TYPE::BOUNDARY);
            }
        }
        guessSetContainsPrecision();
    }
    /**
     * Fills boundary of a 2D circle at an arc length of every `step` units.
     * This is the same as calling fillUniformBoundary(ceil(2*pi*radius / step));
     **/
    void fillUniformBoundaryWithStep(double step) {
        static_assert(vec_t::dimension == 2,
                      "Boundary filling with step for circles is supported for 2 "
                      "dimensions only.");
        fillUniformBoundary(static_cast<int>(std::ceil(2*M_PI*radius/step)));
    }
    /**
     * @brief
     * Fills domain interior with approximately uniformly distributed nodes.
     * For one dimension this is simple linspace, for 2 dimensions we use
     * Fibonachi spiral. Default type of nodes is 1.
     * Higher dimensions are currently unsupported.
     * @param num_of_points Number of points to fill the area with.
     */
    void fillUniformInterior(int num_of_points) {
        static_assert(vec_t::dimension <= 3,
                      "Uniform discretization of volumes of spheres of dimension >=4 is "
                      "currently not supported.");
        clearInternalNodes();
        if (vec_t::dimension == 1) {
            positions.append(linspace(vec_t{center[0] - radius}, {center[0] + radius},
                                      {num_of_points}, false));
            for (int i = 0; i < num_of_points; ++i) types.push_back(NODE_TYPE::INTERNAL);
        } else if (vec_t::dimension == 2) {  // Fill circle using Fibonacci spiral.
            scalar_t golden_angle = M_PI * (3 - std::sqrt(5));
            scalar_t sqrt_num_of_points = std::sqrt(num_of_points);
            for (int i = 0; i < num_of_points; ++i) {
                scalar_t theta = i * golden_angle;
                scalar_t r = std::sqrt(i) / sqrt_num_of_points;
                positions.push_back(center + radius * r * vec_t{std::cos(theta), std::sin(theta)});
                types.push_back(NODE_TYPE::INTERNAL);
            }
        } else if (vec_t::dimension == 3) {  // Uniform cuboid, taking ones inside
            /// @todo TODO(jureslak): Making a circle {0, 0, 0}, 1 and filling it with 300 points
            /// results in 485 points. Find better fill algorithm.
            int count = std::ceil(std::cbrt(num_of_points * 6 / M_PI)) - 2;
            scalar_t E = count * count * count * M_PI / 6;
            scalar_t R = std::cbrt(E / num_of_points) * radius;
            for (const vec_t& point :
                 linspace(vec_t(center - vec_t(R)), vec_t(center + vec_t(R)),
                          {count, count, count}, false)) {
                if ((point - center).squaredNorm() < radius * radius) {
                    positions.push_back(point);
                    types.push_back(NODE_TYPE::INTERNAL);
                }
            }
        }
        guessSetContainsPrecision();
    }
    /**
     * @brief Fills boundary and interior randomly with uniform distribution.
     * @sa fillRandomInterior
     * @sa fillRandomBoundary
     **/
    void fillRandom(int internal_count, int boundary_count) {
        fillRandomInterior(internal_count);
        fillRandomBoundary(boundary_count);
    }
    /// Randomly fills ball interior with points.
    void fillRandomInterior(int num_of_points) {
        clearInternalNodes();  // http://mathworld.wolfram.com/BallPointPicking.html
        std::mt19937 generator(get_seed());
        std::uniform_real_distribution<scalar_t> uniform(0, 1);
        std::normal_distribution<scalar_t> gauss(0, 1);
        for (int i = 0; i < num_of_points; ++i) {
            vec_t x;
            for (int j = 0; j < vec_t::dimension; ++j) x[j] = gauss(generator);
            scalar_t norm = x.norm();
            if (norm < 1e-6) continue;
            scalar_t u = std::pow(uniform(generator), 1.0 / vec_t::dimension);
            positions.push_back(center + u * radius / norm * x);
            types.push_back(NODE_TYPE::INTERNAL);
        }
        guessSetContainsPrecision();
    }
    /// Randomly fills ball boundary with points.
    void fillRandomBoundary(int num_of_points) {
        clearBoundaryNodes();  // http://mathworld.wolfram.com/SpherePointPicking.html
        std::mt19937 generator(get_seed());
        std::normal_distribution<scalar_t> gauss(0, 1);
        for (int i = 0; i < num_of_points; ++i) {
            vec_t x;
            for (int j = 0; j < vec_t::dimension; ++j) x[j] = gauss(generator);
            scalar_t norm = x.norm();
            if (norm < 1e-6) continue;
            positions.push_back(center + radius / norm * x);
            types.push_back(NODE_TYPE::BOUNDARY);
        }
        guessSetContainsPrecision();
    }

    scalar_t volume() const override {
        switch (vec_t::dimension) {
            case 1: return 2.0 * radius;
            case 2: return M_PI * radius * radius;
            case 3: return 4.0 / 3.0 * M_PI * radius * radius * radius;
            default: return std::pow(std::sqrt(M_PI) * radius, vec_t::dimension) /
                                     std::tgamma(1 + vec_t::dimension / 2.0);
        }
    }
    scalar_t surface_area() const override { return vec_t::dimension * volume() / radius; }

  protected:
    using Domain<vec_t>::bbox;
    using Domain<vec_t>::contains_precision;
    using Domain<vec_t>::normal_vector;
    using Domain<vec_t>::thickness;

  private:
    /**
     * @brief vec_test if a point is contained in the domains circle.
     * A point is inside if @f$\|point - center\| \leq radius@f$
     * @param point The point we are interested in,
     * @return True if point is conatined in the domain circle and false otherwise.
     */
    bool inside(const vec_t& point) const override {
        scalar_t margin = contains_precision * thickness * normal_vector;
        bool inside = (point - center).squaredNorm() <= margin + radius * radius;
        return inside == (normal_vector == INSIDE);
    }
    /**
     * Return a new CircleDomain, which is a copy of this one.
     */
    CircleDomain* clone() const override {
        auto p = new CircleDomain(center, radius);
        this->clone_base(p);
        return p;
    }
};

/// Print basic info about the domain.
template <class vec_t>
std::ostream& operator<<(std::ostream& os, const CircleDomain<vec_t>& d) {
    os << "CircleDomain:\n"
       << "    center: " << d.center << "\n"
       << "    radius: " << d.radius << "\n";
    return d.output_report(os);
}

}  // namespace mm

#endif  // SRC_DOMAIN_HPP_
