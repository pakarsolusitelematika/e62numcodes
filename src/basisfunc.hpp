#ifndef SRC_BASISFUNC_HPP_
#define SRC_BASISFUNC_HPP_

#include "includes.hpp"
#include "common.hpp"

namespace mm {

/**
 * @file basisfunc.hpp
 * @brief A library with basis functions: Monomials, Multiquadric, Gaussians.
 * @example basisfunc_test.cpp
 */

/**
 * @brief A virtual class for Basis function.
 * @details A class to use as a template when creating a new basis function
 * class. Virtual functions tell you what you need.
 *
 * @tparam vec_t A n-dimensional vector type to be used.
 */
template <class vec_t>
class BasisFunctions {
  public:
    typedef typename vec_t::scalar_t scalar_t;  ///< numeric data type
    typedef std::array<int, vec_t::dimension> derivative_t;  ///< type for derivatives
    /**
     * @brief Describes the type of basis functions.
     * @details Type 0: All functions are centered on the center point.
     * Type 1: Each function is centered on a different point.
     */
    static const int type = 0;
    /// How many functions does it have
    virtual int size() const = 0;
    /// Virtual destructor so that the correct destuctors get called when deleting
    /// via base pointer class
    virtual ~BasisFunctions() {}

    /**
     * @brief Evaluate the function or its derivative at `point`.
     *
     * @param index Which basis function to evaluate
     * @param point The point in which to evaluate the function.
     * @param derivative The description of which derivative you want. E.g. {1, 1, 3}
     * means @f$\frac{d^5}{dx dy dz^3}@f$
     *
     * @return The value of the derivative at `point`.
     */
    virtual scalar_t operator()(int index, const vec_t& point,
                                const derivative_t& derivative) const = 0;

    // THESE FUNCTIONS ARE VIRTUAL FOR OVERRIDING POSSIBILITIES
    // FOR POTENTIAL PERFORMANCE GAIN THEY CAN BE MADE NON VIRTUAL
    /// Evaluate `index`-th function at `point`
    virtual scalar_t operator()(int index, const vec_t& point) const {
        return operator()(index, point, derivative_t{{0}});
    }
    /// Overload with default parameter index = 0
    virtual scalar_t operator()(const vec_t& point) const { return operator()(0, point); }
    /// Overload with default parameter index = 0
    virtual scalar_t operator()(const vec_t& point, const derivative_t& derivative) const {
        return operator()(0, point, derivative);
    }
};

/// Print basic info about the functions.
template <class vec_t>
std::ostream& operator<<(std::ostream& os, const BasisFunctions<vec_t>& m) {
    return os << "Unknown BasisFunction, number of functions = " << m.size() << ".\n";
}

/**
 * @brief Gaussian basis functions
 * @details A function class generating a function
 * @f$\frac{1}{\sqrt{2 \pi s}} \exp\left(-\frac{point^2}{2s}\right)@f$, where s
 * = shape. It can be used as a MLS weight function or `type = 1` basis function
 *
 * @tparam vec_t A n-dimensional vector type to be used.
 */
template <class vec_t>
class Gaussians : public BasisFunctions<vec_t> {
  public:
    int size_;  ///< Number of functions saved
    vec_t shape;  ///< Actually it's diagonal elements of the covariance matrix

    using typename BasisFunctions<vec_t>::scalar_t;
    using typename BasisFunctions<vec_t>::derivative_t;
    using BasisFunctions<vec_t>::operator();
    /// Every function should have a different center
    static const int type = 1;
    /**
     * @brief A constructor sets the values internally.
     *
     * @param _shape This is a vector representing diagonal elements of the
     * covariance matrix: @f$\Sigma_{i, j} = \delta_{i, j} shape[i]@f$
     * @param n_functions The number of functions to allow calling.
     */
    Gaussians(vec_t _shape = 1, int n_functions = 1) : size_(n_functions), shape(_shape) {
        for (auto x : shape) assert(x > 0 && "All std's in Gaussian distribution should be > 0.");
    }

    /**
     * @brief Simple constructor making a single function with very simple shape
     *
     * @param _shape This is squared standard deviation @f$\sigma^2 = shape@f$
     * and @f$\Sigma = shape * I@f$
     */
    Gaussians(scalar_t _shape = 1) : size_(1), shape(_shape) {
        assert(_shape > 0 && "Std in Gaussian distribution should be > 0.");
    }

    /// Return The number of functions that was set in the constructor.
    int size() const override { return size_; }
    /**
     * @brief Evaluate the gaussian derivatives at `point`.
     *
     * @param index Only to check if we are over the boundary set by size().
     * @param point The point in which to evaluate the function.
     * @param derivative The description of which derivative you want. E.g. {1, 1, 3}
     * means @f$\frac{d^5}{dx dy dz^3}@f$
     *
     * @return The value of the gaussian derivative at `point`.
     */
    scalar_t operator()(int index, const vec_t& point,
                        const derivative_t& derivative) const override {
        assert(0 <= index && index < size_);
        for (int x : derivative) {
            assert(x >= 0 && "What do you mean derivative of order < 0?");
        }
        scalar_t result = 1;
        for (int i = 0; i < vec_t::dimension; i++) {
            int der = derivative[i];
            assert(der <= 3 &&
                   "Not implemented error: Only derivatives up to 3 are available");
            scalar_t exp_fac = -1.0 / (2 * shape[i]);
            scalar_t val = point[i];
            if (der == 1) {
                result *= 2 * exp_fac * val;
            } else if (der == 2) {
                result *= 2 * exp_fac * (2 * exp_fac * val * val + 1);
            } else if (der == 3) {
                result *= 4 * exp_fac * exp_fac * val * (2 * exp_fac * val * val + 3);
            }
            result *= std::exp(exp_fac * (val * val)) / std::sqrt(2 * M_PI * shape[i]);
        }
        return result;
    }

    template <typename V>
    friend std::ostream& operator<<(std::ostream& os, const Gaussians<V>& m);
};

/// Print basic info about the functions.
template <class V>
std::ostream& operator<<(std::ostream& os, const Gaussians<V>& m) {
    return os << "Gaussians " << V::dimension << "D with shape " << m.shape
              << ", number of functions = " << m.size() << ".\n";
}

/**
 * @brief Non-normalized Gaussian basis function.
 * @details A function class generating a function
 * @f$\exp\left(-\frac{point^2}{s^2}\right)@f$, where s
 * = shape. Note the difference in s^2 between Gaussians and NNGaussians
 * Suitable for weight function or basis 1 type
 * @tparam vec_t A n-dimensional vector type to be used.
 */
template <class vec_t>
class NNGaussians : public BasisFunctions<vec_t> {
  public:
    int size_;  ///< Number of functions saved
    vec_t shape;  ///< Actually it's diagonal elements of the covariance matrix

    using typename BasisFunctions<vec_t>::scalar_t;
    using typename BasisFunctions<vec_t>::derivative_t;
    using BasisFunctions<vec_t>::operator();
    /// Every function should have a different center
    static const int type = 1;
    /**
     * @brief A constructor sets the values internally.
     *
     * @param _shape This is a vector representing diagonal elements of the
     * covariance matrix: @f$\Sigma_{i, j} = \delta_{i, j} shape[i]@f$
     * @param n_functions The number of functions to allow calling.
     */
    NNGaussians(vec_t _shape = 1, int n_functions = 1) : size_(n_functions), shape(_shape) {
        for (auto x : shape) assert(x > 0 && "All std's in Gaussian distribution should be > 0.");
    }

    /**
     * @brief Simple constructor making a single function with very simple shape
     *
     * @param _shape This is squared standard deviation @f$\sigma^2 = shape@f$
     * and @f$\Sigma = shape * I@f$
     */
    NNGaussians(scalar_t _shape = 1) : size_(1), shape(_shape) {
        assert(_shape > 0 && "Std in Gaussian distribution should be > 0.");
    }

    /// Return The number of functions that was set in the constructor.
    int size() const override { return size_; }
    /**
     * @brief Evaluate the gaussian derivatives at `point`.
     *
     * @param index Only to check if we are over the boundary set by size().
     * @param point The point in which to evaluate the function.
     * @param derivative The description of which derivative you want. E.g. {1, 1, 3}
     * means @f$\frac{d^5}{dx dy dz^3}@f$
     *
     * @return The value of the gaussian derivative at `point`.
     */
    scalar_t operator()(int index, const vec_t& point,
                        const derivative_t& derivative) const override {
        assert(0 <= index && index < size_);
        for (int x : derivative) {
            assert(x >= 0 && "What do you mean derivative of order < 0?");
        }
        scalar_t result = 1;
        for (int i = 0; i < vec_t::dimension; i++) {
            int der = derivative[i];
            assert(der <= 3 &&
                   "Not implemented error: Only derivatives up to 3 are available");
            scalar_t exp_fac = -1.0 / ((shape[i])*(shape[i]));
            scalar_t val = point[i];
            if (der == 1) {
                result *= 2 * exp_fac * val;
            } else if (der == 2) {
                result *= 2 * exp_fac * (2 * exp_fac * val * val + 1);
            } else if (der == 3) {
                result *= 4 * exp_fac * exp_fac * val * (2 * exp_fac * val * val + 3);
            }
            result *= std::exp(exp_fac * (val * val));
        }
        return result;
    }
    template <typename V>
    friend std::ostream& operator<<(std::ostream& os, const NNGaussians<V>& m);
};

/// Print basic info about the functions.
template <class V>
std::ostream& operator<<(std::ostream& os, const NNGaussians<V>& m) {
    return os << "NNGaussians " << V::dimension << "D with shape " << m.shape
              << ", number of functions = " << m.size() << ".\n";
}

/**
 * @brief Multiquadric basis functions
 * @details A function class generating a function @f$\sqrt{1 + s^2 p^Tp}@f$.
 * It can be used as a `type = 1` basis function.
 *
 * @tparam vec_t A n-dimensional vector type to be used.
 */
template <class vec_t>
class MultiQuadric : public BasisFunctions<vec_t> {
  public:
    using typename BasisFunctions<vec_t>::scalar_t;
    using typename BasisFunctions<vec_t>::derivative_t;
    using BasisFunctions<vec_t>::operator();

    int size_;  ///< number of functions
    vec_t shape;  ///< 's': a nonlinear parameter in Multiquadric function
    /// Every function should have a different center.
    static const int type = 1;

    /**
     * @brief A constructor that sets the values internally.
     *
     * @param _shape The non-linear parameter `s` in the function definition.
     * @param n_functions The number of functions to allow calling.
     */
    MultiQuadric(vec_t _shape = 1, int n_functions = 1)
        : size_(n_functions), shape(_shape) {
            for (auto x : shape) assert(x > 0 &&
                "All shape parameters in MultiQuadric must be > 0.");
        }

    /**
     * @brief A simple constructor that sets the values internally.
     *
     * @param _shape The non-linear parameter `s` in the function definition.
     * @param n_functions The number of functions to allow calling.
     */
    MultiQuadric(scalar_t _shape = 1, int n_functions = 1)
        : size_(n_functions), shape(_shape) {
            assert(_shape > 0 && "Shape parameter in MultiQuadric must be > 0.");
        }

    /// Return The number of functions that was set in the constructor.
    int size() const override { return size_; }
    /**
     * @brief Evaluate the MQ derivative at `point`.
     *
     * @param index Only to check if we are over the boundary set by size().
     * @param point The point in which to evaluate the derivative.
     * @param derivative The description of which derivative you want. E.g. {1, 1, 3}
     * means @f$\frac{d^5}{dx dy dz^3}@f$
     *
     * @return The value of the MQ or MQ derivative at `point`.
     *
     * @todo TODO(Ivan) Write unit tests for MQ
     */
    scalar_t operator()(int index, const vec_t& point,
                        const derivative_t& derivative) const override {
        for (int x : derivative) {
            assert(x >= 0 && "What do you mean derivative of order < 0?");
        }
        assert(vec_t::dimension <= 3 &&
            "Multiquadrics in dimensions higher than 3 are not available.");

        assert(0 <= index && index < size_);

        // element wise normalize and squared norm
        scalar_t rsqr = (point.cwiseQuotient(shape)).squaredNorm();

        // basic multiquadric
        scalar_t mq = std::sqrt(1 + rsqr);
        scalar_t result = mq;

        int der_order = 0;
        for (int i : derivative) {
            der_order += i;
        }
        assert(der_order <= 2 &&
            "Not implemented error: Only derivatives up to order 2 are available");

        if (der_order == 1) {
            for (int i = 0; i < vec_t::dimension; i++) {
                if (derivative[i] == 1) {
                    result = point[i]/(shape[i]*shape[i]*mq);
                    break;
                }
            }
        } else if (der_order == 2) {
            for (int i = 0; i < vec_t::dimension; i++) {
                if (derivative[i] == 2) {
                    result = (1 - point[i]*point[i]/(shape[i]*shape[i]*mq*mq)) /
                             (shape[i]*shape[i]*mq);
                    break;
                } else if (derivative[i] == 1) {
                    result = 1;
                    for (int j = 0; j < vec_t::dimension; j++) {
                        if (derivative[j] == 1) result *= point[j]/(shape[j]*shape[j]);
                    }
                    result /= -(mq*mq*mq);
                    break;
                }
            }
        }
        return result;
    }

    template <class V>
    friend std::ostream& operator<<(std::ostream& os, const MultiQuadric<V>& m);
};

/// Print basic info about the functions.
template <class V>
std::ostream& operator<<(std::ostream& os, const MultiQuadric<V>& m) {
    return os << "MultiQuadric " << V::dimension << "D with shape " << m.shape
              << ", number of functions = " << m.size() << ".\n";
}

/**
 * @brief Inverse multiquadric basis functions
 * @details A function class generating a function @f$\frac{1}{\sqrt{1 + p^Tp/s^2}}@f$.
 * It can be used as a `type = 1` basis function.
 *
 * @tparam vec_t A n-dimensional vector type to be used.
 */
template <class vec_t>
class InverseMultiQuadric : public BasisFunctions<vec_t> {
  public:
    using typename BasisFunctions<vec_t>::scalar_t;
    using typename BasisFunctions<vec_t>::derivative_t;
    using BasisFunctions<vec_t>::operator();

    int size_;  ///< number of functions
    vec_t shape;  ///< 's': a nonlinear parameter in Inverse multiquadric function

    /// Every function should have a different center.
    static const int type = 1;
    /**
     * @brief A constructor that sets the values internally.
     *
     * @param _shape The non-linear parameter `s` in the function definition.
     * @param n_functions The number of functions to allow calling.
     */
    InverseMultiQuadric(vec_t _shape = 1, int n_functions = 1)
        : size_(n_functions), shape(_shape) {
            for (auto x : shape) assert(x > 0 &&
                 "All shape parameters in InverseMultiQuadric must be > 0.");
        }

    /**
     * @brief A constructor that sets the values internally.
     *
     * @param _shape The non-linear parameter `s` in the function definition.
     * @param n_functions The number of functions to allow calling.
     */
    InverseMultiQuadric(scalar_t _shape = 1, int n_functions = 1)
        : size_(n_functions), shape(_shape) {
            assert(_shape > 0 && "Shape parameter in InverseMultiQuadric must be > 0.");
        }

    /// Return The number of functions that was set in the constructor.
    int size() const override { return size_; }
    /**
     * @brief Evaluate the MQ derivative at `point`.
     *
     * @param index Only to check if we are over the boundary set by size().
     * @param point The point in which to evaluate the derivative.
     * @param derivative The description of which derivative you want. E.g. {1, 1, 3}
     * means @f$\frac{d^5}{dx dy dz^3}@f$
     *
     * @return The value of the MQ or MQ derivative at `point`.
     *
     * @todo TODO(Ivan) Write unit tests for MQ
     */
    scalar_t operator()(int index, const vec_t& point,
                        const derivative_t& derivative) const override {
        for (int x : derivative) {
            assert(x >= 0 && "What do you mean derivative of order < 0?");
        }
        assert(vec_t::dimension <= 3 &&
            "Multiquadrics in dimensions higher than 3 are not available.");

        assert(0 <= index && index < size_);

        // element wise divide and  squared norm
        scalar_t rsqr = (point.cwiseQuotient(shape)).squaredNorm();

        // basic multiquadric
        scalar_t mq = std::sqrt(1 + rsqr);
        scalar_t result = 1/mq;

        int der_order = 0;
        for (int i : derivative) {
            der_order += i;
        }
        assert(der_order <= 2 &&
            "Not implemented error: Only derivatives up to order 2 are available");

        if (der_order == 1) {
            for (int i = 0; i < vec_t::dimension; i++) {
                if (derivative[i] == 1) {
                    result = -point[i]/(shape[i]*shape[i]*mq*mq*mq);
                    break;
                }
            }
        } else if (der_order == 2) {
            for (int i = 0; i < vec_t::dimension; i++) {
                if (derivative[i] == 2) {
                    result = (-1 + 3*point[i]*point[i]/(shape[i]*shape[i]*mq*mq)) /
                             (shape[i]*shape[i]*mq*mq*mq);
                    break;
                } else if (derivative[i] == 1) {
                    result = 1;
                    for (int j = 0; j < vec_t::dimension; j++) {
                        if (derivative[j] == 1) result *= point[j]/(shape[j]*shape[j]);
                    }
                    result *= 3/(mq*mq*mq*mq*mq);
                    break;
                }
            }
        }
        return result;
    }
    template <class V>
    friend std::ostream& operator<<(std::ostream& os, const InverseMultiQuadric<V>& m);
};

/// Print basic info about the functions.
template <class V>
std::ostream& operator<<(std::ostream& os, const InverseMultiQuadric<V>& m) {
    return os << "InverseMultiQuadric " << V::dimension << "D with shape " << m.shape
              << ", number of functions= " << m.size() << ".\n";
}

/**
 * @brief A basis function class creating monomial functions
 * @details It generates functions
 * @f$\{1, x, y, z, x^2, y^2, z^2, xy, xy, yz, \ldots\}@f$
 * depending on the dimensions of vec_t and desired order
 * @param order Defines order of monomials, e.g. order 3 in 2D results in
 * @f$\{1, x, y, x^2, y^2, xy \}@f$
 * @param shape Shapes monomials, default value is 1, otherwise the monomials are scaled as
 * @f$\{1, x/s_x, y/s_y, (x/s_x)^2, (y/s_y)^2, x/(s_x)y/(s_y) \}@f$
 * @tparam vec_t A (1..3)-dimensional vector type to be used
 */
template <class vec_t>
class Monomials : public BasisFunctions<vec_t> {
    int size_;  ///< the number of functions
    /// A vector describing monomials with the powers of every coordinate
    std::vector<std::vector<int>> powers;
  public:
    vec_t shape = vec_t(scalar_t{1.});  ///< shape function
    using typename BasisFunctions<vec_t>::scalar_t;
    using typename BasisFunctions<vec_t>::derivative_t;
    using BasisFunctions<vec_t>::operator();
    /**
     * @brief Generate all monomials and save them for later use
     * @param order A number showing the maximum order of monomials to use
     *
     * Example: If you call this with an order = 3 parameter and Vec2d template parameter,
     * your basis will consist of @f$ \{1, x, y, x^2, xy, y^2\} @f$.
     * @todo TODO: write death tests for assertions.
     */
    Monomials(int order) {
        assert(0 <= order);
        // Generate functions and save them
        int dim = vec_t::dimension;
        for (int ord = 0; ord < order; ord++) {
            if (dim == 1) {
                powers.push_back({ord});
            } else if (dim == 2) {
                for (int i = 0; i < ord + 1; i++) {
                    powers.push_back({i, ord - i});
                }
            } else if (dim == 3) {
                for (int i = 0; i < ord + 1; i++) {
                    for (int j = 0; j < ord + 1 - i; j++) {
                        powers.push_back({i, j, ord - i - j});
                    }
                }
            }
        }
        size_ = powers.size();
    }
    /**
     * @brief Generate all monomials and save them for later use
     * @details Generate all Monomials of orders up to `order`. Note that _shape
     * parameter is here only to maintain the same signature as Gaussians.
     *
     * @param _shape A dummy shape parameter to maintain signature.
     * @param order A number showing the maximum order of monomials to use
     */
    Monomials(vec_t _shape, int order = 1) : Monomials(order) { shape = _shape; }
    /**
     * @brief Construct monomials with specific powers.
     *
     * @param _powers List of list of powers e.g.
     * {{1, 2}, {0, 3}, {2, 0}} means {x y^2, y^3, x^2}
     */
    Monomials(const std::vector<std::vector<int>>& _powers) {
        for (const auto& p : _powers) {
            assert(p.size() == vec_t::dimension &&
                "Each list of powers in monomials must be size vec_t::dimension");
        }
        powers = _powers;
        size_ = powers.size();
    }

    /// Get the number of monomials generated and saved
    int size() const override { return size_; }
    /**
     * @brief Evaluates `index`-th monimial's derivative in `point`
     * @details It loads the monomial number `index` and calls it with the
     * parameter `point` and derived like `derivative` suggests
     *
     * @param index A number in [0, size()) specifying the index of a function
     * to call
     * @param point A `vec_t` point
     * @param derivative The description of which derivative you want. E.g. {1, 1, 3}
     * means @f$\frac{d^5}{dx dy dz^3}@f$
     *
     * @return Value of the derivative
     */
    scalar_t operator()(int index, const vec_t& point,
                        const derivative_t& derivative) const override {
        assert(size_ > index && "The desired monomial is not created, increase order");
        for (int x : derivative) assert(x >= 0);
        scalar_t result = 1;
        for (int i = 0; i < vec_t::dimension; i++) {
            if (derivative[i] > powers[index][i]) {
                result = 0;
                break;
            }
            result *= std::pow(point[i], powers[index][i] -
                    derivative[i])/std::pow(shape[i], powers[index][i]);
            for (int j = powers[index][i]; j > powers[index][i] - derivative[i]; j--) {
                result *= j;
            }
        }
        return result;
    }

    template <class V>
    friend std::ostream& operator<<(std::ostream& os, const Monomials<V>& m);
};

/// Print basic info about the functions.
template <class V>
std::ostream& operator<<(std::ostream& os, const Monomials<V>& m) {
    return os << "Monomials " << V::dimension << "D: "
              << m.powers << ", number of functions = " << m.size() << ".\n";
}

/**
 * @brief Quartic spline used for weights
 * @details A function class generating a function
 * @f$ w\left(\frac{\|x\|}{r}\right)@f$, where
 * @f[ w(d) = \begin{cases} 1 - 6d^2 + 8d^3 - 3d^4; & d \le 1 \\ 0; & d \ge 1 \end{cases}, @f]
 * where r = shape = support squared domain radius. It is intended to be used as a MLS weight function.
 * @tparam vec_t A n-dimensional vector type to be used.
 */
template <class vec_t>
class QuarticSpline : public BasisFunctions<vec_t> {
  public:
    vec_t shape;  ///< Should represent the radius of the support domain
    using typename BasisFunctions<vec_t>::scalar_t;
    using typename BasisFunctions<vec_t>::derivative_t;
    using BasisFunctions<vec_t>::operator();
    /// Every function should have a different center
    static const int type = 1;
    /**
     * @brief A constructor sets the values internally.
     * @param _shape Squared radius of support domain.
     */
    QuarticSpline(scalar_t _shape) : shape(_shape) {
        assert(_shape > 0 && "Domain radius in QuarticSpline must be > 0.");
    }

    /**
     * @brief A constructor sets the values internally.
    * @param _shape Squared radius of support domain.
     */
    QuarticSpline(vec_t _shape) : shape(_shape[0]) {
        assert(_shape > 0 && "Domain radius in QuarticSpline must be > 0.");
    }
    /// Return The number of functions that was set in the constructor.
    int size() const override { return 1; }

    /**
     * @brief Evaluate the spline or its derivatises at `point`.
     *
     * @param point The point in which to evaluate the function.
     * @param derivative The description of which derivative you want. E.g. {1, 1, 3}
     * means @f$\frac{d^5}{dx dy dz^3}@f$
     *
     * @return The value of the quartic spline derivative at `point`.
     */
    scalar_t operator()(int /* index */, const vec_t& point,
                      const derivative_t& derivative) const override {
        for (int x : derivative) {
            assert(x >= 0 && "What do you mean derivative of order < 0?");
            assert(x <= 1 && "Not implemented error: derivatives not implemented.");
        }
        scalar_t d = point.squaredNorm() / shape[0];
        if (d > 1) return 0;
        return 1 - 6*d*d + 8*d*d*d - 3*d*d*d*d;
    }
    template <class V>
    friend std::ostream& operator<<(std::ostream& os, const QuarticSpline<V>& m);
};

/// Print basic info about the functions.
template <class V>
std::ostream& operator<<(std::ostream& os, const QuarticSpline<V>& m) {
    return os << "QuarticSpline " << V::dimension << "D with shape " << m.shape << ".\n";
}
}  // namespace mm

#endif  // SRC_BASISFUNC_HPP_
