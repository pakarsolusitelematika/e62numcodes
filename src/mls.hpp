#ifndef SRC_MLS_HPP_
#define SRC_MLS_HPP_

#include "basisfunc.hpp"
#include "includes.hpp"
#include "types.hpp"

/// @cond
#include "Eigen/Dense"
#include "Eigen/LU"
#include "Eigen/SVD"

namespace mm {

/// @endcond

/**
 * @file mls.hpp
 * @brief A library with functions and algorithms to initilize, run and harvest MLS.
 * @example mls_test.cpp
 */

/**
 * @brief A class for generating approximations using Moving Least Square algorithm
 *
 * For the details on the method, see
 * http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Moving_Least_Squares_(MLS).
 *
 * For additional examples and performance analysis see
 * http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/wiki/index.php/Analysis_of_MLSM_performance.
 *
 * @tparam vec_t A @f$k@f$-dimensional vector type with static type member
 * 'scalar_t' (usually double, but can be float or complex) and static int member
 * 'dimensions' (=@f$k@f$ in the wiki).
 * Internally the class uses scalar_t to reference vec_t::scalar_t.
 * @tparam BasisFunctionsTemplate A functions class with a vec_t
 * template parameter, 'int size()'(=@f$m@f$) member function and
 * operator()(int, vec_t) overload representing a set of @f$m@f$ basis functions.
 * Internally the class uses BasisFunctionClass to reference BasisFunctionTemplate<vec_t>.
 * @tparam WeightFunctionTemplate A functions class with a vec_t
 * template parameter, 'int size()' member function and
 * operator()(int, vec_t) overload. Internally the class uses
 * WeightFunctionClass to reference WeightFunctionTemplate<vec_t>.
 */
template <class vec_t, template <class> class BasisFunctionsTemplate,
          template <class> class WeightFunctionTemplate>
class EngineMLS {
  public:
    /// numeric scalar data type, eg. double
    typedef typename vec_t::scalar_t scalar_t;
    /// Eigen matrix data type
    typedef typename Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic> ei_matrix_t;
    /// typedef for WeightFunction
    typedef WeightFunctionTemplate<vec_t> WeightFunctionClass;
    /// typedef for BasisFunction
    typedef BasisFunctionsTemplate<vec_t> BasisFunctionsClass;
    /// typedef for derivatives
    typedef std::array<int, vec_t::dimension> derivative_t;

  private:
    /// A list of vec_t points that show the position of support domain
    Range<vec_t> support_domain;
    /// A Function class of which the first function will be used as a weight
    WeightFunctionClass weight;
    /// A point in which we center type=0 basis functions and where we center the weight
    vec_t center_point;
    /// Indices of support domain points in which we center type=1 basis functions
    Range<int> basis_centers;

    /// Basis functions to combine into the best model
    BasisFunctionsClass basis_functions;
    /// Coefficients in the best linear combination of basis functions
    VecX<scalar_t> coefficients;

    /// Saved matrix inverse @f$M^{-1}@f$. If size is 0x0, then no point is saved.
    ei_matrix_t saved_inverse;
    /// Condition number of saved matrix. Value of -1 means it has not been calculated yet.
    scalar_t cond_number;
    /// Point where `saved_inverse` was calculated, NaN if no calulations were performed yet.
    vec_t saved_point;

  public:
    /**
     * @brief Just sets the parameters to internal members
     * @param _basis_functions Basis functions @f$\vec b@f$ to use in the algorithm.
     * The size determines the number of coefficients @f$\vec \alpha@f$
     * @param _support_domain A vector of vec_t values representing the
     * points in which we have the original function values @f$\vec s@f$
     * @param _weight A function class of size at least 1 of which only the
     * first function will be used (@f$w@f$)
     */
    EngineMLS(const BasisFunctionsClass& _basis_functions,
              const Range<vec_t>& _support_domain, const WeightFunctionClass& _weight)
        : support_domain(_support_domain), weight(_weight), basis_functions(_basis_functions),
          saved_inverse(), cond_number(-1.), saved_point(std::numeric_limits<double>::quiet_NaN()) {
        int n_functions = basis_functions.size();
        assert(n_functions <= support_domain.size() &&
               "MLS cannot have more basis functions than points in support domain");
        coefficients.resize(n_functions);
        if (basis_functions.type == 1) {
            basis_centers.resize(n_functions);
            for (int i = 0; i < n_functions; i++) {
                basis_centers[i] = i;
            }
        }
    }
    /// Allow default constructor
    EngineMLS() : weight(0), basis_functions(0) {}
    /// Allow move constructor
    EngineMLS(EngineMLS&&) = default;
    /// Allow move assignment
    EngineMLS& operator=(EngineMLS&&) = default;
    /// Allow copy constructor
    EngineMLS(const EngineMLS&) = default;
    /// Allow copy assignment
    EngineMLS& operator=(const EngineMLS&) = default;

    /**
     * @brief Override internal support domain @f$\vec s@f$ with new values.
     *  Note, that using setSupport does not change the centre of the support,
     *  effectively that means that the weight function remains centred
     *  regarding the old support. Use setBasisCentre to centre also the weight.
     * @param _support_domain New values for support domain @f$\vec s@f$.
     */
    void setSupport(const Range<vec_t>& _support_domain) {
        support_domain = _support_domain;
        assert(basis_functions.size() <= support_domain.size() &&
               "MLS cannot have more basis functions than points in support domain");
        saved_inverse.resize(0, 0);
    }

    /**
     * @brief Sets the centers of type=1 basis functions
     * @param _basis_centers Indexes of the points on which you wish to center
     * the basis functions
     */
    void setBasisCenters(const Range<int>& _basis_centers) {
        assert(basis_functions.type == 1 &&
               "Setting basis centers only makes sense for type=1 basis functions.");
        assert(_basis_centers.size() == basis_functions.size() &&
               "Incorrect number of centers for basis functions.");
        basis_centers = _basis_centers;
    }

    /**
     * @brief Same as setupFunctionAt but uses the first point in support domain
     * as the center point
     * @param values Function values in support domain (@f$\vec u@f$).
     */
    template<class container_t>
    void setupFunction(const container_t& values) {
        setupFunctionAt(support_domain[0], values);
    }

    /**
     * @brief Sets up the function to use around point @f$p@f$
     * @details Sets the center point and calls the almighty generator of the
     * coefficient for the best local approximation using the given basis
     * functions
     *
     * @tparam container_t A container with operator `scalar_t [](int) const`
     * and method `.size()` and
     *
     * @param p Center point of approximation @f$\vec p@f$
     * @param _basis_centers Indexes of the points on which you wish to center
     * the basis functions
     * @param values Function values in support domain (@f$\vec u@f$).
     */
    template<class container_t>
    void setupFunctionAt(const vec_t& p, const container_t& values,
                         const Range<int>& _basis_centers) {
        setBasisCenters(_basis_centers);
        setupFunctionAt(p, values);
    }

    /**
     * @brief Sets up the function to use around point @f$p@f$
     * @details Sets the center point and calls the almighty genereation of the
     * coefficient for the best local approximation using the given basis
     * functions
     *
     * @param p Center point of approximation @f$p@f$.
     * @param values Function values in support domain (@f$\vec u@f$).
     */
    template<class container_t>
    void setupFunctionAt(const vec_t& p, const container_t& values) {
        assert(values.size() == support_domain.size() &&
               "Values size should match the size of support domain.");
        center_point = p;
        int n_points = support_domain.size();
        // Get the matrix we need
        ei_matrix_t base_values_inv = getMatrixSolution(p);
        // Get weighted values
        VecX<scalar_t> weighted_values(n_points);
        for (int i = 0; i < n_points; i++) {
            weighted_values[i] = values[i] * weight(0, support_domain[i] - p);
        }
        coefficients = base_values_inv * weighted_values;
    }

    /**
     * @brief Get the approximation @f$\hat u @f$ value at point @f$\vec x@f$
     * @details Evaluates the basis functions at point @f$\vec x @f$ and  multiplies the
     * values with corresponding coefficients to get the value of @f$\hat u @f$ at point
     * @f$\vec x@f$. You have to call setupFunction before calling this function.
     *
     * @param x A vec_t point in which to evaluate the generated
     * approximation
     * @return A vec_t::scalar_t value of the approximation
     */
    scalar_t evaluateAt(const vec_t& x) const { return evaluateAt(x, {{0}}); }

    /**
     * A vectorized version of evaluateAt.
     */
    Range<scalar_t> evaluateAt(const Range<vec_t>& xs) const {
        return evaluateAt(xs, {{0}});
    }

    /**
     * A vectorized version of evaluateAt with option to request derivatives
     * instead of values.
     */
    Range<scalar_t> evaluateAt(const Range<vec_t>& xs,
                               const derivative_t& mode) const {
        Range<scalar_t> result(xs.size());
        int i = 0;
        for (const auto& x : xs) {
            result[i++] = evaluateAt(x, mode);
        }
        return result;
    }

    /**
     * @brief Get the approximation @f$\hat u @f$ derivative at point @f$\vec x @f$
     * @details Evaluates the basis functions at point @f$\vec x @f$ and  multiplies the
     * values with corresponding coefficients to get the value of @f$\hat u @f$ at point
     * @f$\vec x @f$. You have to call setupFunction before calling this function.
     *
     * @param x A vec_t point in which to evaluate the generated
     * approximation
     * @param mode The description of which derivative you want. E.g. {1, 1, 3}
     * means @f$\frac{d^5}{dx dy dz^3}@f$
     *
     * @return A vec_t::scalar_t value of the approximation
     */
    scalar_t evaluateAt(const vec_t& x, const derivative_t& mode) const {
        scalar_t result = 0;
        for (int i = 0; i < basis_functions.size(); i++) {
            if (basis_functions.type == 0) {
                result += basis_functions(i, x - center_point, mode) * coefficients[i];
            } else {
                result += basis_functions(i, x - support_domain[basis_centers[i]], mode) *
                          coefficients[i];
            }
        }
        return result;
    }

    /// Overload of getShapeAt for mode = {0,...,0}
    VecX<scalar_t> getShapeAt(const vec_t& cpoint) {
        return getShapeAt(cpoint, {{0}});
    }

    /**
     * @brief Creates shape vector @f$\vec \varphi@f$ around current center point @f$\vec p@f$.
     * @details Calculates the shape vector so that we can quickly approximate
     * when we get (new) values @f$\vec u@f$. We do this by dot multiplying the shape vector @f$\vec \varphi@f$
     * and the vector of function values @f$\vec u@f$.
     *
     * @param cpoint The point @f$\vec p@f$ for which to make the shape vector
     * @param mode The description of which derivative you want. E.g. {1, 1, 3}
     * means @f$\frac{d^5}{dx dy dz^3}@f$
     *
     * @return scalar_t vector containing the shape vector @f$\vec \varphi@f$
     * @todo TODO: write shape tests for derivatives
     */
    VecX<scalar_t> getShapeAt(const vec_t& cpoint, const derivative_t& mode) {
        int n_functions = basis_functions.size();
        int n_points = support_domain.size();

        // Get the matrix we need
        ei_matrix_t base_values_inv = getMatrixSolution(cpoint);
        VecX<scalar_t> shape_vector(n_points);

        // Get basis values
        Eigen::Matrix<scalar_t, 1, Eigen::Dynamic> basis_values(n_functions);
        // If basis functions are centered on center_function
        if (basis_functions.type == 0) {
            // Evaluate basis functions in given points
            for (int i = 0; i < n_functions; i++) {
                basis_values(i) = basis_functions(i, cpoint - saved_point, mode);
            }
            // If basis functions are centered on closest support domain points
        } else {
            // Create a matrix of basis functions values in points
            for (int i = 0; i < n_functions; i++) {
                basis_values(i) =
                    basis_functions(i, cpoint - support_domain[basis_centers[i]], mode);
            }
        }

        // Set shape
        VecX<scalar_t> shape = basis_values * base_values_inv;
        for (int i = 0; i < n_points; i++) {
            shape_vector[i] = shape(i) * weight(0, support_domain[i] - saved_point);
        }
        return shape_vector;
    }

    /// Get number of coefficients @f$\vec \varphi@f$
    int size() const { return coefficients.size();}

    /// Return basis function object.
    BasisFunctionsClass getBasis() const {
        return basis_functions;
    }
    /// Returns basis shape parameter.
    vec_t getBasisShape() const {
        return basis_functions.shape;
    }
    /// Returns weight values.
    VecX<scalar_t> getWeightValues() const {
        return getWeightValues(center_point);
    }
    /**
     * @brief Get weight values in support domain
     * @param cpoint Point @f$\vec p@f$in which to center the weights
     * typically usage is to check the weight in the support, i.e.
     * getWeightValues(support[0]).
     * @return Weight values or diagonal elements of matrix@f$W@f$:
     * @f$W_{j, j} = w\left(\vec s_j-\vec p\right)@f$
     */
    VecX<scalar_t> getWeightValues(const vec_t& cpoint) const {
        int n_points = support_domain.size();
        VecX<scalar_t> result(n_points, 0);

        for (int i = 0; i < n_points; ++i) {
            result[i] = weight(0, support_domain[i] - cpoint);
        }
        return result;
    }
    /// Returns weight shape parameter
    vec_t getWeightShape() const {
        return weight.shape;
    }
    /// Resets basis shape parameter.
    void resetBasisShape(const vec_t& shape) {
        basis_functions.shape = shape;
    }
    /// Resets weight shape parameter.
    void resetWeightShape(const vec_t& shape) {
        weight.shape = shape;
    }

    /// Return saved inverse of a last used system matrix .
    ei_matrix_t getSavedInverse() const {
        return saved_inverse;
    }

    /**
     * @brief Get the list of coefficients @f$\vec \alpha@f$ that are saved internally
     * @return A vector of coefficients @f$\vec \alpha@f$
     */
    VecX<scalar_t> getCoefficients() const { return coefficients; }

    /**
     * Return the condition number of matrix used during last computation.
     * MLS Engine also prints a warning if the condition number of a matrix is
     * greater than HIGH_CONDITION or not a normal number.
     */
    scalar_t getConditionNumber() const { return cond_number; }

    template <class V, template <class> class B, template <class> class W>
    friend std::ostream& operator<<(std::ostream& os, const EngineMLS<V, B, W>& mls);

    /// Return matrix of a system at given point.
    ei_matrix_t getMatrix(const vec_t& cpoint) const {
        int n_functions = basis_functions.size();
        int n_points = support_domain.size();
        assert(n_points >= n_functions &&
               "Cannot have more basis functions than there are points in support domain.");
        ei_matrix_t base_values(n_points, n_functions);

        // If basis functions are centered on center_function
        if (basis_functions.type == 0) {
            // Evaluate basis functions in given points
            for (int j = 0; j < n_points; j++) {
                scalar_t w = weight(0, support_domain[j] - cpoint);
                for (int i = 0; i < n_functions; i++) {
                    base_values(j, i) =
                            w * basis_functions(i, support_domain[j] - cpoint);
                }
            }
        } else {  // If basis functions are centered on closest support domain points
            assert(basis_centers.size() == n_functions &&
                   "Wrong number of basis_centers. It seems you set manual_centers"
                           "to true, but did not bother to actually set them.");

            // Create a matrix of basis functions values in points
            for (int j = 0; j < n_points; j++) {
                scalar_t w = weight(0, support_domain[j] - cpoint);
                for (int i = 0; i < n_functions; i++) {
                    base_values(j, i) = w * basis_functions(
                            i, support_domain[j] - support_domain[basis_centers[i]]);
                }
            }
        }
        return base_values;
    }

  private:
    /**
     * @brief Creates a matrix we need to calculate coefficients or shape vector
     * @details Creates matrices @f$W@f$ and @f$B@f$ for the given point @f$p@f$
     * then calculate and return its pseudoinverse
     * @param cpoint The point @f$p@f$ around which to make the matrix
     * @return Matrix @f$(WB)^+@f$
     */
    ei_matrix_t getMatrixSolution(const vec_t& cpoint) {
        int n_functions = basis_functions.size();
        int n_points = support_domain.size();
        assert(n_points >= n_functions &&
               "Cannot have more basis functions than there are points in support domain.");

        // check if we already computed inverse for given setup
        if (saved_point == cpoint && saved_inverse.size() == n_points * n_functions) {
            return saved_inverse;
        }
        ei_matrix_t base_values = getMatrix(cpoint);
        ei_matrix_t base_values_inv = pinv(base_values, 1e-9);

        saved_point = cpoint;
        saved_inverse = base_values_inv;

        return base_values_inv;
    }

    /**
     * @brief Pseudo inverse of a matrix.
     * @details Calculates Moore-Penrose pseudoinverse of matrix m
     * @param m Matrix to inverse
     * @param epsilon Singular values lower than this will be ignored.
     * @return Pseudo inverse of m
     */
    ei_matrix_t pinv(ei_matrix_t& m, scalar_t epsilon = 1e-50) {
        typedef Eigen::JacobiSVD<ei_matrix_t> SVD;
        typedef typename SVD::SingularValuesType SingularValuesType;

        SVD svd(m, Eigen::ComputeThinU | Eigen::ComputeThinV);
        const SingularValuesType singVals = svd.singularValues();
        SingularValuesType invSingVals = singVals;
        double low_sing = 0;
        for (int i = 0; i < singVals.rows(); i++) {
            if (singVals(i) <= epsilon * singVals(0)) {
                invSingVals(i) = 0.0;  // FIXED can not be safely inverted
            } else {
                low_sing = invSingVals(i);
                invSingVals(i) = 1.0 / invSingVals(i);
            }
        }
        cond_number = singVals(0) / low_sing;
        if (cond_number > HIGH_COND_NUMBER) {
            std::cout << "[mls] High condition number (" << cond_number << ") "
                      << "in approximation." << std::endl;
        }
        if (!std::isnormal(cond_number)) {
            std::cout << "[mls] Condition number is an abnormal number ("
                      << cond_number << ")." << std::endl;
        }
        return ei_matrix_t(svd.matrixV() * invSingVals.asDiagonal() *
                           svd.matrixU().transpose());
    }
};

/// Print basic MLS engine information.
template <class V, template <class> class B, template <class> class W>
std::ostream& operator<<(std::ostream& os, const EngineMLS<V, B, W>& mls) {
    int type = mls.basis_functions.type;
    int used = mls.saved_inverse.size() != 0;
    std::vector<std::string> type_names = {"same center", "different centers"};
    os << "EngineMLS:\n"
       << "    dimension: " << V::dimension << '\n';
    if (used) os << "    last point: " << mls.saved_point << '\n';
    else os << "    last point: " << "engine not used yet" << '\n';
    if (used) os << "    cond num: " << mls.cond_number << '\n';
    else os << "    cond num: " << "engine not used yet" << '\n';
    os << "    num of bases: " << mls.basis_functions.size() << '\n'
       << "    support size: " << mls.support_domain.size() << '\n'
       << "    function type: " << type_names[type] << " (type " << type << ")\n"
       << "    basis: "  << mls.basis_functions
       << "    weight: "  << mls.weight;
    return os;
}

}  // namespace mm

#endif  // SRC_MLS_HPP_
